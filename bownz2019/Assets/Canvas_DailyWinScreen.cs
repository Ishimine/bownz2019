﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Canvas_DailyWinScreen : MonoBehaviour
{
    public UI_ConteinerOpenClose OpenClose;

    public Text text;

    public void SetTime(float seconds)
    {
        Debug.Log("Seconds: " + seconds);
        TimeSpan t = TimeSpan.FromSeconds(seconds);
        text.text = $"{t.Hours:00}:{t.Minutes:00}:{t.Seconds:00}:{t.Milliseconds:0}";
    }

public void Close()
    {
        OpenClose.Close();
    }
                     
    public void Open()
    {
        OpenClose.Open();
    }
         
    public void Show()
    {
        OpenClose.Show();
    }
      
    public void Hide()
    {
        OpenClose.Hide();
    }

    public void MenuReques()
    {
        this.PostNotification(Notifications.GameModeRequest_BackToLevelSelection);
    }

    public void RetryRequest()
    {
        this.PostNotification(Notifications.GameModeRequest_Retry);
    }
}
