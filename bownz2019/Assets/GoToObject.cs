﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoToObject : MonoBehaviour
{
    public bool enable = false;
    public float speed = 20;

    private Vector2 vel;
    private Transform target;

    private bool destroyOnReach = false;


    private CodeAnimator codeAnimator = new CodeAnimator();

    public void Initialize(Transform nTarget, bool destroyOnReach)
    {
        this.destroyOnReach = destroyOnReach;
        target = nTarget;
        enable = true;
    }

    private void FixedUpdate()
    {
        if(!enable) return;
        transform.position = Vector2.SmoothDamp(transform.position, target.position, ref vel, .15f);
    }

    private void OnDisable()
    {
        codeAnimator.Stop(this);
        if(destroyOnReach)
            Destroy(this);

    }
}
