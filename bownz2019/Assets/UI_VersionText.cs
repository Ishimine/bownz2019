﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_VersionText : MonoBehaviour
{
    public Text Text;

    private void OnEnable()
    {
        Text.text = Application.version;
    }
}
