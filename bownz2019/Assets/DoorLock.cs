﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;
using UnityEngine;

public class DoorLock : MonoBehaviour
{
   public Sprite lockSprite;
   public Sprite unlockSprite;

   public Color cLock;
   public Color cUnlock;

   public SpriteRenderer render;

   public float unlockDuration =.3f;
   public AnimationCurve unlockSizeCurve;
   public float unlockSpriteSwapKeyValue = .5f;

   public float lockDuration =.3f;
   public AnimationCurve lockSizeCurve;
   public float lockSpriteSwapKeyValue = .5f;

   private IEnumerator rutine;

   private bool isLock = false;

   public void Initialize(bool isUnlock)
   {
       isLock = !isUnlock;
       render.sprite = (isUnlock) ? unlockSprite : lockSprite;
       transform.localScale = Vector3.one;
        render.color = (isUnlock) ? cUnlock : cLock;
   }

   public void Lock()
   {
       if(isLock) return;
       StopRutine();
       rutine = AnimationRutine(lockDuration, unlockSprite, lockSprite, lockSizeCurve, lockSpriteSwapKeyValue,true, cLock);
       StartCoroutine(rutine);
   }

   public void Unlock()
   {
       if(!isLock) return;
       StopRutine();
       rutine = AnimationRutine(unlockDuration, lockSprite, unlockSprite, unlockSizeCurve, unlockSpriteSwapKeyValue,false, cUnlock);
       StartCoroutine(rutine);
   }

   private void StopRutine()
   {
       if (rutine != null) StopCoroutine(rutine);
   }

   private IEnumerator AnimationRutine(float duration, Sprite startSprite, Sprite endSprite, AnimationCurve sizeCurve, float swapSpriteAt, bool lockTargetState, Color targetColor)
   {
       isLock = lockTargetState;
       render.sprite = startSprite;
       render.transform.localScale = Vector3.one  + Vector3.one * sizeCurve.Evaluate(0) * .5f;

       float x = 0;
       do
       {
           x += Time.deltaTime / lockDuration;
           render.transform.localScale = Vector3.one  +Vector3.one * sizeCurve.Evaluate(x)* .5f;
           yield return null;
       } while (x < swapSpriteAt);

       render.sprite = endSprite;
       render.color = targetColor;
           
       do
       {
           x += Time.deltaTime / lockDuration;
           render.transform.localScale = Vector3.one  +Vector3.one * sizeCurve.Evaluate(x)* .5f;
           yield return null;
       } while (x < 1);
   }

  
}