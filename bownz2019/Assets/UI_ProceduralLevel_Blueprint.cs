﻿using System;
using System.Collections;
using System.Collections.Generic;
using Procedural;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class UI_ProceduralLevel_Blueprint : MonoBehaviour
{
    public UnityEventBool OnIsUnlock;

    public Button button;
    public int seedOffset = 0;
    public SeudoBlueprint blueprint;
    private string BaseId => "Day_" + DailyMode.CurrentDate.DayOfYear.ToString();

    public bool blockByProgress = true;

    private void OnEnable()
    {
        RefreshButtonEnableState();
        this.AddObserver(OnDailyDateChange, Notifications.OnDailyDateChange);
    }

    private void OnDisable()
    {
        this.RemoveObserver(OnDailyDateChange, Notifications.OnDailyDateChange);
    }

    private void OnDailyDateChange(object arg1, object arg2)
    {
        RefreshButtonEnableState();
    }

    private void RefreshButtonEnableState()
    {
        OnIsUnlock?.Invoke(IsThisLevelCompleted());

        if(!blockByProgress || seedOffset < 1)
        {
            button.interactable = true;
            return;
        }
        //     button.interactable = LevelsProgress.Contains(LevelsProgress.GetKeyFrom(BaseId + "_" + (seedOffset - 1), System.DateTime.Now.Year, false));
        button.interactable = DailyProgress.Contains(DailyMode.CurrentDate, (seedOffset - 1).ToString());
    }

    private bool IsThisLevelCompleted()
    {
        return DailyProgress.Contains(DailyMode.CurrentDate, seedOffset.ToString());
        //      return LevelsProgress.Contains(LevelsProgress.GetKeyFrom(BaseId + "_" + (seedOffset), System.DateTime.Now.Year, false));
    }

    public void Pressed()
    {
        blueprint.SeedOffset = seedOffset;
        
        Debug.Log($"Current Daily Seed: {DailyMode.CurrentDailySeed}");
        
        blueprint.Seed = DailyMode.CurrentDailySeed + seedOffset;
        blueprint.TagsSeed = DailyMode.CurrentDailySeed;
        this.PostNotification(Notifications.ProceduralLevel_Request, new ProceduralLevelRequest(blueprint.CreateBlueprint(), BaseId + "_" + seedOffset));
    }
}