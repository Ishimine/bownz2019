﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.Serialization;
using Sirenix.OdinInspector;

public class UI_ShopPowerUpItem : UI_VideoRewardButton
{
    public UnityEvent OnRewardItem;
    public Action OnRewardItemCallback;

    public UnityEventBool OnNotEnoughCoins;

    public UnityEvent OnFailItem;

    public Button buyWithCoinButton;
    public Button buyWithVideoButton;

    public ShopReward targetReward;

    public Text text;
    
    private bool hasEnoughCoins = false;

    private void Awake()
    {
        this.AddObserver(OnShopOpenClose, Notifications.ShopOpenClose);
    }

    private void OnDestroy()
    {
        this.RemoveObserver(OnShopOpenClose, Notifications.ShopOpenClose);
    }

    private void OnShopOpenClose(object arg1, object arg2)
    {
        RefreshButtonInteractible();
    }

    private void OnEnable()
    {
        RefreshButtonInteractible();
        RefreshText();
    }

    [Button]
    private void RefreshButtonInteractible()
    {
        RefreshCanBuyWithCoin();
        bool canBuy = targetReward.CanBuy();
        
        buyWithCoinButton.gameObject.SetActive(Settings.CanBuyPowerUpWithCoin);
        buyWithVideoButton.gameObject.SetActive(Settings.CanBuyPowerUpWithVideo);
        buyWithCoinButton.interactable = canBuy && hasEnoughCoins;
        buyWithVideoButton.interactable = canBuy && AdsManager.IsVideoReady();
    }

    private void RefreshCanBuyWithCoin()
    {
        Argument<bool, int> arg = new Argument<bool, int>(false, coinCost);
        this.PostNotification(Notifications.HasEnoughCoins, arg);
        OnNotEnoughCoins?.Invoke(arg.Item0);
        hasEnoughCoins = arg.Item0;
    }

    private void RefreshText()
    {
        if (text != null)
            text.text =$"x {coinCost.ToString()}";
    }

    protected override void OnFailed()
    {
        OnFailItem?.Invoke();
    }

    protected override void OnReward()
    {
        targetReward.Reward();
        OnRewardItem?.Invoke();
        OnRewardItemCallback?.Invoke();
        RefreshButtonInteractible();
    }

    private void OnValidate()
    {
        if (!Application.isPlaying)
            RefreshText();
    }
}