﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 using System;

public  class ShieldReward : ShopReward
{
    public const string Id = Notifications.RewardPowerUpShield;
    public override string RewardId => Id;

    protected override void _Reward()
    {
        this.PostNotification(Notifications.RewardPowerUpShield);
    }
}

