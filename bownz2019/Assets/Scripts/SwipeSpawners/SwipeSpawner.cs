﻿using System.Collections;
using System.Collections.Generic;
using Lean.Touch;
using UnityEngine;
using Sirenix.OdinInspector;
using System;

public class SwipeSpawner : MonoBehaviour, IUseInputController
{
    private static bool  isActive = false;
    public static bool  IsActive
    {
        get { return isActive; }
    }

    private static float lifeSpan;
    public static float LifeSpan { get { return lifeSpan; } set { lifeSpan = value; } }

    private static int maxCount;
    public static int MaxCount { get { return maxCount; } set { maxCount = value; } }

    private static int maxSimultaneousSwipes;
    public static int MaxSimultaneousSwipes { get { return maxSimultaneousSwipes; } set { maxSimultaneousSwipes = value; } }

    private static bool dieOnTouch = true;
    public static bool DieOnTouch { get { return dieOnTouch; } set { dieOnTouch = value; } }

    public GameObject swipePrefab;

    public List<GameObject> totalElements = new List<GameObject>();

    [ShowInInspector, ReadOnly]
    private Queue<SwipePlatform> inactiveElements = new Queue<SwipePlatform>();

    [ShowInInspector, ReadOnly]
    private List<SwipePlatform> activeElements = new List<SwipePlatform>();

    [ShowInInspector, ReadOnly]
    public Dictionary<int, SwipePlatform> currentFingers = new Dictionary<int, SwipePlatform>();

    [SerializeField] private int defaultPoolSize = 5;

    SwipePlatform current;

    private void Awake()
    {
        MaxCount = Settings.SwipePlatform_MaxCount;
        LifeSpan = Settings.SwipePlatform_LifeSpan;
        MaxSimultaneousSwipes = Settings.MaxSimultaneousSwipes;
        DieOnTouch = true;
    }

    [Button]
    public void InstantiateElements(int ammount)
    {
        if (ammount > totalElements.Count)
        {
            int dif = ammount - totalElements.Count;
            GameObject current;
            for (int i = 0; i < dif; i++)
            {
                current = Instantiate(swipePrefab, transform);
                totalElements.Add(current);
                current.gameObject.SetActive(false);
                current.name = "Platform N°" + totalElements.Count;
                inactiveElements.Enqueue(current.GetComponent<SwipePlatform>());
            }
        }
    }


    [Button]
    public void ClearPool()
    {
        for (int i = totalElements.Count - 1; i >= 0; i--)
        {
            if (Application.isPlaying)
                Destroy(totalElements[i].gameObject);
            else
                DestroyImmediate(totalElements[i].gameObject);
        }
        totalElements.Clear();
    }

    [Button]
    public void ClearAllCurrentFingers()
    {
        List<KeyValuePair<int, SwipePlatform>> platforms = new List<KeyValuePair<int, SwipePlatform>>(currentFingers);
        for (int i = platforms.Count - 1; i >= 0; i--)
        {
            currentFingers.Remove(platforms[i].Key);
            platforms[i].Value.Die(false);
        }
    }

    [Button]
    public void Activate()
    {
        isActive = true;
        Debug.Log(this + "  Activate");
        InputManager.RegisterController(this);
        InstantiateElements(defaultPoolSize);

        SwipePlatform.OnCreationState -= OnCreationResponse;
        SwipePlatform.OnCreationState += OnCreationResponse;

        SwipePlatform.OnDieStateStart -= OnDieStartResponse;
        SwipePlatform.OnDieStateStart += OnDieStartResponse;

        SwipePlatform.OnDieStateDone -= OnDieDoneResponse;
        SwipePlatform.OnDieStateDone += OnDieDoneResponse;

        SwipePlatform.OnAnyAliveState -= OnAliveSwipePlatformResponse;
        SwipePlatform.OnAnyAliveState += OnAliveSwipePlatformResponse;
    }

    private void OnAliveSwipePlatformResponse(SwipePlatform swipePlatform)
    {
        if (activeElements.Count > MaxCount)
            activeElements[0].Die(false);

        if (currentFingers.ContainsKey(swipePlatform.Id))
            currentFingers.Remove(swipePlatform.Id);
    }

    private void OnCreationResponse(SwipePlatform swipePlatform)
    {
        activeElements.Add(swipePlatform);
    }

    private void OnDieStartResponse(SwipePlatform swipePlatform)
    {
        activeElements.Remove(swipePlatform);
    }

    private void OnDieDoneResponse(SwipePlatform swipePlatform)
    {
        inactiveElements.Enqueue(swipePlatform);
    }

    public void Deactivate()
    {
        isActive = false;
        InputManager.UnregisterController(this);
        SwipePlatform.OnCreationState -= OnCreationResponse;
        SwipePlatform.OnDieStateDone -= OnDieStartResponse;
        SwipePlatform.OnAnyAliveState -= OnAliveSwipePlatformResponse;
        ClearAllCurrentFingers();
    }

    public void FingerDown(LeanFinger leanFinger)
    {
        if (activeElements.Count >= maxSimultaneousSwipes) return;
        if (inactiveElements.Count == 0) InstantiateElements(totalElements.Count + 1);

        current = inactiveElements.Dequeue();
        current.Id = leanFinger.Index;
        currentFingers.Add(leanFinger.Index, current);

        current?.FingerDown(leanFinger.GetWorldPosition(10));
    }

    public void FingerExpired(LeanFinger leanFinger)
    {
        currentFingers.Remove(leanFinger.Index);
    }

    public void FingerSet(LeanFinger leanFinger)
    {
        if (currentFingers.ContainsKey(leanFinger.Index))
        {
            current = currentFingers[leanFinger.Index];
            current?.FingerSet(leanFinger.GetWorldPosition(10, null));
        }
    }

    public void FingerSwipe(LeanFinger leanFinger)
    {
    }

    public void FingerTap(LeanFinger leanFinger)
    {
    }

    public void FingerUp(LeanFinger leanFinger)
    {
        current?.FingerUp(leanFinger.GetWorldPosition(10, null));
        currentFingers.Remove(leanFinger.Index);
    }

    public void Gesture(List<LeanFinger> leanFingers)
    {
    }

    private void OnEnable()
    {
        this.AddObserver(OnSwipeSpawnerSetEnable, Notifications.SwipeSpawnerSetEnable);
        this.AddObserver(SwipeSpawnerClear, Notifications.SwipeSpawnerClear);
        this.AddObserver(KillClosestAtRange_Response, Notifications.SwipeSpawner_KillClosestAtRange);
        this.AddObserver(OnInputActiveChange, Notifications.InputActiveChange);
    }

    private void OnDisable()
    {
        this.RemoveObserver(OnSwipeSpawnerSetEnable, Notifications.SwipeSpawnerSetEnable);
        this.RemoveObserver(SwipeSpawnerClear, Notifications.SwipeSpawnerClear);
        this.RemoveObserver(KillClosestAtRange_Response, Notifications.SwipeSpawner_KillClosestAtRange);
        this.RemoveObserver(OnInputActiveChange, Notifications.InputActiveChange);
    }

    private void OnInputActiveChange(object arg1, object arg2)
    {
        ClearAllCurrentFingers();
    }

    private void KillClosestAtRange_Response(object sender, object request)
    {
        KillClosestAtRange((KillClosestAtRangeRequest)request);
    }

    private void KillClosestAtRange(KillClosestAtRangeRequest request)
    {
        KillClosestAtRange(request.position,request.maxRange);
    }

    private void KillClosestAtRange(Vector2 position, float range)
    {
        SwipePlatform closestPlatform = GetClosestPlatform(position);
        if (closestPlatform != null)
        {
            if (Vector2.Distance(closestPlatform.GetCenterPosition(), position) <= range)
            {
                closestPlatform.Die(false);
            }
        }
    } 

    private SwipePlatform GetClosestPlatform(Vector2 position)
    {
        float minDistance = float.MaxValue;
        SwipePlatform current = null;
        float currentDistance;
        foreach (var item in activeElements)
        {
            currentDistance = Vector2.Distance((Vector2)item.GetCenterPosition(), position);
            if (currentDistance < minDistance)
            {
                minDistance = currentDistance;
                current = item;
            }
        }
        return current;
    }

    private void SwipeSpawnerClear(object arg1, object arg2)
    {
        ClearAllCurrentFingers();
        TurnOffAllActive();
    }

    private void TurnOffAllActive()
    {
        for (int i = activeElements.Count-1; i >= 0; i--)
        {
            activeElements[i].Die(false);
        }
    }

    private void OnSwipeSpawnerSetEnable(object arg1, object arg2)
    {
        Debug.Log("OnSwipeSpawnerSetEnable: " + ((bool)arg2));
        if ((bool)arg2)
            Activate();
        else
            Deactivate();
    }
}


public struct KillClosestAtRangeRequest
{
    public Vector2 position;
    public float maxRange;

    public KillClosestAtRangeRequest(Vector2 position, float maxRange)
    {
        this.position = position;
        this.maxRange = maxRange;
    }
}
    
