﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

using System;
// ReSharper disable once CheckNamespace
public class SwipePlatform : MonoBehaviour
{
    public int Id { get; set; }

    public float LenghtProportion { get; set; }

    public static Action<SwipePlatform> OnCreationState { get; set; }

    public static Action<SwipePlatform> OnDieStateStart { get; set; }

    public static Action<SwipePlatform> OnDieStateDone { get; set; }

    public static Action<SwipePlatform> OnAnyAliveState { get; set; }

    public static Action<SwipePlatform> OnFailedCreation { get; set; }

    [ReadOnly]
    private SwipePlatformState currentState;

    public ContactFilter2D filter;

    [TabGroup("Dependencies")]   [SerializeField] private LineRenderer render = null;   public LineRenderer Render { get => render; }
    [TabGroup("Dependencies")] [SerializeField] private CapsuleCollider2D col = null;   public CapsuleCollider2D Col { get => col; }
    [TabGroup("Dependencies")] [SerializeField] private Rigidbody2D rb = null;          public Rigidbody2D Rb { get => rb; }

    [TabGroup("Audio")] public SoundPlayGroup sFXs_Bounce; 
    [TabGroup("Audio")] public SoundPlay sFXs_Done; 
    [TabGroup("Audio")] public SoundPlay sFXs_Dissapear;

    [ShowInInspector] private string currentStateName = null;

    [HideInInspector] public bool isAlive = false;

    private Vector2 startPosition = Vector2.zero;
    public Vector2 StartWorldPosition { get { return startPosition; } set { startPosition = value; } }

    public CodeAnimator sizeAnimator = new CodeAnimator();
    public CodeAnimator deadTimer = new CodeAnimator();
    public CodeAnimator stringAnimator = new CodeAnimator();

    private Vector2 lastPosition;

    public void SetColor(Color c)
    {
        render.startColor = c;
        render.endColor = c;
    }

    public void SetWrongColor()
    {
        SetColor(Settings.SwipePlatform_WrongColor);
    }
    public void SetActiveColor()
    {
        SetColor(Settings.SwipePlatform_ActiveColor);
    }
    public void SetInactiveColor()
    {
        SetColor(Settings.SwipePlatform_InactiveColor);
    }

    public void Update()
    {
        currentState?.Update();
    }

    public void SwitchState<T>() where T : SwipePlatformState
    {
        SwitchState<T>(null);
    }

    public void SwitchState<T>(object[] parametros) where T : SwipePlatformState
    {
        if (currentState != null && currentState.GetType() == (typeof(T)))
            return;

        List<object> tempParameters = new List<object>();
        tempParameters.Add(this);
        if(parametros != null && parametros.Length > 0)
        {
            for (int i = 0; i < parametros.Length; i++)
                tempParameters.Add(parametros[i]);
        }
        currentState = (T)Activator.CreateInstance(typeof(T), tempParameters.ToArray());
        currentStateName = typeof(T).ToString();
    }

    public void FingerDown(Vector2 position)
    {
        StartWorldPosition = position;
        SwitchState<SwipePlatform_CreationState>();
        lastPosition = position;
    }

    public void FingerUp(Vector2 position)
    {
        currentState?.FingerUp(position);
        lastPosition = position;
    }


    public void FingerSet(Vector2 position)
    {
        currentState?.FingerSet(position);
        lastPosition = position;
    }

    public void Die(bool playSound)
    {
        if(playSound)
            sFXs_Dissapear.Play();

        currentState?.Die();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        currentState?.OnCollisionEnter2D(collision);
        this.PostNotification(Notifications.SwipePlatform_Touched, collision.gameObject);
    }

    public Vector2 GetCenterPosition()
    {
        return Vector2.Lerp(StartWorldPosition, lastPosition, .5f);
    }
}

public abstract class SwipePlatformState
{
    protected readonly SwipePlatform owner;
    public SwipePlatformState(SwipePlatform owner)
    {
        owner.transform.localScale = Vector3.one;
//        Debug.Log("  SwipePlatform_StateChangedTo <color=green>" + this + "</color>  " + "  " + owner.name);
        this.owner = owner;
    }

    int count = 0;
    Collider2D[] results = new Collider2D[1];
    public virtual void Update()
    {
        //Si es trigger significa que aun no se solidifico y por ende no puede producir contacto
        if (owner.Col.isTrigger)
        {
            count = owner.Col.OverlapCollider(owner.filter, results);
            if (count == 0 && owner.Col.size.x >= Settings.SwipePlatform_MinSize.x) //Se solidifica
            {
                owner.Col.isTrigger = false;
            } 
            else
            {
                owner.SetWrongColor();
            }
        }
        else if (!owner.Col.isTrigger && owner.Col.size.x < Settings.SwipePlatform_MinSize.x)
        {
            owner.Col.isTrigger = true;
            owner.SetInactiveColor();
        }
    }
    float distance;
    Vector3[] positions;
    public virtual void FingerSet(Vector2 position)
    {
        distance = Vector2.Distance(owner.StartWorldPosition, position);

        owner.transform.localRotation = Quaternion.Euler(Vector3.forward * owner.StartWorldPosition.GetAngle(position));    //Rotamos el objeto en relacion a los Touchs Inicial y Actual

        float xSize = Mathf.Lerp(0, Settings.SwipePlatform_MaxSize.x, distance / Settings.SwipePlatform_MaxSize.x);
        float ySize = Mathf.Lerp(Settings.SwipePlatform_MinSize.y, Settings.SwipePlatform_MaxSize.y, distance / Settings.SwipePlatform_MaxSize.x);

        owner.Render.startWidth = ySize;
        owner.Render.endWidth = ySize;
        owner.Col.size = new Vector2(xSize, ySize);     //Actualizamos el largo de la linea con un Limite extraido de SETTINGS
        owner.Col.offset = Vector2.right * owner.Col.size.x *.5f;

        float extraProportion = Mathf.Clamp01(owner.Col.size.x / owner.Col.size.y);

        owner.Render.SetPosition(0, Vector2.zero);
        owner.Render.SetPosition(owner.Render.positionCount-1, Vector2.right * owner.Col.size.x);
    }

    public virtual void FingerUp(Vector2 position) { }
    public virtual void OnCollisionEnter2D(Collision2D collision) { }
    public virtual void Die()
    {
        owner.SwitchState<SwipePlatform_DieState>();
    }
}

/// <summary>
/// Al iniciar la plataforma se encuentra como Trigger, y se solidica en el momento en que NO este en contacto con ningun objeto con el cual pueda interactuar.
/// para saber si puede o no interactuar se utiliza el filtro.
/// </summary>
public class SwipePlatform_CreationState : SwipePlatformState
{
    public SwipePlatform_CreationState(SwipePlatform owner) : base(owner)
    {
        owner.Col.enabled = true;

        owner.Render.positionCount = 2;
        owner.SetInactiveColor();
        owner.Col.isTrigger = true;
        owner.transform.position = owner.StartWorldPosition;
        owner.Col.size = Vector2.up * Settings.SwipePlatform_MaxSize.y;
        owner.gameObject.SetActive(true);
        SwipePlatform.OnCreationState?.Invoke(owner);
        owner.isAlive = false;
    }

    int count;
    RaycastHit2D[] results = new RaycastHit2D[1];

    public override void Update()
    {
        base.Update();
    }

    public override void FingerSet(Vector2 position)
    {
        base.FingerSet(position);

        if (owner.Col.size.x < Settings.SwipePlatform_MinSize.x)
            owner.SetWrongColor();
        else
            owner.SetInactiveColor();
    }

    public override void FingerUp(Vector2 position)
    {
        if (owner.Col.size.x < Settings.SwipePlatform_MinSize.x)
        {
            owner.SwitchState<SwipePlatform_DieState>();
            SwipePlatform.OnFailedCreation?.Invoke(owner);
        }
        else
        {
            owner.sFXs_Done.Play();
            owner.SwitchState<SwipePlatform_LiveState>();
        }
    }

    public override void OnCollisionEnter2D(Collision2D collision)
    {
        base.OnCollisionEnter2D(collision);
        if(!owner.filter.IsFilteringLayerMask(collision.gameObject))
        {
            //Vibration.Vibrate((long)40);

            if(Settings.UseVibration) Vibration.Vibrate((long)Settings.SwipePlatform_TouchVibrationForce);

            owner.SwitchState<SwipePlatform_LiveState>();
            owner.SwitchState<SwipePlatform_TouchedState>(new object[] { collision.GetContact(0).point , true });
        }
    }
}

public class SwipePlatform_LiveState : SwipePlatformState
{
    public SwipePlatform_LiveState(SwipePlatform owner) : base(owner)
    {
        owner.LenghtProportion = owner.Col.size.x / Settings.SwipePlatform_MaxSize.x;

        //owner.deadTimer.StartWaitAndExecute(owner, Settings.SwipePlatform_LifeSpan, owner.SwitchState<SwipePlatform_DieState>, false);

  
        Color color = Color.white;
        owner.Render.startColor = color;
        owner.Render.endColor = color;

        owner.deadTimer.StartAnimacion(owner,
            x =>
            {
                color = Color.Lerp(Color.white, Settings.SwipePlatform_ActiveColor, x*2);
                owner.Render.startColor = color;
                owner.Render.endColor = color;
            }, DeltaTimeType.deltaTime, AnimationType.Simple, null,
            ()=>
            {
                owner.SwitchState<SwipePlatform_DieState>();
                owner.sFXs_Dissapear.Play();
            }, SwipeSpawner.LifeSpan);

        if(!owner.isAlive)
        {
            SwipePlatform.OnAnyAliveState?.Invoke(owner);
            owner.isAlive = true;
        }
    }

    public override void Die()
    {
        base.Die();
    }

    public override void OnCollisionEnter2D(Collision2D collision)
    {
        base.OnCollisionEnter2D(collision);
        if (!owner.filter.IsFilteringLayerMask(collision.gameObject))
        {
            owner.SwitchState<SwipePlatform_TouchedState>(new object[] { collision.GetContact(0).point, true });
        }
    }

    public override void FingerSet(Vector2 position)
    {
    }

    public override void Update()
    {
        if(owner.Col.isTrigger)
            base.Update();
    }
}



public class SwipePlatform_TouchedState : SwipePlatformState
{
    readonly Vector2 touchedPosition;

    Vector2 brokenPos;
    Vector2 brokenVec;

    bool dieAfter;

    public SwipePlatform_TouchedState(SwipePlatform owner, Vector2 touchedPosition, bool dieAfter) : base(owner)
    {
        if(owner.LenghtProportion == -1) owner.LenghtProportion = owner.Col.size.x / Settings.SwipePlatform_MaxSize.x;

        this.dieAfter = dieAfter;
        if(SwipeSpawner.DieOnTouch)
            owner.Col.isTrigger = true;

        //owner.SetColor(Color.white);

        this.touchedPosition = touchedPosition;
        if (!owner.isAlive)
        {
            SwipePlatform.OnAnyAliveState?.Invoke(owner);
            owner.isAlive = true;
        }
        owner.deadTimer.Stop(owner);
        Animation(touchedPosition);
        owner.sFXs_Bounce.PlayRandom();
    }

    private void Animation(Vector2 touchedPosition)
    {


        Vector2 localTouchedPosition = owner.transform.InverseTransformPoint(touchedPosition);
       // owner.SetActiveColor();
        owner.deadTimer.Stop(owner);        //Detenemos el contador

        Vector3[] positions = new Vector3[owner.Render.positionCount];
        owner.Render.GetPositions(positions);

        Vector2 center = Vector2.Lerp(positions[0], positions[positions.Length - 1], .5f); // Es diferente al centro del OWNER porque el owner es el PIVOT izquierdo.

        owner.Render.positionCount = 3;
        owner.Render.SetPositions(new Vector3[] { positions[0], center, positions[positions.Length - 1] });

        Vector2 dir = ((Vector2)owner.transform.TransformPoint(center) - touchedPosition).normalized;
        Vector2 intersection = new Vector2(666, 666);

        float platformLenght = Math.Abs(positions[0].x - positions[positions.Length - 1].x);
        float intensity = (1 - Math.Abs((center.x - localTouchedPosition.x) / (platformLenght / 2))) * (platformLenght / Settings.SwipePlatform_MaxSize.x);

        if (!UnityExtentions.LineIntersection(positions[0], positions[positions.Length - 1],
            owner.transform.InverseTransformPoint(touchedPosition) + Vector3.up, owner.transform.InverseTransformPoint(touchedPosition) + Vector3.down,
            ref intersection))
        {
            owner.stringAnimator.StartWaitAndExecute(owner, Settings.SwipePlatform_BounceCurve.Time * owner.LenghtProportion,
                ()=>
                {
                    if(SwipeSpawner.DieOnTouch)
                        owner.SwitchState<SwipePlatform_DieState>();
                    else
                        owner.SwitchState<SwipePlatform_LiveState>();
                }
                , false);
            return;
        }

        float time = 0;

        positions = new Vector3[3];
        owner.Render.GetPositions(positions);
        Color startColor = owner.Render.startColor;
        owner.stringAnimator.StartAnimacion(owner,
            x =>
            {
                time += Time.deltaTime / Settings.SwipePlatform_BounceCurve.Time;

                owner.SetColor(Color.Lerp(startColor, Settings.SwipePlatform_ActiveColor, x));

                owner.Render.SetPosition(1, intersection + Vector2.down * Settings.SwipePlatform_BounceCurve.Curve.Evaluate(x) * intensity * Settings.SwipePlatform_DeformationLenght);
            }, DeltaTimeType.deltaTime, AnimationType.Simple, null,
            ()=>
            {
                if (dieAfter && SwipeSpawner.DieOnTouch)
                    owner.SwitchState<SwipePlatform_DieState>();
                else
                    owner.SwitchState<SwipePlatform_LiveState>();
            }, Settings.SwipePlatform_BounceCurve.Time * owner.LenghtProportion);

    }
    public override void Update() { }

    public override void FingerSet(Vector2 position)
    {
    }
}

public class SwipePlatform_DieState : SwipePlatformState
{
    public SwipePlatform_DieState(SwipePlatform owner) : base(owner)
    {
        SwipePlatform.OnDieStateStart?.Invoke(owner);

        if (owner.deadTimer != null) owner.deadTimer.Stop(owner);
        if (owner.stringAnimator != null) owner.stringAnimator.Stop(owner);

        Vector3 startScale = owner.transform.localScale;

        //Sacamos 1 nodo y lo dejamos con 2 nodos
        Vector2 p = owner.Render.GetPosition(owner.Render.positionCount - 1);
        owner.Render.positionCount = 2;
        owner.Render.SetPosition(1, p);

        //Centramos el objeto para que la animacion de Scale sea desde los lados
        owner.transform.position = owner.transform.TransformPoint(Vector2.right * owner.Col.size.x / 2);
        owner.Col.offset = Vector3.zero;

        //Reposicionamos los puntos de los nodos
        owner.Render.SetPosition(0, owner.Render.GetPosition(0) + Vector3.left * owner.Col.size.x / 2);
        owner.Render.SetPosition(1, owner.Render.GetPosition(1) + Vector3.left * owner.Col.size.x / 2);

        owner.sizeAnimator.StartAnimacion(owner,
            x =>
            {
                owner.transform.localScale = Vector3.Lerp(startScale, Vector3.zero, x);
            }, DeltaTimeType.deltaTime, AnimationType.Simple, Settings.SwipePlatform_DissapearCurve.Curve, Die, Settings.SwipePlatform_DissapearCurve.Time);
        //owner.Col.isTrigger = true;

        owner.LenghtProportion = -1;
    }

    public override void Update() { }
    public override void FingerSet(Vector2 position)
    {
    }
    public override void Die()
    {
        if (owner.stringAnimator != null) owner.stringAnimator.Stop(owner);
        if (owner.deadTimer != null) owner.deadTimer.Stop(owner);
        if (owner.stringAnimator != null) owner.stringAnimator.Stop(owner);

        owner.Col.enabled = false;

        owner.gameObject.SetActive(false);
        SwipePlatform.OnDieStateDone?.Invoke(owner);
    }

    public override void OnCollisionEnter2D(Collision2D collision)
    {
        base.OnCollisionEnter2D(collision);
        if (!owner.filter.IsFilteringLayerMask(collision.gameObject))
        {
            owner.SwitchState<SwipePlatform_TouchedState>(new object[] { collision.GetContact(0).point, false });
        }
    }
}