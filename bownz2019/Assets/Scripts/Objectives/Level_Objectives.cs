﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class Level_Objectives : MonoBehaviour
{
    [Header("Challenge")]
    [SerializeField, HorizontalGroup, HideLabel] LevelProgress challenge = new LevelProgress(10, 10, 3, 0);
    public LevelProgress Challenge => challenge;

    [Header("Normal")]
    [SerializeField, HorizontalGroup, HideLabel] LevelProgress normal = new LevelProgress(20, 20, 3, 3);
    public LevelProgress Normal => normal;
    [SerializeField, ReadOnly] private string id;
    public string Id  => id;
    [SerializeField, ReadOnly] private string idChallenge;
    public string IdChallenge  => idChallenge;    

    [Button]
    private void AutoNormal()
    {
        if(challenge.Stars == 0 || challenge.Time == 0 || challenge.Platforms == 0)
        {
            Debug.LogWarning("No todos los valores estan inicializados");
            return;
        }
        
        normal = new LevelProgress(
                (int)((challenge.Platforms + challenge.Platforms / 2) * 1.4f),
                Mathf.CeilToInt((challenge.Platforms + challenge.Time / 2) * 1.4f),
                challenge.Platforms / 2,
                challenge.Stars);
    }

    public void SetKeys(string key, string keyChallenge)
    {

    }
}

[System.Serializable]
public struct LevelProgress
{
    [LabelWidth(60)] public int Platforms;
    [LabelWidth(60)] public float Time;
    [LabelWidth(60)] public int Deaths;
    [LabelWidth(60)] public int Stars;

    public float this[int key]
    {
        get
        {
            switch (key)
            {
                case 0:
                    return Platforms;
                case 1:
                    return Time;
                case 2:
                    return Deaths;
                case 3:
                default:
                    return Stars;
            }
        }
        set
        {
            switch (key)
            {
                case 0:
                     Platforms = (int)value;
                    break;
                case 1:
                     Time = value;
                    break;
                case 2:
                     Deaths = (int)value;
                    break;
                case 3:
                default:
                    Stars = (int)value;
                    break;
            }
        }
    }

    public LevelProgress(int platforms, float time, int deaths, int stars)
    {
        Platforms = platforms;
        Time = time;
        Stars = stars;
        Deaths = deaths;
    }

    public override string ToString()
    {
        return   $"Platforms: {Platforms.ToString()} Time:{Time.ToString()} Stars:{Stars.ToString()} Deaths{Deaths.ToString()}";
    }

    public LevelProgress Merge_KeepBestScore(LevelProgress other)
    {
        return new LevelProgress(
            Platforms = Mathf.Min(Platforms, other.Platforms),
            Time = Mathf.Min(Time, other.Time),
            Stars = Mathf.Max(Stars, other.Stars),
            Deaths = Mathf.Min(Deaths, other.Deaths));
    }

    public bool IsCompletedBy(LevelProgress other, bool compareStarsToo)
    {
        //        return other.Platforms <= Platforms && other.Time <= Time && other.Deaths <= Deaths && (compareStarsToo)?other.Stars>= Stars: true;
        return other.Platforms > Platforms || !(other.Time <= Time) || other.Deaths > Deaths || (!compareStarsToo) || other.Stars>= Stars;
    }

    public bool IsNullScore()
    {
        return Time == float.MaxValue && Platforms == int.MaxValue && Deaths == int.MaxValue && Stars == 0;
    }
}
