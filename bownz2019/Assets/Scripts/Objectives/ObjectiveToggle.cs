﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using System;

public class ObjectiveToggle : MonoBehaviour
{
    [SerializeField] private Image[] imgs = null;
    [SerializeField] private Color cSucess = Color.green;
    [SerializeField] private Color cFail = Color.gray;
    [SerializeField] private Color cReset = Color.white;

    private CodeAnimator codeAnimator = new CodeAnimator();

    [SerializeField] private float animationDuration = .75f;
    [SerializeField] private AnimationCurve colorCurve = null;
    [SerializeField] private AnimationCurve sizeCurve = null;


    public bool swapSprites = false;


    private Action onAnimationDone;
    public Action OnAnimationDone
    {
        get { return onAnimationDone; }
        set { onAnimationDone = value; }
    }

    public Text txt;
    public Text Text => txt;

    [Button]
    public void SetSucess()
    {
        if(gameObject.activeInHierarchy)
            StartColorAnimation(Color.white, cSucess);
    }

    [Button]
    public void SetFail()
    {
    }

    [Button]
    public void SetReset()
    {
        SetColor(cReset);
    }

    private void StartColorAnimation(Color startColor, Color targetColor)
    {
        Color c;
        codeAnimator.StartAnimacion(this,
            x =>
            {
                c = Color.Lerp(startColor, targetColor, colorCurve.Evaluate(x));
                SetColor(c);

                transform.localScale = Vector3.one * sizeCurve.Evaluate(x);
            }, DeltaTimeType.deltaTime, AnimationType.Simple, null,()=> OnAnimationDone?.Invoke(), animationDuration);
    }

    public Color GetColor()
    {
        return imgs[0].color;
    }

    public void SetColor(Color c)
    {
        foreach (var img in imgs)
            img.color = c;
    }
}
