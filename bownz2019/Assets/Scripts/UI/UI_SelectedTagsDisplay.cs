﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_SelectedTagsDisplay : MonoBehaviour
{
   [SerializeField] private Canvas_LevelConfiguration canvas_LevelConfiguration;
    public Text txt;

    public bool isAdd; 

    private void OnEnable()
    {
        if(isAdd)
        {
            canvas_LevelConfiguration.OnAddTagsChange -= RefreshText;
            canvas_LevelConfiguration.OnAddTagsChange += RefreshText;
        }
        else
        {
            canvas_LevelConfiguration.OnRemoveTagsChange -= RefreshText;
            canvas_LevelConfiguration.OnRemoveTagsChange += RefreshText;
        }
       
        if(isAdd)
            RefreshText(canvas_LevelConfiguration.AddTags);
        else
            RefreshText(canvas_LevelConfiguration.RemoveTags);
    }

    private void OnDisable()
    {
        if (isAdd)
            canvas_LevelConfiguration.OnAddTagsChange -= RefreshText;
        else
            canvas_LevelConfiguration.OnRemoveTagsChange -= RefreshText;
    }

    private void RefreshText(List<STag> obj)
    {
        txt.text = "";
        for (int i = 0; i < obj.Count; i++)
        {
            txt.text += ">" + obj[i].name + " \n";
        }
    }
}
