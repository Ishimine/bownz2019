﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

public class UI_DebugValueChangeDisplay : UI_ValueChangeMsgDisplay
{

    public float distFromBot;
    public float moveDistance;

    public RectTransform conteiner;
    public Text txt;
    public Image icon;

    private CodeAnimator codeAnimator = new CodeAnimator();
    private float currentDistance;

    [SerializeField] private CodeAnimatorCurve animatorCurveIn = null;
    [SerializeField] private CodeAnimatorCurve animatorCurveOut = null;

    [SerializeField] private AnimationCurve numberAnimCurve = new AnimationCurve();

    ValueChangeMsg msg;

    Queue<ValueChangeMsg> msgsQueue = new Queue<ValueChangeMsg>();


    private bool showing = false;

    private void Start()
    {
    }

    public override void Show(ValueChangeMsg valueChangeMsg)
    {
        msgsQueue.Enqueue(valueChangeMsg);
        ShowNext();
    }

    private void ShowNext()
    {
        if(!showing && msgsQueue.Count > 0)
            _Show(msgsQueue.Dequeue());
    }

    private void _Show(ValueChangeMsg valueChangeMsg)
    {
        showing = true;
        msg = valueChangeMsg;
        if (valueChangeMsg.Icon == null)
            icon.color = Color.clear;
        else
        {
            icon.color = Color.white;
            icon.sprite = valueChangeMsg.Icon;
        }
        txt.text = valueChangeMsg.StartValue.ToString(valueChangeMsg.format);
        SetPosition(valueChangeMsg.Position);
        Open(valueChangeMsg);   
    }

    private void SetPosition(MsgPosition msgPosition)
    {
        if (msgPosition == MsgPosition.Bottom)
        {
            conteiner.anchorMin = new Vector2(.5f, 0);
            conteiner.anchorMax = new Vector2(.5f, 0);
            moveDistance = Mathf.Abs(distFromBot + conteiner.rect.height);
            conteiner.anchoredPosition = Vector3.down * conteiner.rect.height;
        }
        else
        {
            conteiner.anchorMin = new Vector2(.5f, 1);
            conteiner.anchorMax = new Vector2(.5f, 1);
            moveDistance = -Mathf.Abs(distFromBot + conteiner.rect.height);
            conteiner.anchoredPosition = Vector3.up * conteiner.rect.height;
        }
    }

    [Button]
    public void Open(ValueChangeMsg toast_Msg)
    {
        Vector2 startPos = conteiner.anchoredPosition;
        codeAnimator.StartAnimacion(this,
            x =>
            {
                conteiner.anchoredPosition = startPos + Vector2.up * moveDistance * x;
            }, DeltaTimeType.deltaTime, AnimationType.Simple, animatorCurveIn.Curve, () => Wait(toast_Msg.Duration), animatorCurveIn.Time);
    }

    private void Wait(float duration)
    {
        codeAnimator.StartAnimacion(
            this,
            x =>
            {
                txt.text = Mathf.Lerp(msg.StartValue, msg.EndValue, x).ToString(msg.format);
            }
            ,
            DeltaTimeType.deltaTime,
            AnimationType.Simple,
            numberAnimCurve,
            Close,
            duration
            );
    }

    [Button]
    private void Close()
    {
        Vector2 startPos = conteiner.anchoredPosition;
        codeAnimator.StartAnimacion(this,
           x =>
           {
               conteiner.anchoredPosition = startPos + Vector2.down * moveDistance * x;
           }, DeltaTimeType.deltaTime, AnimationType.Simple, animatorCurveOut.Curve,
           ()=>
           {
               showing = false;
               ShowNext();
           }, animatorCurveOut.Time);
    }

    private void OnDisable()
    {
        showing = false;
    }
}
