﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

public class UI_ChallengeModeToggle : MonoBehaviour
{
    [ShowInInspector, ReadOnly] public bool IsChallengeMode
    {
        get  => Settings.IsChallengeMode;
    }

    public Toggle toggle;

    public void Toggle()
    {
        Settings.IsChallengeMode = !Settings.IsChallengeMode;
    }

    private void OnEnable()
    {
        toggle.isOn = Settings.IsChallengeMode;
    }
}
