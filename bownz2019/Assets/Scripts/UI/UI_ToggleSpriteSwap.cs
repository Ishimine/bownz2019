﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

public class UI_ToggleSpriteSwap : MonoBehaviour
{
    public Toggle toggle;
    public Image image;
    public Sprite onSprite;
    public Sprite offSprite;

    public void Toggle()
    {
        Pressed(!toggle.isOn);
    }

    public void Pressed(bool value)
    {
        if (value) IsOn();
        else IsOff();
    }

    [Button]
    public void IsOn()
    {
        image.sprite = onSprite;
    }

    [Button]
    public void IsOff()
    {
        image.sprite = offSprite;
    }

    private void OnEnable()
    {
        toggle.isOn = false;
        Pressed(false);   
    }

}
