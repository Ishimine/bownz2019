﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_ImageToggle : MonoBehaviour
{
    public GameObject onImage;
    public GameObject offImage;


    private bool state;
    public bool IsOn => state;
    private void Awake()
    {
        SetOn(false);
    }

    public void SetOn(bool value)
    {
        state = value;
        onImage.gameObject.SetActive(value);
        offImage.gameObject.SetActive(!value);
    }



}
