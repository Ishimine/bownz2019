﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class UI_RaysRandom : MonoBehaviour
{
    public ConstantRotation[] rays;

    public Vector2 rotationSpeed = new Vector2(90, 720);
    public Vector2 length = new Vector2(90, 720);

    public ConstantRotation constantRotation;

    public bool useRandom = false;


    [Button]
    public void Initialize()
    {
        if (useRandom)
        {
            for (int i = 0; i < rays.Length; i++)
            {
                rays[i].enabled = true;
                rays[i].rotationSpeed = Random.Range(rotationSpeed.x, rotationSpeed.y) * ((Random.Range(0, 2) == 0) ? 1 : -1);
                rays[i].transform.rotation = Quaternion.Euler(0, 0, rays[i].transform.rotation.eulerAngles.z + rays[i].rotationSpeed);
                rays[i].transform.rotation = Quaternion.Euler(0, 0, rays[i].transform.rotation.eulerAngles.z + rays[i].rotationSpeed);
            }
        }
        else
        {
            float step = 360 / rays.Length;
            for (int i = 0; i < rays.Length; i++)
            {
                rays[i].enabled = false;
                rays[i].transform.rotation = Quaternion.Euler(0, 0, step * i);
            }
        }
    }
}
