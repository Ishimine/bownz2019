﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.UI;

public class UI_ButtonProgression : MonoBehaviour
{
    public Image background;
    public UI_Button_Id buttonId;
    private LevelProgress playerScore;
    public UI_ImageToggle[] toggles;

    public Color perfectLevelColor;
    private void Register()
    {
        Unresgister();
        buttonId.OnIdChange += OnIdChange;
    }
    private void Unresgister()
    {
        buttonId.OnIdChange -= OnIdChange;
    }

   private void OnEnable()
    {
        Register();
    }

    private void OnDisable(){Unresgister();}

    private void OnIdChange(UI_Button_Id obj)
    {
          playerScore = obj.PlayerScore;
        LevelProgress objective = buttonId.TargetScore;

        toggles[0].SetOn(playerScore.Time <= objective.Time);
        toggles[1].SetOn(playerScore.Platforms <= objective.Platforms);
        toggles[2].SetOn(playerScore.Deaths <= objective.Deaths);

        if (toggles[0].IsOn && toggles[1].IsOn && toggles[2].IsOn)
            background.color = perfectLevelColor;
        else
            background.color = Color.white;
    }
}
