﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_Conteiner_Swap : MonoBehaviour
{
    public UI_ConteinerOpenClose[] uI_Conteiners;

    public void Swap()
    {
        foreach (UI_ConteinerOpenClose item in uI_Conteiners)
            item.Swap();
    }
}
