﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiButtonToggleSoundFx : UI_Button_Toggle
{
    private void OnEnable()
    {
        Initialize(AudioSettings.SoundFx);
    }

    public override void SetState(bool nState)
    {
        base.SetState(nState);
        AudioSettings.SoundFx = nState;
    }


}
