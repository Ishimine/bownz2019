﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiButtonToggleMusic : UI_Button_Toggle
{
    private void OnEnable()
    {
        Initialize(AudioSettings.Music);
    }

    public override void SetState(bool nState)
    {
        base.SetState(nState);
        AudioSettings.Music = nState;
    }
}
