﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIButtonOpenCloseDebug : MonoBehaviour
{
    public bool state = false;

    public void Toggle()
    {
        state = !state;
        this.PostNotification(Notifications.Debug_OpenClose, state);
    }
  
}
