﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class Canvas_Debug : MonoBehaviour
{
    public UI_ConteinerOpenClose openClose;

    public GameObject reporterPrefab;

    private Reporter reporter;

    public Reporter Reporter
    {
        get
        {
            if (reporter == null)
            {
                reporter = Instantiate(reporterPrefab).GetComponent<Reporter>();
                reporter.SetEnable(true);
            }
            return reporter;
        }
    }

    public void OnEnable()
    {
        this.AddObserver(OpenClose, Notifications.Debug_OpenClose);
    }

    public void OnDisable()
    {
        this.RemoveObserver(OpenClose, Notifications.Debug_OpenClose);
    }

    private void OpenClose(object arg1, object arg2)
    {
        OpenClose((bool)arg2);
    }

    [Button]
    private void OpenClose(bool value)
    {
        if (value)
            openClose.Open();
        else
            openClose.Close();
    }

    public void Close()
    {
        OpenClose(false);
    }

    public void SwapReporter()
    {
        Reporter.gameObject.SetActive(!Reporter.gameObject.activeSelf);

    }
    
}
