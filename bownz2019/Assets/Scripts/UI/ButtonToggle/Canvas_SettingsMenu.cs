﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Canvas_SettingsMenu : MonoBehaviour
{
    public UI_ConteinerOpenClose openClose;

    private void Awake()
    {
         this.AddObserver(SetEnable, Notifications.SettingsMenu_Enable);
    }
   
    private void OnDestroy()
    {
        this.RemoveObserver(SetEnable, Notifications.SettingsMenu_Enable);
    }
                                                          
    private void SetEnable(object arg1, object arg2)
    {
        gameObject.SetActive((bool) arg2);
    }

    private void OnEnable()
    {
        this.AddObserver(SettingsMenu_OpenClose, Notifications.SettingsMenu_OpenClose);
    }

    private void OnDisable()
    {
        this.RemoveObserver(SettingsMenu_OpenClose, Notifications.SettingsMenu_OpenClose);
    }

    private void SettingsMenu_OpenClose(object arg1, object arg2)
    {
        OpenOrClose((bool)arg2);
    }

    public void OpenOrClose(bool value)
    {
        if (openClose.InAnimation) return;
        if ( value)
        {
            openClose.Open();
            this.PostNotification(Notifications.PauseRequest, true);
        }
        else
        {
            openClose.Close(()=> this.PostNotification(Notifications.PauseRequest, false));
        }
    }
                        
    public void SwapOpenClose()
    {
        OpenOrClose(!openClose.IsOpen);
    }

    public void Quit()
    {
        this.PostNotification(Notifications.GameMode_Quit);
    }
}