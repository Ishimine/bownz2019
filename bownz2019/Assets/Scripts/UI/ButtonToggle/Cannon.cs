﻿using System;
using System.Collections;
using System.Collections.Generic;
using Lean.Touch;
using UnityEngine;
using Sirenix.OdinInspector;

public class Cannon : MonoBehaviour
{
    public float centeringDuration = 0.3f;
    public AnimationCurve curve;
    public float fireVelocity = 10;

    public Transform cannonTransform;

    private CodeAnimator codeAnimator = new CodeAnimator();

    private BallStateMachine ball;

    public bool automaticFire = false;

    [ShowIf("automaticFire")] public float waitTime;

    public ParticlesPlay particles;

    public AnimationCurve knockBackCannonCurve;
    public float knockBackCannonDuration = .4f;
    public float knockBackForce = .5f;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.isTrigger) return;
        if (collision.attachedRigidbody.gameObject.tag.Equals("Player"))
            CenterBall(collision.attachedRigidbody.gameObject.GetComponent<BallStateMachine>());
    }

    private void CenterBall(BallStateMachine ball)
    {
        this.ball = ball;
        ball.Rb.velocity = Vector2.zero;
        ball.Rb.isKinematic = true;

        //ball.ChangeToPosing();

        ball.transform.SetParent(transform);
        Vector2 startPos = ball.transform.position;
        codeAnimator.StartAnimacion(this,
            x =>
            {
                ball.transform.position = Vector2.Lerp(startPos, transform.position, x);
            }, DeltaTimeType.deltaTime, AnimationType.Simple, curve,
            ()=>
            {
                if (automaticFire)
                    codeAnimator.StartWaitAndExecute(this, waitTime, Fire, false);
            }, centeringDuration);
    }

    [Button]
    public void Fire()
    {
        if (ball == null) return;

        ball.transform.SetParent(null);
        ball.Rb.isKinematic = false;
        ball.Rb.AddForce(transform.up * fireVelocity, ForceMode2D.Impulse);
        ball = null;

particles.Play();
        codeAnimator.StartAnimacion(this,
            x =>
            {
                cannonTransform.localPosition = Vector3.up + Vector3.up * x;
            }, DeltaTimeType.deltaTime, AnimationType.Simple, knockBackCannonCurve, knockBackCannonDuration);
    }

    private void OnEnable()
    {
        if (!automaticFire)
        {
            InputManager.OnFingerUp -= TapFire;
            InputManager.OnFingerUp += TapFire;
        }
    }

    private void TapFire(LeanFinger obj)
    {
        Fire();
    }

    private void OnDisable()
    {
        if (!automaticFire)
            InputManager.OnFingerUp -= TapFire;
    }
}
