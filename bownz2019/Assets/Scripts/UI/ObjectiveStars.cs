﻿using System.Collections;
using System.Collections.Generic;
using Pooling.Poolers;
using UnityEngine;
using Sirenix.OdinInspector;

public class ObjectiveStars : MonoBehaviour
{
    [TabGroup("General")] public float border = 20;
    [TabGroup("General")] public SetPooler setPooler;
    [TabGroup("General")] public UI_StarMedal starMedal;

    [Header("Counting")]
    [TabGroup("Animation"),SerializeField] private float coutingDuration = 1.3f;
    public float CoutingDuration
    {
        get { return coutingDuration; }
        set { coutingDuration = value; }
    }

    [TabGroup("Animation")]
    [Header("Fusion")]
    [TabGroup("Animation"),SerializeField] private float duration;
    [TabGroup("Animation"),SerializeField] private AnimationCurve animationCurve = new AnimationCurve();

    [TabGroup("Animation")]
    [Header("Shrink")]
    [TabGroup("Animation"),SerializeField] private float shrinkDuration = 1.5f;
    [TabGroup("Animation"),SerializeField] private float rotation = 480;
    [TabGroup("Animation"),SerializeField] private AnimationCurve shrinkCurve = new AnimationCurve();
    [TabGroup("Animation"),SerializeField] private AnimationCurve rotationCurve = new AnimationCurve();
    [TabGroup("Animation"),SerializeField] private AnimationCurve colorCurve = new AnimationCurve();

    public List<ObjectiveToggle> stars = new List<ObjectiveToggle>();

    IEnumerator rutine;

    public void Initialize(int totalStars)
    {
        starMedal.gameObject.SetActive(false);
        if (stars.Count > 0)
        {
            for (int i = stars.Count - 1; i >= 0; i--)
            {
                setPooler.EnqueueScript(stars[i]);
            }
        }
        stars.Clear();

        ObjectiveToggle current;
        for (int i = 0; i < totalStars; i++)
        {
            current = setPooler.DequeueScript<ObjectiveToggle>();
            current.transform.SetParent(transform);
            current.gameObject.SetActive(true);
            current.gameObject.transform.localScale = Vector3.one;
            current.SetReset();
            stars.Add(current);
        }
        PositionStars();
    }

    public void AlreadyCompleted()
    {
        starMedal.transform.localPosition = Vector3.zero;
        starMedal.gameObject.SetActive(true);
        starMedal.Show(false, null);
    }


    public void Show (int collectedStars)
    {
        if (!gameObject.activeInHierarchy) return;

        if (stars.Count == collectedStars)
        {
            stars[stars.Count - 1].OnAnimationDone -= AllCollectedAnimation;
            stars[stars.Count - 1].OnAnimationDone += AllCollectedAnimation;
        }

        if (rutine != null) StopCoroutine(rutine);
        rutine = CountingRutine(collectedStars);
        StartCoroutine(rutine);
    }

    [Button]
    private void PositionStars()    
    {
        float xSize = Mathf.Min((transform as RectTransform).sizeDelta.y, (transform as RectTransform).sizeDelta.x / stars.Count);
        Vector2 size = new Vector2(xSize - border, xSize - border);
        Vector2 startPosition = ((float)stars.Count / 2) * Vector2.left * xSize + Vector2.right * xSize*.5f;
        for (int i = 0; i < stars.Count; i++)
        {
            (stars[i].transform as RectTransform).sizeDelta = size;
            (stars[i].transform as RectTransform).anchoredPosition = startPosition + Vector2.right * xSize * i;
        }
    }

    [Button]
    private void AllCollectedAnimation()
    {
        stars[stars.Count - 1].OnAnimationDone -= AllCollectedAnimation;

        if (rutine != null) StopCoroutine(rutine);
        rutine = AllCollectedAnimationRutine();
        StartCoroutine(rutine);
    }

    IEnumerator CountingRutine(int collectedStars)
    {
        float pause = coutingDuration / collectedStars;

        for (int i = 0; i < stars.Count; i++)
        {
            if (i < collectedStars)
            {
                stars[i].SetSucess();
                if(i != stars.Count-1)  yield return new WaitForSeconds(pause);
            }
        }
    }

    IEnumerator AllCollectedAnimationRutine()
    {
        Debug.Log("AllColelctedANimationRutine");
        float x = 0;
        Vector2[] startPositions = new Vector2[stars.Count];

        for (int i = 0; i < stars.Count; i++) startPositions[i] = (stars[i].gameObject.transform as RectTransform).anchoredPosition;
        do
        {
            x += Time.deltaTime / duration;
            for (int i = 0; i < stars.Count; i++)
            {
                (stars[i].gameObject.transform as RectTransform).anchoredPosition =  Vector2.LerpUnclamped(startPositions[i], Vector2.zero, animationCurve.Evaluate(x));
            }
            yield return null;
        } while (x < 1);

        for (int i = 1; i < stars.Count; i++) stars[i].gameObject.SetActive(false);

        x = 0;
        Vector3 startScale = stars[0].transform.localScale;
        Color startColor = stars[0].GetColor();
        float startRotation = stars[0].transform.rotation.eulerAngles.z;
        do
        {
            x += Time.deltaTime / shrinkDuration;
            stars[0].SetColor(Color.LerpUnclamped(startColor, Color.white, colorCurve.Evaluate(x)));
            stars[0].transform.localScale = Vector3.LerpUnclamped(startScale, Vector3.zero, shrinkCurve.Evaluate(x));
            stars[0].transform.rotation = Quaternion.Euler(0, 0, Mathf.LerpUnclamped(startRotation, startRotation + rotation, rotationCurve.Evaluate(x)));
            yield return null;
        } while (x < 1);

        starMedal.transform.localPosition = Vector3.zero;
        starMedal.Show(true, ()=> Settings.UiInteraction = true);
    }

    private void OnDisable()
    {
        if (rutine != null) StopCoroutine(rutine);
    }



}
