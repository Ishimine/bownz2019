﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class UI_StarMedal : MonoBehaviour
{
    /*  private Action onAnimationDone;
      public Action OnAnimationDone
      {
          get { return onAnimationDone; }
          set { onAnimationDone = value; }
      }*/

    [Header("Animation")]
    [SerializeField] private float duration = 2f;
    [SerializeField] private float medalSize = 1;
    [SerializeField] private UI_RaysRandom rays = null;
    [SerializeField] private AnimationCurve raysSizeCurve = null;
    [SerializeField] private AnimationCurve sizeCurve = null;
    [SerializeField] private AnimationCurve heightCurve = null;

    IEnumerator rutine;


    private void OnEnable()
    {
    }

    public void Show(bool useAnimation, Action postAction)
    {
        gameObject.SetActive(true);

        if (useAnimation)
        {
            transform.localScale = Vector2.zero;
            rays.gameObject.SetActive(true);
            rutine = Animation(postAction);
            StartCoroutine(rutine);
        }
        else
        {
            rays.gameObject.SetActive(false);
            transform.localPosition = Vector2.up * heightCurve.Evaluate(1);
            transform.localScale = Vector2.one * medalSize;
        }
    }

    IEnumerator Animation(Action postAction)
    {
        float x = 0;
        Vector2 startPosition = transform.localPosition;
        Vector2 startScale = Vector2.zero;
        rays.gameObject.SetActive(true);
        rays.Initialize();
        do
        {
            x += Time.deltaTime / duration;
            rays.transform.localScale = Vector3.LerpUnclamped(Vector3.zero, Vector3.one, raysSizeCurve.Evaluate(x));
            transform.localPosition = startPosition + Vector2.up * heightCurve.Evaluate(x);
            transform.localScale = startScale + Vector2.one * sizeCurve.Evaluate(x) * medalSize;
            yield return null;
        } while (x < 1);

        Inventory.Add(Inventory.IDs.Coin_Star, 1, true);
        postAction?.Invoke();
    }
    private void OnDisable()
    {
        if (rutine != null) StopCoroutine(rutine);
    }
}
