﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class UI_ValueChangeMsgDisplay : MonoBehaviour
{
    public abstract void Show(ValueChangeMsg valueChangeMsg);
}
