﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using System;
public class UI_EnableByFlag : MonoBehaviour
{
    [ShowInInspector, ReadOnly] private bool UI_Interactive = Settings.UiInteraction;

    public Selectable selectable;

    public Action<bool> OnChange { get; set; }


    private void OnValidate()
    {
        if (selectable == null)
            selectable = GetComponent<Selectable>();
    }

    private void OnEnable()
    {
        Settings.OnUI_InteractionChange -= SelectableSetActive;
        Settings.OnUI_InteractionChange += SelectableSetActive;
        SelectableSetActive(Settings.UiInteraction);
    }

    private void OnDisable()
    {
        Settings.OnUI_InteractionChange -= SelectableSetActive;
    }

    private void SelectableSetActive(bool value)
    {
        selectable.interactable = value;
OnChange?.Invoke(value);
    }

}
