﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_PlayerMoney : MonoBehaviour
{
    public Text text;

    private void OnEnable()
    {
        /*Inventory.OnElementChange -= OnElementChange_Response;
        Inventory.OnElementChange += OnElementChange_Response;*/
        Inventory.OnInventoyChange -= OnInventoryChange;
        Inventory.OnInventoyChange += OnInventoryChange;

        UpdateText(Inventory.Get(Inventory.IDs.Coin));
    }

    private void OnDisable()
    {
        //Inventory.OnElementChange -= OnElementChange_Response;

        Inventory.OnInventoyChange -= OnInventoryChange;

    }

    private void OnInventoryChange()
    {
        UpdateText(Inventory.Get(Inventory.IDs.Coin.ToString()));
    }

    private void OnElementChange_Response(string key, int arg2, int arg3)
    {
        if  (key == Inventory.IDs.Coin.ToString())
            UpdateText(arg3);
    }

    public void UpdateText(int ammount)
    {
        text.text = ammount.ToString();
    }
}
