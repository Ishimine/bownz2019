﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UI_HoldButton : MonoBehaviour, IPointerDownHandler, IPointerClickHandler, IPointerExitHandler
{
    [SerializeField] private float holdDuration = 1.5f;
    [SerializeField] private float timer = 0;
    [SerializeField] private float clickTimer = .4f;

    bool isPointerDown = false;

    public UnityEvent LongHoldEvent;
    public UnityEvent ClickEvent;
    public UnityEvent FailEvent;

    public bool executeHoldOnPointerUp;

    private Action<float> onHoldProgress;
    public Action<float> OnHoldProgress
    {
        get { return onHoldProgress; }
        set { onHoldProgress = value; }
    }

    public void Initialize()
    {

    }

    private void Update()
    {
        if(isPointerDown)
        {
            timer += Time.deltaTime;
            OnHoldProgress?.Invoke(Mathf.Clamp01( timer / holdDuration));
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        timer = 0;
        isPointerDown = true;
    }

    //Sobre el boton
    public void OnPointerClick(PointerEventData eventData)
    {
        if (timer <= clickTimer)
            ClickEvent.Invoke();
        else if (timer >= holdDuration)
            LongHoldEvent?.Invoke();
        else
            FailEvent?.Invoke();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        isPointerDown = false;
    }
}
