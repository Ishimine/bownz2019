﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.Events;
using Sirenix.OdinInspector;

public class AutoStyle : MonoBehaviour
{
    [Header("OnMaterialChange")]
    public UnityEventMaterial onBackground;
    public UnityEventMaterial onBackGodray;
    public UnityEventMaterial onFrontalGodray;
    public UnityEventSpriteShape onSpriteShape;

    private void OnEnable()
    {
        StyleManager.OnStyleChanged -= StyleChange_Response;
        StyleManager.OnStyleChanged += StyleChange_Response;
        StyleChange_Response(StyleManager.Current);
    }

    private void OnDisable()
    {
        StyleManager.OnStyleChanged -= StyleChange_Response;
    }

    [Button]
    private void StyleChange_Response(StyleProfile nProfile)
    {
        onBackground?.Invoke(nProfile.Background);
        onBackGodray?.Invoke(nProfile.BackgroundGodrays);
        onFrontalGodray?.Invoke(nProfile.ForegroundGodrays);
        onSpriteShape?.Invoke(nProfile.SpriteShape);
    }
}
[System.Serializable]
public class UnityEventSpriteShape : UnityEvent<SpriteShape>
{

}

[System.Serializable]
public class UnityEventMaterial : UnityEvent<Material>
{

}
