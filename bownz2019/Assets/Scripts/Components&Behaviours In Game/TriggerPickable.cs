﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerPickable : Pickable
{

    [SerializeField] private Collider2D col;
    public Collider2D Col
    {
        get { return col; }
        set { col = value; }
    }

    [SerializeField] private ContactFilter2D contactFilter = new ContactFilter2D { useLayerMask = true };

    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        if (!Active) return;
        if (!contactFilter.IsFilteringLayerMask(collision.gameObject))
        {
            PickUp(collision.gameObject);
        }
    }
}
