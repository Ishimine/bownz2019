﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[RequireComponent(typeof(Collider2D))]
public class SquashAtCollition : MonoBehaviour
{
    [Header("Animacion")]
    public bool usarPosDelay = false;
    bool aplicarDelay = false;

    [Range(0,1)]
    public float offset;
    [Range(0,1)]
    public float deformacion = .15f; 
    
    public float duracion = 3;
    public AnimationCurve curva = AnimationCurve.Linear(0,0,1,1);

    [Header("Dependencia")]
    public Transform tVisual;
    public Transform tSkin;

    private Rigidbody2D rb;

    IEnumerator rutina;


    public void AplicarDelay()
    {
        aplicarDelay = true;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!collision.enabled) return;
        AplicarDelay();
        if (rutina != null)
        {
            StopCoroutine(rutina);
        }

        ContactPoint2D[] contacts = new ContactPoint2D[1];

        collision.GetContacts(contacts);
        StartSquash(contacts[0].point);
    }

    public void StartSquash(Vector3 point)
    {
        if (!gameObject.activeSelf) return;
        rutina = Squash(point);
        StartCoroutine(rutina);
    }

    private void OnEnable()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    public void OnDisable()
    {
        if (rutina != null) StopCoroutine(rutina);
    }

    IEnumerator Squash(Vector3 p)
    {
        float t = offset;
        float defAct = deformacion;
        float c = 0;
        do
        {
            t += (Time.deltaTime / duracion);

            //Rotacion
            Vector2 dif = tVisual.position - p;
            float rot = Mathf.Atan2(dif.y, dif.x) * Mathf.Rad2Deg;
            tVisual.rotation = Quaternion.Euler(tVisual.rotation.x, tVisual.rotation.y, rot);
            tSkin.rotation = tVisual.parent.rotation;
            
            c = curva.Evaluate(t);
            float x = -c * deformacion + 1;
            float y = c * deformacion + 1;
            tVisual.localScale = new Vector3(x, y,1);
 
            yield return null;
            if (usarPosDelay)
            {
                PosicionDelay();
            }
        } while (t < 1);
        tVisual.localScale = new Vector3(1, 1,1);
    }

    public void PosicionDelay()
    {
        if (usarPosDelay && aplicarDelay)
        {
            transform.position = transform.position - (Vector3)rb.velocity/4*3 * Time.fixedDeltaTime;
            aplicarDelay = false;
        }
    }
}
