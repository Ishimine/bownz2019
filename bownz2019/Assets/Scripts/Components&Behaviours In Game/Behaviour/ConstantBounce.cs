﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
[RequireComponent(typeof(Rigidbody2D))]
public class ConstantBounce : MonoBehaviour
{
    public LayerMask bounceMask;
    float FuerzaRebote
    {
        get => Settings.BounceForce;
    }

    private Rigidbody2D rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void OnValidate()
    {
        if (rb == null) rb = GetComponent<Rigidbody2D>();
    }

    Bounciness bounciness;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!collision.enabled) return;

        ContactPoint2D[] contacts = new ContactPoint2D[1];
        collision.GetContacts(contacts);
        Vector2 dir = ((Vector2)transform.position - contacts[0].point).normalized;
        if(collision.rigidbody != null)
            bounciness = collision.rigidbody.GetComponent<Bounciness>();

        if(bounciness == null && collision.collider != null)
            bounciness = collision.collider.GetComponent<Bounciness>();

        float fReboteFinal = FuerzaRebote * ((bounciness != null)?bounciness.value:1);
        rb.AddForce(dir * fReboteFinal, ForceMode2D.Impulse);
        bounciness = null;
    }
}
