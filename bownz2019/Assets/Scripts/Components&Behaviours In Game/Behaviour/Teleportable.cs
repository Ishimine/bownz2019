﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Teleportable : MonoBehaviour
{
    private Action onTeleportationStarted;
    public Action OnTeleportationStarted    { get { return onTeleportationStarted;  }   set { onTeleportationStarted = value;   } }
        
    private Action onTeleportationEnded;
    public Action OnTeleportationEnded      { get { return onTeleportationEnded;    }   set { onTeleportationEnded = value;     } }

    [SerializeField] private Rigidbody2D rb;
    public Rigidbody2D Rb                   { get { return rb; } }

    private void OnValidate()
    {
        if(rb == null) rb = GetComponent<Rigidbody2D>();
    }

    public void TeleportationStarted()
    {
        OnTeleportationStarted?.Invoke();
        this.PostNotification(Notifications.Ball_Teleporting_Start);
    }

    public void TeleportationEnded()
    {
        OnTeleportationEnded?.Invoke();
        this.PostNotification(Notifications.Ball_Teleporting_End);
    }
}
