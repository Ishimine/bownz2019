﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Rigidbody2D))]
public class LimitadorVelocidad : MonoBehaviour {

    public Rigidbody2D rb;

    public float sqrMaxVelocity = 10;

    private void OnValidate()
    {
        if (rb != null)
            rb = GetComponent<Rigidbody2D>();
    }

    public void FixedUpdate()
    {

        if (rb.velocity.sqrMagnitude > sqrMaxVelocity)
        {
            rb.velocity = Vector3.ClampMagnitude(rb.velocity, sqrMaxVelocity);
        }
    }
}
