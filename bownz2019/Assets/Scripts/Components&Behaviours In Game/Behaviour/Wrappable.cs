﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;

public class Wrappable : MonoBehaviour
{
    [ShowInInspector, ReadOnly] private static List<Wrappable> elements = new List<Wrappable>();
    public static List<Wrappable> Elements
    {
        get { return elements; }
        set { elements = value; }
    }

    private Action<Wrappable> onWrapStart;
    public Action<Wrappable> OnWrapStart { get { return onWrapStart; } set { onWrapStart = value; } }

    private Action<Wrappable> onWrapEnd;
    public Action<Wrappable> OnWrapEnd { get { return onWrapEnd; } set { onWrapEnd = value; } }

    [SerializeField] private Collider2D col = null;
    public Collider2D Col { get { return col; } }

    [ShowInInspector]
    private bool[] canWrap = new bool[2] { true, true };


    public bool CanWrap
    {
        get => (canWrap[0] || canWrap[1]);
    }

    public void ResetFlags()
    {
        canWrap[0] = true;
        canWrap[1] = true;
    }

    Vector3 offset;
    Vector3 dif;

    public void SetWrap(bool xWrap, bool yWrap)
    {
        canWrap[0] = xWrap;
        canWrap[1] = yWrap;
    }

    public void Wrap(BoxCollider2D boxCollider)
    {
        this.PostNotification(Notifications.Ball_Teleporting_Start);

        if (!CanWrap)
            return;

        OnWrapStart?.Invoke(this);

        //gameObject.PostNotification(Notifications.Wrappable_Start);

        offset = Vector3.zero;

        dif = (transform.position - boxCollider.transform.position)*2;

        if(canWrap[0] && Mathf.Abs(dif.x) > boxCollider.bounds.size.x) //Chequea si YA se hizo el wrap en X
        {
            offset.x = dif.x;
            canWrap[0] = false;
        }   

        if(canWrap[1] && Mathf.Abs(dif.y) > boxCollider.bounds.size.y) //Chequea si YA se hizo el wrap en Y
        {
            offset.y = dif.y;
            canWrap[1] = false;
        }

        transform.position -= offset;
        //gameObject.PostNotification(Notifications.Wrappable_Done);

        OnWrapEnd?.Invoke(this);
        this.PostNotification(Notifications.Ball_Teleporting_End);
    }


    private void OnEnable()
    {
        elements.Add(this);

        if (col == null)
            Debug.LogError(this + " No tiene definido el collider");
    }

    private void OnDisable()
    {
        Debug.Log(this + "Was disable");
        elements.Remove(this);
    }
}
