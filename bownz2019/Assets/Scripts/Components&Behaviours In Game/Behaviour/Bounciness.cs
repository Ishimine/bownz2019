﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bounciness : MonoBehaviour
{
    public float value = 1;
    public float Value
    {
        get
        {
            return value;
        }
    }
}
