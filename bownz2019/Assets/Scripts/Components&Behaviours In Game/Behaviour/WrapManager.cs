﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class WrapManager : MonoBehaviour
{
    private static bool wrapX = false;
    [ShowInInspector,ReadOnly] public static bool WrapX { get { return wrapX; } set { wrapX = value; } }

    private static bool wrapY = false;
    [ShowInInspector, ReadOnly]  public static bool WrapY { get { return wrapY; } set { wrapY = value; } }

    private BoxCollider2D boxCollider;

    private ContactFilter2D filter2D = new ContactFilter2D { useTriggers = true };

    private void OnEnable()
    {
        if (boxCollider == null)
        {
            boxCollider = MainCameraManager.CurrentCollider;
        }
        boxCollider.isTrigger = true;
        boxCollider.size = MainCameraManager.Current.GetOrtographicSize();

        MainCameraManager.OnCameraSizeChange -= OnCameraSizeChangeResponse;
        MainCameraManager.OnCameraSizeChange += OnCameraSizeChangeResponse;
    }

    private void OnDisable()
    {
        MainCameraManager.OnCameraSizeChange -= OnCameraSizeChangeResponse;
    }

    private void OnCameraSizeChangeResponse(Vector2 obj)
    {
        boxCollider.size = MainCameraManager.Current.GetOrtographicSize();
    }

    public void FixedUpdate()
    {
        for (int i = Wrappable.Elements.Count-1; i >= 0; i--)
        {
            if (Wrappable.Elements[i].isActiveAndEnabled && !boxCollider.IsTouching(Wrappable.Elements[i].Col, filter2D))
                Wrappable.Elements[i].Wrap(boxCollider);
            else
                Wrappable.Elements[i].ResetFlags();
        }
    }

    private void UpdateCollider(Rect playArea)
    {
        boxCollider.size = playArea.size;
        boxCollider.offset = playArea.center;
    }

   
}
