﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

public class GetDropdownOptionsFromChilds : MonoBehaviour
{
    [SerializeField] private Transform parent = null;
    [SerializeField] private Dropdown dropdown = null;

    List<Dropdown.OptionData> options;

    [Button]
    public void FindOptions()
    {
        List<Transform> childs = new List<Transform>();
        for (int i = 0; i < parent.childCount; i++)
        {
            childs.Add(parent.GetChild(i));
        }

        options = new List<Dropdown.OptionData>();

        for (int i = 0; i < childs.Count; i++)
        {
            Debug.Log("CHild: " + childs[i]);
            options.Add(new Dropdown.OptionData(childs[i].gameObject.name));
        }
        dropdown.options = options;
    }

    private void OnEnable()
    {
        FindOptions();
    }
}
