﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Sirenix.OdinInspector;
[RequireComponent(typeof(Rigidbody2D))]
public class HealthModifierSurface : MonoBehaviour
{
    [ShowInInspector,ReadOnly] string Type => (ammount <0)?"Damage":"Heal";
    
    public LayerMask targetLayer;
    [SerializeField] private int ammount;

    private Action<Health> onHealthModified;
    public Action<Health> OnHealthModified
    {
        get { return onHealthModified; }
        set { onHealthModified = value; }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Check(collision.gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Check(collision.rigidbody.gameObject);
    }

    private void Check(GameObject go)
    {
        if (targetLayer.Contains(go.layer))
        {
            Health h = go.GetComponent<Health>();
            if (h != null)
            {
                h?.Modify(ammount);
                OnHealthModified?.Invoke(h);
            }
        }
    }
}
