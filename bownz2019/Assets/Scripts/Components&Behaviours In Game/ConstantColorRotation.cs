﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConstantColorRotation : MonoBehaviour
{
    [SerializeField] private SpriteRenderer[] renderers;
    public SpriteRenderer[] Renders
    {
        get { return renderers; }
        set { renderers = value; }
    }

    [SerializeField] private Image[] images;



    [SerializeField] private float speedR = 0.45f;
    [SerializeField] private float speedG = 1.3f;
    [SerializeField] private float speedB = 0.75f;

    Vector3 color;

    private void Update()
    {
        color.x += Time.deltaTime * speedR;
        color.y += Time.deltaTime * speedG;
        color.z += Time.deltaTime * speedB;

        Color c = new Color(
            Mathf.PingPong(color.x,.5f) + .5f,
            Mathf.PingPong(color.y,.5f) + .5f,
            Mathf.PingPong(color.z,.5f) + .5f);

        foreach (SpriteRenderer render in renderers)
            render.color = c;

        foreach (Image img in images)
            img.color = c;
    }

    public void SetRenders(SpriteRenderer[] renderers)
    {
        this.renderers = renderers;
    }
}
