﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class CameraKillbox : MonoBehaviour
{
    [SerializeField] float extraSize = 0;

    public EdgeCollider2D edgeCollider;

    private Camera cam;

    public ContactFilter2D filter2D = new ContactFilter2D() { useLayerMask = true, useTriggers = true};

    private void OnEnable()
    {
        if (cam == null) cam = Camera.main;
    }

    [Button]
    void ShapeEdgeCollider()
    {
        if (cam == null) cam = Camera.main;
        Vector2[] pointsPositions = new Vector2[5];
        Vector2 camSize = cam.GetOrtographicSize() /2 + Vector2.one * extraSize;
        pointsPositions[0] = new Vector2(-camSize.x, -camSize.y);
        pointsPositions[1] = new Vector2(-camSize.x, camSize.y);
        pointsPositions[2] = new Vector2(camSize.x, camSize.y);
        pointsPositions[3] = new Vector2(camSize.x, -camSize.y);
        pointsPositions[4] = new Vector2(-camSize.x, -camSize.y);
        edgeCollider.points = pointsPositions;
    }

    private void FixedUpdate()
    {
        edgeCollider.offset = cam.transform.position;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(filter2D.IsFilteringLayerMask(collision.gameObject))
        {
            Health h = collision.gameObject.GetComponent<Health>();
            h.Damage(h.Max);
        }
    }
}
