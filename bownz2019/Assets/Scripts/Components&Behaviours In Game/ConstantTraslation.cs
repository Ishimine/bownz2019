﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstantTraslation : MonoBehaviour
{
    [SerializeField] private float speed = 2;
    [SerializeField] private Vector2 direction = Vector2.right;
  
    public void SetDirection(Vector2 nDir)
    {
        direction = nDir.normalized;
    }

    public void SetSpeed(float nSpeed)
    {
        this.speed = nSpeed;
    }

    public void FixedUpdate()
    {
        transform.Translate(Time.deltaTime * direction, Space.Self);
    }
}
