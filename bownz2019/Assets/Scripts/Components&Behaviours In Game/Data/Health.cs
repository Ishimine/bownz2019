﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Sirenix.OdinInspector;

public class Health : MonoBehaviour
{
    [SerializeField]
    private int max = 1;
    public int Max
    {
        get { return max; }
        set { max = value; }
    }

    [SerializeField]
    private int current;
    public int Current
    {
        get { return current; }
        set
        {
            OnChangeValue?.Invoke(current, value);
            current = value;
        }
    }

    private Action<int> onDamageReceived;
    public Action<int> OnDamageReceived
    {
        get { return onDamageReceived; }
        set { onDamageReceived = value; }
    }

    private Action<int> onHealReceived;
    public Action<int> OnHealReceived
    {
        get { return onHealReceived; }
        set { onHealReceived = value; }
    }

    private Action<int, int> onChangeValue;
    public Action<int,int> OnChangeValue
    {
        get { return onChangeValue; }
        set { onChangeValue = value; }
    }

    private Action<Health> onDead;
    public Action<Health> OnDead
    {
        get { return onDead; }
        set { onDead = value; }
    }

    public void Initialize(int maxValue)
    {
        Max = maxValue;
        Current = max;
    }

    public void Damage(int ammount)
    {
        Modify(-ammount);
    }

    public void Heal(int ammount)
    {
        Modify(ammount);
    }

    [Button]
    public void Modify(int ammount)
    {
        if (!enabled || ammount ==0) return;

        HealthException healthException = new HealthException(Current, ammount);
        this.PostNotification(Notifications.BeforeHealthModify, healthException);
        ammount = healthException.Ammount;

        Current += ammount;

        if (ammount < 0)
            OnDamageReceived?.Invoke(-ammount);
        else 
            OnHealReceived?.Invoke(ammount);

        if (Current <= 0)
            OnDead?.Invoke(this);
    }
}

public class HealthException
{
    public int Current;
    public int Ammount;

    public HealthException(int current, int ammount)
    {
        Current = current;
        Ammount = ammount;
    }
}