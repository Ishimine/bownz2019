﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ThreatSeeker : MonoBehaviour
{
    public bool useMovementUpdate = true;

    private Action<Threat> onThreatFound;
    public Action<Threat> OnThreatFound
    {
        get { return onThreatFound; }
        set { onThreatFound = value; }
    }

    private Action<Threat> onThreathLost;
    public Action<Threat> OnThreahLost
    {
        get { return onThreathLost; }
        set { onThreathLost = value; }
    }

    private Action<Threat> onThreathUpdate;
    public Action<Threat> OnThreathUpdate
    {
        get { return onThreathUpdate; }
        set { onThreathUpdate = value; }
    }

    [SerializeField] float searchWaitTime = .35f;
    [SerializeField] float distance = 0;
    float realDistance;

    Dictionary<Threat, Threat> threats = new Dictionary<Threat, Threat>();

    private IEnumerator rutine;

    private void OnEnable()
    {
        realDistance = distance*2;
    }

    private void OnDisable()
    {
        if(rutine != null) StopCoroutine(rutine);
    }

    private void StartSearch()
    {
        rutine = SearchRutine();
        StartCoroutine(rutine);
    }

    private IEnumerator SearchRutine()
    {
        var list = Threat.Elements;
        Threat current;
        for (int i = 0; i < list.Count; i++)
        {
            current = list[i];
            if(Mathf.Abs(Vector2.SqrMagnitude(transform.position - current.transform.position)) < realDistance)
            {
                if(threats.ContainsKey(current))
                {
                    OnThreathUpdate?.Invoke(current);
                }
                else
                {
                    threats.Add(current, current);
                    OnThreatFound?.Invoke(current);
                }
            }
            else if(threats.ContainsKey(current))
            {
                threats.Remove(current);
                OnThreahLost?.Invoke(current);
            }
        }
        yield return new WaitForSeconds(searchWaitTime);
    }

}
