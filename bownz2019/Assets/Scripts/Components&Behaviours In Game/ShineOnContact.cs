﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShineOnContact : MonoBehaviour
{
    public bool active = true;

    [SerializeField] private SpriteRenderer render;
    public SpriteRenderer Render
    {
        get { return render; }
        set { render = value; }
    }

    [SerializeField] private Color shineColor = Color.white;

   [SerializeField]  private Color startColor;

    [SerializeField] CodeAnimatorCurve animatorCurve = new CodeAnimatorCurve();

    private CodeAnimator codeAnimator = new CodeAnimator();

    public bool useColorFromAwake = true;

    private void Awake()
    {
        if(useColorFromAwake)
            startColor = render.color;
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (!active) return;
        Shine();
    }

    public void Shine()
    {
        codeAnimator.StartAnimacion(this,
          x =>
          {
              render.color = Color.Lerp(shineColor, startColor, x);
          }, DeltaTimeType.deltaTime, AnimationType.Simple, animatorCurve.Curve, animatorCurve.Time);
    }

}
