﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class FlashFX : MonoBehaviour
{
    bool active = false;
    public SpriteRenderer render;

    public float speed = .7f;

    [SerializeField] private AnimationCurve animationCurve = null;
    [SerializeField] private float intensity;

    CodeAnimator codeAnimator = new CodeAnimator();
    IEnumerator rutine;

    public Color baseColor;

    public void Update()
    {
        if (!active) return;

        render.color = Color.Lerp(baseColor, Color.white, animationCurve.Evaluate(intensity));
        intensity -= Time.deltaTime * speed;

        if (intensity <= 0) active = false;
    }

    public void Flash()
    {
        Flash(1);
    }
    [Button]
    public void Flash(float intensity)
    {
        active = true;
        this.intensity = intensity;
    }
}
