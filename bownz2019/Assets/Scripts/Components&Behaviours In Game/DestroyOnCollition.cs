﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DestroyOnCollition : MonoBehaviour
{
    public UnityEvent OnDesctroy;

    public LayerMask layerMask;
    bool destroying = false;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Check(collision.gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Check(collision.gameObject);
    }

    private void Check(GameObject gameObject)
    {
        if (!gameObject.activeSelf) return;

        Debug.Log("Collided with " + gameObject);
        if (!layerMask.Contains(gameObject.layer)) return;

        if (destroying) return;
        destroying = true;
        OnDesctroy?.Invoke();
        StartCoroutine(WaitNext());
    }

    IEnumerator WaitNext()
    {
        yield return new WaitForFixedUpdate();
        Destroy(gameObject);
    }
}
