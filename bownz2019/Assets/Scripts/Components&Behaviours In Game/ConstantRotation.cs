﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstantRotation : MonoBehaviour
{
    public float rotationSpeed = 30;

    private void Update()
    {
        transform.rotation = Quaternion.Euler(0,0, transform.eulerAngles.z + Time.deltaTime * rotationSpeed);
    }
}
