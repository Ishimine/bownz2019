﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class SetEnableChild : MonoBehaviour
{
    public List<GameObject> childs;
    private void OnEnable()
    {
        FindChilds();
    }

    [Button]
    public void FindChilds()
    {
       childs = new List<GameObject>();
        for (int i = 0; i < transform.childCount; i++)
        {
            childs.Add(transform.GetChild(i).gameObject);
        }
    }

    public void SelectEnable(int index)
    {
        SetChildEnable(index, true,true);
    }

    [Button]
    public void SetChildEnable(int index, bool value, bool applyInverseToSiblings = true)
    {
        if(index < 0 || index >= transform.childCount)
        {
            Debug.LogWarning("Invalid ID: " + index);
            return;
        }
        if(applyInverseToSiblings)
        {
            for (int i = 0; i < childs.Count; i++)
            {
                childs[i].SetActive((i == index)?value:!value);
            }
        }
        else childs[index].SetActive(value);
    }

}
