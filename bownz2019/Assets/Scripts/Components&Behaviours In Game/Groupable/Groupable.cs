﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Groupable<T> : MonoBehaviour where T : Groupable<T>
{
    public static List<T> elements = new List<T>();
    public static List<T> Elements
    {
        get => elements;
    }

    public abstract T MySelf
    {
        get;
    }

    protected virtual void OnEnable()
    {
        elements.Add(MySelf);
    }

    protected virtual void OnDisable()
    {
        elements.Remove(MySelf);
    }

}
