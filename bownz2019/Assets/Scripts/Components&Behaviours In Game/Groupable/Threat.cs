﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Threat : Groupable<Threat>
{
    public override Threat MySelf => this;
}
