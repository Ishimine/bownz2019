﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[ExecuteInEditMode]
public class RoundedBoxColliderFitter : MonoBehaviour
{
    public BoxCollider2D col;
    public SpriteRenderer render;

    #if UNITY_EDITOR
    private void Update()
    {
        if (col == null) col = GetComponent<BoxCollider2D>();
        if (render == null) render = GetComponent<SpriteRenderer>();
        Fit();
    }
    #endif

    [Button]
    public void Fit()
    {
        float border = col.edgeRadius * 2;
        col.size = render.size - Vector2.one * border;
    }

    private void Awake()
    {
        Fit();
    }
}
