﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Canvas_Shop_UI : MonoBehaviour
{
    public Text text;

    public bool CanSell => Shop.CanBuy;
    public Button button;

    private enum ButtonState
    {
        None,
        Buy,
        Equip
    }

    private ButtonState buttonState;

    Product product;
    private ProductDisplay productDisplay;

    public void NextPage()
    {
        this.PostNotification(Notifications.Shop_NextPage);
    }

    public void PreviousPage()
    {
        this.PostNotification(Notifications.Shop_PreviousPage);
    }

    public void UpdateCostTag(ProductDisplay nProductDisplay)
    {
        productDisplay = nProductDisplay;
        this.product = productDisplay.Product;
        if (Shop.IsBuyable(product) && CanSell)
        {
            text.text = product.GetPriceTag();
            buttonState =  ButtonState.Buy;
            button.interactable = true;
        }
        else if (Inventory.Contains(product.PrimaryId))
        {
            if (ReferenceEquals(BallSkin.PlayerSelectedSkin, productDisplay.Item) || ReferenceEquals(TrailBall.PlayerSelection, productDisplay.Item))
            {
                text.text = "";
                button.interactable = false;
            }
            else
            {
                text.text = "Use";
                buttonState = ButtonState.Equip;
                button.interactable = true;
            }
        }
        else
        {
            text.text = "LOCK";
            buttonState = ButtonState.None;
            button.interactable = false;
        }
    }

    public void TryBuy()
    {
        switch (buttonState)
        {
            case ButtonState.None:
                break;
            case ButtonState.Buy:
                this.PostNotification(Notifications.Shop_BuyButtonPressed);
                break;
            case ButtonState.Equip:
                this.PostNotification(Notifications.Shop_EquipButtonPressed);
                text.text = "";
                button.interactable = false;
                break;
        }
        if(productDisplay != null)
            UpdateCostTag(productDisplay);
    }

    private void OnDisable()
    {
        product = null;
    }

    public void GoToMain()
    {
        this.PostNotification(Notifications.BackToMain);
    }
}
