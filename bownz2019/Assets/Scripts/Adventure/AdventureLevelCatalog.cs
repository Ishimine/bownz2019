﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="AdventureMode/Catalog")]
public class AdventureLevelCatalog : ScriptableSingleton<AdventureLevelCatalog>
{
    public override AdventureLevelCatalog Myself => this;

    [SerializeField] private AdventureLevel[] adventureLevels;
    public static AdventureLevel[] AdventureLevels { get { return Instance.adventureLevels; } }

}
