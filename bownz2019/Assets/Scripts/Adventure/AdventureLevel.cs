﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AdventureMode/Level", fileName = "Adv_Level_00")]
public class AdventureLevel : ScriptableObject
{
    public LevelProgress objectives;
    public ProceduralLevelRequest[] levelStages;
}
