﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class ExtractPaths : MonoBehaviour
{
    public Transform root;
    public BoxCollider2D roofCollider;
    public BoxCollider2D floorCollider;

    public List<LineRenderer> lines;

    public Material lineMaterial;
    public float lineWidth = .15f;
    public List<Vector3[]> paths;

    [Button]
    public void Extract()
    {
        Vector2 roofColliderOffset = roofCollider.offset;
        Vector2 floorColliderOffset = floorCollider.offset;
        roofCollider.offset = Vector2.down * .03f;
        floorCollider.offset = Vector2.up * .03f;

        Rigidbody2D rb = root.gameObject.AddComponent<Rigidbody2D>();
        CompositeCollider2D col = root.gameObject.AddComponent<CompositeCollider2D>();
        col.vertexDistance = 1f;
        paths = new List<Vector3[]>();
        for (int i = 0; i < col.pathCount; i++)
        {
            Vector2[] path = new Vector2[col.GetPathPointCount(i)+1];
            col.GetPath(i, path);
            path[path.Length - 1] = path[0];
            Vector3[] path3 = new Vector3[path.Length];
            for (int j = 0; j < path.Length; j++)  path3[j] = path[j];
            paths.Add(path3);
        }
        CreateLines(paths);

        DestroyImmediate(col);
        DestroyImmediate(rb);

        floorCollider.offset = floorColliderOffset;                         
        roofCollider.offset = roofColliderOffset;
    }

    private void CreateLines(List<Vector3[]> paths)
    {
        DestroyLines();
        for (int i = 0; i < paths.Count; i++)
        {
            GameObject go = new GameObject();
            go.transform.SetParent(transform);
            LineRenderer line = go.AddComponent<LineRenderer>();
            line.positionCount = paths[i].Length;
            line.SetPositions(paths[i]);
            line.useWorldSpace = false;
            line.material = lineMaterial;
            line.widthMultiplier = lineWidth;
            lines.Add(line);
        }
    }

    [Button]
    public void DestroyLines()
    {
        for (int i = lines.Count - 1; i >= 0; i--)
        {
            DestroyImmediate(lines[i].gameObject);
        }
        lines.Clear();

        for (int i = transform.childCount - 1; i >= 0; i--)
        {
            DestroyImmediate(transform.GetChild(i).gameObject);
        }
    }

}
