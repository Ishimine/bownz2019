﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Touch;
using Sirenix.OdinInspector;
using System;

public class InputManager : MonoSingleton<InputManager>
{
    private static bool active = true;
    public override InputManager Myself => this;

    public static bool Active
    {
        get => active;
        private set => active = value;
    }

    public static Action<LeanFinger> OnFingerDown { get; set; }

    public static Action<LeanFinger> OnFingerSet { get; set; }

    public static Action<LeanFinger> OnFingerUp { get; set; }

    public static Action<LeanFinger> OnFingerSwipe { get; set; }

    public static Action<LeanFinger> OnFingerTap { get; set; }

    public static Action<LeanFinger> OnFingerExpired { get; set; }

    public Action<List<LeanFinger>> OnGesture { get; set; }

    private void OnEnable()
    {
        Unregister();
        Register();
    }

    private void OnDisable()
    {
        Unregister();
    }

    #region EventRegister
    private void Register()
    {
        Unregister();
        LeanTouch.OnFingerSwipe += FingerSwipe;
        LeanTouch.OnFingerDown += FingerDown;
        LeanTouch.OnFingerSet += FingerSet;
        LeanTouch.OnFingerTap += FingerTap;
        LeanTouch.OnFingerUp += FingerUp;
        LeanTouch.OnFingerExpired += FingerExpired;
        LeanTouch.OnGesture += Gesture;
    }
    private void Unregister()
    {
        LeanTouch.OnFingerSwipe -= FingerSwipe;
        LeanTouch.OnFingerDown -= FingerDown;
        LeanTouch.OnFingerSet -= FingerSet;
        LeanTouch.OnFingerTap -= FingerTap;
        LeanTouch.OnFingerUp -= FingerUp;
        LeanTouch.OnFingerExpired -= FingerExpired;
        LeanTouch.OnGesture -= Gesture;
    }
    #endregion

    private void FingerTap(LeanFinger leanFinger)
    {
        if (!Active || leanFinger.IsOverGui) return;
        OnFingerTap?.Invoke(leanFinger);
    }
    private void FingerDown(LeanFinger leanFinger)
    {
        if (!Active || leanFinger.IsOverGui) return;
        OnFingerDown?.Invoke(leanFinger);
        this.PostNotification(Notifications.OnFingerDown, leanFinger);
    }

    private void FingerSet(LeanFinger leanFinger)
    {
        if (!Active) return;
        OnFingerSet?.Invoke(leanFinger);
        this.PostNotification(Notifications.OnFingerSet, leanFinger);
    }

    private void FingerUp(LeanFinger leanFinger)
    {
        if (!Active) return;
        OnFingerUp?.Invoke(leanFinger);
        this.PostNotification(Notifications.OnFingerUp, leanFinger);
    }

    private void FingerSwipe(LeanFinger leanFinger)
    {
        if (!active) return;
        OnFingerSwipe?.Invoke(leanFinger);
    }

    private void FingerExpired(LeanFinger leanFinger)
    {
        if (!Active) return;
        OnFingerExpired?.Invoke(leanFinger);
    }

    private void Gesture(List<LeanFinger> leanFingers)
    {
        if (!Active) return;
        OnGesture?.Invoke(leanFingers);
    }

    public static void UnregisterController(IUseInputController useInputController)
    {
        Instance.Unregister(useInputController);
    }

    public static void RegisterController(IUseInputController useInputController)
    {
        Instance.Register(useInputController);
    }

    private void Unregister(IUseInputController useInputController)
    {
        OnFingerTap -= useInputController.FingerTap;
        OnFingerDown -= useInputController.FingerDown;
        OnFingerSet -= useInputController.FingerSet;
        OnFingerUp -= useInputController.FingerUp;
        OnFingerSwipe -= useInputController.FingerSwipe;
        OnFingerExpired -= useInputController.FingerExpired;
        OnGesture -= useInputController.Gesture;
    }

    private void Register(IUseInputController useInputController)
    {
        Unregister(useInputController);
        OnFingerTap += useInputController.FingerTap;
        OnFingerDown += useInputController.FingerDown;
        OnFingerSet += useInputController.FingerSet;
        OnFingerUp += useInputController.FingerUp;
        OnFingerSwipe += useInputController.FingerSwipe;
        OnFingerExpired += useInputController.FingerExpired;
        OnGesture += useInputController.Gesture;
    }

    public static void CleanController()
    {
        Instance.Clean();
    }

    private void Clean()
    {
        OnFingerTap = null;
        OnFingerDown = null;
        OnFingerSet = null;
        OnFingerUp = null;
        OnFingerSwipe = null;
        OnFingerExpired = null;
        OnGesture = null;
    }

    [Button]
    public static void SetActive(bool value)
    {
        Instance._SetActive(value);
    }
      
    private void _SetActive(bool value)
    {
        Active = value;
        this.PostNotification(Notifications.InputActiveChange, value);
    }

    public void Update()
    {
        if (!Settings.UiInteraction) return;
        if(Input.GetKey(KeyCode.Escape))
            this.PostNotification(Notifications.BackButtonPressed);
    }
}
