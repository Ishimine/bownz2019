﻿using UnityEngine;
using System.Collections;
using Sirenix.OdinInspector;
public class MonoStateMachine : MonoBehaviour, IStateMachine
{

#if UNITY_EDITOR
    [ShowInInspector, ReadOnly] string CurrentStateName
    {
        get
        {
            return (stateMachine.CurrentState != null) ? stateMachine.CurrentState.ToString() : "Null";
        }
    }
#endif

    StateMachine stateMachine = new StateMachine();
    public State CurrentState { get => stateMachine.CurrentState; set => stateMachine.CurrentState = value; }
    public void SwitchState(State nState)
    {
        stateMachine.SwitchState(nState);
    }
}
