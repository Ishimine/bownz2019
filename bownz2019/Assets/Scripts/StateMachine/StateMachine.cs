﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine : IStateMachine
{
    private State currentState;
    public State CurrentState
    {
        get { return currentState; }
        set
        {
            Debug.Log(this + " CurrentState: " + value.ToString());
            currentState = value;
        }
    }

    public void SwitchState(State nState) 
    {
        nState.SetMachine(this);
        if (currentState != null && currentState.GetType().Equals(nState.GetType()))
            return;

        if (currentState != null)
            currentState.Exit();

        currentState = nState;

        if (currentState != null)
            currentState.Enter();
    }
}

public interface IStateMachine
{
    void SwitchState(State nState);
    State CurrentState { get; set; }
}