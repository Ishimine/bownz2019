﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class State
{
    private StateMachine stateMachine = null;
    
    public void SetMachine(StateMachine stateMachine)
    {
//        Debug.Log("SetMachine: " + stateMachine);
        if (this.stateMachine != null)
            Debug.LogWarning("SOBREESCRIBIENDO LA MAQUINA DE ESTADOS WTF: " + this);
        this.stateMachine = stateMachine;
    }

    protected void SwitchState(State nState)
    {
        stateMachine.SwitchState(nState);
    }

    public virtual void Enter()
    {
        AddListeners();
    }

    public virtual void Exit()
    {
        RemoveListeners();
    }

    protected virtual void OnDestroy()
    {
        RemoveListeners();
    }
    protected virtual void AddListeners()
    {
    }

    protected virtual void RemoveListeners()
    {
    }
}
