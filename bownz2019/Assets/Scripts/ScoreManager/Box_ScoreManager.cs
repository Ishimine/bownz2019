﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class Box_ScoreManager : MonoBehaviour
{
    [SerializeField] private int score;

    private bool airCombo = false;

    [SerializeField] private float[] coinValues = new float[] { 1, 5, 3, 15, 3};

    //Indica el valor del multiplicador en relacion a cuantas plataformas se utilizaron para agarrar la estrella
    [SerializeField] private float[] platformMultiplicators = new float[] { 5, 5, 4, 3, 2, 1 };

    private int touchedPlatforms = 0;

    int currentAirMultiplicator = 1;

    private void ResetScore()
    {
        UpdateScore(0);
    }

    private void OnEnable()
    {
        this.AddObserver(OnCoinPickedUp, Notifications.OnCoinPickedUp);
        this.AddObserver(OnGameMode_ResetResponse, Notifications.GameMode_Reset);
        this.AddObserver(OnSwipePlatform_Touched_Response, Notifications.SwipePlatform_Touched);
    }

    private void OnDisable()
    {
        this.RemoveObserver(OnCoinPickedUp, Notifications.OnCoinPickedUp);
        this.RemoveObserver(OnGameMode_ResetResponse, Notifications.GameMode_Reset);
        this.RemoveObserver(OnSwipePlatform_Touched_Response, Notifications.SwipePlatform_Touched);
    }

    private void OnSwipePlatform_Touched_Response(object arg1, object arg2)
    {
        currentAirMultiplicator = 1;
        airCombo = false;
        touchedPlatforms++;
    }

    private void OnGameMode_ResetResponse(object source, object value)
    {
        UpdateScore(0);
    }

    private void OnCoinPickedUp(object source, object value)
    {
        if (airCombo)
            currentAirMultiplicator++;
        else
            airCombo = true;

        int id = (int)value;
        Color color = (id == 0) ? ColorPallete.NormalCoin : ColorPallete.GoldenCoin;

        int scoreGained = (int)GetCoinPickedUp(id, touchedPlatforms) * currentAirMultiplicator;
        PopTextManager.ShowPopMessage(new PopTextRequest(((CoinType)source).Position, color, "+"+scoreGained.ToString()));

        //Inventory.Add(Inventory.IDs.Coin, scoreGained);

        UpdateScore(score + scoreGained);
        touchedPlatforms = 0;
    }

    [Button]
    private float GetCoinPickedUp(int coinId, int touchedPlatforms)
    {
        if(coinId >= coinValues.Length)
            Debug.LogWarning("ID de la moneda atrapada NO registrado en la tabla de valores para puntaje");

        return GetMultiplicator(touchedPlatforms) * coinValues[Mathf.Min(coinId, coinValues.Length-1)];
    }

    private void UpdateScore(int nValue)
    {
        score = nValue;
        this.PostNotification(Notifications.ScoreUpdate, score);
    }

    private float GetMultiplicator(int touchedPlatforms)
    {
        float retValue = platformMultiplicators.Length;
        for (int i = platformMultiplicators.Length - 1; i >= 0; i--)
        {
            if (touchedPlatforms <= i)
                retValue = platformMultiplicators[i];
        }
        Debug.Log("Multiplicator: " + retValue);
        return retValue;
    }
}



