﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using Sirenix.OdinInspector;

public class UI_Medal : MonoBehaviour
{
    public Image[] tintables;

    public Color cCompleted;
    public Color cIncompleted;

    public GameObject[] enableOnActiveObjects;

    private void SetEnableObjects(bool value)
    {
        foreach (GameObject go in enableOnActiveObjects)
            go.SetActive(value);
    }
    [Button]
    public void SetComplete()
    {
        SetColor(cCompleted);
        SetEnableObjects(true);
    }

    [Button]
    public void SetIncomplete()
    {
        SetColor(cIncompleted);
        SetEnableObjects(false);
    }

    private void SetColor(Color c)
    {
        foreach (Image item in tintables)
        {
            item.color = c;
        }
    }


}
