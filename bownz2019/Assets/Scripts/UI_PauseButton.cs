﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class UI_PauseButton : MonoBehaviour, IPressed
{
    public UnityEventBool OnPressed;

    public Sprite pauseIcon;
    public Sprite playIcon;
    public Image render;

    private CodeAnimator codeAnimator = new CodeAnimator();

    public bool InPause => GameModeState.InPause;

    public void Pressed()
    {
        OnPressed?.Invoke(!InPause);

        if (!InPause)
            Pause();
        else
            codeAnimator.StartWaitAndExecute(this, .5f, Resume, true);
    }

    private void Pause()
    {
        this.PostNotification(Notifications.PauseRequest,true);
    }

    private void Resume()
    {
        this.PostNotification(Notifications.PauseRequest,false);
    }

    private void OnDisable()
    {        this.RemoveObserver(OnPauseChangeResponse, Notifications.PauseChange);

    }

    private void OnEnable()
    {
          this.AddObserver(OnPauseChangeResponse, Notifications.PauseChange);
        OnPauseChangeResponse(null, GameModeState.InPause);
    }

    private void OnPauseChangeResponse(object arg1, object arg2)
    {
        Debug.Log("OnPauseChangeResponse " + (bool)arg2);
        if ((bool)arg2)
            render.sprite = playIcon;
        else
            render.sprite = pauseIcon;
    }
}
