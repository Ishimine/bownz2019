﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExtraLifePopUp : MonoBehaviour
{
    [SerializeField] private Face_Conteiner faces = null;
    [SerializeField] private SpriteRenderer bodyRender = null;
    [SerializeField] private SpriteRenderer faceRender = null;

    [SerializeField] private float duration = 2.7f;

    [SerializeField] private float heightLenght;
    [SerializeField] private AnimationCurve heightCurve;
    [SerializeField] private AnimationCurve alphaCurve;

    private CodeAnimator codeAnimator = new CodeAnimator();

    private void Awake()
    {
        faceRender.sprite = faces.Happy.GetRandom();
    }

    public void SetSkin(BallSkin skin)
    {
        bodyRender.sprite = skin.Body;
    }

    public void SetTrail(GameObject trailPrefab)
    {
        Instantiate(trailPrefab, transform);
    }

    private void Start()
    {
        StartAnimation();
    }

    private void StartAnimation()
    {
        float a;
        Vector2 startPosition = gameObject.transform.localPosition;
        codeAnimator.StartAnimacion(this,
            x =>
            {
                a = alphaCurve.Evaluate(x);
                bodyRender.color = new Color(bodyRender.color.r, bodyRender.color.g, bodyRender.color.b, a);
                faceRender.color = new Color(faceRender.color.r, faceRender.color.g, faceRender.color.b, a);
                transform.localPosition = startPosition + Vector2.up * heightCurve.Evaluate(x);
            }, DeltaTimeType.deltaTime, AnimationType.Simple, null, ()=> Destroy(gameObject), duration);
    }
}
