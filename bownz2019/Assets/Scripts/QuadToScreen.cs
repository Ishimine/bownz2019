﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;


public class QuadToScreen : MonoBehaviour
{
    public Camera camera;

    private void Awake()
    {
        MainCameraManager.OnCameraSizeChange += x => UpdateScale();
    }

    private void OnEnable()
    {
        UpdateScale();
    }

    public void Update()
    {
        UpdatePosition();
    }

    [Button]
    private void UpdateScale()
    {
        transform.localScale = new Vector2(camera.GetHorizontalSize(), camera.orthographicSize*2);
    }

    [Button]
    private void UpdatePosition()
    {
        transform.position = new Vector3(camera.transform.position.x, camera.transform.position.y, transform.position.z);
    }
}
