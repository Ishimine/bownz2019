﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Classic_Terrain_DinamicSpawnArea : MonoBehaviour
{
    [SerializeField] private Transform upperLimit = null;
    [SerializeField] private Transform lowerLimit = null;

    [SerializeField] private BoxCollider2D boxCollider2D = null;

    public float refreshEvery = 1;
    [SerializeField] float edge = .75f;

    IEnumerator rutine;


    private void OnEnable()
    {
        rutine = UpdateRutine();
        StartCoroutine(rutine);
    }

    IEnumerator UpdateRutine()
    {
        do
        {
            boxCollider2D.transform.position = Vector3.Lerp(upperLimit.position, lowerLimit.position, .5f);
            boxCollider2D.size = new Vector2(PlayArea.Area.width- edge, Vector2.Distance(upperLimit.position, lowerLimit.position)- edge);
            yield return new WaitForSecondsRealtime(refreshEvery);
        } while (true);
    }

    private void OnDisable()
    {
        StopCoroutine(rutine);
    }

}
