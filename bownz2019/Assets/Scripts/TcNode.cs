﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class TcNode : MonoBehaviour
{
    [SerializeField] private Transform visualRoot;
    [SerializeField] private SpriteRenderer[] renders;
    [SerializeField] private ParticleSystem pSystem;

    public bool ready = false;

    private float value;
    public float Value
    {
        get => value;
        set
        {
            this.value = value;
            Transform transform1 = transform;
            transform1.localPosition = position + transform1.up * value;
            if (ready && value > 0)
            {
                pSystem.Play();
                ready = false;
            }
        }
    }

    private float alpha;
    public float Alpha
    {
        get => alpha;
        set
        {
            alpha = value; 
            ApplyAlpha(alpha);
        }
    }

    public void ResetNode()
    {
        ready = true;
    }

    public float Scale
    {
        get => transform.localScale.y;
        set
        {
            transform.localScale = new Vector3(1, value, 1);
        }
    }
    
    [SerializeField] private Vector3 position;
    public Vector3 Position
    {
        get => position;
        set
        {
            position = value;
            transform.position = position;
        }
    }

    public void SetParticleScale(float value)
    {
        pSystem.transform.localScale = Vector3.one * value;
    }

    public Vector2 VisualScale
    {
        get => visualRoot.localScale;
        set
        {
            visualRoot.localScale = value;
            SetParticleScale(value.y);
        }
    }

    private void OnDrawGizmos()
    {
        Transform transform1 = transform;
        DrawArrow.ForDebug(transform1.position, transform1.up);
    }

    public void ApplyAlpha(float amount)
    {
        foreach (SpriteRenderer render in renders)
        {
            Color color = render.color;
            color = new Color(color.r,color.g,color.b, amount);
            render.color = color;
        }
    }

    [Button]
    public void Play()
    {   
        pSystem.Play();
    }
}