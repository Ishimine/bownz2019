﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;
using Newtonsoft.Json;

[CreateAssetMenu(menuName =("PlayerProgress/Inventory"))]
public class Inventory : ScriptableSingleton<Inventory>
{

    private string fileName = "Inventory.savedata";
    public override Inventory Myself => this;

    public enum IDs
    {
        Coin,
        Skin,
        Trail,
        Coin_Star,
        GiftBox,
        DailyReward_Level
    };

    public static Action OnInventoyChange;

   /* private static Action<string, int, int> onElementChange;
    public static Action<string, int, int> OnElementChange
    {
        get { return onElementChange; }
        set { onElementChange = value; }
    }*/

    [ShowInInspector, ReadOnly] private Dictionary<string, DoubleInt> elements = new Dictionary<string, DoubleInt>();
    public Dictionary<string, DoubleInt> Elements
    {
        get { return elements; }    
    }
/*
    private void BroadcastChange(string key, int startValue, int endValue)
    {
        Debug.Log($"Inventory Key:{key} {startValue} => {endValue}");
        OnElementChange?.Invoke(key,startValue,endValue);

        if (SpriteLibrary.ContainsKey(key))
            UI_Messenger.Show(new ValueChangeMsg(MsgPosition.Top, 1.5f, startValue, endValue, "0.", SpriteLibrary.Get(key)));
    }*/


    public static int Get(IDs key)
    {
        return Instance._Get(key);
    }

    public int _Get(IDs key)
    {
        return _Get(key.ToString());
    }

    public static int Get(string key)
    {
        return Instance._Get(key);
    }

    public  int _Get(string key)
    {
        if (elements.ContainsKey(key))
        {
            return Instance.elements[key].GetValue();
        }
        else
            return 0;
    }

    public static void Set(string key, int value, bool save)
    {
         Instance._Set( key,  new DoubleInt(0,value), save);
    }

    public void _Set(string key, DoubleInt value, bool save)
    {
        int startValue = 0;

        if (elements.ContainsKey(key))
        {
            startValue = elements[key].GetValue();
            elements[key] = value;
        }
        else
            elements.Add(key, value);
        
        OnInventoyChange?.Invoke();
        //BroadcastChange(key, startValue,  value.GetValue());
        if(save) Save();
    }

    private void Save()
    {
        SaveToDisk();
        if(Settings.UseFirebaseDatabase)
            SaveToDatabase();
    }

    public static void Add(string key, int value, bool save)
    {
        Instance._Add(key, value,save);
    }

    public static void Add(IDs key, int value, bool save)
    {
        Instance._Add(key, value,save);
    }
    [Button]
    public void _Add(IDs key, int value, bool save)
    {
        _Add(key.ToString(), value, save);
    }

    public void AddMoney(int value, bool save)
    {
        _Add(IDs.Coin.ToString(), value, save);
    }

    [Button]
    public void _Add(string key, int value, bool save)
    {
        if (elements.ContainsKey(key))
                _Set(key, new DoubleInt(elements[key].x,elements[key].y + value), save);
        else    _Set(key, new DoubleInt(0,value), save);
        
        AnalyticsManager.SendEvent_EarnCurrency(key,value);
    }

    public static void Consume(IDs key, int ammount, bool save)
    {
        Consume(key.ToString(), ammount,save);
    }

    public static void Consume(string key, int ammount, bool save)
    {
        Instance._Consume(key,  ammount, save);
    }

    public void _Consume(string key, int ammount, bool save)
    {
        _Set(key, new DoubleInt(elements[key].x + ammount, elements[key].y), save);
        AnalyticsManager.SendEvent_SpendCurrency(key, ammount);
    }
  
    [Button]
    public void _Consume(IDs key , int ammount, bool save)
    {
        _Consume(key.ToString(), ammount,save);
    }
            
    public static bool Contains(IDs key)
    {
        return Instance._Contains(key.ToString());
    }

    public static bool Contains(string key)
    {
        return Instance._Contains(key);
    }

    public bool _Contains(string key)
    {
        return elements.ContainsKey(key);
    }

    [Button]
    public void LoadFromDisk()
    {
        Clear();
        string jsonData = JsonConvert.SerializeObject(elements);
        AdministradorDeDatos.CargarDatos(ref jsonData, fileName);
        elements = JsonConvert.DeserializeObject<Dictionary<string,DoubleInt>>(jsonData);
    }

    public static void SaveToDisk()
    {
        Instance._SaveToDisk();
    }
    
    [Button]
    public void _SaveToDisk()
    {
        string jsonData = JsonConvert.SerializeObject(elements);
        AdministradorDeDatos.GuardarDatos(ref jsonData, fileName);
    }

    public static int[] GetAllOwnedTrailIndexes()
    {
        var keys = Instance.elements.Keys;
        List<int> skinsIndexes = new List<int>();
        foreach (string item in keys)
        {
            if (item.Contains(ProductType.Trail.ToString()))
                skinsIndexes.Add(int.Parse(item.Remove(0, 5)));
        }
        return skinsIndexes.ToArray();
    }

    public static int GetRandomOwnedTrailIndex()
    {
        int[] skinsIndexes = GetAllOwnedTrailIndexes();
        return (skinsIndexes.Length == 0) ? 0 : skinsIndexes.GetRandom();
    }

    public static int[] GetAllOwnedSkinsIndexes()
    {
        var keys = Instance.elements.Keys;
        List<int> skinsIndexes = new List<int>();
        foreach (string item in keys)
        {
            if (item.Contains("Skin"))
                skinsIndexes.Add(int.Parse(item.Remove(0, 4)));
        }
        return skinsIndexes.ToArray();
    }

    public override void InitializeSingleton()
    {
        base.InitializeSingleton();
        LoadFromDisk();
        if(Settings.UseFirebaseDatabase)
            LoadFromDatabase();
        
        FirebaseStarter.OnSignStateChange += OnSignStateChange;
    }

    private void OnSignStateChange(bool obj)
    {
        Debug.Log("OnSignStateChange: " + obj);
        if (obj)
        {
            LoadFromDatabase();
        }
    }
  
    [Button]
    public void Clear()
    {
        elements.Clear();
        Add(IDs.Skin, 1, false);
        Add(IDs.Trail, 1, false);
    }

    public string txt;

    [Button]
    public string SaveToJson()
    {
        txt = JsonConvert.SerializeObject(elements);
        Debug.Log(txt);
        return txt;
    }
    
    [Button]
    public void LoadFromJson()
    {
        elements = JsonConvert.DeserializeObject<Dictionary<string, DoubleInt>>(txt);
    }

    [Button()]
    public void LoadFromDatabase()
    {
        FirebaseStarter.LoadDataFromDatabase("Inventory", OnAsyncLoadFromDatabase);
    }

    private void OnAsyncLoadFromDatabase(bool value, string data)
    {
        if (value)
        {
            elements = JsonConvert.DeserializeObject<Dictionary<string, DoubleInt>>(data);
            Debug.Log("Load Inventory From Database Complete");
            OnInventoyChange?.Invoke();
        }
        else
        {
            Debug.Log("Load Inventory From Database failed");
        }
    }

    [Button()]
    public void SaveToDatabase()
    {
        FirebaseStarter.SaveDataAsJson_WithTransaction("Inventory", SaveToJson(), InventoryMerger, OnDatabaseTransaction);
    }

    private string InventoryMerger(string cloudData, string localData)
    {
        if (cloudData == string.Empty || localData == string.Empty )
            return "";

        Dictionary<string, DoubleInt> local  = JsonConvert.DeserializeObject<Dictionary<string, DoubleInt>>(cloudData);
        Dictionary<string, DoubleInt> cloud  = JsonConvert.DeserializeObject<Dictionary<string, DoubleInt>>(localData);

        local = MergeInventory(local, cloud);
        return JsonConvert.SerializeObject(local);
    }

    private void OnDatabaseTransaction(bool sucess)
    {
        Debug.Log("Inventory Transaction:" + sucess);
    }
    
    private Dictionary<string,DoubleInt> MergeInventory(Dictionary<string,DoubleInt> localData, Dictionary<string,DoubleInt> cloudData)
    {
        //Debug.Log("↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓     ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓ MergeInventory ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓    ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓");
        ///Siempre guardamos el valor mas alto
        foreach (KeyValuePair<string,DoubleInt> item in cloudData)
        {
            if (localData.ContainsKey(item.Key))
            {
                localData[item.Key] = SyncItemAmmount(localData[item.Key], cloudData[item.Key], PlayerPrefs.GetInt("WasReset", 1) == 1);
                PlayerPrefs.SetInt("WasReset", 0);
            }
            else localData.Add(item.Key, item.Value);
        }
        //Debug.Log("↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑     ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑ MergeInventory ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑    ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑");
        return localData;
    }
     
     public DoubleInt SyncItemAmmount(DoubleInt local, DoubleInt cloud, bool wasReset)
     {
         Debug.Log("<color=pink> =============      ↓↓↓↓↓ Sync ↓↓↓↓↓      ============= </color>");
         DoubleInt aux = local;
         if (wasReset)
         {
             int dif = Mathf.Clamp((local.y - local.x),0,int.MaxValue);
             aux.x = cloud.x;
             aux.y = cloud.y + dif;
         }
         else
         {
             aux.x = Mathf.Max(local.x, cloud.x);
             aux.y = Mathf.Max(local.y, cloud.y);
         }
         //aux.Clamp(Vector2Int.zero, Vector2Int.one * int.MaxValue);
         Debug.Log("<color=pink> =============      ↑↑↑↑↑ Sync ↑↑↑↑↑      ============= </color>");
         return aux;
     }
}

[System.Serializable]
public struct DoubleInt
{
    public int x;
    public int y;
    
    public int this[int index]
    {
        get
        {
            if (index == 0)
                return x;
            if (index == 1)
                return y;
            else
                throw new Exception("Out of bounds");
        }
    }

    public DoubleInt(int x, int y)
    {
        this.x = x;
        this.y = y;
    }
    
    public int GetValue()
    {
        return y - x;
    }
    
}
