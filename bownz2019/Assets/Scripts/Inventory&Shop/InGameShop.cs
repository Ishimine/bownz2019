﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;

public class InGameShop : MonoBehaviour 
{
    [ShowInInspector,ReadOnly] private int currentPage;
    public int CurrentPage
    {
        get
        {
            return currentPage;
        }
        protected set
        {
            currentPage = (int)Mathf.Repeat(value, pagesCount);
            OnPageChanged?.Invoke(currentPage);
            RefreshPage();
        }
    }

    public Transform displayPosition;

    [SerializeField] private int pagesCount = 1;

    
    private ProductDisplay activeDisplay;
    private ProductDisplay selectedProductDisplay;

    private Action<int> onPageChanged;
    public Action<int> OnPageChanged
    {
        get { return onPageChanged; }
        set { onPageChanged = value; }
    }

    public ProductShelf[] shelfs;

    private int totalCapacity = 0;

    [SerializeField] private Product[] products;

    private void Awake()
    {
        Initialize();
    }

    private void Start()
    {
        CancelSelection();
    }

    private void OnEnable()
    {
        this.AddObserver(BuyButtonPressed_Response, Notifications.Shop_BuyButtonPressed);
        this.AddObserver(EquipButtonPressed_Response, Notifications.Shop_EquipButtonPressed);
    }

    private void OnDisable()
    {
        this.RemoveObserver(BuyButtonPressed_Response, Notifications.Shop_BuyButtonPressed);
        this.RemoveObserver(EquipButtonPressed_Response, Notifications.Shop_EquipButtonPressed);
    }

    private void BuyButtonPressed_Response(object arg1, object arg2)
    {
        Shop.TryBuy(selectedProductDisplay.Product);
    }

    private void EquipButtonPressed_Response(object arg1, object arg2)
    {
        selectedProductDisplay.Equip();
    }

    public void SetSelectedProduct(ProductDisplay selectedProductDisplay)
    {
        CancelSelection();

        this.selectedProductDisplay = selectedProductDisplay;

        activeDisplay = Instantiate(selectedProductDisplay.Product.GetDisplayPrefab(), transform).GetComponent<ProductDisplay>();

        activeDisplay.Size = 3;
        activeDisplay.PositionAt(displayPosition.position, Vector2.up);

        activeDisplay.ShowNameTag = Shop.CanBuy;
        activeDisplay.ShowPriceTag = false;

        activeDisplay.gameObject.SetActive(true);
        activeDisplay.OnDisplayDone -= OnDisplayDone_Response;
        activeDisplay.OnDisplayDone += OnDisplayDone_Response;
        activeDisplay.SetActive(true);
        activeDisplay.Display(selectedProductDisplay.Product);
       
    }

    private void OnDisplayDone_Response()
    {
        Debug.Log("OnDisplayDone_Response");
        this.PostNotification(Notifications.Shop_CanSelect,true);
    }

    public void CancelSelection()
    {
        if (activeDisplay != null)
        {
            selectedProductDisplay.Unselected();
            activeDisplay.SetActive(false);
            activeDisplay.OnDisplayDone -= OnDisplayDone_Response;
            activeDisplay.gameObject.SetActive(false);
            activeDisplay = null;
        }
    }

    [Button]
    private void Initialize()
    {
        Debug.Log(this + "Initialize");
        totalCapacity = 0;
        foreach (var item in shelfs)
            totalCapacity += item.Capacity;
        LoadProducts();
    }

    private void LoadProducts()
    {
        products = Shop.GetDefaultProducts();
        pagesCount = Mathf.CeilToInt((float)products.Length / totalCapacity);
    }

    [Button, ButtonGroup] public void FirstPage()         { CurrentPage = 0; }
    [Button, ButtonGroup] public void PreviousPage()      { CurrentPage--; }
    [Button, ButtonGroup] public void NextPage()          { CurrentPage++; }
    [Button, ButtonGroup] public void LastPage()          { CurrentPage = pagesCount - 1; }

    [Button] private void SetPage(int index) { CurrentPage = index; }

    private void RefreshPage()
    {
        CancelSelection();
        int minIndex = currentPage * totalCapacity;
        Product[] currentShelfProducts;
        int currentLenght;
        for (int i = 0; i < shelfs.Length; i++)
        {
            shelfs[i].ClearDisplay();
            if (minIndex < products.Length)
            {
                currentLenght = Mathf.Min(products.Length - minIndex, shelfs[i].Capacity);
                currentShelfProducts = new Product[currentLenght];
                Array.Copy(products, minIndex, currentShelfProducts, 0, currentLenght);
                shelfs[i].DisplayProducts(currentShelfProducts);
                minIndex += shelfs[i].Capacity;
            }
        }
    }
}
