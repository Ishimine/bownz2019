﻿using System;
using System.Collections;
using System.Collections.Generic;
using Pooling.Poolers;
using UnityEngine;
using Sirenix.OdinInspector;

public abstract class ProductShelf : MonoBehaviour
{
   [ShowInInspector] private List<ProductDisplay> productDisplays = new List<ProductDisplay>();

    public abstract int Capacity { get; }

    public GameObject[] poolObjects_Prefabs;

    [ShowInInspector] GenericMonoPools<ProductDisplay> pools;

    private void Awake()
    {
        pools = new GenericMonoPools<ProductDisplay>(transform);
        foreach (GameObject item in poolObjects_Prefabs)
        {
            pools.CreatePool(item.GetComponent<ProductDisplay>().GetType(), item);
        }
    }

    public void DisplayProducts(Product[] products)
    {
        ApplyProductsToDisplay(products);
    }

    [Button]
    public void ApplyProductsToDisplay(Product[] products)
    {
        ProductDisplay productDisplay = null;
        for (int i = 0; i < products.Length; i++)
        {
            productDisplay = GetProductDisplay(ref products[i]);
            productDisplay.gameObject.name = i + ": " + productDisplay.GetType().ToString();
            productDisplay.Display(products[i]);
            productDisplays.Add(productDisplay);
            productDisplay.ShowPriceTag = (Inventory.Get(productDisplay.Product.PrimaryId)) == 0;
            PositionProduct(productDisplay, i);
        }
    }

    private ProductDisplay GetProductDisplay(ref Product product)
    {
        GameObject displayPrefab = product.GetDisplayPrefab();                      //Pedimos el prefab
        ProductDisplay productDisplay = pools.Dequeue(displayPrefab.GetComponent<ProductDisplay>().GetType()) as ProductDisplay; //Le pedimos al pool un objeto acorde al tipo

        //Configuramos estado inicial        
        productDisplay.ShowNameTag = false;
        productDisplay.transform.SetParent(transform);                           
        productDisplay.gameObject.SetActive(true);  
        productDisplay.SetActive(true);             
        return productDisplay;
    }

    protected abstract void PositionProduct(ProductDisplay products, int index);

    [Button]
    public void ClearDisplay()
    {
        foreach (ProductDisplay item in productDisplays)
        {
            pools.Enqueue(item, item.GetType());
            item.SetActive(false);
        }
        productDisplays.Clear();
    }
}

[System.Serializable]
public class GenericMonoPools<T> where T : MonoBehaviour
{
    [ShowInInspector,ReadOnly] private readonly Transform root;
    [ShowInInspector, ReadOnly] Dictionary<String, SetPooler> pools = new Dictionary<String, SetPooler>();

    public GenericMonoPools(Transform root)
    {
        this.root = root;
        pools = new Dictionary<String, SetPooler>();
    }

    public Component Dequeue(Type type)
    {
        return GetPool(type).DequeueScript(type);
    }

    public O Dequeue<O>() where O : T
    {
        return GetPool<O>().DequeueScript<O>();
    }

    public void Enqueue<O>(O item, Type type) where O : T
    {
        GetPool(type).EnqueueScript(item.GetComponent<Poolable>());
    }

    public void Enqueue<O>(O item) where O : T
    {
        GetPool<O>().EnqueueScript(item.GetComponent<Poolable>());
    }

    private SetPooler GetPool(Type key)
    {
        return pools[key.ToString()];
    }

    private SetPooler GetPool<O>()
    {
        Type type = typeof(O);
        return GetPool(type);
    }

    public void CreatePool(Type key, GameObject prefab)
    {
        if (pools.ContainsKey(key.ToString())) return;

        SetPooler nPool =  new GameObject().AddComponent<SetPooler>();
        nPool.prefab = prefab;
        nPool.transform.SetParent(root);
        pools.Add(key.ToString(), nPool);
    }

    public void AddPool(Type key, SetPooler pool)
    {
        pool.transform.name = "Pool of " + key.ToString();
        pools.Add(key.ToString(), pool);
    }
}