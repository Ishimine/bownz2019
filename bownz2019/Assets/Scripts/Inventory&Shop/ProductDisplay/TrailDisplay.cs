﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailDisplay : ProductDisplay
{
    public GameObject trail_Prefab;
    public GameObject currentTrail = null;
    public Transform root;

    public override object Item => currentTrail;

    private void SetTrailPrefab(GameObject prefab)
    {
        trail_Prefab = prefab;
        if (currentTrail != null) Destroy(currentTrail);
        currentTrail = Instantiate(trail_Prefab, root.transform);
        currentTrail.transform.localPosition = Vector3.right * .65f * Size;
        currentTrail.transform.localScale = Vector3.one * (Size / 2);
    }

    public override void Equip()
    {
        base.Equip();
        TrailBall.SetPlayerSelection(product.PrimaryId);
    }

    public override void Selected()
    {
        if(currentTrail != null)  currentTrail.SetActive(false);
    }

    public override void Unselected()
    {
        if (currentTrail != null) currentTrail.SetActive(true);
    }

    public override void _Disable()
    {
        if (currentTrail != null) currentTrail.SetActive(false);
    }

    public override void _Enable()
    {
        if (currentTrail != null) currentTrail.SetActive(true);
    }


    protected override void _Display()
    {
        SetTrailPrefab(GameObjectLibrary.Get(product.PrimaryId));
        OnDisplayDone?.Invoke();
    }
}
