﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class ProductDisplay : MonoBehaviour
{
    public Collider2D buttonCollider;

          
    public  abstract object Item { get; }

    private Action<bool> onActiveChangeDone;
    public Action<bool> OnActiveChangeDone
    {
        get { return onActiveChangeDone; }
        set { onActiveChangeDone = value; }
    }

    private Action onDisplayDone;
    public Action OnDisplayDone
    {
        get { return onDisplayDone; }
        set { onDisplayDone = value; }
    }

    private float size = 1;
    public float Size
    {
        get { return size; }
        set
        {
            size = value;
            buttonCollider.transform.localScale = Vector3.one * value;
            tagsRoot.transform.localScale = buttonCollider.transform.localScale;
        }
    }

    private bool active = false;
    public bool Active
    {
        get { return active; }
    }

    public TextMesh nameTagText;
    public TextMesh priceTagText;

    public Transform priceTagConteiner;
    public Transform nameTagConteiner;

    public Transform tagsRoot;

    protected Product product;
    public Product Product => product;

    private bool isButton = true;
    public bool IsButton
    {
        get
        {
            return isButton;
        }
        set
        {
            isButton = value;
            buttonCollider.enabled = value;
        }
    }

    private bool showPriceTag = true;
    public bool ShowPriceTag
    {
        get { return showPriceTag; }
        set
        {
            showPriceTag = value;
            priceTagConteiner.gameObject.SetActive(value);
        }
    }

    private bool showNameTag = true;
    public bool ShowNameTag
    {
        get { return showNameTag; }
        set
        {
            showNameTag = value;
            nameTagConteiner.gameObject.SetActive(value);
        }
    }



    public void Display(Product product)
    {
        this.product = product;
        gameObject.SetActive(true);
        priceTagText.text = (Shop.CanBuy)?product.GetPriceTag():"LOCK";
        nameTagText.text = product.DisplayText;
        _Display();
    }

    protected abstract void _Display();

    public abstract void Selected();
    public abstract void Unselected();

    public virtual void Equip() { }

    protected virtual void OnEnable()
    {
    }

    public void SetActive(bool value)
    {
        if (active == value) return;

        active = value;

        if (value)
            _Enable();
        else
            _Disable();
    }

    public abstract void _Disable();
    public abstract void _Enable();

    /// <summary>
    /// 
    /// </summary>
    /// <param name="position">Position</param>
    /// <param name="pivotPosition">Pivot direction (normalized) </param>
    public virtual void PositionAt(Vector2 position, Vector2 pivotPosition)
    {
        transform.position = position + pivotPosition.normalized * size/2;
        tagsRoot.position = transform.position;
    }
}
