﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteDisplay : ProductDisplay
{
    public Sprite icon;
    public SpriteRenderer spriteRenderer;


    public override object Item => icon;

    protected override void _Display()
    {
        icon = SpriteLibrary.Get(product.SecondaryId);
        OnDisplayDone?.Invoke();
    }

    protected override void OnEnable()
    {
        base.OnEnable();
    }

    public override void _Disable()
    {
        spriteRenderer.gameObject.SetActive(false);
        OnActiveChangeDone?.Invoke(false);
    }

    public override void _Enable()
    {
        spriteRenderer.gameObject.SetActive(true);
        OnActiveChangeDone?.Invoke(true);
    }

    public override void Selected()
    {
        spriteRenderer.color = new Color(1, 1, 1, 1.5f);
    }

    public override void Unselected()
    {
        spriteRenderer.color = new Color(1, 1, 1, 1);
    }
}
