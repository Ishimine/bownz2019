﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 
/// </summary>
public class SkinDisplay : ProductDisplay
{
    public BallSkin skin;
    public BallStateMachine ball;

   public override object Item => skin;


    protected override void _Display()
    {
        int id = int.Parse(product.SecondaryId);
        skin = Body_Conteiner.GetSkin(id);
        ball.SkinComponent.UsePlayerSelection = false;
        ball.ballSize = Size;
        ball.TransitionToSkin(skin, ()=> OnDisplayDone?.Invoke());
        ball.TrailComponent.SetDefaultTrail();
    }

    public override void Equip()
    {
        base.Equip();
        BallSkin.SetPlayerSkin(skin);
    }

    public override void PositionAt(Vector2 position, Vector2 pivotPosition)
    {
        base.PositionAt(position, pivotPosition);
        ball.transform.localPosition = Vector3.zero;
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        ball.ballSize = Size;
    }

    public override void _Disable()
    {
        ball.SwitchState(new BallStates.Disable(ball));
        OnActiveChangeDone?.Invoke(false);
    }

    public override void _Enable()
    {
        ball.AppearToPosing(()=> OnActiveChangeDone?.Invoke(true));
    }

    public override void Selected()
    {
        ball.Disappear();
    }

    public override void Unselected()
    {
        ball.AppearToPosing();
    }
}
