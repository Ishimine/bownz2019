﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu(menuName =("PlayerProgress/Shop"))]
public class Shop : ScriptableSingleton<Shop>
{
    public override Shop Myself => this;

    [SerializeField] private List<Product> availableProducts;
    public List<Product> AvailableProducts
    {
        get { return availableProducts; }
        set { availableProducts = value; }
    }

    [SerializeField] private bool canBuy = false;
    public static bool CanBuy => Instance.canBuy;

    public static Product[] GetDefaultProducts()
    {
        return Instance.availableProducts.ToArray();
    }

    [Button]
    public void CreateAllSkinProducts(Body_Conteiner body_Conteiner)
    {
        for (int i = 0; i < body_Conteiner.bodies.Length; i++)
        {
            CreateSkinProduct(body_Conteiner,i);
        }
    }

    [Button]
    public void CreateTrails(int count)
    {
        for (int i = 0; i < count; i++)
        {
            AddProduct(Product.CreateTrailProduct(i.ToString(),i,(i+1)*10));
        }
    }

    [Button]
    public void CreateSkinProduct(Body_Conteiner body_Conteiner, int skinIndex)
    {
        Product product = Product.CreateSkinProduct(body_Conteiner.Skins[skinIndex].Name, skinIndex, body_Conteiner.Skins[skinIndex].Price);
        AddProduct(product);
    }

    [Button]
    private void RemoveSkinProduct(int skinIndex)
    {
        RemoveProduct(ProductType.Skin.ToString() + skinIndex);
    }

    [Button]
    private void AddProduct(Product product)
    {
        RemoveProduct(product.PrimaryId);
        availableProducts.Add(product);
#if UNITY_EDITOR
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
#endif
    }

    [Button]
    private void RemoveProduct(string PrimaryId)
    {
        var existentProduct = availableProducts.Find(x => x.PrimaryId == PrimaryId);
        if (existentProduct != null) availableProducts.Remove(existentProduct);
    }

    public static bool TryBuy(Product product)
    {
        if (product.Price <= Inventory.Get(Inventory.IDs.Coin))
        {
            if(product.Price > 0)
                Inventory.Consume(Inventory.IDs.Coin, product.Price, false);

            if(product.IsAccumulative)
                Inventory.Add(product.PrimaryId, 1,true);
            else
                Inventory.Set(product.PrimaryId, 1,true);
            return true;
        }
        else
        {
            Debug.Log("Not Enough Money, Honey D:");
        }
        return false;
    }

    public static bool IsBuyable(Product product)
    {
        if(Instance.availableProducts.Contains(product))
        {
            if (product.IsAccumulative || Inventory.Get(product.PrimaryId) == 0)
                return true;
        }
        return false;
    }
}

[System.Serializable]
public class Product
{
    public string DisplayText;
    public string SecondaryId;
    public int Price;
    public bool IsAccumulative = false;
       

    public string PrimaryId => Type.ToString() + SecondaryId.ToString();

    public ProductType Type;

    public Product(string displayText, string id, int cost, ProductType type)
    {
        DisplayText = displayText;
        SecondaryId = id;
        Price = cost;
        Type = type;
    }

    public string GetPriceTag()
    {
        return(Price == 0) ? "FREE" : "$"+Price.ToString();
    }

    public static Product CreateTrailProduct(string displayText, int skinIndex, int cost)
    {
        return new Product(displayText, skinIndex.ToString(), cost, ProductType.Trail);
    }

    public static Product CreateSkinProduct(string displayText, int skinIndex, int cost)
    {
        return new Product(displayText, skinIndex.ToString(), cost, ProductType.Skin);
    }

    public static Product CreateProduct(string displayText, string productId, int cost)
    {
        return new Product(displayText, productId, cost, ProductType.Normal);
    }

    public override string ToString()
    {
        return string.Format("FullID: {0} | ID: {1} | DisplayText: {2} | Cost{3} ", PrimaryId, SecondaryId,DisplayText, Price.ToString());
    }

    public GameObject GetDisplayPrefab()
    {
        switch (Type)
        {
            case ProductType.Skin:
                return GameObjectLibrary.Get("ProductDisplay_Skin");
            case ProductType.Trail:
                return GameObjectLibrary.Get("ProductDisplay_Trail");
            case ProductType.Normal:
            default:
                return GameObjectLibrary.Get("ProductDisplay_Sprite");
        }
    }
}

public enum ProductType { Normal, Skin, Trail }

