﻿using Lean.Touch;
using Sirenix.OdinInspector;
using UnityEngine;

public class Ball_Animation : MonoStateMachine
{

    [TabGroup("Dependencies"),SerializeField] public SpriteRenderer rBody;
    [TabGroup("Dependencies"),SerializeField] public SpriteRenderer rFace;
    [TabGroup("Dependencies"),SerializeField] public Face_Conteiner faces;

    [TabGroup("Animation"), SerializeField] private float faceSmoothTime = .2f;
    [TabGroup("Animation"), SerializeField] public float AngularSpeedToGetDizzy =  20;
    [TabGroup("Animation"), SerializeField] public Vector2 dizzyDuration = new Vector2(1,3);

    [TabGroup("Animation"), SerializeField] public Vector2 timeBetweenRandomPosingAnimations = new Vector2(2,7);

    [TabGroup("Animation"), SerializeField] public float MaxFaceDisplacementDistance = .5f;
    /// <summary>
    /// Determina la distancia maxima a la cual el desplazamiento de la cara sera el MAXIMO
    /// </summary>
    [TabGroup("Animation"), SerializeField] public float maxTargetDistance = 7;

    private Vector2 faceVel;
   [ShowInInspector,ReadOnly] private Vector2 targetFaceLocalPosition;
    public Vector2 TargetFaceLocalPosition => targetFaceLocalPosition;

    private Rigidbody2D rb;
    public Rigidbody2D Rb => rb;

    public CodeAnimator codeAnimator = new CodeAnimator();

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void OnEnable()
    {
        this.AddObserver(OnFingerDown, Notifications.OnFingerDown);
        this.AddObserver(OnFingerSet, Notifications.OnFingerSet);
        this.AddObserver(OnFingerUp, Notifications.OnFingerUp);
    }

    private void OnDisable()
    {
        this.RemoveObserver(OnFingerDown, Notifications.OnFingerDown);
        this.RemoveObserver(OnFingerSet, Notifications.OnFingerSet);
        this.RemoveObserver(OnFingerUp, Notifications.OnFingerUp);

        codeAnimator.Stop(this);

        SwitchState(new BallAnimation.Dissable(this));
    }

    public bool ignoringFinger = false;

    private void OnFingerDown(object arg1, object arg2)
    {
        if (!ignoringFinger)
        {
            ignoringFinger = Random.Range(0f, 1f) < .65f;
            if(!ignoringFinger)
                ((BallAnimation.BallAnimationState)CurrentState)?.OnFingerSet((LeanFinger)arg2);
        }
    }

    private void OnFingerSet(object arg1, object arg2)
    {
        if (ignoringFinger) return;
        else
        {
            ignoringFinger = Random.Range(0f, 1f) < .01f;
            if (!ignoringFinger)
            {
                ((BallAnimation.BallAnimationState)CurrentState)?.OnFingerSet((LeanFinger)arg2);
            }
            else
            {
//                Debug.LogWarning("Ignoring");
                StopLooking();
            }
        }
    }

    private void OnFingerUp(object arg1, object arg2)
    {
        if(ignoringFinger)
            ignoringFinger = false;
        else
            ((BallAnimation.BallAnimationState)CurrentState)?.OnFingerUp((LeanFinger)arg2);
    }

    [Button]
    public void Dissable()
    {
        SwitchState(new BallAnimation.Dissable(this));
    }

    [Button]
    public void Posing()
    {
        Dissable();
        SwitchState(new BallAnimation.Posing(this));
    }

    [Button]
    public void Playing()
    {
        SwitchState(new BallAnimation.Playing(this));
    }

    public void Sad()
    {
        SetFace(faces.Sad.GetRandom());
        codeAnimator.StartWaitAndExecute(this, Random.Range(.4f, 1),()=> SetFace(faces.Idle.GetRandom()), false);
    }

    [Button]
    public void Dead()
    {
        SwitchState(new BallAnimation.Dead(this));
    }

    public void StopLooking()
    {
        targetFaceLocalPosition = Vector2.zero;
    }

    public void SetTargetLookFromLocalPosition(Vector2 targetPosition)
    {
        targetFaceLocalPosition = targetPosition;
    }

    public void SetTargetLookFromWorldPosition(Vector2 targetPosition)
    {
        targetPosition = transform.InverseTransformPoint(targetPosition);
        targetFaceLocalPosition = targetPosition.normalized * (Mathf.Lerp(0, MaxFaceDisplacementDistance, targetPosition.sqrMagnitude / maxTargetDistance));
    }

    private void Update()
    {
        rFace.transform.localPosition = Vector2.SmoothDamp(rFace.transform.localPosition, targetFaceLocalPosition, ref faceVel, faceSmoothTime);
    }

    private void FixedUpdate()
    {
        ((BallAnimation.BallAnimationState)CurrentState)?.FixedUpdate();
    }

    public void LookingAtTargetIn(LookTarget lookTarget)
    {
        switch (lookTarget.reaction)
        {
            case LookTarget.LookReaction.Happy:
                SetFace(faces.Happy.GetRandom());
                break;
            case LookTarget.LookReaction.Fear:
                SetFace(faces.Fear.GetRandom());
                break;
            case LookTarget.LookReaction.Angry:
                SetFace(faces.Angry.GetRandom());
                break;
        }
    }

    public void LookingAtTargetStay(LookTarget lookTarget)
    {
        SetTargetLookFromWorldPosition(lookTarget.gameObject.transform.position);
    }


    public void SetFace(Sprite sprite)
    {
        if (sprite == null)
            sprite = faces.DefaultFace;
        rFace.sprite = sprite;
    }

    
   

}

namespace BallAnimation
{
    public class BallAnimationState : State
    {
        protected Ball_Animation Owner;
        protected Face_Conteiner Faces => Owner.faces;
        protected CodeAnimator CodeAnimator => Owner.codeAnimator;


        public BallAnimationState(Ball_Animation owner)
        {
            Owner = owner;
        }

        protected void SwitchState(BallAnimationState nState)
        {
            Owner.SwitchState(nState);
        }

        public void SetFace(Sprite sprite)
        {
            Owner.SetFace(sprite);
        }

        public virtual void OnFingerDown(LeanFinger leanFinger)
        {
        }

        public virtual void OnFingerSet(LeanFinger leanFinger)
        {
        }

        public virtual void OnFingerUp(LeanFinger leanFinger)
        {
        }

        public virtual void FixedUpdate()
        {

        }
    }

    public class Posing : BallAnimationState
    {
        public Posing(Ball_Animation owner) : base(owner)
        {
        }

        public override void Enter()
        {
            base.Enter();
            RandomAnimation();
        }

        private void RandomAnimation()
        {
            int random = UnityEngine.Random.Range(0, 8);
            if (Owner.TargetFaceLocalPosition.sqrMagnitude < .05f)
            {
                switch (random)
                {
                    case 0:
                    case 1:
                        Blink();
                        break;
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                        SetFace(Faces.DefaultFace);
                        RecursiveLookRandomDirection();
                        break;
                    case 6:
                    case 7:
                        SetFace(Faces.Happy.GetRandom());
                        CodeAnimator.StartWaitAndExecute(
                                  Owner,
                                  UnityEngine.Random.Range(.25f, .7f),
                                  ()=>
                                  {
                                      SetFace(Faces.DefaultFace);
                                      Skipped();
                                  },
                                  false);
                        break;
                    case 8:
                        Skipped();
                        break;
                }
            }
            else Skipped();

//            Debug.Log(random + " " +Owner.transform.parent.parent);
        }

        private void RecursiveLookRandomDirection()
        {
            if(Random.Range(0f,1f) < .65f)
            {
                LookRandomDirection();
                CodeAnimator.StartWaitAndExecute(
                   Owner,
                   UnityEngine.Random.Range(.25f, 1.4f),
                   RecursiveLookRandomDirection,
                   false);
            }
            else
            {
                Owner.StopLooking();
                Skipped();
            }
        }

        private void LookRandomDirection()
        {
            Owner.SetTargetLookFromWorldPosition((Vector2)Owner.transform.position + new Vector2(Random.Range(-10, 10), Random.Range(-10, 10) * Owner.transform.localScale.x));
        }

        private void Skipped()
        {
            CodeAnimator.StartWaitAndExecute(
                Owner,
                UnityEngine.Random.Range(Owner.timeBetweenRandomPosingAnimations.x,
                Owner.timeBetweenRandomPosingAnimations.y),
                RandomAnimation,
                false);
        }

        private void Blink()
        {
            SetFace(Faces.BlinkFace);
            CodeAnimator.StartWaitAndExecute(
                  Owner,
                  .25f,
                  ()=>
                  {
                      SetFace(Faces.DefaultFace);
                      Skipped();
                  }
                  ,
                  false);
        }



        public override void OnFingerDown(LeanFinger leanFinger)
        {
            base.OnFingerSet(leanFinger);

            if(Random.Range(0f,1f) < .4f)
                SetFace(Faces.Happy.GetRandom());

        }


        public override void OnFingerSet(LeanFinger leanFinger)
        {
            base.OnFingerSet(leanFinger);
            Owner.SetTargetLookFromWorldPosition(leanFinger.GetLastWorldPosition(-10));
        }

        public override void OnFingerUp(LeanFinger leanFinger)
        {
            base.OnFingerUp(leanFinger);
            Owner.StopLooking();
            RandomAnimation();
        }
    }

    public class Playing : BallAnimationState
    {
        bool isDizzy = false;
        public Playing(Ball_Animation owner) : base(owner)
        {

        }

        public override void FixedUpdate()
        {
            base.FixedUpdate();
//            Debug.Log("Owner.Rb.angularVelocity: " + Owner.Rb.angularVelocity);
            if(!isDizzy && Mathf.Abs(Owner.Rb.angularVelocity) > Owner.AngularSpeedToGetDizzy)
            {
                isDizzy = true;
                SetFace(Faces.Dizzy.GetRandom());
                Owner.codeAnimator.StartWaitAndExecute(
                    Owner,
                    UnityEngine.Random.Range(Owner.dizzyDuration.x, Owner.dizzyDuration.y),
                    ()=>
                    {
                        isDizzy = false;
                        SetFace(Faces.DefaultFace);
                    }, false);
            }
        }

        public override void Exit()
        {
            base.Exit();
            isDizzy = false;
        }

        public override void Enter()
        {
            base.Enter();
            Owner.StopLooking();
            SetFace(Faces.Happy.GetRandom());
            CodeAnimator.StartWaitAndExecute(Owner, .8f, () => SetFace(Faces.DefaultFace), false);
        }

        protected override void AddListeners()
        {
            base.AddListeners();
            this.AddObserver(OnPickedCoin_Response, Notifications.OnCoinPickedUp);
        }

        protected override void RemoveListeners()
        {
            base.RemoveListeners();
            this.RemoveObserver(OnPickedCoin_Response, Notifications.OnCoinPickedUp);
        }

        private void OnPickedCoin_Response(object arg1, object arg2)
        {
            switch ((int)arg2)
            {
                case 0:
                    SetFace(Faces.Happy.GetRandom());
                    break;
                case 1:
                    SetFace(Faces.Love.GetRandom());
                    break;
                case 2:
                    SetFace(Faces.Love.GetRandom());
                    //SetFace(Faces.Surprise.GetRandom());
                    break;
                default:
                    break;
            }
            isDizzy = false;
            CodeAnimator.StartWaitAndExecute(
                Owner, 
                UnityEngine.Random.Range(.7f, 1.5f),
                () => SetFace(Faces.DefaultFace), 
                false);
        }
    }

    public class Dead : BallAnimationState
    {
        public Dead(Ball_Animation owner) : base(owner)
        {
        }
        public override void Enter()
        {
            base.Enter();
            switch (UnityEngine.Random.Range(0,4))
            {
                case 0:
                    SetFace(Faces.Sad.GetRandom());
                    break;
                case 1:
                    SetFace(Faces.Fear.GetRandom());
                    break;
                case 2:
                    SetFace(Faces.Pain.GetRandom());
                    break;
                case 3:
                    SetFace(Faces.Dizzy.GetRandom());
                    break;
                default:
                    break;
            }
        }
    }

    public class Dissable : BallAnimationState
    {
        public Dissable(Ball_Animation owner) : base(owner)
        {
        }
        public override void Enter()
        {
            Owner.StopLooking();
        }
    }
}
