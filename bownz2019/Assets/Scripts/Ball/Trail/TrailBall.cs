﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailBall : MonoBehaviour
{
    public Transform ballRoot;

    public GameObject defaulTrail;

    private static GameObject playerSelected;
    public static GameObject PlayerSelection
    {
        get
        {
            if (playerSelected == null) playerSelected = GameObjectLibrary.Get(PlayerPrefs.GetString("PlayerSelection_Trail", "Trail0"));
            return playerSelected;
        }
    }


    [SerializeField] private GameObject trailPrefab = null;
    private GameObject TrailPrefab
    {
        get
        {
            if (trailPrefab == null) trailPrefab = PlayerSelection;
            return trailPrefab;
        }
    }

    private bool usePlayerSelection = true;
    public bool UsePlayerSelection
    {
        get { return usePlayerSelection; }
        set { usePlayerSelection = value; }
    }

    [SerializeField] private Wrappable wrappable = null;

    private GameObject current;
     
    public void SetTrailPrefab(GameObject trailPrefab)
    {
        if (this.trailPrefab == trailPrefab) return;

        this.trailPrefab = trailPrefab;
        if(current != null)
        {
            DestroyCurrent();
            CreateNew();
        }
    }

    public static void SetPlayerSelection(string trailId)
    {
        playerSelected = GameObjectLibrary.Get(trailId); ;
        PlayerPrefs.SetString("PlayerSelection_Trail", trailId);
    }

    private void OnEnable()
    {
        if (usePlayerSelection)
            SetTrailPrefab(playerSelected);

        wrappable.OnWrapStart -= OnStartResponse;
        wrappable.OnWrapStart += OnStartResponse;

        wrappable.OnWrapEnd -= OnEndResponse;
        wrappable.OnWrapEnd += OnEndResponse;

        if (current == null)
            CreateNew();
    }

    private void OnDisable()
    {
        wrappable.OnWrapEnd -= OnEndResponse;
        wrappable.OnWrapStart -= OnStartResponse;
    }

    private void OnStartResponse(Wrappable obj)
    {
        Debug.Log("OnStartResponse");
        DestroyCurrent();
    }

    private void OnEndResponse(Wrappable obj)
    {
        Debug.Log("OnEndResponse");
        CreateNew();
    }

    public void DestroyCurrent()
    {
        Destroy(current, 3);
        current.transform.SetParent(null);
        current = null;
    }

    public void CreateNew()
    {
        current = Instantiate(TrailPrefab, ballRoot);
    }

    public void Hide()
    {
        current.gameObject.SetActive(false);
    }

    public void SetDefaultTrail()
    {
        SetTrailPrefab(defaulTrail);
    }
}
