﻿using BallStates;
using Sirenix.OdinInspector;
using System;
using UnityEngine;

public class BallStateMachine : MonoStateMachine
{
    public bool useFloatAnimation = true;

    [SerializeField] private Collider2D col = null;                     public Collider2D Col { get => col; }
    [SerializeField] private Rigidbody2D rb = null;                     public Rigidbody2D Rb { get => rb; }
    [SerializeField] private Wrappable wrappable = null;                public Wrappable Wrappable { get => wrappable; }
    [SerializeField] private Health health = null;                      public Health Health { get => health; }
    [SerializeField] private ConstantBounce constantBounce = null;      public ConstantBounce ConstantBounce  => constantBounce;
    [SerializeField] private Ball_Animation ball_Animation = null;      public Ball_Animation Ball_Animation => ball_Animation;
    [SerializeField] private TrailBall trailBall = null;                public TrailBall TrailComponent => trailBall;
    [SerializeField] private BallSkinComponent skinComponent = null;    public BallSkinComponent SkinComponent => skinComponent;

    public float DefaultBallSize => Settings.BallSize;
    public float ballSize = 1;
    public bool useWrap = false;

    private Vector2 defaultStartPosition = Vector2.zero;
    public Vector2 DefaultStartPosition
    {
        get => defaultStartPosition;
        set
        {
            Debug.Log("NEW START POSITION: " + value);
            defaultStartPosition = value;
        }
    }

    public CodeAnimator codeAnimator = new CodeAnimator();

    [TabGroup("Audio")] public SoundPlayGroup sFXs_dead;
    [TabGroup("Audio")] public SoundPlayGroup sFXs_onPickUpCoinVoices;

    [Header("Dead")]
    [TabGroup("Animation")] public float deadJumpHeight = 5;
    [TabGroup("Animation")] public float deadSizeMax = 5;
    [TabGroup("Animation")] public CodeAnimatorCurve deadCurve;


    [Header("Appear")]
    [TabGroup("Animation")] public float appearRotation = 175;
    [TabGroup("Animation")] public CodeAnimatorCurve appearCurve;

    [Header("Disappear")]
    [TabGroup("Animation")] public float disappearRotation = 175;
    [TabGroup("Animation")] public CodeAnimatorCurve disappearCurve;

    [Header("Waiting")]
    [TabGroup("Animation")] public float waitingHeight = 3;
    [TabGroup("Animation")] public float waitingVelocity = 1;
    [TabGroup("Animation")] public AnimationCurve waitingCurve;

    private void Awake()
    {
        SwitchState(new Awaking(this));
    }

    [Button]
    private void ChangeToPlaying()
    {
        SwitchState(new Living(this));
    }

    [Button]
    public void ChangeToPosing()
    {
        SwitchState(new Posing(this));
    }

    [Button]
    public void AppearToLiving(Action postAction = null)
    {
        transform.localScale = Vector3.zero;
        SwitchState(new Appearing(this,()=> SwitchState(new Living(this, postAction)), null));
    }

    [Button]
    public void AppearToWaiting(Action postAction = null)
    {
        rb.simulated = false;
        transform.localScale = Vector3.zero;

        SwitchState(new Appearing(this, () =>
        {
            postAction?.Invoke();
            SwitchState(new Waiting(this));
        }, null
        ));
    }

    [Button]
    public void AppearToPosing(Action postAction = null)
    {
        if (!gameObject.activeSelf) return;

        transform.localScale = Vector3.zero;
        SwitchState(new Appearing(
            this, 
            () => SwitchState(new Posing(this, postAction)), 
            null));
    }

    [Button]
    public void Disappear(Action postDisappearAction = null)
    {
        SwitchState(new Dissappearing(this, null, postDisappearAction));
    }

    [Button]
    public void Die()
    {
        SwitchState(new Dying(this));
    }

    public void AppearTo(BallState nState)
    {
        SwitchState(new Appearing(this, () => SwitchState(nState), null));
    }

    public void TransitionToSkin(BallSkin nSkin, Action postAction)
    {
        ((BallState)CurrentState).TransitionToSkin(nSkin, postAction);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        this.PostNotification(Notifications.BackgroundContactReaction, new BackgroundContactReactionData() { point = collision.GetContact(0).point, color = ColorPallete.BackgroundFxTerrain, force = 1 });
        (CurrentState as BallState)?.OnCollisionEnter2D(collision);
    }

    private void OnEnable()
    {
        ballSize = DefaultBallSize;
        rb.gravityScale = Settings.BallGravity;
        
        
        this.AddObserver(OnGameReadyResponse, Notifications.GameMode_Ready);
        this.AddObserver(OnGameGoResponse, Notifications.GameMode_Go);
        this.AddObserver(OnCoinPickedUp, Notifications.OnCoinPickedUp);
        this.AddObserver(OnBallSizeChange, Notifications.OnBallSizeChange);
        this.AddObserver(OnBallGravityChange, Notifications.OnBallGravityChange);

        codeAnimator.Stop(this);
    }

    private void OnDisable()
    {
        this.RemoveObserver(OnGameReadyResponse, Notifications.GameMode_Ready);
        this.RemoveObserver(OnGameGoResponse, Notifications.GameMode_Go);
        this.RemoveObserver(OnCoinPickedUp, Notifications.OnCoinPickedUp);
        this.RemoveObserver(OnBallSizeChange, Notifications.OnBallSizeChange);
        this.RemoveObserver(OnBallGravityChange, Notifications.OnBallGravityChange);

        if (CurrentState is Appearing)
        {
            SwitchState(new Awaking(this));
        }
        codeAnimator.Stop(this);
    }

    private void OnBallSizeChange(object arg1, object arg2)
    {
        ballSize = (float)arg2;
        SnapToFullSize();
    }

    private void OnBallGravityChange(object arg1, object arg2)
    {
        rb.gravityScale = (float)arg2;
    }

    private void OnCoinPickedUp(object arg1, object arg2)
    {
        sFXs_onPickUpCoinVoices.PlayRandom();
    }

    private void OnGameGoResponse(object arg1, object arg2)
    {
        SwitchState(new Living(this));
    }

    private void OnGameReadyResponse(object sender, object value)
    {
        trailBall.DestroyCurrent();
        transform.position = DefaultStartPosition;
        AppearToWaiting();
        trailBall.CreateNew();
    }

    public void AirJump()
    {
        Rb.AddForce(Settings.BounceForce * Vector2.up*1.5f, ForceMode2D.Impulse);
    }
    public void SnapToFullSize()
    {
        transform.localScale = Vector3.one * ballSize;
    }

    [Button]
    public void Duplicate()
    {
        BallStateMachine nBall = Instantiate(gameObject, null).GetComponent<BallStateMachine>();
        nBall.SkinComponent.SetSkin(Body_Conteiner.GetRandomOwnedSkin());
        nBall.AppearToLiving();
    }

    [Button]
    public void Shield()
    {
        Shield shield = Instantiate(GameObjectLibrary.Get("Shield")).GetComponent<Shield>();
        shield.EquipTo(gameObject);
    }
}

namespace BallStates
{
    public abstract class BallState : State
    {
        private Action onEnterAction;
        public Action OnEnterAction
        {
            get { return onEnterAction; }
            set { onEnterAction = value; }
        }

        protected readonly BallStateMachine Owner;
        public BallState(BallStateMachine owner, Action onEnterAction = null)
        {
            this.onEnterAction = onEnterAction;
            Owner = owner;
        }

        public override void Enter()
        {
            base.Enter();
            OnEnterAction?.Invoke();
        }

        public virtual void OnCollisionEnter2D(Collision2D collision) { }

        public virtual void TransitionToSkin(BallSkin nSkin, Action onEnterAction)
        {
            Debug.Log("TransitionToSkin from " + this);
            BallState postState = this;
            postState.OnEnterAction = onEnterAction;
            Owner.Disappear(() =>
            {
                Owner.SkinComponent.SetSkin(nSkin);
                Owner.AppearTo(postState);
            });
        }
    }

    /// <summary>
    /// Inicializa todos los componentes y se mantiene apagada
    /// </summary>
    public class Awaking : BallState
    {
        public Awaking(BallStateMachine owner, Action onEnterAction = null) : base(owner, onEnterAction)
        {
        }

        public override void Enter()
        {
            base.Enter();

            Owner.Rb.simulated = false;
            Owner.ConstantBounce.enabled = true;
            Owner.Wrappable.enabled = false;
            Owner.Health.enabled = false;
            Owner.Col.isTrigger = true;

            Owner.transform.localScale = Vector2.zero;
            Owner.transform.rotation = Quaternion.identity;
        }

        public override void TransitionToSkin(BallSkin nSkin, Action onEnterAction)
        {
            Debug.Log("TransitionToSkin from " + this);
            Owner.SkinComponent.SetSkin(nSkin);
            Owner.AppearToPosing(onEnterAction);
        }
    }

    public class Waiting : BallState
    {
        public Waiting(BallStateMachine owner, Action onEnterAction = null) : base(owner, onEnterAction)
        {
        }

        public override void Enter()
        {
            base.Enter();

            Owner.Rb.simulated = false;
            Owner.ConstantBounce.enabled = false;
            Owner.Wrappable.enabled = false;
            Owner.Health.enabled = false;
            Owner.Col.isTrigger = true;
            float time = 0;
            Vector2 startPosition = Owner.transform.localPosition;

            Owner.codeAnimator.StartInfiniteAnimation(Owner, delta =>
            {
                if (Owner.useFloatAnimation)
                {
                    time += delta * Owner.waitingVelocity;
                    Owner.transform.localPosition = startPosition + Vector2.up * Owner.waitingCurve.Evaluate(time) * Owner.waitingHeight;
                }
            }, DeltaTimeType.deltaTime);

            this.PostNotification(Notifications.BallWaiting, this);

            Owner.Ball_Animation.Posing();
        }

        public override void Exit()
        {
            base.Exit();
            Owner.codeAnimator.Stop(Owner);
        }
    }

    public class Posing : BallState
    {
        public Posing(BallStateMachine owner, Action onEnterAction = null) : base(owner, onEnterAction)
        {
        }

        public override void Enter()
        {
            base.Enter();
            Owner.Rb.simulated = false;
            Owner.ConstantBounce.enabled = false;
            Owner.Wrappable.enabled = false;
            Owner.Health.enabled = false;
            Owner.Col.isTrigger = true;
            Owner.Ball_Animation.Dissable();
            Owner.Ball_Animation.Posing();
        }
    }

    public class Dissappearing : BallState
    {
        Action onDissapearDoneAction;

        private readonly bool disableAfterDissapear;

        public Dissappearing(BallStateMachine owner, Action onEnterAction, Action onDissapearDoneAction, bool disableAfterDissapear = true) : base(owner, onEnterAction)
        {
            this.disableAfterDissapear = disableAfterDissapear;
            this.onDissapearDoneAction = onDissapearDoneAction;
        }

        public override void Enter()
        {
            base.Enter();
       
            float startRotation = Owner.transform.rotation.z;
            Vector3 startSize = Owner.transform.localScale;
            Owner.codeAnimator.StartAnimacion(Owner,
                x =>
                {
                    Owner.transform.rotation = Quaternion.Euler(0, 0, Mathf.Lerp(startRotation, Owner.appearRotation, x));
                    Owner.transform.localScale = Vector3.LerpUnclamped(startSize, Vector3.zero, x);
                }, DeltaTimeType.deltaTime, AnimationType.Simple, Owner.disappearCurve.Curve, DissapearDone, Owner.disappearCurve.Time);

            Owner.Ball_Animation.Dissable();
        }

        public override void Exit()
        {
            base.Exit();
            Owner.codeAnimator.Stop(Owner);
        }

        private void DissapearDone()
        {
            if(disableAfterDissapear)            SwitchState(new Disable(Owner));
            onDissapearDoneAction?.Invoke();
        }
    }

    public class Disable : BallState
    {
        public Disable(BallStateMachine owner) : base(owner)
        {
        }

        public override void Enter()
        {
            base.Enter();

            Owner.transform.localScale = Vector3.zero;
            Owner.Rb.simulated = false;
            Owner.ConstantBounce.enabled = false;
            Owner.Wrappable.enabled = Owner.useWrap;
            Owner.Health.enabled = false;
            Owner.Col.isTrigger = true;
        }

        public override void Exit()
        {
            base.Exit();
        }

        public override void TransitionToSkin(BallSkin nSkin, Action onEnterAction)
        {
            Debug.Log("TransitionToSkin from " + this);
            Owner.SkinComponent.SetSkin(nSkin);
            Owner.AppearTo(new Posing(Owner, onEnterAction));
        }
    }

    public class Appearing : BallState
    {
        Action postAppearingAction;
        public Appearing(BallStateMachine owner,Action postAppearingAction,  Action onEnterAction) : base(owner, onEnterAction)
        {
            this.postAppearingAction = postAppearingAction;
        }

        public override void Enter()
        {
            base.Enter();
            Owner.Rb.angularVelocity = 0;
            Owner.Rb.velocity = Vector2.zero;
            float startRotation = Owner.appearRotation;
            Vector3 startSize = Owner.transform.localScale;
            Owner.codeAnimator.StartAnimacion(Owner,
                x =>
                {
                    Owner.transform.rotation = Quaternion.Euler(0, 0, Mathf.Lerp(startRotation, 0, x));
                    Owner.transform.localScale = Vector3.LerpUnclamped(startSize, Vector3.one * Owner.ballSize, x);
                }, DeltaTimeType.deltaTime, AnimationType.Simple, Owner.appearCurve.Curve,
               postAppearingAction, Owner.appearCurve.Time);
        }

        public override void Exit()
        {
            base.Exit();
            Owner.codeAnimator.Stop(Owner);
        }

        public override void TransitionToSkin(BallSkin nSkin, Action onEnterAction)
        {
            Owner.SkinComponent.SetSkin(nSkin);
            onEnterAction?.Invoke();
            //postAppearingAction += onEnterAction;
        }
    }
  
    public class Living : BallState
    {
        public Living(BallStateMachine owner, Action onEnterAction = null) : base(owner, onEnterAction)
        {
         
        }

        public override void Enter()
        {
            base.Enter();
            Owner.Rb.simulated = true;
            Owner.ConstantBounce.enabled = true;
            Owner.Wrappable.enabled = Owner.useWrap;
            Owner.Health.enabled = true;
            Owner.Col.isTrigger = false;

            Owner.Health.Initialize(1);
            Owner.Health.OnDead = Die;
            Owner.Health.OnDamageReceived = Damaged;
            Owner.Health.OnHealReceived = Healed;

            Owner.Ball_Animation.Playing();
        }
            
        private void Die(Health health)
        {
            SwitchState(new Dying(Owner));
        }

        private void Damaged(int ammount)
        {

        }

        private void Healed(int ammount)
        {

        }

        public override void Exit()
        {
            base.Exit();
        }
    }

    public class Dying : BallState
    {
        public Dying(BallStateMachine owner) : base(owner)
        {
         
        }

        public override void Enter()
        {
            base.Enter();
            Owner.Col.isTrigger = true;
            Owner.Rb.simulated = false;
            Owner.ConstantBounce.enabled = false;
            Owner.Wrappable.enabled = false;
            Vector2 startPos = Owner.transform.localPosition;
            Vector3 startSize = Owner.transform.localScale;
            float t = 0;
            this.PostNotification(Notifications.Ball_DeadStart, this);
            Owner.codeAnimator.StartAnimacion(Owner,
                x =>
                {
                    t += Time.deltaTime;
                    Owner.transform.localPosition = startPos + Vector2.up * x * Owner.deadJumpHeight;
                    Owner.transform.localScale = startSize + Vector3.one * t * Owner.deadSizeMax * Owner.ballSize;
                }, DeltaTimeType.deltaTime, AnimationType.Simple, Owner.deadCurve.Curve, DeadAnimationDone, Owner.deadCurve.Time);

            Owner.sFXs_dead.PlayRandom();
            Owner.Ball_Animation.Dead();
            if (Settings.UseVibration) Vibration.Vibrate((long)Settings.BallDeadVibrationForce);
        }
        private void DeadAnimationDone()
        {
            this.PostNotification(Notifications.Ball_DeadEnd, this);
        }

        public override void Exit()
        {
            base.Exit();
            Owner.codeAnimator.Stop(Owner);
        }
    }
}

