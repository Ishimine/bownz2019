﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;

[CreateAssetMenu(menuName = "Skins/BallSkin")]
public class BallSkin : ScriptableObject
{
    private static BallSkin playerSelectedSkin;
    public static BallSkin PlayerSelectedSkin
    {
        get
        {
            if (playerSelectedSkin == null)
                playerSelectedSkin = Body_Conteiner.GetSkin(PlayerPrefs.GetInt("PlayerSelection_Skin", 0));
             return playerSelectedSkin;
        }
        set
        {
            playerSelectedSkin = value;
            OnPlayerSelectedSkinChange?.Invoke(value);
            PlayerPrefs.SetInt("PlayerSelection_Skin", Body_Conteiner.GetSkinIndex(value));
        }
    }

    private static Action<BallSkin> onPlayerSelectedSkinChange;
    public static Action<BallSkin> OnPlayerSelectedSkinChange
    {
        get { return onPlayerSelectedSkinChange; }
        set { onPlayerSelectedSkinChange = value; }
    }

    public static void SetPlayerSkin(int index)
    {
        SetPlayerSkin(Body_Conteiner.GetSkin(index));
    }

    public static void SetPlayerSkin(BallSkin skin)
    {
        PlayerSelectedSkin = skin;
    }

    public static void SetPlayerSkin(string index)
    {
        SetPlayerSkin(int.Parse(index));
    }

    [PreviewField(Alignment = ObjectFieldAlignment.Center, Height = 100)]
    public Sprite Body;
    public string Name;
    public string Description;
    [MinValue(0)]
    public int Price;
}
