﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
using System.Linq;
using Sirenix.OdinInspector;
#endif

[CreateAssetMenu(menuName = "Skins/Bodies")]
public class Body_Conteiner : ScriptableSingleton<Body_Conteiner>
{
    public Sprite masterSheet;
    public Sprite[] bodies;

    public override Body_Conteiner Myself => this;

    [SerializeField] private List<BallSkin> skins;
    public List<BallSkin> Skins
    {
        get { return skins; }
        set { skins = value; }
    }

    public static string BallSkinsPath => "Assets/_Prefabs&SO/_ScriptableObjects/Resources/Skins";

    public static BallSkin GetSkin(int index)
    {
        return Instance.Skins[(int)Mathf.Repeat(index, Instance.Skins.Count)];
    }

    public static BallSkin GetRandomSkin()
    {
        return Instance.Skins.GetRandom();
    }

    public static BallSkin GetRandomOwnedSkin()
    {
        BallSkin[] ballSkins = GetSkins(Inventory.GetAllOwnedSkinsIndexes());
        return (ballSkins.Length == 0)?Instance.Skins[0]: ballSkins.GetRandom();
    }

    public static int GetSkinIndex(BallSkin skin)
    {
        return Instance.Skins.IndexOf(skin);
    }

    public static BallSkin[] GetSkins(int[] indexes)
    {
        return Instance._GetSkins(indexes);
    }

    public BallSkin[] _GetSkins(int[] indexes)
    {
        return skins.GetElements(indexes);
    }

#if UNITY_EDITOR
    [Button]
    public void ProcessMasterSheet()
    {
        string spriteSheet = AssetDatabase.GetAssetPath(masterSheet.texture);
        bodies = AssetDatabase.LoadAllAssetsAtPath(spriteSheet).OfType<Sprite>().ToArray();
        CreateSkins();
    }

    public void CreateSkins()
    {
        for (int i = 0; i < bodies.Length; i++)
        {
            if (!AlreadyExist(bodies[i]))
                CreateNewSkin(bodies[i]);
        }
    }

    private bool AlreadyExist(Sprite sprite)
    {
        foreach (var item in Skins)
            if (item.Body == sprite) return true;

        return false;
    }

    private void CreateNewSkin(Sprite sprite)
    {
        BallSkin nSkin = CreateInstance<BallSkin>();
        nSkin.Body = sprite;
        nSkin.name = sprite.name;
        AssetDatabase.CreateAsset(nSkin, BallSkinsPath + "/Skin_" + sprite.name + ".asset"); 
        AssetDatabase.SaveAssets(); 
        AssetDatabase.Refresh();

        Skins.Add(nSkin);
    }
#endif





}
