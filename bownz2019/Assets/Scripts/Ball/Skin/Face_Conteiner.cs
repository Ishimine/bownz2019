﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(menuName = "Skins/Faces")]
public class Face_Conteiner : ScriptableObject
{
    public Sprite DefaultFace => Idle[0];
    public Sprite BlinkFace => Idle[1];

    [PreviewField] public Sprite[] Idle;
    [PreviewField] public Sprite[] Happy;
    [PreviewField] public Sprite[] Love;
    [PreviewField] public Sprite[] Sad;
    [PreviewField] public Sprite[] Surprise;
    [PreviewField] public Sprite[] Angry;
    [PreviewField] public Sprite[] Fear;
    [PreviewField] public Sprite[] Confused;
    [PreviewField] public Sprite[] Dizzy;
    [PreviewField] public Sprite[] Pain;
    [PreviewField] public Sprite[] Bored;
}

