﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallSkinComponent : MonoBehaviour
{
    private bool usePlayerSelection = true;
    public bool UsePlayerSelection
    {
        get { return usePlayerSelection; }
        set { usePlayerSelection = value; }
    }

    public SpriteRenderer rBody;
    public SpriteRenderer rFace;
    public TrailBall trail;

    public void SetSkin(BallSkin skin)
    {
        if (skin == null)
            Debug.LogError("Skin es NULO");
        else
            rBody.sprite = skin.Body;
    }

    private void OnEnable()
    {
        if (usePlayerSelection) SetSkin(BallSkin.PlayerSelectedSkin);

        BallSkin.OnPlayerSelectedSkinChange -= OnPlayerSelectedSkinChange;
        BallSkin.OnPlayerSelectedSkinChange += OnPlayerSelectedSkinChange;
    }

    private void OnDisable()
    {
        BallSkin.OnPlayerSelectedSkinChange -= OnPlayerSelectedSkinChange;
    }

    private void OnPlayerSelectedSkinChange(BallSkin nSkin)
    {
        if (usePlayerSelection) SetSkin(nSkin);
    }

   
}
