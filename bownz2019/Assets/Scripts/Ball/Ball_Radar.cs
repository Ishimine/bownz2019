﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CircleCollider2D))]
public class Ball_Radar : MonoBehaviour
{
    private CircleCollider2D circleCollider;
    public Ball_Animation ball_Animation;

    public RuntimeSet happyTargets;
    public RuntimeSet fearTargets;

    LookTarget lookTarget = null;

    private void Awake()
    {
        circleCollider = GetComponent<CircleCollider2D>();
    }

    private void FixedUpdate()
    {
        if (lookTarget != null && lookTarget.gameObject != null)
            ball_Animation.LookingAtTargetStay(lookTarget);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(lookTarget == null)
        {
            if(happyTargets.Contains(collision.gameObject))
                lookTarget = new LookTarget(LookTarget.LookReaction.Happy, collision.gameObject);
            else if (fearTargets.Contains(collision.gameObject))
                lookTarget = new LookTarget(LookTarget.LookReaction.Fear, collision.gameObject);

            if (lookTarget != null)
                ball_Animation.LookingAtTargetIn(lookTarget);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (lookTarget != null && collision.gameObject == lookTarget.gameObject)
        {
            lookTarget = null;
            ball_Animation.StopLooking();
           /* if (collision.gameObject.GetComponent<StarCoin>() != null)
                ball_Animation.Sad();*/
        }
    }

    private void OnEnable()
    {
        this.AddObserver(OnGameModeReady, Notifications.GameMode_Ready);
        lookTarget = null;
    }

    private void OnDisable()
    {
        this.AddObserver(OnGameModeReady, Notifications.GameMode_Ready);
    }

    private void OnGameModeReady(object arg1, object arg2)
    {
        lookTarget = null;
    }
}   

public class LookTarget
{
    public enum LookReaction { Happy, Fear, Angry}
    public LookReaction reaction;
    public GameObject gameObject;

    public LookTarget(LookReaction reaction, GameObject gameObject)
    {
        this.reaction = reaction;
        this.gameObject = gameObject;
    }
}