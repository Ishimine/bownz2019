﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Procedural;
using Sirenix.OdinInspector;

[CreateAssetMenu(fileName = "Settings")]
public class Settings : ScriptableSingleton<Settings>
{
    public override Settings Myself => this;

    public bool IsDebugBuild { get; set; } = false;

    public static Action<bool> OnUI_InteractionChange { get; set; }


    public static bool useOnlyLocalValues = false;

    private static bool uI_Interaction = true;

    public static bool UiInteraction
    {
        get { return uI_Interaction; }
        set
        {
            uI_Interaction = value;
            OnUI_InteractionChange?.Invoke(value);
        }
    }

    public static Action<bool> OnChallengeModeChange { get; set; }

    public static string ChallengeID = "_Challenge";

    private static bool isChallengeMode = false;

    public static bool IsChallengeMode
    {
        get { return isChallengeMode; }
        set
        {
            isChallengeMode = value;
            OnChallengeModeChange?.Invoke(value);
        }
    }


    #region Gameplay

    [Header("General")] [TabGroup("Gameplay"), SerializeField]
    private RemoteFloat bounceForce = new RemoteFloat()
        {defaultValue = 6, Key = "BounceForce", UseRemoteVariable = true};

    public static float BounceForce 
    {
        get => (useOnlyLocalValues)?Instance.bounceForce.defaultValue:Instance.bounceForce.GetValue();
        set => Instance.bounceForce.defaultValue = value;
    }

    [TabGroup("Gameplay"), SerializeField] private RemoteFloat ballGravity = new RemoteFloat(){ defaultValue = 1, Key = "BallGravity", UseRemoteVariable = true};

    public static float BallGravity
    {
        get => Instance.ballGravity.GetValue();
        set
        {
            Instance.ballGravity.defaultValue = value;
            Instance.PostNotification(Notifications.OnBallGravityChange, value);
        }
    }

    [TabGroup("Gameplay"), SerializeField]
    public Vector2 defaultPortrait_PlayAreaSize = new Vector2(14, 22);
    public static Vector2 Portrait_PlayAreaSize
    {
        get => Instance.defaultPortrait_PlayAreaSize;
    }

    [TabGroup("Gameplay"), SerializeField]
    private Vector2 defaultLandscape_PlayAreaSize = new Vector2(22, 14);
    public static Vector2 Horitonza_PlayAreaSize
    {
        get => Instance.defaultLandscape_PlayAreaSize;
    }

    [TabGroup("Gameplay"), SerializeField]
    private float swipePlatform_LifeSpan = 2;
    public static float SwipePlatform_LifeSpan { get => Instance.swipePlatform_LifeSpan; }

    [TabGroup("Gameplay"), SerializeField]
    private int swipePlatform_MaxCount = 1;
    public static int SwipePlatform_MaxCount { get => Instance.swipePlatform_MaxCount; }

    [TabGroup("Gameplay"), SerializeField]
    private int maxSimultaneousSwipes = 2;
    public static int MaxSimultaneousSwipes { get => Instance.maxSimultaneousSwipes; }

    [TabGroup("Gameplay"), SerializeField]
    private Vector2 swipePlatform_MinSize = new Vector2(3.25f, .3f);
    public static Vector2 SwipePlatform_MinSize { get => Instance.swipePlatform_MinSize; }

    [TabGroup("Gameplay"), SerializeField]
    private Vector2 swipePlatform_MaxSize = new Vector2(3.25f, .3f);
    public static Vector2 SwipePlatform_MaxSize { get => Instance.swipePlatform_MaxSize; }


    [TabGroup("Gameplay"), SerializeField]
    private float slowMotionAmmount = .5f;
    public static float SlowMotionAmmount { get => Instance.slowMotionAmmount; }

    [TabGroup("Gameplay"), SerializeField]
    private float redCoinFlatAcceleration = .05f;
    public static float RedCoinFlatAcceleration { get => Instance.redCoinFlatAcceleration; }

    [TabGroup("Gameplay"), SerializeField]
    private float blueCoinFlatDeacceleration = .04f;
    public static float BlueCoinFlatDeacceleration { get => Instance.blueCoinFlatDeacceleration; }

    [TabGroup("Gameplay"), SerializeField]
    private float slowMotionDuration = 3f;
    public static float SlowMotionDuration { get => Instance.slowMotionDuration; }

    [TabGroup("Gameplay"), SerializeField]
    private float starSlowMotionDuration = 4f;
    public static float StarSlowMotionDuration { get => Instance.starSlowMotionDuration; }

    [TabGroup("Gameplay"), SerializeField]
    public static string VerticalLevelId { get => "VerticalLevel"; }

     [TabGroup("Gameplay"), SerializeField] public bool useVibration = false;

     public static bool UseVibration
     {
         get => Instance.useVibration;
         set
         {
             PlayerPrefs.SetInt("UseVibration", (value)?1:0);
             Instance.useVibration = value;
         }
     }

     [TabGroup("GamePlay"), SerializeField] private float ballDeadVibrationForce = 100;
     public static float BallDeadVibrationForce
     {
        get => Instance.ballDeadVibrationForce;
        set => Instance.ballDeadVibrationForce = value;
     }

    [TabGroup("Gameplay"), SerializeField] private float swipePlatform_TouchVibrationForce;
    public static float SwipePlatform_TouchVibrationForce
    {
        get => Instance.swipePlatform_TouchVibrationForce;
        set => Instance.swipePlatform_TouchVibrationForce = value; 
    }

    [Header("Puzzle")]
    [TabGroup("Gameplay"), SerializeField]
    private float swipePlatform_PuzzleLifeSpan = 2;
    public static float SwipePlatform_PuzzleLifeSpan { get => Instance.swipePlatform_PuzzleLifeSpan; }

    [TabGroup("Gameplay"), SerializeField]
    private int swipePlatform_PuzzleMaxCount = 1;
    public static int SwipePlatform_PuzzleMaxCount { get => Instance.swipePlatform_PuzzleMaxCount; }

    [TabGroup("Gameplay"), SerializeField] private int puzzleMaxSimultaneousSwipes = 6;
    public static int PuzzleMaxSimultaneousSwipes { get => Instance.puzzleMaxSimultaneousSwipes; }

    [TabGroup("Gameplay"), SerializeField] private bool useCloudSaving = false;
    public static bool UseGooglePlayCloudSaving { get { return Instance.useCloudSaving; } }

    [TabGroup("Gameplay"), SerializeField] private RemoteBool useFirebaseDatabase = new RemoteBool(){ defaultValue = false, Key = "UseFirebaseDatabase", UseRemoteVariable = true};
    public static bool UseFirebaseDatabase 
    {
        get => Instance.useFirebaseDatabase.GetValue(); 
        set => Instance.useFirebaseDatabase.defaultValue = value;
    }

    [TabGroup("Gameplay"), SerializeField] public RemoteFloat ballSize = new RemoteFloat()
        {defaultValue = .9f, Key = "BallSize", UseRemoteVariable = true};
    public static float BallSize
    {
        get => (useOnlyLocalValues) ? Instance.ballSize.defaultValue : Instance.ballSize.GetValue();
        set
        {
            Instance.ballSize.defaultValue = value;
            Instance.PostNotification(Notifications.OnBallSizeChange, value);
        }
    }

    #endregion

    #region InAppPurchase

#pragma warning disable IDE0044 // Add readonly modifier
    [TabGroup("InAppPurchase"), ReadOnly] private bool test = false;
#pragma warning restore IDE0044 // Add readonly modifier


    [TabGroup("InAppPurchase"), SerializeField] private bool hasRemoveAds = false;
    public static bool HasRemoveAds
    {
        get { return Instance.hasRemoveAds; }
        set { Instance.hasRemoveAds = value; }
    }

    #endregion

    #region Input
    [TabGroup("Input")]
    public string Input;
    #endregion

    #region Graphics

    public enum Style { None, Abstract, Normal}


    [TabGroup("Graphics"), SerializeField] private Style currentStyle;
    public static Style CurrentStyle { get => Instance.currentStyle; }

    [TabGroup("Graphics"),SerializeField] private Color swipePlatform_ActiveColor = Color.clear;
    public static Color SwipePlatform_ActiveColor { get => Instance.swipePlatform_ActiveColor; }

    [TabGroup("Graphics"), SerializeField] private Color swipePlatform_InactiveColor = Color.clear;
    public static Color SwipePlatform_InactiveColor { get => Instance.swipePlatform_InactiveColor; }

    [TabGroup("Graphics"), SerializeField] private Color swipePlatform_WrongColor = Color.clear;
    public static Color SwipePlatform_WrongColor { get => Instance.swipePlatform_WrongColor; }

    [TabGroup("Graphics"), SerializeField] private float swipePlatform_DeformationLenght = .5f;
    public static float SwipePlatform_DeformationLenght { get => Instance.swipePlatform_DeformationLenght; }

    [TabGroup("Graphics"), SerializeField] private AnimationCurve appearCurve = AnimationCurve.EaseInOut(0, 0, 1, 1);
    public static AnimationCurve AppearCurve { get => Instance.appearCurve; }

    [TabGroup("Graphics"), SerializeField] private CodeAnimatorCurve swipePlatform_BounceCurve = null;
    public static CodeAnimatorCurve SwipePlatform_BounceCurve { get => Instance.swipePlatform_BounceCurve; }

    [TabGroup("Graphics"), SerializeField] private CodeAnimatorCurve swipePlatform_DissapearCurve = null;
    public static CodeAnimatorCurve SwipePlatform_DissapearCurve { get => Instance.swipePlatform_DissapearCurve; }
                                                        
    [TabGroup("Graphics"), SerializeField]  private bool defaultUseBloom = true;
    [TabGroup("Graphics"), ReadOnly] private bool useBloom = true;
   public static bool UseBloom
    {
        get => Instance.useBloom;
        set
        {
            Instance.useBloom = value; 
            Instance.PostNotification(Notifications.Vfx_BloomSetEnable,value);
        }
    }

    [TabGroup("Graphics"), SerializeField]  private bool defaultBackground_Enable = true;
    [TabGroup("Graphics"), ReadOnly]  public bool background_Enable;
   public static bool Background_Enable
    {
        get => Instance.background_Enable;
        set
        {
            Instance.background_Enable = value; 
            Instance.PostNotification(Notifications.Vfx_BackgroundSetEnable,value);
        }
    }

    #endregion

    #region Audio
    [TabGroup("Audio")]
    public string Audio;

    [TabGroup("Graphics"), SerializeField] private int defaultBackgroundDetail = 1;
    [TabGroup("Graphics"), ReadOnly] private int backgroundDetail;
    public static int Background_Detail
    {
        get => Instance.backgroundDetail;
        set
        {
            Instance.backgroundDetail = Mathf.Clamp(value,0,3);
            Instance.PostNotification(Notifications.Vfx_BackgroundSetLevel, value);
        }
    }

    [SerializeField] private RemoteBool canBuyWithVideo = new RemoteBool(){ defaultValue = true, Key = "CanBuyWithVideo"};
    public static bool CanBuyPowerUpWithVideo => Instance.canBuyWithVideo.GetValue();
    [SerializeField] private RemoteBool canBuyPowerUpWithCoin = new RemoteBool(){ defaultValue = true, Key = "CanBuyPowerUpWithCoin"}; 
    public static bool CanBuyPowerUpWithCoin => Instance.canBuyPowerUpWithCoin.GetValue();

    #endregion

    public override void InitializeSingleton()
    {
        base.InitializeSingleton();
        useBloom = defaultUseBloom;
        background_Enable = defaultBackground_Enable;
        backgroundDetail = defaultBackgroundDetail;
        
        this.AddObserver(OnRemoteConfigFetchDone ,Notifications.RemoteConfigFetched);
    }

    [TabGroup("Daily"), SerializeField] private SeudoBlueprintTypeC dailyBlueprint_Easy;
    [TabGroup("Daily"), SerializeField] private RemoteInt dailyStagesCount_Easy = new RemoteInt(){ defaultValue = 2, Key = "DailyStagesCount_Easy", UseRemoteVariable = true};
    [TabGroup("Daily"), SerializeField] private RemoteInt dailyModulesPerStage_Easy = new RemoteInt(){ defaultValue = 2, Key = "DailyModulesPerStage_Easy", UseRemoteVariable = true};
    
    [TabGroup("Daily"), SerializeField] private SeudoBlueprintTypeC dailyBlueprint_Medium;
    [TabGroup("Daily"), SerializeField] private RemoteInt dailyStagesCount_Medium = new RemoteInt(){ defaultValue = 2, Key = "DailyStagesCount_Medium", UseRemoteVariable = true};
    [TabGroup("Daily"), SerializeField] private RemoteInt dailyModulesPerStage_Medium = new RemoteInt(){ defaultValue = 2, Key = "DailyModulesPerStage_Medium", UseRemoteVariable = true};

    [TabGroup("Daily"), SerializeField] private SeudoBlueprintTypeC dailyBlueprint_Hard;
    [TabGroup("Daily"), SerializeField] private RemoteInt dailyStagesCount_Hard = new RemoteInt(){ defaultValue = 2, Key = "DailyStagesCount_Hard", UseRemoteVariable = true};
    [TabGroup("Daily"), SerializeField] private RemoteInt dailyModulesPerStage_Hard = new RemoteInt(){ defaultValue = 2, Key = "DailyModulesPerStage_Hard", UseRemoteVariable = true};



    private void OnRemoteConfigFetchDone(object arg1, object arg2)
    {
        dailyBlueprint_Easy.totalStages = dailyStagesCount_Easy.GetValue();
        dailyBlueprint_Easy.modulesPerStage = dailyModulesPerStage_Easy.GetValue();

        dailyBlueprint_Medium.totalStages = dailyStagesCount_Medium.GetValue();
        dailyBlueprint_Medium.modulesPerStage = dailyModulesPerStage_Medium.GetValue();

        dailyBlueprint_Hard.totalStages = dailyStagesCount_Hard.GetValue();
        dailyBlueprint_Hard.modulesPerStage = dailyModulesPerStage_Hard.GetValue();

        useVibration = PlayerPrefs.GetInt("UseVibration") == 1;
    }
}
