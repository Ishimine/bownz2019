﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class Settings_Procedural : ScriptableSingleton<Settings_Procedural>
{
    public override Settings_Procedural Myself => this;

    [SerializeField] public Vector2 minMaxLevelHeight = new Vector2(40,2000);
    public static Vector2 MinMaxLevelHeight { get { return Instance.minMaxLevelHeight; } }

    [SerializeField] public int timePerModule = 40;
    public static int TimePerModule { get { return Instance.timePerModule; } }

    [SerializeField] public int platformsPerModule = 10;
    public static int PlatformsPerModule { get { return Instance.platformsPerModule; } }
}
