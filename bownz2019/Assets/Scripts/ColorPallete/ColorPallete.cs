﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ColorPallete : ScriptableSingleton<ColorPallete>
{
    public override ColorPallete Myself => this;

    [Header("Backgrounds")]
    [SerializeField] private float backgroundAlpha = .7f;

    [SerializeField] private Color backgroundFxTerrain = Color.clear;
    public static Color BackgroundFxTerrain { get { return Instance.backgroundFxTerrain; } }

    [SerializeField] private Color backgroundFxCoin = Color.clear;
    public static Color BackgroundFxCoin { get { return Instance.backgroundFxCoin; } }

    [SerializeField] private Color backgroundFxSuperCoin = Color.clear;
    public static Color BackgroundFxSuperCoin { get { return Instance.backgroundFxSuperCoin; } }

    [SerializeField] private Color greenBackgroundFx = Color.clear;
    public static Color GreenBackgroundFx { get => Instance.greenBackgroundFx;  }

    [Header("Coins")]
    [SerializeField] private Color normalCoin = Color.clear;
    public static Color NormalCoin { get =>  Instance.normalCoin;  }

    [SerializeField] private Color goldenCoin = Color.clear;
    public static Color GoldenCoin { get => Instance.goldenCoin;  }

    [SerializeField] private Color redCoin = Color.clear;
    public static Color RedCoin { get =>  Instance.redCoin;  }

    [SerializeField] private Color blueCoin = Color.clear;
    public static Color BlueCoin { get =>  Instance.blueCoin;  }

    [SerializeField] private Color greenCoin = Color.clear;
    public static Color GreenCoin { get => Instance.greenCoin;  }


    [SerializeField] private Color lockedCoin = Color.clear;
    public static Color LockedCoin { get => Instance.lockedCoin;  }

    [SerializeField] private Color keyCoin = Color.gray;
    public static Color KeyCoin { get => Instance.keyCoin;  }

    [SerializeField] private Color keyCoinIcon = Color.yellow;
    public static Color KeyCoinIcon { get => Instance.keyCoinIcon;  }

    [Header("Other")]
    [SerializeField] private Color wall = Color.clear;
    public static Color Wall { get { return Instance.wall; } }

    [SerializeField] private Color spike = Color.clear;
    public static Color Spike { get { return Instance.spike; } }

    public static Color AsBackgroundColor(Color color)
    {
        color.a = Instance.backgroundAlpha;
        return color;
    }
}
