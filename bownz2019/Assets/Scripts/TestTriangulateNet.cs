﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

using TriangleNet.Geometry;
using TriangleNet.Topology;

public class TestTriangulateNet : MonoBehaviour
{
    public PolygonCollider2D polygonCollider;

    public MeshFilter meshFilter;
    public  Mesh mesh = null;
    Polygon polygon;


    [Button]
    public void CreateMesh()
    {
        polygon = new Polygon();

        List<List<Vector2>> paths = polygonCollider.GetAllPaths();

        Contour contour;

        List<Vertex> vertices = new List<Vertex>();
        for (int i = 0; i < paths.Count; i++)
        {
            for (int j = 0; j < paths[i].Count; j++)
            {
                vertices.Add(new Vertex(paths[i][j].x, paths[i][j].y));
            }
            contour = new Contour(vertices);
            polygon.Add(contour);
        }
    }

    [Button]
    void Clear()
    {
        mesh.Clear();
    }

    [Button]
    void Refresh()
    {
        
        var m = polygon.Triangulate();
        var verts = ToVector3(m.Vertices);
        var tris = ToTriangles(m.Triangles);

        mesh.Clear();
        if (mesh == null)
        {
            mesh = new Mesh();
            mesh.MarkDynamic();
            meshFilter.sharedMesh = mesh;
        }

        mesh.vertices = verts;
        mesh.triangles = tris;
    }

    Vector3[] ToVector3(ICollection<Vertex> vert)
    {
        List<Vertex> vertList = new List<Vertex>();
        foreach (var vt in vert) vertList.Add(vt);

        var v = new Vector3[vert.Count];
        for (int i = 0; i < vertList.Count; i++)
        {
            v[i].x = (float)vertList[i].x;
            v[i].z = (float)vertList[i].y;
        }

        return v;
    }

    int[] ToTriangles(ICollection<Triangle> tris)
    {
        List<Triangle> triList = new List<Triangle>();
        foreach (var t in tris) triList.Add(t);


        int[] ts = new int[tris.Count * 3];
        for (int i = 0; i < triList.Count; i++)
        {
            Triangle tri = triList[i];
            ts[i * 3 + 2] = tri.vertices[0].ID;
            ts[i * 3 + 1] = tri.vertices[1].ID;
            ts[i * 3 + 0] = tri.vertices[2].ID;
        }

        return ts;
    }
}
