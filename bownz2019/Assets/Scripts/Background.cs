﻿using System;
using System.Collections;
using System.Collections.Generic;
using Pooling.Poolers;
using UnityEngine;
using Sirenix.OdinInspector;
using Random = System.Random;

public class Background : MonoBehaviour
{
    public MeshFilter[] meshFilters;

    public Transform bgRoot;

    [SerializeField] private Material background;
    [SerializeField] public Material godRayMaterial;
    [SerializeField] public Material godRayMaterial_Front;

    public SetPooler godRayPool;
                                                           
    private List<GameObject> godRays = new List<GameObject>();

    public Material BackgroundMaterial
    {
        get => background;
        set
        {
            background = value;
            UpdateHeight(value);
        }
    }
    public Material GodRayMaterialMaterial
    {
        get => godRayMaterial;
        set
        {
            godRayMaterial = value;
            UpdateHeight(value);
        }
    }
    public Material GodRayMaterial_FrontMaterial
    {
        get => godRayMaterial_Front;
        set
        {
            godRayMaterial_Front = value;
            UpdateHeight(value);
        }
    }

    [SerializeField] private float raysMultiplicator = 1;
    [SerializeField] private float raysHeightOffset = 100;

    [SerializeField] private float raysMultiplicator_Front = 1;
    [SerializeField] private float raysHeightOffset_Front = 100;

    [SerializeField] private float height;

    private void Awake()
    {
        /*background.EnableKeyword("_Height");
        godRayMaterial.EnableKeyword("_Height");
        godRayMaterial_Front.EnableKeyword("_Height");*/
    }

    [Button]
    public void PrintVertices()
    {
        for (int i = 0; i < meshFilters.Length; i++)
        {
            Debug.Log("Mesh N" + i);
            Vector3[] vertices = meshFilters[i].mesh.vertices;
            for (int j = 0; j < vertices.Length; j++)
            {
                Debug.Log($"J:{j} => {vertices[j]}");
            }
        }
    }

    [Button]
    public void SetPositionAndSize(Rect rect)
    {
        foreach (MeshFilter item in meshFilters)
        {   
            item.mesh.SetVertices(new List<Vector3>(GetCorners(rect)));
            item.mesh.SetUVs(0, GetUvs(rect, item.transform));
            item.mesh.RecalculateBounds();
        }
        height = rect.size.y;
        UpdateHeight(background);
        UpdateHeight(godRayMaterial);
        UpdateHeight(godRayMaterial_Front);

        CreateGodRays(rect);
    }

    private void CreateGodRays(Rect rect)
    {
        ClearCurrentGodRays();

        Vector3 startPos = rect.center - Vector2.up * rect.height / 2;
        Vector3 direction = new Vector2(UnityEngine.Random.Range(-1f, 1f),1).normalized;

        float height = 0;
        float targetHeight = rect.height + 10;
        float angle =  Mathf.Lerp(0,65, Mathf.Abs(direction.x)) * -Mathf.Sign(direction.x);
        while (height < targetHeight)
        {
            GameObject ray = godRayPool.Dequeue().gameObject;

            ray.gameObject.transform.SetParent(transform);
            float lenght = UnityEngine.Random.Range(5f,50f);
            float thickness = UnityEngine.Random.Range(.5f, 2);
               
            height += UnityEngine.Random.Range(10, Mathf.Lerp(30, 10, height / targetHeight));

            ray.gameObject.transform.position = startPos + height * Vector3.up + 8 * Mathf.Sign(direction.x) * Vector3.right;

            ray.gameObject.transform.rotation = Quaternion.Euler(0, 0, angle);
            ray.gameObject.transform.localScale = Vector3.up * lenght + Vector3.right * thickness + Vector3.forward;
            godRays.Add(ray);
        }
    }

    private void ClearCurrentGodRays()
    {
        for (int i = godRays.Count - 1; i >= 0; i--)
        {
            godRayPool.EnqueueObject(godRays[i]);
        }
        godRays.Clear();
    }

    public void UpdateHeight(Material mat)
    {
       if(!mat.IsKeywordEnabled("_Height"))
            mat.EnableKeyword("_Height");

        if (!mat.IsKeywordEnabled("_PlayAreaHeight"))
            mat.EnableKeyword("_PlayAreaHeight");

        mat.SetFloat("_PlayAreaHeight", PlayArea.Area.height*1.5f);

        mat.SetFloat("_Height", height /*(height * raysMultiplicator) + raysHeightOffset*/);
        //background.SetFloat("_Height", (height * raysMultiplicator) + raysHeightOffset);
        //godRayMaterial_Front.SetFloat("_Height", (height * raysMultiplicator_Front) + raysHeightOffset_Front);
    }

    private Vector3[] GetCorners(Rect rect)
    {
        return new Vector3[]
        {
            new Vector3(rect.min.x, rect.min.y),
            new Vector3(rect.max.x, rect.max.y),
            new Vector3(rect.max.x, rect.min.y),
            new Vector3(rect.min.x, rect.max.y)
        };
    }

    private List<Vector2> GetUvs(Rect rect, Transform targetTransform)
    {
        List<Vector2> vectores = new List<Vector2>();
        vectores.Add(targetTransform.TransformPoint(new Vector2(rect.min.x, rect.min.y)));
        vectores.Add(targetTransform.TransformPoint(new Vector2(rect.max.x, rect.max.y)));
        vectores.Add(targetTransform.TransformPoint(new Vector2(rect.max.x, rect.min.y)));
        vectores.Add(targetTransform.TransformPoint(new Vector2(rect.min.x, rect.max.y)));
        return vectores;
    }


    private void RefreshBackgroundLevel(object o, object o1)
    {
        int level = (int) o1;
                                             
        meshFilters[1].gameObject.SetActive(level >= 1);
        meshFilters[2].gameObject.SetActive(level >= 2);
        meshFilters[3].gameObject.SetActive(level >= 3);
    }

    private void RefreshBackgroudEnable(object o, object o1)
    {
        bgRoot.gameObject.SetActive(Settings.Background_Enable);
    }

    private void OnEnable()
    {
        this.AddObserver(RefreshBackgroudEnable, Notifications.Vfx_BackgroundSetEnable);
        this.AddObserver(RefreshBackgroundLevel, Notifications.Vfx_BackgroundSetLevel);
        RefreshBackgroudEnable(null,null);
        RefreshBackgroundLevel(null, Settings.Background_Detail);
    }

    private void OnDisable()
    {
        this.RemoveObserver(RefreshBackgroudEnable, Notifications.Vfx_BackgroundSetEnable);
        this.RemoveObserver(RefreshBackgroundLevel, Notifications.Vfx_BackgroundSetLevel);
    }
}
