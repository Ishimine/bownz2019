﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwapColor : MonoBehaviour, IPressed
{
    public Color colorA;
    public Color colorB;
    public SpriteRenderer render;


    public void Pressed()
    {
        render.color = (render.color == colorA) ? colorB : colorA;
    }
}
