﻿using System;
using System.Collections;
using System.Collections.Generic;
using Firebase.Analytics;
using GooglePlayGames;
using Sirenix.OdinInspector;
using UnityEngine;
using Newtonsoft;
using Newtonsoft.Json;

// ReSharper disable SuggestVarOrType_BuiltInTypes

[CreateAssetMenu]
public class DailyProgress : ScriptableSingleton<DailyProgress>
{
    public override DailyProgress Myself => this;

    public const string fileName = "DailyProgress.savedata";

    public AnimationCurve giftProgressCurve;

    [ShowInInspector, ReadOnly] private HashSet<string> _completed = new HashSet<string>();
    private HashSet<string> Completed
    {
        get
        {
            if(!initialized)
                LoadFromDisk();
            return _completed;
        }
        set
        {
            if(!initialized)
                LoadFromDisk();
            _completed = value;
        }
    }

    public static int CompletedCount => Instance.Completed.Count;
    
    private bool isDirty = false;

    private int currentLevelExp;
    public static int CurrentLevelExp
    {
        get
        {
            if (Instance.isDirty) Instance.CalculateCurrentLevel();
            return Instance.currentLevelExp;
        }
    }

    private int currentRoundLevel;
    public static int CurrentRoundLevel
    {
        get
        {
            if (Instance.isDirty) Instance.CalculateCurrentLevel();
            return Instance.currentRoundLevel;
        }
    }

    private float currentLevel;
    public static float CurrentLevel
    {
        get
        {
            if (Instance.isDirty) Instance.CalculateCurrentLevel();
            return Instance.currentLevel;
        }
    }

    private int expToLevelUp;
    public static int ExpToLevelUp
    {
        get
        {
            if (Instance.isDirty) Instance.CalculateCurrentLevel();
            return Instance.expToLevelUp;
        }
    }

    private bool initialized = false;

    /*public int RewardLevel
    {
        get => Inventory.Get(Inventory.IDs.DailyReward_Level);
        set => Inventory.Set(Inventory.IDs.DailyReward_Level.ToString(), value);
    }*/

    public override void InitializeSingleton()
    {
        base.InitializeSingleton();
        LoadFromDisk();
        if(Settings.UseFirebaseDatabase)
            LoadFromDatabase();

        FirebaseStarter.OnSignStateChange += OnSignStateChange;
    }

    private void OnSignStateChange(bool obj)
    {
        if (obj)
        {
            LoadFromDatabase();
        }
    }


    public float CalculateCurrentLevel()
    {
        return CalculateLevelFromExp(Completed.Count);
    }

    public float CalculateLevelFromExp(int totalExpPoints)
    {
        return CalculateLevelFromExp(totalExpPoints, giftProgressCurve);
    }

    public float CalculateLevelFromExp(int totalExpPoints, AnimationCurve progressCurve)
    {
        float level = 0;
        int remainingExpPoints = totalExpPoints;

        int currentTargetExp;
        do
        {
            currentTargetExp = (int)progressCurve.Evaluate(level);
            if (remainingExpPoints >= currentTargetExp) //Subida de nivel
            {
                Debug.Log($"Subida de Nivel {level} => {level+1} ");
                remainingExpPoints -= currentTargetExp;
                Debug.Log($"TotalExp: {totalExpPoints}  RemainingExp {remainingExpPoints}");
                level++;
            }
        } while (remainingExpPoints >= currentTargetExp);

        Debug.Log($"remainingExpPoints {remainingExpPoints} / expToLevelUp {currentTargetExp}");
        level += (float) remainingExpPoints / currentTargetExp;

        currentLevelExp = remainingExpPoints;
        currentRoundLevel = (int) level;
        this.expToLevelUp = currentTargetExp;

        Debug.Log("End Level: " + level);

        currentLevel = level;
        isDirty = false;
        return level;
    }

    public static void Add(DateTime dateTime, string extraId)
    {
        Instance._Add(dateTime, extraId);
    }
                    
    public static bool Contains(DateTime dateTime, string extraId)
    {
       return Instance._Contains(dateTime,  extraId);
    }

    public bool _Contains(DateTime dateTime, string extraId)
    {
        return _Contains(DateToString(dateTime), extraId);
    }

    [Button]
    public void _Add(DateTime dateTime, string extraId)
    {
        _Add(DateToString(dateTime), extraId);
    }

    public void _Add(string dateTime, string extraId)
    {
        if(_Contains(dateTime,extraId))return;
        _completed.Add($"{dateTime}_ {extraId}");
        Save();
        SaveToLeaderboard();
        
        isDirty = true;
        CheckForLevelUp();
        
        AnalyticsManager.SendEvent_DailyLevelsCompleted(DailyProgress.CompletedCount);
    }

    private void Save()
    {
        SaveToDisk();
        if(Settings.UseFirebaseDatabase)
            SaveToDatabase();
    }

    private void CheckForLevelUp()
    {
        float lastLevel = CalculateLevelFromExp(Mathf.Max(Completed.Count-1,0));
        float currentLevel = CalculateLevelFromExp(Completed.Count);
                
        Debug.Log($"LastLevel:{lastLevel} CurrentLevel: {currentLevel}");

        if ((int) lastLevel < (int) currentLevel)
        {
            AnalyticsManager.SendEvent_LevelUp("DailyMode", (int)currentLevel);
            Inventory.Add(Inventory.IDs.GiftBox, 1, true);
            this.PostNotification(Notifications.ShowGiftUnlockSuccess, expToLevelUp);
        }
        else
            this.PostNotification(Notifications.ShowGiftUnlockProgress, new Tuple<int,int, int>(Mathf.Max(currentLevelExp - 1,0), currentLevelExp, expToLevelUp));
    }

    private string DateToString(DateTime dateTime)
    {
        return $"{dateTime.Year}_{dateTime.DayOfYear}";
    }

    public bool _Contains(string dateTime, string extraId)
    {
        return Completed.Contains($"{dateTime}_ {extraId}");
    }

    [Button]
    public void SaveToDisk()
    {
        AdministradorDeDatos.GuardarDatos(ref _completed, fileName);
    }
    [Button]
    public void LoadFromDisk()
    {
        AdministradorDeDatos.CargarDatos(ref _completed, fileName);
        initialized = true;

    }
    [Button]
    public void Clear()
    {
        Completed.Clear();
    }

    private void OnEnable()
    {
    }

    public void SaveToLeaderboard()
    {
        if (Social.Active.localUser.authenticated)
        {
            AnalyticsManager.SendEvent_PostScore("DailyMode", Completed.Count);
            Debug.Log("**************    SendingScore");
            Social.Active.ReportScore(Completed.Count, GPGSIds.leaderboard_daily, OnReportScoreCallback);
            ((PlayGamesPlatform)Social.Active).ReportScore(Completed.Count, GPGSIds.leaderboard_daily, OnReportScoreCallback);
        }
        else
        {
            Debug.Log("**************    Player Not authenticated");
        }
    }

    private void OnReportScoreCallback(bool sucess)
    {
        Debug.Log($"**************    Report score was :{sucess}");
    }


    [Button]
    public string SaveToJson()
    {
        string txt = JsonConvert.SerializeObject(_completed);
        Debug.Log(txt);
        return txt;
    }

    [Button]
    public void SaveToDatabase()
    {
        Debug.Log("Saving to database");
        FirebaseStarter.SaveDataAsJson_WithTransaction("DailyProgress",SaveToJson(), DataMerger);
    }

    [Button]
    public void LoadFromDatabase()
    {
        Debug.Log("Loading fron database");
        FirebaseStarter.LoadDataFromDatabase("DailyProgress", OnLoadFromDatabaseCallback);
    }

    private void OnLoadFromDatabaseCallback(bool sucess, string arg)
    {
        if (sucess)
        {
            Debug.Log("Load DailyProgress From Database Complete");
            _completed = JsonConvert.DeserializeObject<HashSet<string>>(arg);
        }
        else
            Debug.Log("Load DailyProgress From Database Failed");

    }

    private string DataMerger(string oData, string nData)
    {
       HashSet<string> oldData = JsonConvert.DeserializeObject<HashSet<string>>(oData);
       HashSet<string> newData = JsonConvert.DeserializeObject<HashSet<string>>(nData);

       foreach (string s in oldData)
           newData.Add(s);
                                                       
       return JsonConvert.SerializeObject(newData);
    }

}

