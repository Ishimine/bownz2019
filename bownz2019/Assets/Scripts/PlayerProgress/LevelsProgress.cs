﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu]
public class LevelsProgress : ScriptableSingleton<LevelsProgress>
{
    public override LevelsProgress Myself => this;

    [ShowInInspector,ReadOnly] private Dictionary<string, LevelProgress> data = new Dictionary<string, LevelProgress>();

    public override void InitializeSingleton()
    {
        base.InitializeSingleton();
        _LoadData();
    }

    /// <summary>
    /// En caso de existir un dato con key similar, actualizara los valores en los que hubo progreso. El resto permanecera igual
    /// </summary>
    public static void AddLevelProgress(string gameModeId, int levelIndex, bool isChallenge, LevelProgress data, bool saveDataToDisk = false)
    {
       string key = GetKeyFrom(gameModeId, levelIndex, isChallenge);
        Debug.Log("Saved " + key + " => " + data);
        if (Instance.data.ContainsKey(key))
            Instance.data[key] = Instance.data[key].Merge_KeepBestScore(data);
        else
            Instance.data.Add(key, data);

        if (saveDataToDisk) Instance._SaveData();
    }

    public static bool Contains(string id)
    {
        return Instance.data.ContainsKey(id);
    }

   public static LevelProgress Get(string gameModeId, int levelIndex, bool isChallenge)
   {
       string key = GetKeyFrom(gameModeId, levelIndex, isChallenge);
       return Instance.data.ContainsKey(key) ? Instance.data[key] : new LevelProgress(int.MaxValue,float.MaxValue, int.MaxValue,0);
   }


   public static string GetKeyFrom(string gameModeId, int levelIndex, bool isChallenge)
   {
       string key = $"{gameModeId}_{levelIndex}";
       if (isChallenge) key += "_Challenge";
       return key;
   }

   public static void OverrideLevelProgress(string key, LevelProgress data, bool saveDataToDisk)
    {
        if (Instance.data.ContainsKey(key))
            Instance.data[key] = data;
        else
            Instance.data.Add(key, data);

        if (saveDataToDisk) Instance._SaveData();
    }

    [Button]
    public void ClearData()
    {
        data = new Dictionary<string, LevelProgress>();
    }

    [Button]
    public void _SaveData()
    {
        AdministradorDeDatos.GuardarDatos(ref data, "LvlsProgress.Dat");
    }


    [Button]
    public void _LoadData()
    {
        AdministradorDeDatos.CargarDatos(ref data, "LvlsProgress.Dat");
    }
}
