﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System;
using Sirenix.OdinInspector;

public class UI_ObjectiveStar : MonoBehaviour
{
    private CodeAnimator codeAnimator = new CodeAnimator();

    public Color cIncompleted = Color.gray;
    public Color cFail = Color.black;
    public Color cCompleted = Color.white;

    public Image rStar;
    public Image rIcon;

    public float shineStartDuration = .2f;
    public float shineOutStartDuration = .8f;
    public AnimationCurve colorCurve;

    private Action onAnimationDone;
    public Action OnAnimationDone { get { return onAnimationDone; } set { onAnimationDone = value; } }


    public AnimationCurve curveIn;
    public float appearDuration = .3f;

    public AnimationCurve curveOut;
    public float disappearDuration = .5f;

    IEnumerator rutine;


    IEnumerator AppearRutine()
    {
        Vector2 startSize = transform.localScale;
        float x = 0;
        float duration = .4f;
        do
        {
            x += Time.deltaTime / duration;
            transform.localScale = Vector3.LerpUnclamped(startSize, Vector3.one, x);
            yield return null;
        } while (x < 1);
    }

   /* [Button]
    public void Dissapear(bool sendAnimationDone)
    {
        Vector2 startSize = transform.localScale;
        codeAnimator.StartAnimacion(this,
           x =>
           {
               transform.localScale = Vector3.Lerp(startSize, Vector3.zero, x);
           }, DeltaTimeType.deltaTime, AnimationType.Simple, curveOut,
           ()=>
           {
               if(sendAnimationDone) onAnimationDone?.Invoke();
           }, disappearDuration);
    }*/

    public void Initialize(bool isCompleted)
    {
        gameObject.SetActive(true);
        transform.localScale = Vector3.one;

        rIcon.gameObject.SetActive(!isCompleted);
        rStar.transform.localScale = Vector3.one;
        rStar.color = (isCompleted)?cCompleted:cIncompleted;
    }

    [Button]
    public void TransitionToCompleted(Action postAction = null)
    {
        rIcon.gameObject.SetActive(false);
        if (rutine != null) StopCoroutine(rutine);
        rutine = TransitionToCompletedRutine();
        StartCoroutine(rutine);
    }

    [Button]
    public void TransitionToFail(Action postAction = null)
    {
        if (rutine != null) StopCoroutine(rutine);
        rutine = TransitionToFailRutine();
        StartCoroutine(rutine);
    }

    IEnumerator TransitionToFailRutine()
    {
        yield return GoToColor(cIncompleted);
        onAnimationDone?.Invoke();
    }

    IEnumerator TransitionToCompletedRutine()
    {
        //yield return AppearRutine();
        yield return GoToColor(Color.white);
        yield return ShineEndRutine();
        onAnimationDone?.Invoke();
    }

    IEnumerator GoToColor(Color targetColor)
    {
        float x = 0;
        Color startColor = rStar.color;
        do
        {
            x += Time.deltaTime / shineStartDuration;
            rStar.color = Color.Lerp(startColor, targetColor, x);
            yield return null;
        } while (x < 1);
    }

    IEnumerator ShineEndRutine()
    {
        Color startColor = rStar.color;
        float x = 0;
        do
        {
            x += Time.deltaTime / shineOutStartDuration;
            rStar.color = Color.Lerp(startColor, cCompleted, colorCurve.Evaluate(x));
            yield return null;
        } while (x < 1);
    }
}
