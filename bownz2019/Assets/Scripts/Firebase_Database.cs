﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Database;
using Firebase.Auth;
using Firebase.Unity.Editor;
using Sirenix.OdinInspector;
using System;

public class Firebase_Database : MonoBehaviour
{
    public Canvas_FirebaseAuthentication Canvas_FirebaseAuthentication;

    FirebaseAuth auth;
    FirebaseApp app;
    DependencyStatus dependencyStatus = DependencyStatus.UnavailableOther;

    private const int MaxScores = 5;

    ArrayList leaderBoard;

    public string displayScores;

    public int score;

    public FirebaseDatabase database;

    public string dataName;
    public int dataValue;

    public object asyncData;

    private void Awake()
    {
  //      Canvas_FirebaseAuthentication.OnLogStateChange += LogStateChange;
    }

    private void LogStateChange(bool obj)
    {
        StartDatabase();
    }

    private void StartDatabase()
    {
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
            dependencyStatus = task.Result;
            if (dependencyStatus == DependencyStatus.Available)
            {
                Initialize();
            }
            else
            {
                Debug.LogError(
                  "Could not resolve all Firebase dependencies: " + dependencyStatus);
            }
        });
    }

    public void Initialize()
    {
        app = FirebaseApp.DefaultInstance;
        app.SetEditorDatabaseUrl("https://bownztest-32057296.firebaseio.com/");

        if (app.Options.DatabaseUrl != null) app.SetEditorDatabaseUrl(app.Options.DatabaseUrl);

        auth = FirebaseAuth.DefaultInstance;
        database = FirebaseDatabase.DefaultInstance;
    }

    public void StartListerner()
    {
        FirebaseDatabase.DefaultInstance
            .GetReference("Leaders").OrderByChild("score")
            .ValueChanged += OnValueChanged;
    }

    private void OnValueChanged(object sender2, ValueChangedEventArgs e2)
    {
        if (e2.DatabaseError != null)
        {
            Debug.LogError(e2.DatabaseError.Message);
            return;
        }
        Debug.Log("Received values for Leaders.");

        #region Manipulamos los valores modificados
        string title = leaderBoard[0].ToString();
        leaderBoard.Clear();
        leaderBoard.Add(title);
        if (e2.Snapshot != null && e2.Snapshot.ChildrenCount > 0)
        {
            foreach (var childSnapshot in e2.Snapshot.Children)
            {
                if (childSnapshot.Child("score") == null
                 || childSnapshot.Child("score").Value == null)
                {
                    Debug.LogError("Bad data in sample.  Did you forget to call SetEditorDatabaseUrl with your project id?");
                    break;
                }
                else
                {
                    Debug.Log("Leaders entry : " +
                     childSnapshot.Child("email").Value.ToString() + " - " +
                     childSnapshot.Child("score").Value.ToString());
                    leaderBoard.Insert(1, childSnapshot.Child("score").Value.ToString()
                     + "  " + childSnapshot.Child("email").Value.ToString());

                    displayScores = "";
                    foreach (string item in leaderBoard)
                    {
                        displayScores += "\n" + item;
                    }
                }
            }
        }
        #endregion
    }

    [Button]
    public void SendDataAsync(string[] pathNodes, object value)
    {
        DatabaseReference databaseReference = GetReference(pathNodes);
        databaseReference.SetValueAsync(value);
    }

    private DatabaseReference GetReference(string[] pathNodes)
    {
        DatabaseReference databaseReference = database.RootReference;
        for (int i = 0; i < pathNodes.Length; i++)
            databaseReference = databaseReference.Child(pathNodes[i]);
        return databaseReference;
    }


    public void PushData(string[] pathNodes, object value)
    {
        database.RootReference.Push();

    }

    [Button]
    public void LoadDataAsync(string[] pathNodes)
    {
        DatabaseReference databaseReference = GetReference(pathNodes);
        databaseReference.GetValueAsync().ContinueWith(
            task =>
            {
                if (task.IsCompleted)
                    Debug.Log("IsCompleted");
                else
                    Debug.Log("Failed");

                asyncData = task.Result.Value.ToString();
                score = (int)task.Result.Value;
            });
    }


    [Button]
    public void LoadData(string dataName)
    {
        database.GetReference("users").Child(auth.CurrentUser.UserId).Child(dataName).GetValueAsync().ContinueWith(
            task =>
            {
                if (task.IsCompleted)
                    Debug.Log("IsCompleted");
                else
                    Debug.Log("Failed");

                asyncData = task.Result.Value.ToString();
                score = (int)task.Result.Value;
            });
    }


    TransactionResult AddScoreTransaction(MutableData mutableData)
    {
        List<object> leaders = mutableData.Value as List<object>;

        if (leaders == null)
        {
            leaders = new List<object>();
        }
        else if (mutableData.ChildrenCount >= MaxScores)
        {
            // If the current list of scores is greater or equal to our maximum allowed number,
            // we see if the new score should be added and remove the lowest existing score.
            long minScore = long.MaxValue;
            object minVal = null;
            foreach (var child in leaders)
            {
                if (!(child is Dictionary<string, object>))
                    continue;
                long childScore = (long)((Dictionary<string, object>)child)["score"];
                if (childScore < minScore)
                {
                    minScore = childScore;
                    minVal = child;
                }
            }
            // If the new score is lower than the current minimum, we abort.
            if (minScore > score)
            {
                return TransactionResult.Abort();
            }
            // Otherwise, we remove the current lowest to be replaced with the new score.
            leaders.Remove(minVal);
        }

        // Now we add the new score as a new entry that contains the email address and score.
        Dictionary<string, object> newScoreMap = new Dictionary<string, object>();
        newScoreMap["score"] = score;
        newScoreMap["email"] = name;
        leaders.Add(newScoreMap);

        // You must set the Value to indicate data at that location has changed.
        mutableData.Value = leaders;
        //return and log success
        return TransactionResult.Success(mutableData);
    }

    [Button]
    public void AddScore(string nameTxt, int scoreValue)
    {
        string name = nameTxt;
        int score = scoreValue;

        if (score == 0 || string.IsNullOrEmpty(name))
        {
            Debug.Log("invalid score or email.");
            return;
        }
        Debug.Log(string.Format("Attempting to add score {0} {1}",
          name, score.ToString()));

        DatabaseReference reference = FirebaseDatabase.DefaultInstance.GetReference("Leaders");

        Debug.Log("Running Transaction...");
        // Use a transaction to ensure that we do not encounter issues with
        // simultaneous updates that otherwise might create more than MaxScores top scores.
        reference.RunTransaction(AddScoreTransaction)
          .ContinueWith(task => {
              if (task.Exception != null)
              {
                  Debug.Log(task.Exception.ToString());
              }
              else if (task.IsCompleted)
              {
                  Debug.Log("Transaction complete.");
              }
          });
    }

   

}
