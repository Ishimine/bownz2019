﻿using System;
using System.Collections;
using System.Collections.Generic;
using Procedural;
using UnityEngine;
using UnityEngine.UI;

public class Canvas_DailyProcedural : MonoBehaviour
{
    public Text text;

    public InputField inputField;

    public Button nextDayButton;
    public Button previousDayButton;

    [SerializeField]
    private string seed;
    public string Seed
    {
        get { return seed; }
        set
        {
            seed = value;
            blueprint.Seed = float.Parse(seed);
        }
    }

    private void OnEnable()
    {
        Seed = DailyMode.CurrentDailySeed.ToString();
        inputField.text = DailyMode.CurrentDailySeed.ToString("0.");
        text.text = "DailySeed: " + Seed.ToString();

        this.AddObserver(OnDailyDateChange, Notifications.OnDailyDateChange);
        RefreshButtons();
    }

    private void OnDisable()
    {
        this.RemoveObserver(OnDailyDateChange, Notifications.OnDailyDateChange);
    }

    [SerializeField] private SeudoBlueprint blueprint;
    public SeudoBlueprint Blueprint { get => blueprint;  set => blueprint = value;  }

    public void LoadLevel()
    {
        LoadLevel(blueprint);
    }

    public void LoadLevel(SeudoBlueprint nBueprint)
    {
        this.PostNotification(Notifications.ProceduralLevel_Request, new ProceduralLevelRequest(nBueprint.CreateBlueprint(), "NullLevel" ));
    }

    public void GoToPreviousDay()
    {
        this.PostNotification(Notifications.PreviousDailyDate);
    }

    public void GoToNextDay()
    {
        this.PostNotification(Notifications.NextDailyDate);
    }
     
    public void GoToShop()
    {
        this.PostNotification(Notifications.GoToShop);
    }
     
    private void OnDailyDateChange(object arg1, object arg2)
    {
        RefreshButtons();
    }

    private void RefreshButtons()
    {
        nextDayButton.gameObject.SetActive(DailyMode.CurrentDate != DateTime.Today);

        //previousDayButton.gameObject.SetActive(DailyMode.CurrentDate.DayOfYear != 1); //Solo se habilita dentro del mismo año
        previousDayButton.gameObject.SetActive((DateTime.Today.DayOfYear - DailyMode.CurrentDate.DayOfYear) < 7); //Solo se habilita si hay una diferencia de al menos 3 dias
    }
}

