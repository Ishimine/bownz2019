﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

public class MenuSelectionCanvas : MonoSingleton<MenuSelectionCanvas>
{
    [SerializeField] private Text txt = null;
    public Text Txt
    {
        get { return txt; }
    }

    [SerializeField] private GameObject leftButton = null;
    [SerializeField] private GameObject rightButton = null;

    private Action onNextPressed;
    public Action OnNextPressed
    {
        get { return onNextPressed; }
        set { onNextPressed = value; }
    }

    private Action onPreviousPressed;
    public Action OnPreviousPressed
    {
        get { return onPreviousPressed; }
        set { onPreviousPressed = value; }
    }

    public override MenuSelectionCanvas Myself => this;

    public void _Set(string modeName, Action onPreviousPressed, Action onNextPressed)
    {
        txt.text = modeName;
        OnPreviousPressed = onPreviousPressed;
        OnNextPressed = onNextPressed;

        leftButton.SetActive(OnPreviousPressed != null);
        rightButton.SetActive(OnNextPressed != null);
        gameObject.SetActive(true);
        UI_ScreenTransitioner.In();
    }

    public static void Set(string modeName, Action onPreviousPressed, Action onNextPressed)
    {
        Instance._Set(modeName, onPreviousPressed, onNextPressed);
    }

    [Button]
    public void NextGameMode()
    {
        UI_ScreenTransitioner.Out(OnNextPressed);
    }

    [Button]
    public void PreviousGameMode()
    {
        UI_ScreenTransitioner.Out(OnPreviousPressed);
    }

    public static void Initialize()
    {
        Debug.Log(" Initialize enter");
        if(Instance == null)
        {
            GameObjectLibrary.Instantiate("MenuSelectionCanvas");
        Debug.Log(" Initialize mid");
        }
        Debug.Log(" Initialize exit");
    }
}
