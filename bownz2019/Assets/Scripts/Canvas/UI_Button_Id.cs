﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Sirenix.OdinInspector;

public class UI_Button_Id : MonoBehaviour
{
    public Text txt;

    public Action<UI_Button_Id> OnIdChange { get;  set; }
    public LevelProgress TargetScore { get; private set; }
    public LevelProgress PlayerScore { get; private set; }
    public LevelRequest LevelRequest { get; private set; }
    public Action<int> OnPressed { get; set; }
    
    [SerializeField] private int index;
    public int Index
    {
        get => index;
        private set
        {
            index = value;
            if (txt != null) txt.text = index.ToString();
            OnIdChange?.Invoke(this);
        }
    }

    public void Pressed()
    {
        OnPressed?.Invoke(Index);
    }

    public void Initialize(int i, LevelProgress levelObjective, LevelProgress playerScore, LevelRequest levelRequest, Action<int> showLevelInfo)
    {
        TargetScore = levelObjective;
        PlayerScore = playerScore;
        LevelRequest = levelRequest;
        OnPressed = showLevelInfo;
        Index = i;
    }
}
