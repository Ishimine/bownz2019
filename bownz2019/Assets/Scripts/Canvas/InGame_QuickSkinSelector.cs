﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class InGame_QuickSkinSelector : MonoBehaviour
{
    [SerializeField] private GameObject ballPrefab = null;

    public float ballSize = 4;

    private BallStateMachine ballStateMachine = null;

    private CodeAnimator codeAnimator = new CodeAnimator();

    private void Awake()
    {
        ballStateMachine = Instantiate(ballPrefab, transform).GetComponent<BallStateMachine>();

        ballStateMachine.ballSize = ballSize;
        ballStateMachine.AppearToPosing();

        ballStateMachine.SkinComponent.UsePlayerSelection = false;
        ballStateMachine.TrailComponent.UsePlayerSelection = false;

        ballStateMachine.gameObject.transform.position = Vector3.down * 2;
    }

    private void OnEnable()
    {
        BallSkin.OnPlayerSelectedSkinChange -= OnPlayerSelectedSkinChange_Response;
        BallSkin.OnPlayerSelectedSkinChange += OnPlayerSelectedSkinChange_Response;
    }
 
    private void OnDisable()
    {
        BallSkin.OnPlayerSelectedSkinChange -= OnPlayerSelectedSkinChange_Response;
    }

    private void OnPlayerSelectedSkinChange_Response(BallSkin obj)
    {
        TransitionToSkin(obj);
    }

    private void TransitionToSkin(BallSkin obj)
    {
        ballStateMachine.Disappear(
            () =>
            {
                ballStateMachine.SkinComponent.SetSkin(BallSkin.PlayerSelectedSkin);
                ballStateMachine.AppearToPosing();
            });
    }
}
