﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Canvas_SlowMotionModifier : MonoBehaviour
{
    public bool destroyWhenEmpty = false;
    public UI_ConteinerOpenClose openClose;
    public Text txtCharges;

    private void Awake()
    {
        this.AddObserver(OnEquipped, Notifications.Modifier_SlowMotionOnSwipe_Equipped);
        this.AddObserver(OnChargeUsed, Notifications.Modifier_SlowMotionOnSwipe_ChargeUsed);
    }
    private void Start()
    {
        openClose.Open();
    }
    private void OnEnable()
    {
    }

    private void OnDisable()
    {
        this.RemoveObserver(OnEquipped, Notifications.Modifier_SlowMotionOnSwipe_Equipped);
        this.RemoveObserver(OnChargeUsed, Notifications.Modifier_SlowMotionOnSwipe_ChargeUsed);
    }

    private void OnEquipped(object arg1, object arg2)
    {
        openClose.Open();
    }

    private void OnChargeUsed(object arg1, object arg2)
    {
        int value = (int)arg2;
        txtCharges.text = value.ToString();
        if (value == 0)
        {
            if (destroyWhenEmpty)
                openClose.Close(() => Destroy(((SlowMotionWhileSwipe_Modificator)arg1).gameObject));
            else
                openClose.Close();
        }
    }
}
