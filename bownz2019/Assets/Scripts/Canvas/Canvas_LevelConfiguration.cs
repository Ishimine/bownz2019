﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using System;
using Procedural;

public class Canvas_LevelConfiguration : MonoBehaviour
{
    public Text heightCounter;
    public Text seedText;

    private Action<List<STag>> onAddTagsChange;
    public Action<List<STag>> OnAddTagsChange       { get { return onAddTagsChange; }       set { onAddTagsChange = value; } }

    private Action<List<STag>> onRemoveTagsChange;
    public Action<List<STag>> OnRemoveTagsChange    { get { return onRemoveTagsChange; }    set { onRemoveTagsChange = value; } }

    [ShowInInspector, ReadOnly] private List<STag> removeTags = new List<STag>();
    public List<STag> RemoveTags
    {
        get { return removeTags; }
        set
        {
            removeTags = value;
            OnRemoveTagsChange?.Invoke(value);
        }
    }

    [ShowInInspector,ReadOnly]  private List<STag> addTags = new List<STag>();
    public List<STag> AddTags { get { return addTags; }
        set
        {
            addTags = value;
            OnRemoveTagsChange?.Invoke(value);
        }
    }

    public UI_ConteinerOpenClose manualConfig;
    public UI_ConteinerOpenClose seededLevels;

    public void SelectedForRemove(STag tag)
    {
        if (RemoveTags.Contains(tag))
            RemoveTags.Remove(tag);
        else
            RemoveTags.Add(tag);

        OnRemoveTagsChange?.Invoke(RemoveTags);
    }

    public void SelectedForAdd(STag tag )
    {
        if (AddTags.Contains(tag))
            AddTags.Remove(tag);
        else
            AddTags.Add(tag);

        OnAddTagsChange?.Invoke(AddTags);
    }

    public void OpenManual()
    {
        manualConfig.Open();
        seededLevels.Close();
    }

    public void OpenSeeded()
    {
        manualConfig.Close();
        seededLevels.Open();
    }

    public void Close()
    {
        manualConfig.Close();
        seededLevels.Close();
    }

    public string Seed
    {
        set { blueprint.Seed =  int.Parse(value); }
    }

     /*
    public float Height
    {
        set
        {
            int endValue = (int)Mathf.Lerp(Settings_Procedural.MinMaxLevelHeight.x, Settings_Procedural.MinMaxLevelHeight.y, value);
            blueprint.Height = endValue;
            heightCounter.text = endValue.ToString();
        }
    }
           */

    [SerializeField] private SeudoBlueprintTypeB blueprint;
    public SeudoBlueprint Blueprint
    {
        get { return blueprint; }
        set { Blueprint = value; }
    }

    public void RandomSeed()
    {
        int seed = UnityEngine.Random.Range(1000, 9999);
        seedText.text = seed.ToString();
        blueprint.Seed = seed;
    }

    public void LoadLevel()
    {
        blueprint.includeModules = addTags.ToArray();
        blueprint.removeModules = removeTags.ToArray();

        this.PostNotification(Notifications.ProceduralLevel_Request, blueprint.CreateBlueprint());
    }
}
