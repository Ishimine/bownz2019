﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using System;

public class Canvas_Level_WinScreen : MonoBehaviour
{
    public UI_ConteinerOpenClose conteinerOpenClose;

    public UI_Medal_ScoreCounter platformsTimer;
    public UI_Medal_ScoreCounter timeTimer;
    public UI_Medal_ScoreCounter deathTimer;

    public ObjectiveStars starsObjective;

    public float counterAnimationDuration = .75f;

    private IEnumerator rutine;

    public AnimationCurve timerAccelerationCurve;

    LevelProgress objectives;
    LevelProgress score;
    LevelProgress oldScore;

    public UI_MoneyResume moneyResume;

    private UI_MoneyResume.MoneyResumeElement[] moneyResumeElements;

    public bool autoLoadNextLevel = true;
    public float waitTimeForNextLevel = 3;
    private CodeAnimator codeAnimator = new CodeAnimator();

    public void Show()
    {
        gameObject.SetActive(true);
        conteinerOpenClose.Open(platformsTimer.StartAnimation);
    }

    public void Initialize(LevelProgress nObjectives, LevelProgress nScore, LevelProgress nOldScore)
    {
        gameObject.SetActive(true);
        moneyResumeElements = CalculateMoneyResume(nObjectives, nScore, nOldScore);

        Settings.UiInteraction = false;
        platformsTimer.Initialize(nOldScore.Platforms, nScore.Platforms, nObjectives.Platforms);
        timeTimer.Initialize(nOldScore.Time, nScore.Time, nObjectives.Time);
        deathTimer.Initialize(nOldScore.Deaths, nScore.Deaths, nObjectives.Deaths);

        platformsTimer.OnEnd = timeTimer.StartAnimation;
        timeTimer.OnEnd = deathTimer.StartAnimation;
        deathTimer.OnEnd = ObjectiveAnimationsEnd;

    }


    private UI_MoneyResume.MoneyResumeElement[] CalculateMoneyResume(LevelProgress nObjectives, LevelProgress nScore,
        LevelProgress nOldScore)
    {
        List<UI_MoneyResume.MoneyResumeElement> elements = new List<UI_MoneyResume.MoneyResumeElement>();

        if (nOldScore.IsNullScore())
            elements.Add(new UI_MoneyResume.MoneyResumeElement("FIRST TIME COMPLETED", 50,
                UI_MoneyResume.Calculation.Addition));
        else
            elements.Add(new UI_MoneyResume.MoneyResumeElement("LEVEL COMPLETED", 10,
                UI_MoneyResume.Calculation.Addition));

        int starsGained = 0;

        if (nOldScore.Time > nObjectives.Time && nScore.Time <= nObjectives.Time)
        {        
            //elements.Add(new UI_MoneyResume.MoneyResumeElement("☆Time", 10, UI_MoneyResume.Calculation.Addition));
            starsGained++;
        }

        if (nOldScore.Platforms > nObjectives.Platforms && nScore.Platforms <= nObjectives.Platforms)
        {
            starsGained++;
            // elements.Add(new UI_MoneyResume.MoneyResumeElement("☆Swipes", 10, UI_MoneyResume.Calculation.Addition));
        }
        if (nOldScore.Deaths > nObjectives.Deaths && nScore.Deaths <= nObjectives.Deaths) 
        {
            starsGained++;
            //       elements.Add(new UI_MoneyResume.MoneyResumeElement("☆Dead", 10, UI_MoneyResume.Calculation.Addition));
        }

        if(starsGained > 0) elements.Add(new UI_MoneyResume.MoneyResumeElement($"GOLDEN STARx{starsGained}", starsGained*20, UI_MoneyResume.Calculation.Addition) );

        if(starsGained > 0 && nObjectives.IsCompletedBy(nScore.Merge_KeepBestScore(nOldScore), false))
            elements.Add(new UI_MoneyResume.MoneyResumeElement("PERFECT LEVEL", 100, UI_MoneyResume.Calculation.Addition) );


         return elements.ToArray();
    }

    private void OnWinScreenResponse(object arg1, object arg2)
    {
        conteinerOpenClose.gameObject.SetActive(true);
        conteinerOpenClose.Open();
    }

    public void Menu()
    {
        if (!Settings.UiInteraction) return;

        conteinerOpenClose.Close();
        UI_ScreenTransitioner.Out(
            () =>
            {
                this.PostNotification(Notifications.GameModeRequest_BackToLevelSelection);
                UI_ScreenTransitioner.In();
            }
            );
    }

    public void Retry()
    {
        if (!Settings.UiInteraction) return;

        conteinerOpenClose.Close();
        UI_ScreenTransitioner.Out(
         () =>
         {
             this.PostNotification(Notifications.GameModeRequest_Retry);
             UI_ScreenTransitioner.In();
         }
         );
    }

    public void Next()
    {
        if (!Settings.UiInteraction) return;

        conteinerOpenClose.Close();
        UI_ScreenTransitioner.Out(
        () =>
        {
            this.PostNotification(Notifications.GameModeRequest_Next);
            UI_ScreenTransitioner.In();
        }
        );
    }

    [Button]
    public void ObjectiveAnimationsEnd()
    {
        moneyResume.Clear();
        moneyResume.OnAnimationEnd += AllAnimationEnd;
        moneyResume.AddElements (moneyResumeElements);
    }

    public void AllAnimationEnd()
    {
        moneyResume.OnAnimationEnd -= AllAnimationEnd;

        if(moneyResume.CurrentValue > 0)
            Inventory.Add(0, (int)moneyResume.CurrentValue, true);

        Settings.UiInteraction = true;

        if (waitTimeForNextLevel > 0 && autoLoadNextLevel && DoesNextLevelExist())
        {
            codeAnimator.StartWaitAndExecute(this, waitTimeForNextLevel, Next, false);
        }
    }
    public bool DoesNextLevelExist()
    {
        BaseException queryBool = new BaseException(false);
        this.PostNotification(Notifications.Query_DoesNextLevelExist, queryBool);
        return queryBool.toggle;
    }
}
