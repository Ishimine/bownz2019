﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Canvas_WinScreen_VerticalLevels : MonoBehaviour
{
    public UI_ConteinerOpenClose conteinerOpenClose;

    private void OnEnable()
    {
    }

    private void OnDisable()
    {
    }

    private void OnWinScreenResponse(object arg1, object arg2)
    {
        conteinerOpenClose.gameObject.SetActive(true);
        conteinerOpenClose.Open();
    }

    public void Menu()
    {
        conteinerOpenClose.Close();
        UI_ScreenTransitioner.Out(
            ()=>
            {
                this.PostNotification(Notifications.GameModeRequest_BackToLevelSelection);
                UI_ScreenTransitioner.In();
            }
            );
    }

    public void Retry()
    {
        conteinerOpenClose.Close();
        UI_ScreenTransitioner.Out(
         () =>
         {
             this.PostNotification(Notifications.GameModeRequest_Retry);
             UI_ScreenTransitioner.In();
         }
         );
    }

    public void Next()
    {
        conteinerOpenClose.Close();
        UI_ScreenTransitioner.Out(
        () =>
        {
            this.PostNotification(Notifications.GameModeRequest_Next);
            UI_ScreenTransitioner.In();
        }
        );
    }
}
