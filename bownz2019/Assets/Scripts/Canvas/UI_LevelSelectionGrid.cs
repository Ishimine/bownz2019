﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
public class UI_LevelSelectionGrid : MonoBehaviour
{
    public string GameModeId
    {
        get => MyGameMode.GameModeID;
    }

    [Min(0)]
    private int currentPage;
    public int CurrentPage
    {
        get { return currentPage; }
        set { currentPage = value; }
    }

    private LevelGameModeState myGameMode;
    public LevelGameModeState MyGameMode
    {
        get { return myGameMode; }
        set { myGameMode = value; }
    }

    public Transform gridTransform; 

    [SerializeField] private GameObject buttonPrefab = null;

    [MinValue(1)] public int buttonsPerPage;
    [MinValue(1)] public int maxPages;
    [SerializeField] private int totalButtons;

    public UI_Button_Id[] button_Ids;

    [Button]
    public void Initialize(int totalButtons)
    {
        this.totalButtons = totalButtons;
        DestroyCurrent();
        int buttonsToSpawn = Mathf.Min(buttonsPerPage,totalButtons);
        button_Ids = new UI_Button_Id[buttonsToSpawn];
        for (int i = 0; i < buttonsToSpawn; i++)
        {
            button_Ids[i] = Instantiate(buttonPrefab, gridTransform).GetComponent<UI_Button_Id>();
            button_Ids[i].gameObject.SetActive(false);
        }
        maxPages = Mathf.CeilToInt((float)totalButtons / buttonsPerPage);
    }

    [Button]
    public void SetPage(int pageNumber)
    {
        pageNumber = (int)Mathf.Repeat(pageNumber, maxPages);
        currentPage = pageNumber;
        int firstPageButton = pageNumber * buttonsPerPage;
        int lastPageButton = Mathf.Min(totalButtons, (pageNumber+1) * buttonsPerPage);
        int buttonsInPage = lastPageButton - firstPageButton;

        for (int i = 0; i < button_Ids.Length; i++)
        {
            if (i < buttonsInPage)
            {
                button_Ids[i].gameObject.SetActive(true);
                int index = firstPageButton + i;
                button_Ids[i].Initialize(
                    index,
                    GetLevelObjective(index),
                    GetPlayerScore(index),
                    new LevelRequest(GameModeId, index), 
                    ShowLevelInfo);
            }
            else
                button_Ids[i].gameObject.SetActive(false);
        }
        this.PostNotification(Notifications.LevelFoldoutInfoClose);
    }

    private LevelProgress GetPlayerScore(int index)
    {
        return MyGameMode.GetPlayerScore(index, Settings.IsChallengeMode);
    }

    private LevelProgress GetLevelObjective(int index)
    {
        return MyGameMode.GetLevelObjective(index, Settings.IsChallengeMode);
    }

    [Button]
    public void NextPage()
    {
        SetPage(currentPage+1);
    }

    [Button]
    public void PreviousPage()
    {
        SetPage(currentPage - 1);
    }

    public void ShowLevelInfo(int index)
    {
        this.PostNotification(
            Notifications.LevelFoldoutInfoRequest,
            new LevelFoldoutInfo_Request(
                "Level " + index,
                index,
                GameModeId,
                LevelsProgress.Get(GameModeId, index, Settings.IsChallengeMode),
                GetLevelObjective(index),
                ((RectTransform)button_Ids[index%buttonsPerPage].transform).position
            ));
    }

    public void Pressed(int id)
    {
        this.PostNotification(Notifications.LevelLoadRequest, new LevelRequest(GameModeId, id));
    }

    [Button]
    public void DestroyCurrent()
    {
        if (button_Ids == null || button_Ids.Length == 0) return;

        for (int i = button_Ids.Length-1; i >= 0; i--)
        {
            if(Application.isPlaying)
                Destroy(button_Ids[i].gameObject);
            else
                DestroyImmediate(button_Ids[i].gameObject);
        }
        button_Ids = null;
    }
}
