﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Canvas_InGame_Pause : MonoBehaviour
{
    public Button pauseButton;


    void Register()
    {
        this.AddObserver(OnGameEnd, Notifications.GameMode_End);
        this.AddObserver(ShowButton, Notifications.GameMode_Go);
        this.AddObserver(ShowButton, Notifications.GameMode_Reset);
        this.AddObserver(ShowButton, Notifications.GameMode_PlayerRespawn);

    }

    void Unregister()
    {
        this.RemoveObserver(OnGameEnd, Notifications.GameMode_End);
        this.RemoveObserver(ShowButton, Notifications.GameMode_Go);
        this.RemoveObserver(ShowButton, Notifications.GameMode_Reset);
        this.RemoveObserver(ShowButton, Notifications.GameMode_PlayerRespawn);
    }

    private void ShowButton(object arg1, object arg2)
    {
        pauseButton.gameObject.SetActive(true);
    }

    private void OnGameEnd(object arg1, object arg2)
    {
        pauseButton.gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        Register();
       pauseButton.gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        Unregister();
    }
}
