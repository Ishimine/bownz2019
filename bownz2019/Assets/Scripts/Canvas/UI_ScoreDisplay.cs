﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using System;

public class UI_ScoreDisplay : MonoBehaviour
{
    public TextMesh txt;

    private float currentValue = 0;
    private int targetValue = 0;
    public float extraSize = 2;

    [SerializeField] private CodeAnimatorCurve curveIn = null;
    [SerializeField] private CodeAnimatorCurve curveOut = null;

    [SerializeField] private Color defaultColor = Color.clear;
    private Color targetColor;

    IEnumerator rutine = null;

    private void Awake()
    {
        txt.text = "";
    }

    public void OnUpdateValue(object source, object value)
    {
        UpdateValue((int)value);
    }

    [Button]
    private void UpdateValue(int value)
    {
        targetValue = value;
        currentValue = targetValue;
        if (currentValue == 0)
            txt.text = "";
        else
            txt.text = currentValue.ToString("0.");

        if (rutine != null) StopCoroutine(rutine);
        rutine = UpdateValueRutine();
        StartCoroutine(rutine);
    }

    IEnumerator UpdateValueRutine()
    {
        yield return AnimIn();
        yield return AnimOut();
    }

    IEnumerator AnimIn()
    {
        float startValue = currentValue;
        Vector3 startSize = txt.transform.localScale;
        Vector3 targetSize = Vector3.one * extraSize;
        float x = 0;
        Color startColor = txt.color;
        yield return null;
        do
        {
            txt.color = Color.Lerp(startColor, targetColor, x);
            x += Time.deltaTime / curveIn.Time;
            txt.transform.localScale = Vector3.Lerp(startSize, targetSize, curveIn.Curve.Evaluate(x));
            yield return null;
        } while (x < 1);
    }

    IEnumerator AnimOut()
    {
        Color startColor = txt.color;
        float startValue = currentValue;
        Vector3 startSize = txt.transform.localScale;
        Vector3 targetSize = Vector3.one;
        float x = 0;
        yield return null;
        do
        {
            txt.color = Color.Lerp(startColor, defaultColor, x);
            x += Time.deltaTime / curveOut.Time;
            txt.transform.localScale = Vector3.Lerp(startSize, targetSize, curveOut.Curve.Evaluate(x));
            yield return null;
        } while (x < 1);
    }

    private void Update()
    {
    }

    private void OnEnable()
    {
        this.AddObserver(OnCoinPickedUp, Notifications.OnCoinPickedUp);
        this.AddObserver(OnUpdateValue, Notifications.ScoreUpdate);
    }

    private void OnCoinPickedUp(object sender, object value)
    {
        switch ((int)value)
        {
            case 0:
                targetColor = ColorPallete.NormalCoin;
                break;
            case 1:
                targetColor = ColorPallete.GoldenCoin;
                break;
            default:
                break;
        }
    }

    private void OnDisable()
    {
        this.RemoveObserver(OnCoinPickedUp, Notifications.OnCoinPickedUp);
        this.RemoveObserver(OnUpdateValue, Notifications.ScoreUpdate);
    }

}
