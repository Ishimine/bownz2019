﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using UnityEngine.Events;
using System;

public class UI_ConteinerOpenClose : MonoBehaviour
{
    public enum SlideDirection { Right, Left, Up, Down }

    public bool useScale = true;
    public bool useSlide = false;
    public bool useColor = false;

    [SerializeField]
    private SlideDirection selectedDirection;

    [Header("Animation In")]
    [MinValue(0.05f)]
    public float timeIn;
    [ShowIf("useScale")] public AnimationCurve curveInScale = AnimationCurve.Linear(0, 0, 1, 1);
    [ShowIf("useColor")] public AnimationCurve curveInColor = AnimationCurve.Linear(0, 0, 1, 1);
    [ShowIf("useSlide")] public AnimationCurve curveInSlide = AnimationCurve.Linear(0, 0, 1, 1);
    [ShowIf("useSlide"), Range(0,1)] public float slideInAmmount = 1;

    [Header("Animation Out")]
    [MinValue(0.05f)]
    public float timeOut;
    [ShowIf("useScale")] public AnimationCurve curveOutScale = AnimationCurve.Linear(0, 0, 1, 1);
    [ShowIf("useColor")] public AnimationCurve curveOutColor = AnimationCurve.Linear(0, 0, 1, 1);
    [ShowIf("useSlide")] public AnimationCurve curveOutSlide = AnimationCurve.Linear(0, 0, 1, 1);
    [ShowIf("useSlide"), Range(0, 1)] public float slideOutAmmount = 1;

    private CodeAnimator codeAnimator = new CodeAnimator();

   [SerializeField] private RectTransform rectTransform;
    protected RectTransform RectTransform { get { if (rectTransform == null) rectTransform = GetComponent<RectTransform>(); return rectTransform; } }

    [ShowInInspector,ReadOnly]   private float SlideDistanceIn => -SlideDistanceOut;

    [ShowInInspector, ReadOnly]
    private float SlideDistanceOut =>
        ((selectedDirection == SlideDirection.Up || selectedDirection == SlideDirection.Down)
            ? RectTransform.rect.width
            : RectTransform.rect.height);

    [ShowInInspector, ReadOnly]
    private Vector2 Direction
    {
        get
        {
            switch (selectedDirection)
            {
                case SlideDirection.Right:
                    return Vector2.right;
                case SlideDirection.Left:
                    return Vector2.left;
                case SlideDirection.Up:
                    return Vector2.up;
                case SlideDirection.Down:
                    return Vector2.down;
            }
            return Vector2.zero;
        }
    }

    public Image rBackground;
    public Color cBackground;

    [SerializeField]
    private UnityEvent onOpenDoneResponse;
    public UnityEvent OnOpenDoneResponse { get { return onOpenDoneResponse; } set { onOpenDoneResponse = value; } }

    [SerializeField]
    private UnityEvent onCloseDoneResponse;
    public UnityEvent OnCloseDoneResponse { get { return onCloseDoneResponse; } set { onCloseDoneResponse = value; } }

    public bool IsOpen => gameObject.activeSelf;
    public bool IsClosed => !gameObject.activeSelf;

    public bool InAnimation { get; private set; }

#if UNITY_EDITOR
    private void OnValidate()
    {
        //Awake();
        //FindDependencies();
    }
#endif

    private void Awake()
    {
        Hide();
    }

    [Button]
    public void Swap()
    {
        if (gameObject.activeSelf)
            Close();
        else
            Open();
    }

    [Button]
    public void Open()
    {
        Open(null);
    }

    public void Open(Action postAction)
    {
        gameObject.SetActive(true);
        Vector3 targetScale = Vector3.one;
        Vector3 startPosition = RectTransform.anchoredPosition;

        InAnimation = true;
        if (useScale) RectTransform.localScale = Vector3.zero;
        if (rBackground != null) rBackground.color = Color.clear;
        if (rBackground != null) rBackground.gameObject.SetActive(true);
        codeAnimator.StartAnimacion(this, x =>
        {
            if (useSlide) RectTransform.anchoredPosition = startPosition - startPosition * curveInSlide.Evaluate(x) /*displacement * curveOutSlide.Evaluate(x)*/;
            if (useScale) RectTransform.localScale = targetScale * curveInScale.Evaluate(x);
            if (useColor && rBackground != null) rBackground.color = Color.Lerp(Color.clear, cBackground, curveInColor.Evaluate(x));
        }, DeltaTimeType.unscaledDeltaTime, AnimationType.Simple, null, 
            ()=>
            {
                OnOpenDoneResponse?.Invoke();
                postAction?.Invoke();
                InAnimation = false;
            }, timeIn);
    }

    [Button]
    public void Close()
    {
        Close(null);
    }

    public void Close(Action postAction = null)
    {
        InAnimation = true;
        Vector3 startLocalScale = RectTransform.localScale;
        Vector3 displacement =  Direction * SlideDistanceOut;
        Color cStartBg = Color.white;
        if(rBackground != null) cStartBg = rBackground.color;

        codeAnimator.StartAnimacion(this, x =>
        {
            if (useSlide) RectTransform.anchoredPosition = displacement * curveOutSlide.Evaluate(x);
            if (useScale) RectTransform.localScale = startLocalScale - startLocalScale * curveOutScale.Evaluate(x);
            if (useColor && rBackground != null) rBackground.color = Color.Lerp(cStartBg, Color.clear, curveOutColor.Evaluate(x));
        }, DeltaTimeType.unscaledDeltaTime, AnimationType.Simple, null, 
        ()=>
        {
            gameObject.SetActive(false);
            if (rBackground != null)    rBackground.gameObject.SetActive(false);
            postAction?.Invoke();
            InAnimation = false;
        }, timeOut);
    }

    [Button]
    public void Show()
    {
        if (useScale) RectTransform.localScale = Vector3.one;
        if (useColor && rBackground != null) rBackground.color = cBackground;
        if (useSlide) RectTransform.anchoredPosition = Vector3.zero;
        InAnimation = false;

    }

    [Button]
    public void Hide()
    {
        if (useScale) RectTransform.localScale = Vector3.zero;
        if (useColor && rBackground != null) rBackground.color = Color.clear;
        if (useSlide) RectTransform.anchoredPosition = Direction * SlideDistanceOut;
        InAnimation = false;
    }

    private void OnDisable()
    {
        if(useScale)   transform.localScale = Vector3.zero;
    }

    private void OnEnable()
    {
    }

    public void OpenOrClose(bool value, Action callback = null)
    {
        if (value)
        {
            Hide();
            Open(callback);
        }
        else
        {
            Show();
            Close(callback);
        }
    }
}
