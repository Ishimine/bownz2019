﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_ChangePlayArea : MonoBehaviour
{
    private int x;
    public int X
    {
        get { return x; }
        set { x = value; }
    }

    private int y;
    public int Y
    {
        get { return y; }
        set { y = value; }
    }

    public void SetX(string xS)
    {
        x = int.Parse(xS);
    }

    public void SetY(string xS)
    {
        y = int.Parse(xS);
    }

    public void ChangePlayArea()
    {
        Debug.Log("Sending play area X:{0} Y:{1}");
        PlayArea.Instance.SetSize(new Vector2(x,y));
    }
}
