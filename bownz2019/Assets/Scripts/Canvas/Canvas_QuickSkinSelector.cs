﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Canvas_QuickSkinSelector : MonoSingleton<Canvas_QuickSkinSelector>
{
    public override Canvas_QuickSkinSelector Myself => this;

    public Image currentSkin;

    private int index;

    private void Start()
    {
    }

    private void OnEnable()
    {
        index = Body_Conteiner.GetSkinIndex(BallSkin.PlayerSelectedSkin);
        SelectSkin(index);
    }

    public void SelectRandom()
    {
        BallSkin.PlayerSelectedSkin = Body_Conteiner.GetRandomSkin();
        index = Body_Conteiner.GetSkinIndex(BallSkin.PlayerSelectedSkin);
    }

    public void SelectNext()
    {
        SelectSkin(++index);
    }

    public void SelectPrevious()
    {
        SelectSkin(--index);
    }

    private void SelectSkin(int index)
    {
        BallSkin.PlayerSelectedSkin = Body_Conteiner.GetSkin(index);
    }
}
