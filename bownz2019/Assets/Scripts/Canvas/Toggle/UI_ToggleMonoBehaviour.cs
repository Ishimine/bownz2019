﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

public abstract class UI_ToggleMonoBehaviour : MonoBehaviour, IToggle
{
    public bool useLocalValue = false;
    [ShowIf("useLocalValue")] public bool localValue = true;
    [SerializeField] private UnityEventBool toggleInializables;

    [SerializeField] private UnityEventBool toggleTargets;
    [SerializeField, ReadOnly] private bool state;

    protected virtual void Start()
    {
        if (useLocalValue)
            Initialize(localValue);
    }

    protected void Initialize(bool value)
    {
        toggleInializables?.Invoke(value);
    }
   
    public void SetState(bool value)
    {
        Debug.Log("SetState: " + value);

        if (state == value) return;
        toggleTargets?.Invoke(value);
        _SetState(value);
        state = value;
    }

    public void Toggle()
    {
        SetState(!state);
    }

    protected abstract void _SetState(bool value);

}

/*
public class UnityEventBool : UnityEvent<bool>
{

}

public interface IToggle
{
    void Toggle();
    void SetState(bool value);
}*/