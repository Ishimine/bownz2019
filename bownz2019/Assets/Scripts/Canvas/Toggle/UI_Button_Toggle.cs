﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using UnityEngine.Events;
using System;

public class UI_Button_Toggle : MonoBehaviour, IToggle
{
    public UnityEventBool OnStateChangeUnity;
    public Action<bool> OnStateChange;

    [ReadOnly,SerializeField] private bool state;
    public bool State   
    {
        get { return state; }
        private set
        {
            state = value;
            OnStateChange?.Invoke(state);
            OnStateChangeUnity?.Invoke(state);
        }
    }

    public ToggleImage[] toggleImages;
    public ToggleText[] toggleTexts;

    private List<IToggle> toggles = new List<IToggle>();
    private List<IToggle> Toggles
    {
        get
        {
            if (toggles.Count != (toggleImages.Length + toggleTexts.Length))
                InitializeIToggle();
            return toggles;
        }
    }

    private void InitializeIToggle()
    {
        toggles = new List<IToggle>();
        toggles.AddRange(toggleImages);
        toggles.AddRange(toggleTexts);
    }

    [Button]
    public void Toggle()
    {
        SetState(!State);
    }

    [Button]
    public virtual void SetState(bool nState)
    {
        SetVisualState(nState);
        State = nState;
    }

    public void Initialize(bool value)
    {
        SetVisualState(value);
        state = value;
    }

    protected void SetVisualState(bool value)
    {
        for (int i = 0; i < Toggles.Count; i++)
        {
            Toggles[i].SetState(value);
        }
    }
}

public interface IToggle
{
    void SetState(bool value);
    void Toggle();
}

[System.Serializable]
public class ToggleImage : IToggle
{
    public Image render;

    public bool changeColor;
    [ShowIf("changeColor")] public Color onColor = Color.white;
    [ShowIf("changeColor")] public Color offColor = Color.clear;

    public bool changeSprite;
    [ShowIf("changeSprite")] public Sprite onSprite;
    [ShowIf("changeSprite")] public Sprite offSprite;

    [SerializeField,ReadOnly] bool state;

    public void SetState(bool value)
    {
        if(changeColor)
            render.color = (value) ? onColor : offColor;
        if(changeSprite)
            render.sprite = (value) ? onSprite : offSprite;
        state = value;
    }

    public void Toggle()
    {
        SetState(!state);
    }
}

[System.Serializable]
public class ToggleText : IToggle
{
    public Text text;

    public bool changeColor;
    [ShowIf("changeColor")] public Color onColor = Color.white;
    [ShowIf("changeColor")] public Color offColor = Color.clear;

    public bool changeText;
    [ShowIf("changeText")] public string onText;
    [ShowIf("changeText")] public string offText;

    bool state;

    public void SetState(bool value)
    {
        if (changeColor)
            text.color = (value) ? onColor : offColor;
        if (changeText)
            text.text = (value) ? onText : offText;
    }

    public void Toggle()
    {
        SetState(!state);
    }
}

[System.Serializable]
public class UnityEventBool : UnityEvent<bool>
{

}
