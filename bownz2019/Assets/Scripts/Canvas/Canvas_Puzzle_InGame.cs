﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Canvas_Puzzle_InGame : MonoBehaviour
{
    public Text timerTxt;

    public UI_ConteinerOpenClose editToolBar_Conteiner;
    public UI_ConteinerOpenClose playToolBar_Conteiner;

    private float time;
    private bool running = false;

    private void OnEnable()
    {
        this.AddObserver(OnGameMode_Go_Response, Notifications.GameMode_Go);
        this.AddObserver(OnGameMode_End_Response, Notifications.GameMode_End);
        this.AddObserver(OnPuzzle_Editing_State_Response, Notifications.Puzzle_Editing_State);
        this.AddObserver(OnPuzzle_Playing_State_Response, Notifications.Puzzle_Playing_State);
    }

    private void OnDisable()
    {
        this.RemoveObserver(OnGameMode_Go_Response, Notifications.GameMode_Go);
        this.RemoveObserver(OnGameMode_End_Response,Notifications.GameMode_End);
        this.RemoveObserver(OnPuzzle_Editing_State_Response, Notifications.Puzzle_Editing_State);
        this.RemoveObserver(OnPuzzle_Playing_State_Response, Notifications.Puzzle_Playing_State);
    }
    
    private void OnPuzzle_Editing_State_Response(object arg1, object arg2)
    {
        editToolBar_Conteiner.Open();
        playToolBar_Conteiner.Close();
    }

    private void OnPuzzle_Playing_State_Response(object arg1, object arg2)
    {
        playToolBar_Conteiner.Open();
        editToolBar_Conteiner.Close();
    }

    private void OnGameMode_Go_Response(object sender, object arg)
    {
        StartTimer();
    }

    private void OnGameMode_End_Response(object arg1, object arg2)
    {
        StopTimer();
        playToolBar_Conteiner.Close();
        editToolBar_Conteiner.Close();
    }

    private void Update()
    {
        if (running)
        {
            time += Time.deltaTime;
            timerTxt.text = time.ToString(".0");
        }
    }

    public void StartTimer()
    {
        time = 0;
        running = true;
    }

    public void StopTimer()
    {
        running = false;
    }

    public void PlayPressed()
    {
        this.PostNotification(Notifications.Puzzle_PlayButton);
    }

    public void StopPressed()
    {
        this.PostNotification(Notifications.Puzzle_StopButton);
    }

    public void ResetPressed()
    {
        this.PostNotification(Notifications.Puzzle_ResetButton);
    }
}
