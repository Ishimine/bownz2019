﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

public class Canvas_InGame_Explorer : MonoBehaviour
{
    private bool active = false;
    public Text timerText;
    public Text deadText;
    public Text platformText;
    float time;

    private int platforms;
    private int deads;

    [ShowInInspector] private LevelProgress objective;

    public UI_ConteinerOpenClose timerConteiner;
    public UI_ConteinerOpenClose platformConteiner;
    public UI_ConteinerOpenClose deadConteiner;

    private void OnEnable()
    {
        this.AddObserver(OnGameMode_Go, Notifications.GameMode_Go);
        this.AddObserver(OnGameMode_End, Notifications.GameMode_End);
        this.AddObserver(OnSwipePlatform_Touched, Notifications.SwipePlatform_Touched);
        this.AddObserver(OnBall_DeadStart, Notifications.Ball_DeadStart);
        this.AddObserver(OnGameMode_Reset, Notifications.GameMode_Reset);

    }

    private void OnDisable()
    {
        this.RemoveObserver(OnGameMode_Go, Notifications.GameMode_Go);
        this.RemoveObserver(OnGameMode_End, Notifications.GameMode_End);
        this.RemoveObserver(OnSwipePlatform_Touched, Notifications.SwipePlatform_Touched);
        this.RemoveObserver(OnBall_DeadStart, Notifications.Ball_DeadStart);
        this.RemoveObserver(OnGameMode_Reset, Notifications.GameMode_Reset);
    }

    private void OnGameMode_Reset(object arg1, object arg2)
    {
        deadText.text = "0";
        timerText.text = "0";
        platformText.text = "0";
    }

    public void SetObjective(LevelProgress levelProgress)
    {
        objective = levelProgress;
    }
    private void OnSwipePlatform_Touched(object arg1, object arg2)
    {
        platforms++;
        platformText.text = platforms.ToString();

        if(platforms > objective.Platforms && platformConteiner.IsOpen) platformConteiner.Close();
    }

 private void OnBall_DeadStart(object arg1, object arg2)
 {
     deads++;
     deadText.text = deads.ToString();

        if(deads > objective.Deaths && deadConteiner.IsOpen) deadConteiner.Close();

 }

    private void OnGameMode_End(object arg1, object arg2)
    {
        StopTimer();
                                
        deadConteiner.Close();
        platformConteiner.Close();
    }

    private void OnGameMode_Go(object arg1, object arg2)
    {
        StartTimer();
        platforms = 0;
        deads = 0;
        deadText.text = "0";
        platformText.text = "0";
                     
        timerConteiner.Open();
        platformConteiner.Open();
        deadConteiner.Open();
    }

    private void StartTimer()
    {
        time = 0;
        timerConteiner.Open();
        active = true;
    }

    private void StopTimer()
    {
        timerConteiner.Close();
        active = false;
    }

    private void Update()
    {
        if (!active) return;


        time += Time.deltaTime;
        timerText.text = time.ToString(".00");

        if (time > objective.Time && timerConteiner.IsOpen)
        {
            timerConteiner.Close();
            StopTimer();
        }
    }
}
