﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class Canvas_Adventure_WinScreen : MonoBehaviour
{
    public UI_ObjectiveDisplay[] displays;
    public UI_StarOrb[] orbs;

    LevelProgress lastScore, currentScore, objectives;

    private CodeAnimator codeAnimator = new CodeAnimator();

    public float waitForNextLevel;
    int animationsDone = 0;

    private void Awake()
    {
        Initialize();
    }

    private void Initialize()
    {
        for (int i = 0; i < displays.Length; i++)
        {
            displays[i].OnSucessAnimationDone += orbs[i].TransitionToCompleted;
            displays[i].OnAnimationDone += NextAnimation;
        }
    }

    private void OnDestroy()
    {
        for (int i = 0; i < displays.Length; i++)
        {
            displays[i].OnSucessAnimationDone -= orbs[i].TransitionToCompleted;
            displays[i].OnAnimationDone -= NextAnimation;
        }
    }

    private void NextAnimation()
    {
        Debug.Log("NextAnimation");
        animationsDone++;
        if (animationsDone < 3)
        {
            displays[animationsDone].StartAnimation(lastScore[animationsDone], currentScore[animationsDone], objectives[animationsDone]);
        }
        else
        {
            BaseException exception = new BaseException(false);
            this.PostNotification(Notifications.Query_DoesNextLevelExist, exception);
            if(exception.toggle)
                codeAnimator.StartWaitAndExecute(this, waitForNextLevel, Next, true);
        }
    }

    [Button]
    public void Show(LevelProgress lastScore, LevelProgress currentScore, LevelProgress objectives)
    {
        gameObject.SetActive(true);

        foreach (var item in displays)
            item.Initialize();

        animationsDone = -1;
        this.lastScore = lastScore;
        this.currentScore = currentScore;
        this.objectives = objectives;
        NextAnimation();
    }

    public void Hide()
    {
    }

    public void Next()
    {
        this.PostNotification(Notifications.GameCycle_EndScreen_Next);
        codeAnimator.Stop(this);
    }

    public void Menu()
    {
        this.PostNotification(Notifications.GameCycle_EndScreen_Menu);
        codeAnimator.Stop(this);
    }

    public void Retry()
    {
        this.PostNotification(Notifications.GameCycle_EndScreen_Retry);
        codeAnimator.Stop(this);
    }

    private void OnEnable()
    {
       /* this.AddObserver(StopCountDown, Notifications.GameCycle_EndScreen_Next);
        this.AddObserver(StopCountDown, Notifications.GameCycle_EndScreen_Menu);
        this.AddObserver(StopCountDown, Notifications.GameCycle_EndScreen_Retry);*/
    }

    private void OnDisable()
    {
      /*  this.RemoveObserver(StopCountDown, Notifications.GameCycle_EndScreen_Next);
        this.RemoveObserver(StopCountDown, Notifications.GameCycle_EndScreen_Menu);
        this.RemoveObserver(StopCountDown, Notifications.GameCycle_EndScreen_Retry);*/
        codeAnimator.Stop(this);
    }

    private void StopCountDown(object arg1, object arg2)
    {
        codeAnimator.Stop(this);
    }
}
