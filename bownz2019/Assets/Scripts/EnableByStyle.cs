﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableByStyle : MonoBehaviour
{
    public Settings.Style style;

    private void Awake()
    {
        gameObject.SetActive(style == Settings.CurrentStyle);
    }
}
