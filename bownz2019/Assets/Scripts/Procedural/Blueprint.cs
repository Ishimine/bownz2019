﻿using System.Collections.Generic;
using UnityEngine;

namespace Procedural
{
    [System.Serializable]
    public struct Blueprint
    {
        public  float Seed;
        public  List<GameObject> Elements;
        public int SeedOffset;
        public Blueprint(float seed, List<GameObject> elements,int seedOffset = 0)
        {
            Seed = seed;
            Elements = elements;
            SeedOffset = seedOffset;
        }
    }
}
