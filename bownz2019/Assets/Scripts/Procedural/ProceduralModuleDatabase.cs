﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;
using System.Linq;
using RotaryHeart.Lib.SerializableDictionary;

#if UNITY_EDITOR
using UnityEditor;
using System.IO;
#endif

[CreateAssetMenu]
public class ProceduralModuleDatabase : ScriptableSingleton<ProceduralModuleDatabase>
{
    private const string FolderPath = "Assets/_Prefabs&SO/_ScriptableObjects/ProceduralTerrain/";
    private const string ScriptableListNameFormat = "ScriptableListOf_{0}.asset";

    public override ProceduralModuleDatabase Myself => this;

    [SerializeField] private new List<STag> sTags = new List<STag>();
    public new List<STag> STags { get { return sTags; }
        set
        {
            sTags = value;
        }
    }

    [SerializeField] private ScriptableListDictionary scriptableLists = new ScriptableListDictionary();
    public ScriptableListDictionary ScriptableLists { get { return scriptableLists; } set { scriptableLists = value; } }

    public GameObject shopModule;
    public GameObject breakModule;
    public GameObject checkpointModule;
    public GameObject roofModule;
    public GameObject floorModule;
    public GameObject baseModule;
    public GameObject startModule;

    public enum FilterType
    {
        And,
        Or
    }

    private string GetScriptableListName(string tag)
    {
        return string.Format(ScriptableListNameFormat, tag);
    }

    public static GameObject[] _CollectModulesWith(STag[] selectedTags, FilterType filterType, bool removeModulesWithUnselectedTags)
    {
        return Instance.CollectModulesWith(selectedTags, filterType, removeModulesWithUnselectedTags);
    }

    public static GameObject[] _CollectModulesWith(STag[] addTags, STag[] removeTags)
    {
        return Instance.CollectModulesWith(addTags, removeTags);
    }

    public GameObject[] CollectModulesWith(STag[] addTags, STag[] removeTags, int maxLevel = 10)
    {
        if(addTags.Length == 0)
        {
            return new GameObject[0];
        }
        //Debug.Log("sTags.Lenght; " + addTags.Length);
        List<GameObject> validModules = new List<GameObject>();
        //Recolectamos los que incluyen los AddTags
        validModules = new List<GameObject>(scriptableLists[addTags[0].name].Elements);
        for (int i = 1; i < addTags.Length; i++)
        {
            foreach (var item in scriptableLists[addTags[i].name].Elements)
            {
                if (!validModules.Contains(item))
                {
                    validModules.Add(item);
                }
            }
        }

        if (removeTags.Length > 0)
        {
            for (int i = 0; i < removeTags.Length; i++)
            {
                Debug.Log($"Removing modules with tag {removeTags[i].name}");
                for (int j = 0; j < scriptableLists[removeTags[i].name].Elements.Count; j++)
                {
                    if (validModules.Contains(scriptableLists[removeTags[i].name].Elements[j]))
                    {
                        validModules.Remove(scriptableLists[removeTags[i].name].Elements[j]);
                    }
                }
            }
        }
        
        for (int i = 0; i < validModules.Count; i++)
        {
            if (validModules[i].GetComponent<Terrain_Module>().Difficulty > maxLevel)
                validModules.RemoveAt(i);
        }
        
        return validModules.ToArray();
    }

/// <summary>
/// 
/// </summary>
/// <param name="selectedTags"></param>
/// <param name="filterType"></param>
/// <param name="removeModulesWithUnselectedTags"></param>
/// <param name="maxLevel">Exclusive</param>
/// <returns></returns>
    public  GameObject[] CollectModulesWith(STag[] selectedTags, FilterType filterType, bool removeModulesWithUnselectedTags)
    {
        Debug.Log("sTags.Lenght; " + selectedTags.Length);
        List<GameObject> validModules = new List<GameObject>();

        switch (filterType)
        {
            case FilterType.And:
                validModules = scriptableLists[selectedTags[0].name].Elements;
                for (int i = 1; i < selectedTags.Length; i++)
                    validModules = validModules.Intersect(scriptableLists[selectedTags[i].name].Elements) as List<GameObject>;
                break;
            case FilterType.Or:
                foreach (STag tag in selectedTags)
                {
                    Debug.Log(tag.name);
                    validModules.AddRange(scriptableLists[tag.name].Elements);
                }
                break;
        }

        if (removeModulesWithUnselectedTags)
        {
            foreach (var item in selectedTags)
            {
                Debug.Log("selectedTags: " + item.ToString());
            }

            foreach (var item in sTags)
            {
                Debug.Log("sTags: " + item.ToString());
            }

            List<STag> unselectedTags = new List<STag>(STags);

            for (int i = 0; i < selectedTags.Length; i++)
            {
                unselectedTags.Remove(selectedTags[i]);
            }

            Debug.Log("unselectedTags.Count " + unselectedTags.Count);

            if (unselectedTags.Count > 0)
            {

                for (int i = 0; i < unselectedTags.Count; i++)
                {
                    Debug.Log("UnselectedTag  " + i + ": " + unselectedTags[i]) ;

                    if (validModules.Count == 0)
                        Debug.LogError("La seleccion genero una lista vacia");

                    foreach (var item in validModules)
                    {
                        Debug.Log("validModules Item: " + item);
                    }

                    Debug.Log(scriptableLists[unselectedTags[i].name]);
                    foreach (var item in scriptableLists[unselectedTags[i].name].Elements)
                    {
                        Debug.Log("Item: " + item);
                    }

                    Debug.Log("scriptableLists[unselectedTags["+i+"].name].Elements.Count" + scriptableLists[unselectedTags[i].name].Elements.Count);
                    for (int j = 0; j < scriptableLists[unselectedTags[i].name].Elements.Count; j++)
                    {
                        Debug.Log("J: " + j);
                        if (validModules.Contains(scriptableLists[unselectedTags[i].name].Elements[j]))
                            validModules.Remove(scriptableLists[unselectedTags[i].name].Elements[j]);
                    }
                }
            }
        }

     
        
        return validModules.ToArray();
    }



#if UNITY_EDITOR
    [Button]
    public void InitializeDatabase()
    {
        Reimport();
        Clear();
        List<string> paths =new List<string>(Directory.GetFiles(Application.dataPath + "/_Prefabs&SO/Prefabs/Terrains/Modular"));

        for (int i = paths.Count-1; i >= 0; i--)
        {
            if (paths[i].Contains(".meta")) paths.RemoveAt(i);
        }

        foreach (string path in paths)
        {
            string  filePath = "Assets" + path.Remove(0, Application.dataPath.Length);
            AnaliceObject(AssetDatabase.LoadAssetAtPath<GameObject>(filePath));
        }
    }

    private void Clear()
    {
        foreach (KeyValuePair<string, ModulesList> list in ScriptableLists)
        {
            list.Value.Clear();
        }
    }
#endif

    private void AnaliceObjects(GameObject[] gos)
    {
        foreach (GameObject item in gos)
            AnaliceObject(item);

#if UNITY_EDITOR
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
#endif
    }


    private void AnaliceObject(GameObject go)
    {
        STag[] objectTags = GetTagsFrom(go);
        foreach (var tag in objectTags)
        {
            if (ScriptableLists.ContainsKey(tag.name))
                ScriptableLists[tag.name].Add(go);
        }
    }

    [Button]
    public void ValidateModule(Terrain_Module[] terrain_Modules)
    {
        foreach (Terrain_Module item in terrain_Modules)
            item.Validate();
    }

    [Button]
    public void CollectTags(RootSTagComponent[] rootSTagComponents)
    {
        foreach (RootSTagComponent item in rootSTagComponents)
        {
            item.CollectTags();
        }

    }
    /// <summary>
    /// Analisa un objeto y extra todos los tags pertinentes
    /// </summary>
    /// <param name="go"></param>
    /// <returns></returns>
    private STag[] GetTagsFrom(GameObject go)
    {
        GameObject testInstance = Instantiate(go, null);
        Debug.Log("GetTagsFrom " + go.name);
        HashSet<STag> retValue = new HashSet<STag>();

        STagComponent[] components = testInstance.GetComponentsInChildren<STagComponent>();

        for (int i = 0; i < components.Length; i++)
        {
            foreach (var tag in components[i].STags)
            {
                retValue.Add(tag);
//                Debug.Log(tag.name);
            }
        }

        foreach (var tag in retValue)
        {
            if (!sTags.Contains(tag))
            {
                retValue.Remove(tag);
            }
        }
        DestroyImmediate(testInstance);
        return new List<STag>(retValue).ToArray();
    }

    
#if UNITY_EDITOR

    [Button]
    public void RenameModules()
    {
        List<GameObject> gameObjects = GetAllModulesAsList();

        Dictionary<string, string> nameChange = new Dictionary<string, string>();

        for (int i = 0; i < gameObjects.Count; i++)
        {
            string path = AssetDatabase.GetAssetPath(gameObjects[i]);
            string nName = $"Module_D{gameObjects[i].GetComponent<Terrain_Module>().Difficulty}_N{i}_";
            nameChange.Add(path, nName);
        }

        foreach (KeyValuePair<string, ModulesList> valuePair in scriptableLists)
        {
            foreach (GameObject valueElement in valuePair.Value.Elements)
            {
                string path = AssetDatabase.GetAssetPath(valueElement);
                nameChange[path] = nameChange[path] + $"_{valuePair.Key}";
            }
        }

        foreach (KeyValuePair<string,string> keyValuePair in nameChange)
           AssetDatabase.RenameAsset(keyValuePair.Key, keyValuePair.Value);
        
        //AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }

    
    

    [Button]
    public void ValidateAll()
    {
        foreach (GameObject gameObject in GetAllModulesAsList())
        {
            gameObject.GetComponentInChildren<Terrain_Module>().Validate();
            gameObject.GetComponentInChildren<Terrain_Module>().Validate();
        }
    }

    [Button]
    public void ValidateAllAsRutine()
    {
        if (!Application.isPlaying)
        {
            Debug.LogError("Unity Should be playing");
        }
        GameObject obj = new GameObject();
        obj.AddComponent<Empty>().StartCoroutine(ValidateAndRemeshAllRutine (obj));
    }
    
    public IEnumerator ValidateAndRemeshAllRutine(GameObject obj)
    {
        List<GameObject> gameObjects = GetAllModulesAsList();
        for (int i = 0; i < gameObjects.Count; i++)
        {
            gameObjects[i].GetComponentInChildren<Terrain_Module>().Validate();
            yield return new WaitForSeconds(.3f);
            gameObjects[i].GetComponentInChildren<Terrain_Module>().Validate();
            yield return new WaitForSeconds(.3f);

            //gameObjects[i].GetComponentInChildren<UpdateStyle>().UpdateMeshAndShape(false);
            //yield return null;
            Debug.Log($"Progress: %{(float)i/gameObjects.Count}");
        }
        yield return null;
        AssetDatabase.SaveAssets();
        Destroy(obj);
    }

    
    [Button]
    public void RefreshStyleAll()
    {
        foreach (GameObject gameObject in GetAllModulesAsList())
        {
            gameObject.GetComponentInChildren<UpdateStyle>().UpdateMeshAndShape(false);
        }
        AssetDatabase.SaveAssets();
    }
    
    
    private List<GameObject> GetAllModulesAsList()
    {
        List<GameObject> gameObjects = new List<GameObject>();

        foreach (KeyValuePair<string, ModulesList> valuePair in scriptableLists)
        {
            foreach (GameObject valueElement in valuePair.Value.Elements)
            {
                if (!gameObjects.Contains(valueElement))
                    gameObjects.Add(valueElement);
            }
        }

        return gameObjects;
    }


    [Button]
    public void Reimport()
    {
        ScriptableLists.Clear();
        foreach (var item in sTags)
        {
            ModulesList list = GetAssetListOfGameObject(item.name, false);
            ScriptableLists.Add(item.name, list);
        }
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }

    private ModulesList GetAssetListOfGameObject(string tag, bool save)
    {
        ModulesList list = AssetDatabase.LoadAssetAtPath<ModulesList>(FolderPath + GetScriptableListName(tag));
        if (list == null)
        {
            list = CreateScriptableList(tag, false);
            if(save)
            {
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
            }
        }
        return list;
    }

    private ModulesList CreateScriptableList(string tag,bool save)
    {
        ModulesList nList = CreateInstance<ModulesList>();
        AssetDatabase.CreateAsset(nList, FolderPath + GetScriptableListName(tag));

        if (save)
        {
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
        return nList;
    }
#endif
}

[System.Serializable]
public class ScriptableListDictionary : SerializableDictionaryBase<string, ModulesList>
{
}