﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class STagComponent : MonoBehaviour, ISTag
{
    [SerializeField] private STag[] sTags;
    public STag[] STags
    {
        get { return sTags; }
    }



}



public interface ISTag
{
    STag[] STags
    {
        get;
    }
}