﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;


public class ScriptableList<T> : ScriptableObject
{
    [SerializeField] private List<T> elements = new List<T>();
    public List<T> Elements { get { return elements; } set { elements = value; } }

    public void Add(T nElement)
    {
        if (!elements.Contains(nElement))
            elements.Add(nElement);
    }
                          
    public void Clear()
    {
        elements.Clear();
    }
}
