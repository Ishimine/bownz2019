using System.Collections;
using System.Collections.Generic;
using Procedural;
using System;
using UnityEngine;
using Sirenix.OdinInspector;
using Random = UnityEngine.Random;

public class ProceduralLevel : MonoBehaviour
{
    //public Action OnConstructionStar { get; set; }
    //public Action OnConstructionEnd { get; set; }
    //public Action<float> OnConstructionProgress { get; set; }

    private const float resolution = 1.6803f;

    [SerializeField, HideInInspector] private List<Terrain_Module> modules = new List<Terrain_Module>();
    public List<Terrain_Module> Modules { get => modules; private set => modules = value; }

    [SerializeField] private ProceduralModuleDatabase Database;

    [SerializeField, HideInInspector] private GameObject roof;
    [SerializeField, HideInInspector] private GameObject floor;

    [SerializeField, HideInInspector] private List<StarCoin> coins = new List<StarCoin>();
    [SerializeField, HideInInspector] private GameObject winCoin;

    [SerializeField] private int starsPerModuleCap = 3;

    public GameObject StarCoin_Prefab;
    public GameObject GoldenCoin_Prefab;

    public Background bg;

    private List<Checkpoint> checkpoints = new List<Checkpoint>();
    public List<Checkpoint> Checkpoints => checkpoints;
    private Checkpoint currentCp;

    public Checkpoint CurrentCp => currentCp;
    public GameObject WinCoin => winCoin;

    public Blueprint CurrentBlueprint { get; private set; }

    private TerrainCameraConfiner cameraConfiner;
    public TerrainCameraConfiner CameraConfiner
    {
        get
        {
            if (cameraConfiner == null)
                cameraConfiner = GameObjectLibrary.Instantiate<TerrainCameraConfiner>("TerrainCameraConfiner",transform.parent);
            return cameraConfiner;
        }
    }

    public void CreateLevel(Blueprint blueprint, Action callback)
    {
       StartCoroutine(CreateFromBlueprint(blueprint, callback));
    }

    private void Awake()
    {
        CreateRoofAndFloor();
    }

    private void CreateRoofAndFloor()
    {
        roof = Instantiate(Database.roofModule, transform);
        floor = Instantiate(Database.floorModule, transform);
    }

    [Button] 
    public void CreateFromSeudoBlueprint(SeudoBlueprint seudoBlueprint, Action callback)
    {
        StartCoroutine(CreateFromBlueprint(seudoBlueprint.CreateBlueprint(), callback)); 
    }

    private IEnumerator CreateFromBlueprint(Blueprint blueprint, Action callback)
    {
        CurrentBlueprint = blueprint;
        
        this.PostNotification(Notifications.OnLevelConstructionStart);
        this.PostNotification(Notifications.OnLevelConstructionProgress, (float)0);
        transform.position = Vector2.zero;
        Debug.Log(" CreateFromBlueprint");

        DestroyCurrentModules();
        DestroyCoins();
        float height = 0;

        int totalSteps = blueprint.Elements.Count + 4;
        yield return null;

        float currentSeed = CurrentBlueprint.Seed;

        for (int i = 0; i < blueprint.Elements.Count; i++)
        {
            Terrain_Module current = Instantiate(blueprint.Elements[i], transform).GetComponent<Terrain_Module>();

            currentSeed = ProcessSeed(currentSeed, height);
            if (Mathf.Abs((currentSeed % 1)) < .5)
                current.transform.localScale = new Vector3(-1, 1, 1);

            modules.Add(current);
            current.transform.position = Vector3.up * height;
            height += current.ModuleHeight;
            yield return null;
            this.PostNotification(Notifications.OnLevelConstructionProgress,(float)i/totalSteps);
        }
        yield return null;
        PositionRoofAndFloor(height);
        this.PostNotification(Notifications.OnLevelConstructionProgress,((float)blueprint.Elements.Count + 1)/totalSteps);
        AddCoins(CurrentBlueprint.Seed);
        this.PostNotification(Notifications.OnLevelConstructionProgress,((float)blueprint.Elements.Count + 2)/totalSteps);
        AddWinCoin();
        this.PostNotification(Notifications.OnLevelConstructionProgress,((float)blueprint.Elements.Count + 3)/totalSteps);
        yield return null;
        bg.SetPositionAndSize(new Rect(Vector2.left * .5f * PlayArea.Area.width, new Vector2(PlayArea.Area.width, height)));
        transform.position = Vector2.down * PlayArea.Area.height/2;
        this.PostNotification(Notifications.OnLevelConstructionProgress,(float)1);
        this.PostNotification(Notifications.OnLevelConstructionEnd);

        ClearCheckpoints();
        ExtractCheckpoints();
                       
        yield return new WaitForSeconds(2);
                               
        CameraConfiner.SetHeight(height);
        callback?.Invoke();
    }

    private float ProcessSeed(float seed, float modificator)
    {
        return Perlin.Noise((seed + modificator * resolution));
    }

    public List<StarCoin> GetStarsInsideRect(Rect rect)
    {
        List<StarCoin> stars = new List<StarCoin>();
        foreach (StarCoin coin in coins)
        {
            if(rect.Contains(coin.transform.position))
                stars.Add(coin);
        }

        return stars;
    }

    private void ExtractCheckpoints()
    {
        for (int i = 0; i < modules.Count; i++)
        {
            Checkpoint cp = modules[i].GetComponentInChildren<Checkpoint>();
            if (cp != null)
            {
                checkpoints.Add(cp);
                cp.OnActivation += ChechpointActivated;
            }
        }
    }

    private void ClearCheckpoints()
    {
        for (int i = 0; i < checkpoints.Count; i++)
            checkpoints[i].OnActivation -= ChechpointActivated;
    }

    private void OnDestroy()
    {
        ClearCheckpoints();
    }

    private void ChechpointActivated(Checkpoint cp)
    {
        currentCp = cp;
    }

    public Checkpoint GetNextCheckpoint()
    {
        if(currentCp == null) return checkpoints[0];
        int index = checkpoints.IndexOf(currentCp);
        if (index == checkpoints.Count) return null;
        return checkpoints[index + 1];
    }

    private void AddCoins(float seed)
    {
        Debug.Log("CreateCoins");
        coins.Clear();
        for (int i = 1; i < modules.Count-1; i++)
        {
            if (!modules[i].CanSpawnCoins) continue;
            List<StarCoin> starCoins = new List<StarCoin>();
            List<StarCoin> starKeyCoins = new List<StarCoin>();

            int totalLenght = modules[i].coinSpawnPoints.Count;

            seed = ProcessSeed(seed, starCoins.Count + starKeyCoins.Count);
            int keyCoinsLength =  Mathf.Abs(Mathf.FloorToInt(Mathf.Lerp(0, Mathf.Min(starsPerModuleCap, totalLenght), seed % 1))); 
            List<Transform> spawnPoints = new List<Transform>(modules[i].coinSpawnPoints);

            for (int j = 0; j < totalLenght; j++)
            {
                seed = ProcessSeed(seed, starCoins.Count + starKeyCoins.Count);
                int index = Mathf.Abs(Mathf.RoundToInt((seed % 1) * (spawnPoints.Count - 1)));
                Transform selectedSpawnPoint = spawnPoints[index];

                if (j <= keyCoinsLength)
                    starKeyCoins.Add(CreateCoinAt(selectedSpawnPoint.position));
                else
                    starCoins.Add(CreateCoinAt(selectedSpawnPoint.position));

                spawnPoints.Remove(selectedSpawnPoint);
            }
            modules[i].UseTopDoor(modules[i].BlockedByCoins, starKeyCoins);
        }
    }

    private StarCoin CreateCoinAt(Vector3 coinSpawnPoint)
    {
        GameObject go = Instantiate(StarCoin_Prefab, transform);
        go.transform.position = coinSpawnPoint;
        StarCoin starCoin = go.GetComponent<StarCoin>();
        coins.Add(starCoin);
        return starCoin;
    }

    private void AddWinCoin()
    {
        Debug.Log(" AddWinCoin");
        if (winCoin == null) winCoin = GameObjectLibrary.Instantiate(GoldenCoin_Prefab,transform);
        winCoin.transform.position = modules[modules.Count - 1].coinSpawnPoints.GetLast().position;
    }
              
    [Button]
    private void DestroyCoins()
    {
        for (int i = coins.Count - 1; i >= 0; i--)
        {
            DestroyElement(coins[i].gameObject);
        }
        coins.Clear();
    }

    private void PositionRoofAndFloor(float levelHeight)
    {
        if (floor == null) CreateRoofAndFloor();
        floor.transform.position = Vector3.zero;
        roof.transform.position = Vector3.up * levelHeight;
    }

    [Button]
    private void DestroyCurrentModules()
    {
        for (int i = 0; i < modules.Count; i++)
            DestroyElement(modules[i].gameObject);
        modules.Clear();
    }

    private void DestroyElement(GameObject o)
    {
        if(Application.isPlaying)
            Destroy(o);
        else 
            DestroyImmediate(o);
    }

#if UNITY_EDITOR

    private void OnDrawGizmosSelected()
    {
        for (int i = 0; i < modules.Count; i++)
        {
            DrawString(modules[i].name + ": "+ modules[i].Difficulty.ToString(), modules[i].transform.position + Vector3.right * 9 + Vector3.up * modules[i].ModuleHeight/2);
        }
    }

    public static void DrawString(string text, Vector3 worldPos, Color? textColor = null, Color? backColor = null)
    {
        UnityEditor.Handles.BeginGUI();
        var restoreTextColor = GUI.color;
        var restoreBackColor = GUI.backgroundColor;

        GUI.color = textColor ?? Color.white;
        GUI.backgroundColor = backColor ?? Color.black;

        var view = UnityEditor.SceneView.currentDrawingSceneView;
        if (view != null && view.camera != null)
        {
            Vector3 screenPos = view.camera.WorldToScreenPoint(worldPos);
            if (screenPos.y < 0 || screenPos.y > Screen.height || screenPos.x < 0 || screenPos.x > Screen.width || screenPos.z < 0)
            {
                GUI.color = restoreTextColor;
                UnityEditor.Handles.EndGUI();
                return;
            }
            Vector2 size = GUI.skin.label.CalcSize(new GUIContent(text));
            var r = new Rect(screenPos.x - (size.x / 2), -screenPos.y + view.position.height + 4, size.x, size.y);
            GUI.Box(r, text, UnityEditor.EditorStyles.numberField);
            GUI.Label(r, text);
            GUI.color = restoreTextColor;
            GUI.backgroundColor = restoreBackColor;
        }
        UnityEditor.Handles.EndGUI();
    }

#endif

}
