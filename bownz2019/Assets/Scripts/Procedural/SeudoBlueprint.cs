﻿using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Procedural
{
    public abstract class SeudoBlueprint: ScriptableObject
    {
        public float resolution = 2048;
        [MinValue(1)]public float Seed = 0;

        [MinValue(1)]public float TagsSeed = 0;

        public int SeedOffset = 0;



        public abstract Blueprint CreateBlueprint();
        [TabGroup("Dependencies"), SerializeField]   protected ProceduralModuleDatabase Database;

    }
}
