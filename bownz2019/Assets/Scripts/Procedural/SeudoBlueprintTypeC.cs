﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Random = UnityEngine.Random;

namespace Procedural
{
    [CreateAssetMenu(menuName = "Procedural/Level Blueprints/TypeC", fileName = "BP_TypeC")]
    public class SeudoBlueprintTypeC : SeudoBlueprint
    {
        private const float GoldenRatio = 1.61803398874989484820458683436f;

        [MinValue(1)]
        public int totalStages = 16;
        public int modulesPerStage = 2;

        [SerializeField] private AnimationCurve difficultyCurve;

        [SerializeField] public STag[] alwaysIncludeModules;
        [SerializeField] public STag[] canBeRemovedModules;
        [SerializeField] public STag[] alwaysRemovedModules;

        [SerializeField] private int tagsToNotRemove = 2;

        [SerializeField] private int maxLevel = 10;

        public override Blueprint CreateBlueprint()
        {
            float currentSeed = Seed * GoldenRatio;

            List<STag> removeModules = GetRemoveTags(TagsSeed * GoldenRatio);

            foreach (var tag in removeModules)
            {
                Debug.Log($"<color=red> Removing tag </color>{tag.name}");
            }

            List<GameObject> allValidModules = new List<GameObject>(Database.CollectModulesWith(alwaysIncludeModules, removeModules.ToArray(), maxLevel));
            List<List<GameObject>> modulesByLevel = SplitByDifficulty(allValidModules);
            List<GameObject> modules = new List<GameObject>();
            int totalModules = totalStages * modulesPerStage + totalStages * 2;

            Debug.Log("Starting Seed: " + currentSeed);

            modules.Add(Database.startModule);
            modules.Add(Database.baseModule);

            for (int i = 0; i < totalStages; i++)
            {
                for (int j = 0; j < modulesPerStage; j++)
                {
                    int value = GetCurrentModuleDifficulty(modules, totalModules, modulesByLevel.Count);
                    List<GameObject> currentValidModules =
                        modulesByLevel[value];
                    modules.Add(currentValidModules[ProcessSeed(ref currentSeed, 0, currentValidModules.Count - 1)]);
                }
                modules.Add(Database.checkpointModule);
                Debug.Log("currentSeed: " + currentSeed);
                modules.Add((/*(currentSeed % 1) > .7f &&*/ (i < totalStages - 1)) ? Database.shopModule : Database.breakModule);
            }
            modules.Add(Database.baseModule);
            modules.Add(Database.breakModule);
            modules.Add(Database.breakModule);
            modules.Add(Database.baseModule);

            return new Blueprint(Seed, modules,SeedOffset);
        }

        private List<STag> GetRemoveTags(float currentSeed)
        {
            List<STag> dontRemoveTags = CalculateDontRemoveTags(ref currentSeed);
                                
            foreach (var tag in dontRemoveTags)
                Debug.Log($"<color=green> Using tag </color> {tag.name} ");
            
            List<STag> removeModules = new List<STag>(canBeRemovedModules);
            
            for (int i = 0; i < dontRemoveTags.Count; i++)
                removeModules.Remove(dontRemoveTags[i]);


            foreach (STag alwaysBeRemovedModule in alwaysRemovedModules)
                if(!removeModules.Contains(alwaysBeRemovedModule)) removeModules.Add(alwaysBeRemovedModule);

            return removeModules;
        }

        private List<STag>  CalculateDontRemoveTags(ref float currentSeed)
        {
            Debug.Log("Dont RemoveTags  Start Seed: " + currentSeed);
            
            
            List<STag> iterationList = new List<STag>(canBeRemovedModules);
            List<STag> dontRemoveTags =  new List<STag>();

            if (canBeRemovedModules.Length <= tagsToNotRemove) return dontRemoveTags;

            for (int i = 0; i < tagsToNotRemove; i++)
            {
                currentSeed = Perlin.Noise(currentSeed);
                Debug.Log("i:" + i +" Seed: " + currentSeed);

                STag selectedTag = iterationList[(int) Mathf.Lerp(0, iterationList.Count - 1, Math.Abs(currentSeed))];
                dontRemoveTags.Add(selectedTag);
                iterationList.Remove(selectedTag);
                
                Debug.Log($"selectedTag: {selectedTag.name}");
            }
            return dontRemoveTags;
        }

        private int GetCurrentModuleDifficulty(List<GameObject> modules, int totalModules, int difficultyLevels)
        {
            float proportion = (float)modules.Count / totalModules;
            int value = Mathf.RoundToInt(difficultyCurve.Evaluate(proportion) * (difficultyLevels-1));
            return value;
        }

        private List<List<GameObject>> SplitByDifficulty(List<GameObject> allValidModules)
        {
            List<GameObject>[] splittedModules = new List<GameObject>[10];
            for (int i = 0; i < splittedModules.Length; i++)  splittedModules[i] = new List<GameObject>();
            for (int i = 0; i < allValidModules.Count; i++)
            {
                Terrain_Module tModule = allValidModules[i].GetComponent<Terrain_Module>();
                splittedModules[tModule.Difficulty].Add(allValidModules[i]);
            }

            List<List<GameObject>> processList = new List<List<GameObject>>();
            for (int i = 0; i < splittedModules.Length; i++)
            {
                if(splittedModules[i].Count > 0)
                    processList.Add(new List<GameObject>(splittedModules[i]));
            }
            return processList;
        }

        public int ProcessSeed(ref float currentSeed, int minValue, int maxValue)
        {
            float x = Mathf.Abs(Perlin.Noise(currentSeed) * Mathf.PI);    
            int value = Mathf.RoundToInt(Mathf.Lerp(minValue, maxValue, x));
            currentSeed = /*(resolution * x * GoldenRatio) / resolution*/ x;
            return value;
        }
    }
}