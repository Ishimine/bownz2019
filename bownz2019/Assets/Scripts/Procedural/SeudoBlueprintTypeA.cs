﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Procedural
{
    [CreateAssetMenu(menuName = "Procedural/Level Blueprints/TypeA", fileName = "BP_TypeA")]
    public class SeudoBlueprintTypeA : SeudoBlueprint
    {
        private const float GoldenRatio = 1.61803398874989484820458683436f;
    
        public int totalStages = 16;
        public int modulesPerStage = 2;

        [SerializeField] public STag[] includeModules;
        [SerializeField] public STag[] removeModules;

        public override Blueprint CreateBlueprint()
        {
            List<GameObject> validModules = new List<GameObject>(Database.CollectModulesWith(includeModules, removeModules));Database.CollectModulesWith(includeModules, removeModules);
            List<GameObject> modules = new List<GameObject>();

            float currentSeed = Seed * GoldenRatio;
            
            modules.Add(Database.breakModule);

            for (int i = 0; i < totalStages; i++)
            {
                for (int j = 0; j < modulesPerStage; j++)
                {
                    modules.Add(validModules[ProcessSeed(ref currentSeed, 0, validModules.Count-1)]);
                }
                modules.Add(Database.checkpointModule);
                modules.Add(Database.breakModule);
            }
            return new Blueprint(Seed, modules);
        }

        public int ProcessSeed(ref float currentSeed, int minValue, int maxValue)
        {
            Debug.Log("Current: " + currentSeed);
            float x = Perlin.Noise(currentSeed);
            int value = Mathf.RoundToInt(Mathf.Lerp(minValue, maxValue, x));
            currentSeed += (1 + value) * GoldenRatio;
            return value;
        }
    }
}