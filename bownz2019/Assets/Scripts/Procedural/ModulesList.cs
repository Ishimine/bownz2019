﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class ModulesList : ScriptableListOfGameObject
{
    [ReadOnly,ShowInInspector] string state => isValid ? "IsValid" : "Invalid";

    private STag myTag;
    public STag MyTag
    {
        get { return myTag; }
        set { myTag = value; }
    }

    bool isValid = false;

    List<Gates> gates;

    [Button]
    public bool ValidateList()
    {
        CollectGates();
        isValid = HasBlankModule();
        isValid = isValid && HasUniqueModule();
        return isValid;
    }
    
    private string ValidMsg()
    {
        return (isValid) ? "List is Valid" : "List is Invalid";
    }

    private void CollectGates()
    {
        gates = new List<Gates>();
        Gates current;
        foreach (var item in Elements)
        {
            current = item.GetComponent<Gates>();
            if(current == null)
            {
                isValid = false;
                break;
            }
            else
                gates.Add(current);
        }
    }

    private bool HasBlankModule()
    {
        foreach (var item in gates)
        {
            if (item.AreAllGatesOpen())
                return true;
        }
        Debug.Log("Doesnt have Blank module");
        return false;
    }

    private bool HasUniqueModule()
    {
        RootSTagComponent rootSTagComponent;
        foreach (var item in gates)
        {
            rootSTagComponent = item.GetComponent<RootSTagComponent>();
            if (rootSTagComponent.Tags.Count == 1 && rootSTagComponent.Tags[1] == myTag)
                return true;
        }
        Debug.Log("NO UNIQUE MODULE");
        return false;
    }

  
}

