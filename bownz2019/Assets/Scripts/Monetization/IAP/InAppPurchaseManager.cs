﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class InAppPurchaseManager : ScriptableObject
{
    #region Events

    private static Action<bool> onHasPremiumPass;
    public static Action<bool> OnHasPremiumPass
    {
        get { return onHasPremiumPass; }
        set { onHasPremiumPass = value; }
    }

    private static Action<bool> onHasRemoveAds;
    public static Action<bool> OnHasRemoveAds
    {
        get { return onHasRemoveAds; }
        set { onHasRemoveAds = value; }
    }

    #endregion

    #region Variables

    private static bool hasPremiumPass = false;
    public static bool HasPremiumPass
    {
        get { return hasPremiumPass = false; }
        set { hasPremiumPass = value; }
    }

    private static bool hasRemoveAds = false;
    public static bool HasRemoveAds
    {
        get { return hasRemoveAds = false; }
        set { hasRemoveAds = value; }
    }

    #endregion
}
