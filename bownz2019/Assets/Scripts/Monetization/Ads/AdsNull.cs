public class AdsNull : Ads_BaseManager
{
    public override float GetBannerHeightInPixels()
    {
        return 0;
    }

    public override float GetBannerWidthInPixels()
    {
        return 0;
    }

    public override void HideBanner()
    {
    }

    public override bool IsInterstitialReady()
    {
        return false;
    }

    public override bool IsVideoReady()
    {
        return false;
    }

    public override void RequestBanner()
    {
    }

    public override void RequestInterstitial()
    {
    }

    public override void RequestVideo()
    {
    }

    public override bool ShowBanner()
    {
        return false;
    }

    public override bool ShowInterstitial()
    {
        return false;
    }

    public override bool ShowVideo()
    {
        return false;
    }

}