﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;
using System;

public  class AdMobManager : Ads_BaseManager
{
    private  BannerView bannerView;
    private  InterstitialAd interstitial;
    private  RewardBasedVideoAd rewardBasedVideo;

    private static bool isDebug = false;
    public static bool IsDebug { get { return isDebug; } set { isDebug = value; } }

    private bool bannerIsPortrait = false;

    private  string AppId
    {
        get
        {
            #if UNITY_ANDROID   
                return "ca-app-pub-3018238902565307~9167044245";
            #elif UNITY_IPHONE
                    return "";
            #else
                    return"unexpected_platform";
            #endif
        }
    }

    private  string BannerId
    {
        get
        {
#if UNITY_ANDROID
            if (isDebug)
                return "ca-app-pub-3940256099942544/6300978111"; //Test ID
            else
                return "ca-app-pub-3018238902565307/8256808323";
                // old banner => return "ca-app-pub-3018238902565307/7079354726";
        #elif UNITY_IPHONE
                    return "ca-app-pub-3940256099942544~1458002511";
        #else
                    return "unexpected_platform";
        #endif
        }
    }

    private  string InterstitialId
    {
        get
        {
#if UNITY_ANDROID
            if (isDebug)
                return "ca-app-pub-3940256099942544/1033173712"; //Test ID
            else
                return "ca-app-pub-3018238902565307/9378318300";
#elif UNITY_IPHONE
                    return "ca-app-pub-3940256099942544/2934735716";
#else
                    return "unexpected_platform";
#endif
        }
    }

    private  string VideoId
    {
        get
        {
#if UNITY_ANDROID
            if (isDebug)
                return "ca-app-pub-3940256099942544/5224354917"; //Test ID
            else
                return "ca-app-pub-3018238902565307/9430289198";
        #elif UNITY_IPHONE
                    return "ca-app-pub-3940256099942544~1458002511";
        #else
                    return "unexpected_platform";
        #endif
        }
    }

    public AdMobManager(bool useBanner, bool useInterstitial, bool useVideo)
    {
        // Initialize the Google Mobile Ads SDK.
        MobileAds.Initialize(AppId);
    
        if (useVideo)
        {
            // Get singleton reward based video ad reference.
            rewardBasedVideo = RewardBasedVideoAd.Instance;

            // Called when an ad request has successfully loaded.
            rewardBasedVideo.OnAdLoaded += HandleVideo_RewardBasedVideoLoaded;

            // Called when an ad request failed to load.
            rewardBasedVideo.OnAdFailedToLoad += HandleVideo_RewardBasedVideoFailedToLoad;

            // Called when an ad is shown.
            rewardBasedVideo.OnAdOpening += HandleVideo_RewardBasedVideoOpened;

            // Called when the ad starts to play.
            rewardBasedVideo.OnAdStarted += HandleVideo_RewardBasedVideoStarted;

            // Called when the user should be rewarded for watching a video.
            rewardBasedVideo.OnAdRewarded += HandleVideo_RewardBasedVideoRewarded;

            rewardBasedVideo.OnAdCompleted += HandleVideo_RewardBasedVideoCompeted;

            // Called when the ad is closed.
            rewardBasedVideo.OnAdClosed += HandleVideo_RewardBasedVideoClosed;
            
            // Called when the ad click caused the user to leave the application.
            rewardBasedVideo.OnAdLeavingApplication += HandleVideo_RewardBasedVideoLeftApplication;

            RequestVideo();
        }

            

        if(useInterstitial)
            RequestInterstitial();
        if(useBanner)
            RequestBanner();
    }

    #region Banner

    public override void RequestBanner()
    {
          if(UseSmartBanner)
              RequestBanner(AdSize.SmartBanner, AdPosition.Bottom);
          else
              RequestBanner(AdSize.Banner, AdPosition.Bottom);
       // RequestBanner(AdSize.IABBanner, AdPosition.Bottom);
    }

    public void RequestBanner(AdSize adSize, AdPosition position)
    {

        if (bannerView != null)
            bannerView.Destroy();

        // Create a 320x50 banner at the top of the screen.
        bannerView = new BannerView(BannerId, adSize, position);

        // Called when an ad request has successfully loaded.
        bannerView.OnAdLoaded += HandleBanner_OnAdLoaded;
        // Called when an ad request failed to load.
        bannerView.OnAdFailedToLoad += HandleBanner_OnAdFailedToLoad;
        // Called when an ad is clicked.
        bannerView.OnAdOpening += HandleBanner_OnAdOpened;
        // Called when the user returned from the app after an ad click.
        bannerView.OnAdClosed += HandleBanner_OnAdClosed;
        // Called when the ad click caused the user to leave the application.
        bannerView.OnAdLeavingApplication += HandleBanner_OnAdLeavingApplication;
        
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();

        // Load the banner with the request.
        bannerView.LoadAd(request);
    }

    public override float GetBannerHeightInPixels()
    {
        float x = bannerView.GetHeightInPixels();
        Debug.Log("<color=brown> BannerHeightInPixels: </color>" + x + "px");
        return x;
    }

    public override float GetBannerWidthInPixels()
    {
        float x = bannerView.GetWidthInPixels();
        Debug.Log("<color=brown> GetWidthInPixels: </color>" + x);
        return x;
    }

    public override bool ShowBanner()
    {
        if (bannerView != null)
        {
            return true;
        }
        else
            return false;
    }

    public override void HideBanner()
    {
        if (bannerView != null)
        {
            bannerView.Hide();
            OnBannerHide?.Invoke();
        }
    }

    #endregion

    #region OnBannerResponse

    public  void HandleBanner_OnAdLoaded(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleBanner_AdLoaded event received");
        OnBannerLoaded?.Invoke();
        OnBannerShow?.Invoke();
    }

    public  void HandleBanner_OnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        MonoBehaviour.print("HandleBanner_FailedToReceiveAd event received with message: "
                            + args.Message);
        OnBannerFailedToLoad?.Invoke();

    }

    private  Action onBannerOpened;
    public  Action OnBannerOpened
    {
        get
        {
            return onBannerOpened;
        }
        set
        {
            onBannerOpened = value;
        }
    }

    public  void HandleBanner_OnAdOpened(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleBanner_AdOpened event received");
        onBannerOpened?.Invoke();
    }

    private  Action onBannerClosed;
    public  Action OnBannerClosed
    {
        get
        {
            return onBannerClosed;
        }
        set
        {
            onBannerClosed = value;
        }
    }

    public  void HandleBanner_OnAdClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleBanner_AdClosed event received");
        onBannerClosed?.Invoke();
       // RequestBanner();
    }

    private  Action onBannerOnAdLeaving;
    public  Action OnBannerOnAdLeaving
    {
        get
        {
            return onBannerOnAdLeaving;
        }
        set
        {
            onBannerOnAdLeaving = value;
        }
    }

    public  void HandleBanner_OnAdLeavingApplication(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleBanner_AdLeavingApplication event received");
        onBannerOnAdLeaving?.Invoke();
    }

    #endregion

    #region Interstitial

    public override void RequestInterstitial()
    {
        // Initialize an InterstitialAd.
        interstitial = new InterstitialAd(InterstitialId);
        // Create an empty ad request.

        // Called when an ad request has successfully loaded.
        interstitial.OnAdLoaded += HandleInterstitial_OnAdLoaded;
        // Called when an ad request failed to load.
        interstitial.OnAdFailedToLoad += HandleInterstitial_OnAdFailedToLoad;
        // Called when an ad is shown.
        interstitial.OnAdOpening += HandleInterstitial_OnAdOpened;
        // Called when the ad is closed.
        interstitial.OnAdClosed += HandleInterstitial_OnAdClosed;
        // Called when the ad click caused the user to leave the application.
        interstitial.OnAdLeavingApplication += HandleInterstitial_OnAdLeavingApplication;

        AdRequest request = new AdRequest.Builder().Build();
        // Load the interstitial with the request.
        interstitial.LoadAd(request);

    }
   

    public override bool IsInterstitialReady()
    {
        if (interstitial == null) return false;
        return interstitial.IsLoaded();
    }

    public override bool ShowInterstitial()
    {
        Debug.Log("<color=blue>AdMobManager: ShowInterstitial</color>");
        if (IsInterstitialReady())
        {
            Debug.Log("<color=blue>Showing</color>");
            interstitial.Show();
            return true;
        }
        Debug.Log("<color=red> Interstitial wasn't Ready... what are you doing!!!</color>" );
        return false;
    }

    #endregion

    #region OnInterstitialResponse


    public  void HandleInterstitial_OnAdLoaded(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleInterstitial_AdLoaded event received");
        OnInterstitialLoaded?.Invoke();
    }

    public  void HandleInterstitial_OnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        MonoBehaviour.print("HandleInterstitial_FailedToReceiveAd event received with message: "
                            + args.Message);

        OnInterstitialFailedToLoad?.Invoke();
    }

    public  void HandleInterstitial_OnAdOpened(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleInterstitial_AdOpened event received");
        OnInterstitialOpened?.Invoke();
    }

    public  void HandleInterstitial_OnAdClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleInterstitial_AdClosed event received");
        OnInterstitialClosed?.Invoke();
        interstitial.Destroy();
        //RequestInterstitial();
    }

    private  Action onInterstitialOnAdLeaving;
    public  Action OnInterstitialOnAdLeaving
    {
        get
        {
            return onInterstitialOnAdLeaving;
        }
        set
        {
            onInterstitialOnAdLeaving = value;
        }
    }

    public  void HandleInterstitial_OnAdLeavingApplication(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleInterstitial_AdLeavingApplication event received");
        onInterstitialOnAdLeaving?.Invoke();
    }

    #endregion

    #region Video

    public override void RequestVideo()
    {
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();

        // Load the rewarded video ad with the request.
        rewardBasedVideo.LoadAd(request, VideoId);
    }

    public override bool IsVideoReady()
    {
       return  rewardBasedVideo.IsLoaded();
    }

    public override bool ShowVideo()
    {
        if(IsVideoReady())
        {
            rewardBasedVideo.Show();
            return true;
        }
        return false;
    }

    #endregion

    #region OnVideoResponse

    public  void HandleVideo_RewardBasedVideoLoaded(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleVideo_RewardBasedVideoLoaded event received");
        OnVideoLoaded?.Invoke();
    }


    public  void HandleVideo_RewardBasedVideoFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        MonoBehaviour.print(
            "HandleVideo_RewardBasedVideoFailedToLoad event received with message: "
                             + args.Message);
        OnVideoFailedToLoad?.Invoke();
        //RequestVideo();
    }

    private  Action onVideoOpened;
    public  Action OnVideoOpened
    {
        get
        {
            return onVideoOpened;
        }
        set
        {
            onVideoOpened = value;
        }
    }

    public  void HandleVideo_RewardBasedVideoOpened(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleVideo_RewardBasedVideoOpened event received");
        onVideoOpened?.Invoke();
    }

    public  void HandleVideo_RewardBasedVideoStarted(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleVideo_RewardBasedVideoStarted event received");
        OnVideoStarted?.Invoke();
    }

 

    public  void HandleVideo_RewardBasedVideoClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleVideo_RewardBasedVideoClosed event received");
        OnVideoClosed?.Invoke();
    }

    public void HandleVideo_RewardBasedVideoCompeted(object sender, EventArgs args)
    {
        OnVideoCompleted?.Invoke();
    }

    private RewardEvent onVideoRewardedR;
    public RewardEvent OnVideoRewardedR
    {
        get
        {
            return onVideoRewardedR;
        }
        set
        {
            onVideoRewardedR = value;
        }
    }

     public  void HandleVideo_RewardBasedVideoRewarded(object sender, Reward args)
     {
         string type = args.Type;
         double amount = args.Amount;
         MonoBehaviour.print(
             "HandleVideo_RewardBasedVideoRewarded event received for "
                         + amount.ToString() + " " + type);

        OnVideoRewarded?.Invoke();
    }

  /*  public void HandleVideo_RewardBasedVideoRewarded(object sender, EventArgs args)
     {
              string type = args.Type;
              double amount = args.Amount;
              MonoBehaviour.print(
                  "HandleVideo_RewardBasedVideoRewarded event received for "
                              + amount.ToString() + " " + type);
              onVideoRewardedR?.Invoke(args);

        Debug.Log("Fucking Rewarded Video");
        //  OnVideoRewarded?.Invoke();
    }
    */

    /// <summary>
    /// This method is invoked after OnAdOpened, when a user click opens another app (such as the Google Play store), backgrounding the current app. 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="args"></param>
    private Action onVideoLeftApplication;
    public  Action OnVideoLeftApplication
    {
        get
        {
            return onVideoLeftApplication;
        }
        set
        {
            onVideoLeftApplication = value;
        }
    }

    public  void HandleVideo_RewardBasedVideoLeftApplication(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleVideo_RewardBasedVideoLeftApplication event received");
        onVideoLeftApplication?.Invoke();
    }
    #endregion

}


public delegate void RewardEvent(Reward reward);