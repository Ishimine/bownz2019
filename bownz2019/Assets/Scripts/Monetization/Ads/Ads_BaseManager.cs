﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class Ads_BaseManager
{
    public abstract void RequestBanner();

    public abstract bool ShowBanner();
    public abstract void HideBanner();

    public abstract void RequestInterstitial();
    public abstract bool ShowInterstitial();
    public abstract bool IsInterstitialReady();

    public abstract void RequestVideo();
    public abstract bool ShowVideo();

    public abstract bool IsVideoReady();



    private bool useSmartBanner = true;
    public bool UseSmartBanner
    {
        get
        {
            return useSmartBanner;
        }

        set
        {
            useSmartBanner = value;
        }
    }

    public abstract float GetBannerHeightInPixels();
    public abstract float GetBannerWidthInPixels();

    private static Action onVideoLoaded;
    public static Action OnVideoLoaded
    {
        get
        {
            return onVideoLoaded;
        }
        set
        {
            onVideoLoaded = value;
        }
    }

    private static Action onVideoRewarded;
    public static Action OnVideoRewarded
    {
        get
        {
            return onVideoRewarded;
        }
        set
        {
            onVideoRewarded = value;
        }
    }

    /// <summary>
    /// This method is invoked when when the rewarded video ad is closed due to the user tapping on the close icon or using the back button. If your app paused its audio output or game loop, this is a great place to resume it. 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="args"></param>
    private static Action onVideoClosed;
    public static Action OnVideoClosed
    {
        get
        {
            return onVideoClosed;
        }
        set
        {
            onVideoClosed = value;
        }
    }

    private static Action onVideoCompleted;
    public static Action OnVideoCompleted
    {
        get
        {
            return onVideoCompleted;
        }
        set
        {
            onVideoCompleted = value;
        }
    }


    private static Action onVideoStarted;
    public static Action OnVideoStarted
    {
        get
        {
            return onVideoStarted;
        }
        set
        {
            onVideoStarted = value;
        }
    }

    private static Action onInterstitialLoaded;
    public static Action OnInterstitialLoaded
    {
        get
        {
            return onInterstitialLoaded;
        }
        set
        {
            onInterstitialLoaded = value;
        }
    }

    private static Action onInterstitialClosed;
    public static Action OnInterstitialClosed
    {
        get
        {
            return onInterstitialClosed;
        }
        set
        {
            onInterstitialClosed = value;
        }
    }

    private static Action onBannerLoaded;
    public static Action OnBannerLoaded
    {
        get
        {
            return onBannerLoaded;
        }
        set
        {
            onBannerLoaded = value;
        }
    }

    private static Action onBannerShow;
    public static Action OnBannerShow
    {
        get
        {
            return onBannerShow;
        }
        set
        {
            onBannerShow = value;
        }
    }

    private static Action onBannerHide;
    public static Action OnBannerHide
    {
        get
        {
            return onBannerHide;
        }
        set
        {
            onBannerHide = value;
        }
    }

    private static Action onInterstitialOpened;
    public static Action OnInterstitialOpened
    {
        get
        {
            return onInterstitialOpened;
        }
        set
        {
            onInterstitialOpened = value;
        }
    }

    private static Action onBannerFailedToLoad;
    public static Action OnBannerFailedToLoad
    {
        get
        {
            return onBannerFailedToLoad;
        }
        set
        {
            onBannerFailedToLoad = value;
        }
    }

    private static Action onInterstitialFailedToLoad;
    public static Action OnInterstitialFailedToLoad
    {
        get
        {
            return onInterstitialFailedToLoad;
        }
        set
        {
            onInterstitialFailedToLoad = value;
        }
    }

    private static Action onVideoFailedToLoad;
    public static Action OnVideoFailedToLoad
    {
        get
        {
            return onVideoFailedToLoad;
        }
        set
        {
            onVideoFailedToLoad = value;
        }
    }

}
