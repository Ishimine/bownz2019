﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BannerHeight
{
    private static Camera mainCamera;
    private static Camera MainCamera
    {
        get
        {
            if(mainCamera == null)
            {
                mainCamera = Camera.main;
            }
            return mainCamera;
        }
    }
    
    public static float GetSmartBannerHeight()
    {
        /*    float value;
            Debug.Log("Screen.height: " + Screen.height);
            float hDP = Screen.height * (160 / Screen.dpi);
            Debug.Log("Screen.heightDP: " + hDP);

            if (hDP <= 400)
            //      if (Screen.height <= 400 * (Screen.dpi / 160))
            {
                Debug.Log("32");
                value = 32 * Screen.dpi / 160; //(Screen.dpi / 160);
            }
            else if (hDP <= 720)
            //       else if (Screen.height <= 720 * (Screen.dpi / 160))
            {
                Debug.Log("50");
                value = 50 * Screen.dpi / 160;// * (Screen.dpi / 160);
            }
            else
            {
                Debug.Log("90");
                value = 90 * Screen.dpi / 160; //(Screen.dpi / 160);
            }

            Debug.Log("Screen.dpi :" + Screen.dpi);
            Debug.Log("value: " + value);

            Debug.Log("value/Screen.dpi: " + value / Screen.dpi);
            Debug.Log("value/100: " + value / 100);

            Debug.Log("Screen.orientation: " + Screen.orientation);

            return value / 100;
            */
        float value;
        float hDP = Screen.height * (160 / Screen.dpi);

        if (hDP <= 400)
        {
            value = 32;
        }
        else if (hDP <= 720)
        {
            value = 50;
        }
        else
        {
            value = 90;
        }
        return GetBannerHeight(value, 160);
    }
    public static float GetStandarBannerHeight()
    {
        return GetBannerHeight(50, 160);
    }

    public static float GetBannerHeight(float inValue, float standarDPI)
    {
        float value =  (Screen.dpi * inValue)/ standarDPI; //(Screen.dpi / 160);
        value /= 100;

        float dif = (float)Screen.height / GetScreenUnitySize();
        return value/ dif;
    }
  
    public static float GetScreenUnitySize()
    {
        return (MainCamera.orthographicSize * 2) * 100;
    }

    public static float PixelsToWorldUnits(float pixels)
    {
        float cameraSize = MainCamera.orthographicSize * 2;
        float pixelsHeight = MainCamera.pixelHeight;
        return (pixels * cameraSize) / pixelsHeight;
    }

    public static float ScaledPixelsToWorldUnits(float pixels)
    {
        float cameraSize = MainCamera.orthographicSize * 2;
        float pixelsHeight = MainCamera.scaledPixelHeight;
        return (pixels * cameraSize) / pixelsHeight;
    }

    /*
  public static float GetBannerHeight2(float inValue, float standarDPI)
  {
      float value;

      value = (inValue * (Screen.dpi / 160))/100;

      Debug.Log("Value: " + value);
      return value;
  }


  public static float GetBannerHeight3(float inValue, float standarDPI)
  {
      float a, b, c, d;
      //A => B
      //C => D
      a = standarDPI;
      b = 50;
      c = Screen.dpi;
      float value = (c*b )/ a;

      return value;
  }


  public static float GetBannerHeight4(float inValue, float standarDPI)
  {
      float a, b, c, d;
      //A => B
      //C => D
      a = standarDPI;
      b = 50;
      c = Screen.dpi;
      float value = (c * b) / a;
      return (GetScreenUnitySize() * value) / Screen.height; ;
  }*/

}
