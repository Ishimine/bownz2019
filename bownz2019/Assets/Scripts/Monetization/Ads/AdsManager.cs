﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Firebase.RemoteConfig;
using System;

[CreateAssetMenu(menuName = "Managers/Ads")]
public class AdsManager : ScriptableSingleton<AdsManager>
{
    [SerializeField, TabGroup("Interstitial")]
    private RemoteBool useInterstitial = new RemoteBool(){UseRemoteVariable = true, defaultValue = false, Key = "UseInterstitial"};
    public bool UseInterstitial => useInterstitial.GetValue();
    
    [SerializeField, TabGroup("VideoReward")]
    private RemoteBool useVideReward = new RemoteBool(){UseRemoteVariable = true, defaultValue = true, Key = "UseVideoReward"};
    public bool UseVideoReward => useVideReward.GetValue();

    [SerializeField, TabGroup("Banner")]
    private RemoteBool useBanner = new RemoteBool(){UseRemoteVariable = true, defaultValue = false, Key = "UseBanner"};
    public bool UseBanner => useBanner.GetValue();

    [SerializeField, ShowIf("UseBanner"), TabGroup("Banner")]
    private RemoteBool useSmartBanner = new RemoteBool() { UseRemoteVariable = true, defaultValue = false, Key = "UseSmartBanner"};
    private bool UseSmartBanner => useSmartBanner.GetValue();

    [SerializeField, ShowIf("UseBanner"), TabGroup("Banner")]
    private bool limitGameAreaByBanner = false;
    public bool LimitGameAreaByBanner { get { return limitGameAreaByBanner; } set { limitGameAreaByBanner = value; } }

    private float minimumRequestTime = 45;

    public RemoteInt int_InitialWaitTime = new RemoteInt() { Key = "ADS_Interstitials_TimeFirst", defaultValue = 45, UseRemoteVariable = false};
    [TabGroup("Interstitial"),SerializeField]
    public float Int_InitialWaitTime
    {
        get
        {
            return int_InitialWaitTime.GetValue();
        }
    }

    public RemoteInt int_BetweenWaitTime = new RemoteInt() { Key = "ADS_Interstitials_TimeBetween", defaultValue = 45, UseRemoteVariable = false };
    [TabGroup("Interstitial"), SerializeField]
    public float Int_BetweenWaitTime
    {
        get
        {
            return int_BetweenWaitTime.GetValue();
        }
    }
    [TabGroup("Interstitial")]
    private bool int_TimeDone = false;
    public bool Int_TimeDone { get { return (useInt_TimeCap) ? int_TimeDone : true; } set { int_TimeDone = value; } }

    [TabGroup("Interstitial")]
    public bool useInt_TimeCap;
    Timer interstitialTimer;
    Timer InterstitialTimer
    {
        get
        {
            if (interstitialTimer == null)
            {
                interstitialTimer = new GameObject().AddComponent<Timer>();
                interstitialTimer.gameObject.name = "InterstitialTimer";
                interstitialTimer.UseRealTime = false;
            }
            return interstitialTimer;
        }
    }

    private Ads_BaseManager current = null;
    public Ads_BaseManager Current { get { return current; } }

    public bool useAdMob = true;

    private CodeAnimator animBanner = new CodeAnimator();
    private CodeAnimator animInterstitial = new CodeAnimator();
    private CodeAnimator animVideo = new CodeAnimator();

    private Empty monoProxy;
    public Empty MonoProxy
    {
        get
        {
            if (monoProxy == null)
            {
                monoProxy = new GameObject().AddComponent<Empty>();
                monoProxy.name = "AdsManager EmptyProxy";
            }
            return monoProxy;   
        }
    }

    private bool isShowingBanner = false;

    private bool isBannerLoaded = false;
    protected bool IsBannerLoaded { get { return isBannerLoaded; } set { isBannerLoaded = value; } }

    public static Action OnVideoFailedToLoadEvent { get; set; }

    public override void InitializeSingleton()
    {
        base.InitializeSingleton();
        if (useAdMob && !InAppPurchaseManager.HasPremiumPass)
            current = new AdMobManager(
                (UseBanner && !InAppPurchaseManager.HasPremiumPass && !InAppPurchaseManager.HasRemoveAds) || 
                (Settings.Instance.IsDebugBuild && UseBanner), 
                (UseInterstitial && !InAppPurchaseManager.HasPremiumPass) || (Settings.Instance.IsDebugBuild && UseBanner), 
                UseVideoReward);
        else
            current = new AdsNull();

        if (useInt_TimeCap)
        {
            InterstitialTimer.OnTimeStarted = () =>
            {
                Debug.Log("<color=green>Interstitial </color> Timer Started");
                Int_TimeDone = false;
            };
            InterstitialTimer.OnTimeDone = InterstitialTimeDone;
            InterstitialTimer.TargetTime = Int_InitialWaitTime;
            InterstitialTimer.OnTimeProgress = OnInterstitialProgress;
            InterstitialTimer.StartTimer();
        }
        Register();
    }

    private void OnInterstitialProgress(float value)
    {
    }

    private void OnHasPremiumPassResponse(bool hasPremiumPass)
    {
        if(hasPremiumPass)
        {
            if(current != null) current.HideBanner();
        }
    }

    private void InterstitialTimeDone()
    {
        Int_TimeDone = true;
        Debug.Log("<color=green>Interstitial </color> Timer Done");
    }

    private void Register()
    {
        Unregister();
        InAppPurchaseManager.OnHasPremiumPass += OnNoAdsPassResponse;
        InAppPurchaseManager.OnHasRemoveAds += OnNoAdsPassResponse;

        Ads_BaseManager.OnInterstitialClosed += OnInterstitialClosed;
        Ads_BaseManager.OnInterstitialLoaded += OnInterstitialLoaded;
        Ads_BaseManager.OnInterstitialOpened += OnInterstitialOpened;
        Ads_BaseManager.OnInterstitialFailedToLoad += OnInterstitialFailedToLoad;

        Ads_BaseManager.OnVideoClosed += OnVideoClosed;
        Ads_BaseManager.OnVideoLoaded += OnVideoLoaded;
        Ads_BaseManager.OnVideoRewarded += OnVideoRewarded;
        Ads_BaseManager.OnVideoStarted += OnVideoStarted;
        Ads_BaseManager.OnVideoFailedToLoad += OnVideoFailedToLoad;
        Ads_BaseManager.OnVideoCompleted += OnVideoCompleted;

        Ads_BaseManager.OnBannerShow += OnBannerShow;
        Ads_BaseManager.OnBannerHide += OnBannerHide;
        Ads_BaseManager.OnBannerLoaded += OnBannerLoaded;
        Ads_BaseManager.OnBannerFailedToLoad += OnBannerFailedToLoad;
    }

    private void OnNoAdsPassResponse(bool value)
    {
        if(InAppPurchaseManager.HasPremiumPass || InAppPurchaseManager.HasRemoveAds)
        {
            HideBanner();
        }
        else
        {
            ShowBanner();
        }
    }

    private void Unregister()
    {
        InAppPurchaseManager.OnHasRemoveAds -= OnNoAdsPassResponse;
        InAppPurchaseManager.OnHasPremiumPass -= OnNoAdsPassResponse;

        Ads_BaseManager.OnInterstitialClosed -= OnInterstitialClosed;
        Ads_BaseManager.OnInterstitialLoaded -= OnInterstitialLoaded;
        Ads_BaseManager.OnInterstitialOpened -= OnInterstitialOpened;
        Ads_BaseManager.OnInterstitialFailedToLoad -= OnInterstitialFailedToLoad;

        Ads_BaseManager.OnVideoClosed -= OnVideoClosed;
        Ads_BaseManager.OnVideoLoaded -= OnVideoLoaded;
        Ads_BaseManager.OnVideoRewarded -= OnVideoRewarded;
        Ads_BaseManager.OnVideoStarted -= OnVideoStarted;
        Ads_BaseManager.OnVideoFailedToLoad -= OnVideoFailedToLoad;
        Ads_BaseManager.OnVideoCompleted -= OnVideoCompleted;

        Ads_BaseManager.OnBannerShow -= OnBannerShow;
        Ads_BaseManager.OnBannerHide -= OnBannerHide;
        Ads_BaseManager.OnBannerLoaded -= OnBannerLoaded;
        Ads_BaseManager.OnBannerFailedToLoad -= OnBannerFailedToLoad;
    }

    private void OnBlackDeadTimeEndedResponse()
    {
        //TryToShowBanner();
    }

    public static bool IsInterstitialReady()
    {
        return Instance.Current.IsInterstitialReady();
    }

    public static bool IsVideoReady()
    {
        return Instance.Current.IsVideoReady();
    }

    public void RequestBanner()
    {
        Debug.Log("<color=blue> >>> ADS </color> RequestBanner ");
        if (!UseBanner || InAppPurchaseManager.HasPremiumPass || InAppPurchaseManager.HasRemoveAds)
        {
            Debug.Log("useBanner: " + useBanner);
            Debug.Log("settings.HasPremiumPass: " + InAppPurchaseManager.HasPremiumPass);
            Debug.Log("settings.HasRemoveAds: " + InAppPurchaseManager.HasRemoveAds);
            return;
        }
        current.RequestBanner(); 
    }

    public void HideBanner()
    {
        Debug.Log("AdsManager: HideBanner");
        if (/*!useBanner || settings.HasPremiumPass || settings.HasRemoveAds ||*/ current == null) return;
        current.HideBanner();
    }

    public void RestartInterstitialTimer()
    {
        Debug.Log("<color=green> Interstitial </color> Timer Restarted");
        InterstitialTimer.Restart();
    }

    public void RequestInterstitial()
    {
        Debug.Log("<color=blue> >>> ADS </color> RequestInterstitial");
        if (current == null || !UseInterstitial || InAppPurchaseManager.HasPremiumPass) return;
        current.RequestInterstitial();
    }

    public void ShowBanner()
    {
        if (!UseBanner || InAppPurchaseManager.HasPremiumPass || InAppPurchaseManager.HasRemoveAds || current == null || !IsBannerLoaded)
        {
            return;
        }
        if (CanShowBanner())
        {
            current.ShowBanner();
        }
    }

    private bool CanShowBanner()
    {
        bool value = false;
    
        return value;
    }

    public void ShowInterstitialAnyway()
    {
        if(UseInterstitial && current != null && current.IsInterstitialReady()) current.ShowInterstitial();
    }

    public void FastShowInterstitial()
    {
        ShowInterstitial();
    }

    public void FastShowVideo()
    {
        ShowVideo();
    }

    public bool ShowInterstitial()
    {
        Debug.Log("<color=green>Interstitial</color> SHOW useInterstitial" + useInterstitial);
        Debug.Log("Current is null" + current == null);
        if (!UseInterstitial || InAppPurchaseManager.HasPremiumPass|| current == null) return false;
        return current.ShowInterstitial();
    }

    public void RequestVideo()
    {
        Debug.Log("<color=blue> >>> ADS </color> RequestVideo ");
        if (!UseVideoReward || current == null) return;
        current.RequestVideo();
    }

    public void ShowVideo()
    {
        if (current == null)
        {
            Debug.LogError("Current is null");
            return;
        }

        if (!current.IsVideoReady())
        {
            current.IsVideoReady();
            return;
        }
        current.ShowVideo();
    }

    public static Action OnVideoRewardedEvent { get; set; }

    private void OnVideoRewarded()
    {
        DoOnMainThread.ExecuteOnMainThread.Enqueue(() =>
        {
            Debug.Log("<color=blue>  OnVideoRewarded </color>");
            OnVideoRewardedEvent?.Invoke();
            InterstitialTimer.Restart();
        });
    }

    public static Action VideoLoadedEvent { get; set; }

    public void OnVideoLoaded()
    {
        DoOnMainThread.ExecuteOnMainThread.Enqueue(() =>
        {
            Debug.Log("<color=blue>  OnVideoLoaded </color>");
            VideoLoadedEvent?.Invoke();});
    }

    public static Action VideoClosedEvent { get; set; }

    public void OnVideoClosed()
    {
        DoOnMainThread.ExecuteOnMainThread.Enqueue(() =>
        {
            Debug.Log("<color=blue>  OnVideoClosed </color>");
            RequestVideo();
        });
    }

    public static Action VideoStartedEvent { get; set; }

    public void OnVideoStarted()
    {
        DoOnMainThread.ExecuteOnMainThread.Enqueue(() =>
        {
            Debug.Log("<color=blue>  OnVideoStarted </color>");
        });
    }

    public static Action BannerLoadedEvent { get; set; }

    public static Action BannerFailedToLoadEvent { get; set; }

    public static Action BannerHideEvent { get; set; }

    public static Action BannerShowEvent { get; set; }

    public static Action InterstitialLoadedEvent { get; set; }

    private static Action InterstitialrFailedToLoadEvent;

    public void OnInterstitialLoaded()
    {
        DoOnMainThread.ExecuteOnMainThread.Enqueue(() =>
        {
            Debug.Log("<color=blue>  OnInterstitialLoaded </color>");
            InterstitialLoadedEvent?.Invoke();
        });
    }

    public static Action InterstitialClosedEvent { get; set; }

    public void OnInterstitialClosed()
    {
        DoOnMainThread.ExecuteOnMainThread.Enqueue(() =>
        {
            Debug.Log("<color=blue>  OnInterstitialClosed </color>");
            InterstitialClosedEvent?.Invoke();
            InterstitialTimer.StartTimer(Int_BetweenWaitTime);
            RequestInterstitial();
        });

    }

    public static Action InterstitialOpenedEvent { get; set; }

    public override AdsManager Myself => this;

    public void OnInterstitialOpened()
    {
        DoOnMainThread.ExecuteOnMainThread.Enqueue(() =>
        {
            Debug.Log("<color=blue>  OnInterstitialOpened </color>");
            InterstitialOpenedEvent?.Invoke();
            InterstitialTimer.Restart();
        });
    }

    public void OnBannerLoaded()
    {
        DoOnMainThread.ExecuteOnMainThread.Enqueue(() =>
        {
            Debug.Log("<color=red> >>>  OnBannerLoaded </color>");
            if(!CanShowBanner()) HideBanner();
            BannerLoadedEvent?.Invoke();

            IsBannerLoaded = true;
        });
    }

    public void OnBannerShow()
    {
        DoOnMainThread.ExecuteOnMainThread.Enqueue(() =>
        {
            Debug.Log("<color=blue>  OnBannerShow </color>");

            BannerShowEvent?.Invoke();
            isShowingBanner = true;
        });
    }

    public void OnBannerHide()
    {
        DoOnMainThread.ExecuteOnMainThread.Enqueue(() =>
        {
            BannerHideEvent?.Invoke();
            isShowingBanner = false;
            Debug.Log("<color=brown> >>>>>>>>>> </color>  OnBannerHide()");
        });
    }

    public void OnBannerFailedToLoad()
    {
        DoOnMainThread.ExecuteOnMainThread.Enqueue(() =>
        {
            Debug.Log("<color=red> >>>  OnBannerFailedToLoad </color>");
            BannerFailedToLoadEvent?.Invoke();
            animBanner.StartWaitAndExecute(MonoProxy, minimumRequestTime, RequestBanner, true);  });
        HideBanner();

        IsBannerLoaded = false;

    }

    public void OnInterstitialFailedToLoad()
    {
        DoOnMainThread.ExecuteOnMainThread.Enqueue(() =>
        {
        Debug.Log("<color=red> >>>  OnInterstitialFailedToLoad </color>");
        animInterstitial.StartWaitAndExecute(MonoProxy, minimumRequestTime,
            ()=>
            {
                Debug.Log("<color=blue> >>>  StartWaitAndExecute Interstitial Done </color>");
                RequestInterstitial();
            }, true);  });
    }

    public void OnVideoFailedToLoad()
    {
        DoOnMainThread.ExecuteOnMainThread.Enqueue(() =>
        {
            Debug.Log("<color=blue> >>>  OnVideoFailedToLoad </color>");
            OnVideoFailedToLoadEvent?.Invoke();
        });
    }

    public void OnVideoCompleted()
    {
        DoOnMainThread.ExecuteOnMainThread.Enqueue(() =>
        {
            Debug.Log("<color=blue> >>>  OnVideoCompleted </color>");
        });
    }

    public void RestorePurchase()
    {
        Debug.Log("RestorePurchase");
    }

    private void OnGameModeStartResponse()
    {
        HideBanner();
        TryToShowBanner();
    }

    private void OnGameModePreStartResponse()
    {
        //HideBanner();
    }
    
    public void TryToShowBanner()
    {
        if (CanShowBanner())    ShowBanner();
        else                    HideBanner();
    }

}