﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;

public class Timer : MonoBehaviour
{
    private bool useRealTime = true;
    public bool UseRealTime
    {
        get { return useRealTime; }
        set { useRealTime = value; }
    }

    [ShowInInspector,ReadOnly]
    private bool active = false;
    public bool Active
    {
        get { return active; }
    }

    [ShowInInspector,ReadOnly]
    private float currentTime;
    public float CurrentTime
    {
        get { return currentTime; }
        set { currentTime = value; }
    }

    [ShowInInspector,ReadOnly]
    private float targetTime;
    public float TargetTime
    {
        get { return targetTime; }
        set { targetTime = value; }
    }


    private Action onTimeStarted;
    public Action OnTimeStarted
    {
        get
        {
            return onTimeStarted;
        }
        set
        {
            onTimeStarted = value;
        }
    }


    private Action onTimeDone;
    public Action OnTimeDone
    {
        get
        {
            return onTimeDone;
        }
        set
        {
            onTimeDone = value;
        }
    }

    private Action<float> onProgress;
    public Action<float> OnTimeProgress
    {
        get
        {
            return onProgress;
        }
        set
        {
            onProgress = value;
        }
    }
    public void Update()
    {
        if (!active) return;
        if (currentTime <= 0)
        {
            onTimeDone?.Invoke();
            active = false;
        }
        else
        {
            if(useRealTime)
                currentTime -= Time.unscaledDeltaTime;
            else
                currentTime -= Time.deltaTime;

//            Debug.Log("CurrentTime: " + currentTime);
//            Debug.Log("TargetTime: " + targetTime);

            onProgress?.Invoke(1 -(currentTime / targetTime));
        }
    }

    public void StartTimer()
    {
        currentTime = targetTime;
        active = true;
        onTimeStarted?.Invoke();
    }

    public void StartTimer(float nTime)
    {
        targetTime = nTime;
        StartTimer();
    }

    public void StartTimer(float nTime, Action onTimeDoneResponse)
    {
        onTimeDone = onTimeDoneResponse;
        StartTimer(nTime);
    }

    public void StopTimer()
    {
        currentTime = targetTime;
        active = false;
    }

    public void ResumeTimer()
    {
        active = true;
    }

    public void PauseTimer()
    {
        active = false;
    }

    public void Restart()
    {
        StopTimer();
        StartTimer();
    }
}
