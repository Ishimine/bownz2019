﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;

public class Proyectile : MonoBehaviour
{
    public Rigidbody2D rb;


    public GameObject visual;
    public Collider2D col;

    [SerializeField] private UnityEvent onCollitionResponse;
    public UnityEvent OnCollitionResponse { get { return onCollitionResponse; } set { onCollitionResponse = value; } }
        
    [SerializeField] private float speed = 1;

    public Poolable poolable;


    private int particlePool;
    public int ParticlePool
    {
        get { return particlePool; }
        set { particlePool = value; }
    }

    public float deadWait = 4;


    public float lifeSpan = 5;

    private CodeAnimator codeAnimator = new CodeAnimator();

    private Action<Proyectile> onCollitionDone;
    public Action<Proyectile> OnCollitionDone
    {
        get { return onCollitionDone; }
        set { onCollitionDone = value; }
    }

    private Action<Proyectile> onLifeSpanEnd;
    public Action<Proyectile> OnLifeSpanEnd
    {
        get { return onLifeSpanEnd; }
        set { onLifeSpanEnd = value; }
    }

    private Action<Proyectile> onDead;
    public Action<Proyectile> OnDead
    {
        get { return onDead; }
        set { onDead = value; }
    }

   [SerializeField] private Particles_ReleaseAndPlay particles;

    private void OnEnable()
    {
        particles.transform.SetParent(transform);
        particles.transform.localPosition = Vector3.zero;
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        Collition();
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        Collition();
    }

    private void Awake()
    {
        codeAnimator.StartWaitAndExecute(this, 5, LifeSpawnEnd, false);
    }

    public void Initialize(float speed)
    {
        rb.simulated = true;
        this.speed = speed;
        visual.SetActive(true);
        col.enabled = true;

        gameObject.SetActive(true);
    }

    public void FixedUpdate()
    {
        rb.MovePosition(transform.position +  transform.right * speed * Time.deltaTime);
    }

    private void Collition()
    {
        particles.ReleaseAndPlay();
        particles.transform.position = transform.position;
        OnCollitionResponse?.Invoke();
        OnCollitionDone?.Invoke(this);
        StartDead();
        visual.SetActive(false);
        col.enabled = false;
    }

    private void LifeSpawnEnd()
    {
        OnLifeSpanEnd?.Invoke(this);
        StartDead();
    }

    private void OnDisable()
    {
        codeAnimator.Stop(this);
    }

    private void StartDead()
    {
        rb.simulated = false;
        codeAnimator.StartWaitAndExecute(this, deadWait, Dead, false);
    }

    private void Dead()
    {
        OnDead?.Invoke(this);
    }

}
