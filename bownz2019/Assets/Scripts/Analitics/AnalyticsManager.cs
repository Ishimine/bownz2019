﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Firebase.Analytics;

public static class AnalyticsManager 
{
    public static void SendEvent_PostScore(string character, int value)
    {
        if(!FirebaseStarter.FirebaseInitilized)return;

        FirebaseAnalytics.LogEvent(
            FirebaseAnalytics.EventPostScore, 
            new Parameter[]
            {
                new Parameter(
                    FirebaseAnalytics.ParameterCharacter,
                    character), 
                new Parameter(
                    FirebaseAnalytics.ParameterScore,
                    value) 
            });
    }
    
    public static void SendEvent_LevelUp(string character, int value)
    {
        if(!FirebaseStarter.FirebaseInitilized)return;

        FirebaseAnalytics.LogEvent(
            FirebaseAnalytics.EventLevelUp, 
            new Parameter[]
            {
                new Parameter(
                    FirebaseAnalytics.ParameterCharacter,
                    character), 
                new Parameter(
                    FirebaseAnalytics.ParameterLevel,
                    value) 
            });
    }

    public static void SendEvent_TutorialBegin()
    {
        if(!FirebaseStarter.FirebaseInitilized)return;

        FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventTutorialBegin);
    }
    
    public static void SendEvent_TutorialComplete()
    {
        if(!FirebaseStarter.FirebaseInitilized)return;

        FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventTutorialComplete);
    }
    
    public static void SendEvent_TutorialSwipeTouched()
    {
        if(!FirebaseStarter.FirebaseInitilized)return;

        FirebaseAnalytics.LogEvent("Tutorial_SwipeTouched");
    }
    
    public static void SendEvent_TutorialStarPickedUp(int count)
    {
        if(!FirebaseStarter.FirebaseInitilized)return;

        FirebaseAnalytics.LogEvent(
            "Tutorial_StarPickedUp", 
            FirebaseAnalytics.ParameterIndex,
            count
            );
    }

    public static void SendEvent(string eventName, string parameterName, string parameterValue)
    {
        if(!FirebaseStarter.FirebaseInitilized)return;

        FirebaseAnalytics.LogEvent(eventName, parameterName, parameterValue);
    }
    
    public static void SendEvent_UnlockAchievement(string achievement)
    {
        if(!FirebaseStarter.FirebaseInitilized)return;

        FirebaseAnalytics.LogEvent(
            FirebaseAnalytics.EventUnlockAchievement, 
            FirebaseAnalytics.ParameterAchievementId, 
            achievement);
    }
    
    public static void SendEvent_SpendCurrency(string currencyName,int amount)
    {
        if(!FirebaseStarter.FirebaseInitilized)return;

        FirebaseAnalytics.LogEvent(
            FirebaseAnalytics.EventSpendVirtualCurrency,
            new Parameter[]
            {
                new Parameter(
                    FirebaseAnalytics.ParameterItemName,
                    currencyName),
                new Parameter(
                    FirebaseAnalytics.ParameterValue,
                    amount)
            });
    }

    public static void SendEvent_EarnCurrency(string currencyName,int amount)
    {
        if(!FirebaseStarter.FirebaseInitilized)return;

        FirebaseAnalytics.LogEvent(
            FirebaseAnalytics.EventEarnVirtualCurrency,
            new Parameter[]
            {
                new Parameter(
                    FirebaseAnalytics.ParameterItemName,
                    currencyName),
                new Parameter(
                    FirebaseAnalytics.ParameterValue,
                    amount)
            });
    }
    
    public static void SendEvent_LevelStart(string levelId)
    {
        if(!FirebaseStarter.FirebaseInitilized)return;

        FirebaseAnalytics.LogEvent(
            FirebaseAnalytics.EventLevelStart,
            FirebaseAnalytics.ParameterLevel,
            levelId);
    }
    
    public static void SendEvent_LevelEnd(string levelId)
    {
        if(!FirebaseStarter.FirebaseInitilized)return;

        FirebaseAnalytics.LogEvent(
            FirebaseAnalytics.EventLevelEnd,
            FirebaseAnalytics.ParameterLevel,
            levelId);
    }

    public static void SendEvent_DailyLevelsCompleted(int amount)
    {
        if(!FirebaseStarter.FirebaseInitilized)return;

        if (PlayerPrefs.GetInt("DailyLevelsCompleted", 0) == 0)
        {
            FirebaseAnalytics.LogEvent(
                "DailyLevelsCompleted",
                FirebaseAnalytics.ParameterValue,
                amount);
            PlayerPrefs.SetInt("DailyLevelsCompleted", 1);
        }
    }

    public static void SendCurrentScreen(string screenName, string screenType)
    {
        if(!FirebaseStarter.FirebaseInitilized)return;
        FirebaseAnalytics.SetCurrentScreen(screenName, screenType);
    }
}
