﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;

public class MoveBetween2Points : MonoBehaviour
{
    public Transform target;
    public Transform from;
    public Transform to;

    public LineRenderer lineRenderer;
    public Color colorA;
    public Color colorB;

    public float duration = 1;
    [Range(0,1)]
    public float startOffset = 0;

    public AnimationCurve curve;

    private void CreateNodes()
    {
        if (from == null)
        {
            from = new GameObject().transform;
            from.SetParent(transform);
            from.name = "From";
        }
        if (to == null)
        {
            to = new GameObject().transform;
            to.SetParent(transform);
            to.name = "To";
        }
    }

    private void FixedUpdate()
    {
#if UNITY_EDITOR
        if (!Application.isPlaying)
        {
            PositionLinePoints();
            target.position = GetPosition(startOffset);
        }
#endif

        target.transform.position = GetPosition((Time.time / duration) + startOffset);
        //rb.MovePosition());
    }

    public void UpdatePosition(float value)
    {
        target.position = GetPosition(value);
    }

    public Vector2 GetPosition(float value)
    {
        float x = curve.Evaluate(Mathf.PingPong(value * 2, 1));
        RefreshLineColor(Mathf.PingPong(value * 4, 1));
       return Vector2.LerpUnclamped(from.position, to.position, x);
    } 

    private void RefreshLineColor(float x)
    {
        if (!lineRenderer) return;
        Color color = Color.Lerp(colorA, colorB, x);
        lineRenderer.startColor = color;
        lineRenderer.endColor = color;
    }

    private void OnEnable()
    {
        this.AddObserver(GameMode_Ready_Response, Notifications.GameMode_Ready);
    }

    private void OnDisable()
    {
        this.RemoveObserver(GameMode_Ready_Response, Notifications.GameMode_Ready);
    }

    private void GameMode_Ready_Response(object arg1, object arg2)
    {
        PositionLinePoints();
    }

    private void PositionLinePoints()
    {
        if (!lineRenderer) return;
        lineRenderer.positionCount = 2;
        lineRenderer.SetPositions(new Vector3[]
        {
            from.transform.localPosition,
            to.transform.localPosition
        });
    }

#if UNITY_EDITOR
    private void OnValidate()
    {
        if (Application.isPlaying) return;

        if (target == null)
            return;
            
        target.position = @from.position;
    }

    private void OnDrawGizmos()
    {   
        Gizmos.color = Color.cyan;
        Vector3 position = @from.position;
        Gizmos.DrawWireSphere(position, .15f);
        Vector3 position1 = to.position;
        Gizmos.DrawWireSphere(position1, .15f);
        Gizmos.DrawLine(position, position1);
    }
#endif
}