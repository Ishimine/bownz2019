﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[ExecuteInEditMode]
public class Terrain_Module : MonoBehaviour
{
    [SerializeField] private bool canSpawnCoins = true;

    public bool CanSpawnCoins => canSpawnCoins && coinSpawnPoints.Count > 0;

    [SerializeField] private bool blockedByCoins = false;
    public bool BlockedByCoins => blockedByCoins && CanSpawnCoins;

    [TabGroup("Dependencies")] public Gates gates;
    [TabGroup("Dependencies")] public Terrain_Wrapper wrapper;
    [TabGroup("Dependencies")] public Transform checkpointsParent;
    [TabGroup("Dependencies")] public List<Transform> checkpointsPoints;
    [TabGroup("Dependencies")] public Transform coinSpawnParent;
    [TabGroup("Dependencies")] public List<Transform> coinSpawnPoints;
    [TabGroup("Dependencies")] public MeshFromColliders levelMesher;
    
    private OneWay_ByCoin topDoor;

    private OneWay_ByCoin TopDoor
    {
        get
        {
            if (topDoor == null)
                topDoor = GameObjectLibrary.Instantiate("TopDoor", transform).GetComponent<OneWay_ByCoin>();
            return topDoor;
        }
    }

    [TabGroup("Properties"), SerializeField, Range(1, 10)] private int difficulty = 1;
    public int Difficulty => Mathf.Clamp(difficulty-1, 0, 9);

    [SerializeField, TabGroup("Properties")] private float moduleHeight;
    public float ModuleHeight
    {
        get { return moduleHeight; }
        set { moduleHeight = value; }
    }

    private bool inverted = false;
    public bool Inverted
    {
        get
        {
            return inverted;
        }
    }

    public RootSTagComponent RootSTagComponent;
    public Transform GetHeighestCoinSpawnPoint()
    {
        float y = 0;
        Transform heighest = null;
        foreach (var item in coinSpawnPoints)
        {
            if (heighest == null)
                heighest = item;
            else if (item.transform.position.y > heighest.position.y)
                heighest = item;
        }
        return heighest;
    }

    public void UpdateUV()
    {
        levelMesher.demoMesh.UpdateMyUv();
    }

    [Button]
    public void Validate()
    {
        Debug.Log("Validating: " + this);
        RefreshHeigth();
        gates.AutoPosition();
        gates.AutoCheck();
        RootSTagComponent.CollectTags();
        ProcessSpawnPoints();

        CompositeCollider2D col = GetComponent<CompositeCollider2D>();
        if (col != null) DestroyImmediate(col,true);
        
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        if (rb != null) DestroyImmediate(rb,true);

        col = wrapper.gameObject.GetComponent<CompositeCollider2D>();
        if (col != null) DestroyImmediate(col,true);
        
        rb = wrapper.gameObject.GetComponent<Rigidbody2D>();
        if (rb != null) DestroyImmediate(rb,true);
    }

    public void UseTopDoor(bool value, List<StarCoin> stars)
    {
        if (value)
        {
            TopDoor.gameObject.SetActive(true);
            TopDoor.transform.localPosition = Vector3.up * ModuleHeight;
            topDoor.Initialize(stars);
        }
        else if (topDoor != null) topDoor.gameObject.SetActive(false);
    }

    [Button]
    public void CreateMesh()
    {
        levelMesher.CreateMeshFromLevelEditor(false, true,true);
    }

    public void RefreshHeigth()
    {
        transform.localPosition = Vector3.zero;
        wrapper.WrapTerrain();
    }

    public void InvertGates()
    {
        inverted = !inverted;
        gates.Invert();
    }

#if UNITY_EDITOR

   

    private void OnValidate()
    {
        if(Application.isEditor && !Application.isPlaying)
        ProcessSpawnPoints();
    }


[ExecuteInEditMode]
    private void FixedUpdate()
    {
        if(Application.isEditor && !Application.isPlaying)
            ProcessSpawnPoints();
    }
#endif

    [Button]
    public void ProcessSpawnPoints()
    {
        checkpointsPoints = new List<Transform>();
        foreach (Transform item in checkpointsParent.transform)
        {
            if (item != checkpointsParent.transform)
                checkpointsPoints.Add(item);
        }

        coinSpawnPoints = new List<Transform>();
        foreach (Transform item in coinSpawnParent.transform)
        {
            if (item != coinSpawnParent.transform)
                coinSpawnPoints.Add(item);
        }
    }
}
