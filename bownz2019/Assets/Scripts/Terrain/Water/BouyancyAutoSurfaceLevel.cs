﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class BouyancyAutoSurfaceLevel : MonoBehaviour
{
    public float surfaceDisplace;
    public BuoyancyEffector2D buoyancyEffector;

    public BoxCollider2D col;

    private void OnValidate()
    {
        UpdateSurface();
    }

    private void OnEnable()
    {
        UpdateSurface();
    }

    private void UpdateSurface()
    {
        buoyancyEffector.surfaceLevel = surfaceDisplace + col.bounds.extents.y;
    }
    /*
#if UNITY_EDITOR
    private void FixedUpdate()
    {
        }
#endif*/
}
