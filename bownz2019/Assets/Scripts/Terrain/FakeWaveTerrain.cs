﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;

public class FakeWaveTerrain : MonoBehaviour, IOnCollisionEnter2D
{
    public bool useDistanceSensitivity = true;
    public bool roundDirection = true;
    public GameObject waveNodePrefab;

    public FakeWaveNode[] fakeWaveNodes;

    [Range(0, 0.99f)]
    public float propagationDamp = .9f;

    public float propagationMinForce = .05f;

    public LayerMask layerMask;

    [SerializeField] float childExtend = .6f;

    public Vector2 defaultForce = new Vector2(.1f,.2f);

    
    public float speed = 1;
    /// <summary>
    /// Porcentaje de perdida que se da cada FixedUpdate
    /// </summary>
    public float dampen = .94f;
    /// <summary>
    /// Fuerza MAXIMA que puede tener un nodo en base a la distancia con su posicion objetivo
    /// </summary>
    public float maxTensionSpeed = 0.1f;
    /// <summary>
    /// Distancia maxima a partir de la cual se aplica la fuerz maxima
    /// </summary>
    public float maxLenght = .3f;
    public float maxVelocity = 10;

    /// <summary>
    /// Tiempo que tarda en propagarse la fuerza de un nodo a otro
    /// </summary>
    public float timeBetweenPropagation = .05f;

    /// <summary>
    /// Fuerza minima por debajo de la cual en la cual se DEJA de simular y se resetea la posicion
    /// </summary>
    public float stopThreshold = .02f;

    public float minSesitivity = .5f;

    /// <summary>
    /// Diferencia de Velocity que tiene que tener la pelota con el terreno para generar el SPLASH mas fuerte posible.
    /// </summary>
    public float impactVelocityNeededForMax = 5;


    public void Initialize()
    {
        for (int i = 0; i < fakeWaveNodes.Length; i++)
        {
            fakeWaveNodes[i].TargetPosition = fakeWaveNodes[i].transform.position;
            fakeWaveNodes[i].gameObject.SetActive(true);
            fakeWaveNodes[i].name = "Index: " + i;
            fakeWaveNodes[i].Owner = this;
        }
    }
    
    public void Splash(int index, float force)
    {
        StartCoroutine(SplashRutine(index, force));
    }

    public void SplashFromCenter(float force)
    {
        Splash(Mathf.RoundToInt((float)fakeWaveNodes.Length / 2),force);
    }

    IEnumerator SplashRutine(int centerIndex, float force)
    {
        int iDir;
        float cForce;
        int cIndex = centerIndex;
        float sensitibity = fakeWaveNodes[centerIndex].sensitivity;
        fakeWaveNodes[centerIndex].AddForce(force * sensitibity, 1* sensitibity);
        cForce = force;
        cIndex = centerIndex;
        cForce *= propagationDamp * sensitibity;
        int count=0;
        do
        {
            yield return new WaitForSeconds(timeBetweenPropagation);
            count++;
            for (int i = 0; i < 2; i++)
            {
                iDir = (i == 0) ? 1 : -1;
                cIndex = (int)Mathf.Repeat(centerIndex + count * iDir, fakeWaveNodes.Length);
                sensitibity = fakeWaveNodes[cIndex].AddForce(cForce, Mathf.Lerp(0, 1, cForce / force));
            }
            cForce *= propagationDamp * sensitibity;
        } while (cForce > propagationMinForce);
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if(layerMask.Contains(collision.gameObject.layer))
        {
            int index = GetClosestNodeIndex(collision.GetContact(0).point);
            Splash(index, Mathf.Lerp(defaultForce.x, defaultForce.y, collision.relativeVelocity.magnitude / impactVelocityNeededForMax));
        }
    }

    private int GetClosestNodeIndex(Vector2 pos)
    {
        float minDistance = float.MaxValue;
        float current;
        int minIndex = 0;
        for (int i = 0; i < fakeWaveNodes.Length; i++)
        {
            current = pos.GetSqrMagnitud(fakeWaveNodes[i].transform.position);
            if (current < minDistance)
            {
                minDistance = current;
                minIndex = i;
                if (minDistance <= childExtend) break; //Si la distancia es MUY chica, no hace falta buscar mas
            }
        }
        return minIndex;
    }
}
