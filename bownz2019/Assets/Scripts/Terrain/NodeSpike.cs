﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class NodeSpike : MonoBehaviour
{
    public IOnCollisionEnter2D owner;
    public SpriteRenderer spikeRender;
    public SpriteRenderer bodyRender;

    public LayerMask targetLayer;

    [SerializeField] private CodeAnimatorCurve curveIn = null;
    [SerializeField] private CodeAnimatorCurve curveOut = null;

    private CodeAnimator codeAnimator = new CodeAnimator();

    public Collider2D col;

    private int index;

    public void Initialize(IOnCollisionEnter2D parentCollider)
    {
        this.owner = parentCollider;
        col.isTrigger = true;
        spikeRender.color = ColorPallete.Wall;
        bodyRender.color = Color.clear;
        spikeRender.transform.localPosition = Vector2.zero;
    }

    [Button]
    public void Show()
    {
        Color startSpikeColor = spikeRender.color;
        Color startBaseColor = bodyRender.color;
        Vector2 spikeStartPosition = spikeRender.transform.localPosition;
        Vector2 spikeTargetPosition = Vector2.up * .5f;
        codeAnimator.StartAnimacion(this, x =>
        {
            spikeRender.color = Color.Lerp(startSpikeColor, ColorPallete.Spike, x);
            bodyRender.color = Color.Lerp(startBaseColor, ColorPallete.Spike, x);
            spikeRender.transform.localPosition = Vector2.Lerp(spikeStartPosition, spikeTargetPosition, x);
        }, DeltaTimeType.deltaTime, AnimationType.Simple, curveIn.Curve, ShowDone, curveIn.Time);
    }

    void ShowDone()
    {
        col.isTrigger = false;
    }

    [Button]
    public void Hide()
    {
        col.isTrigger = true;
        Color startSpikeColor = spikeRender.color;
        Color startBaseColor = bodyRender.color;
        Vector2 spikeStartPosition = spikeRender.transform.localPosition;
        codeAnimator.StartAnimacion(this, x =>
        {
            spikeRender.color = Color.Lerp(startSpikeColor, ColorPallete.Wall, x);
            bodyRender.color = Color.Lerp(startBaseColor, Color.clear, x);
            spikeRender.transform.localPosition = Vector2.Lerp(spikeStartPosition, Vector2.zero, x);
        }, DeltaTimeType.deltaTime, AnimationType.Simple, curveOut.Curve, HideDone, curveOut.Time);
    }

    void HideDone()
    {

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        owner?.OnCollisionEnter2D(collision);
    }
}
