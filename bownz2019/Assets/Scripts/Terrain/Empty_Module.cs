﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using mattatz.Triangulation2DSystem.Example;

public class Empty_Module : MonoBehaviour
{
    public TerrainWalls terrainWalls;

    public BoxCollider2D[] colliders;
    public MeshFilter[] meshFilters;

    [SerializeField] List<Vector3> defaultVectors = new List<Vector3>();


    [SerializeField] Vector3[] current;

    [Button]
    private void GetDefaultVectors()
    {
        meshFilters[0].mesh.GetVertices(defaultVectors);
        for (int i = 0; i < defaultVectors.Count; i++)
        {
            defaultVectors[i] = new Vector3(defaultVectors[i].x, defaultVectors[i].y - transform.position.y);
        }
    }

    [Button]
    public void SetHeight(float nHeight)
    {
        //terrainWalls.SetHeight(nHeight, false);
        //demoMesh.UpdateMyUV();
        List<Vector3> vertices = new List<Vector3>();

        for (int i = 0; i < meshFilters.Length; i++)
        {
            meshFilters[i].mesh.GetVertices(vertices);
            for (int j = 0; j < vertices.Count; j++)
            {
                vertices[j] =  new Vector3(defaultVectors[j].x, (defaultVectors[j].y * nHeight) - .5f); 
            }
            current = new Vector3[vertices.Count];
            vertices.CopyTo(current);
            meshFilters[i].mesh.SetVertices(vertices);
            FinishWall(nHeight, vertices,meshFilters[i]);
            meshFilters[i].mesh.RecalculateBounds();
        }
        

    }

    private void FinishWall(float nHeight, List<Vector3> vertices, MeshFilter mFilter)
    {
        for (int i = 0; i < colliders.Length; i++)
        {
            colliders[i].size = new Vector2(defaultVectors[1].x - defaultVectors[0].x, nHeight);
            colliders[i].offset = new Vector2(colliders[i].offset.x, colliders[i].size.y / 2 - .5f);
        }

        for (int i = 0; i < vertices.Count; i++)
        {
            vertices[i] = (new Vector3(vertices[i].x, vertices[i].y) + mFilter.transform.position)/10;
        }
           
        mFilter.mesh.SetUVs(0,vertices);
    }
}
