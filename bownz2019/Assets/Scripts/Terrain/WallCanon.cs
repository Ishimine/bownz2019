﻿using System.Collections;
using System.Collections.Generic;
using Pooling.Poolers;
using UnityEngine;
using Sirenix.OdinInspector;

public class WallCanon : MonoBehaviour
{
    [SerializeField] private SetPooler pool;
    public SetPooler Pool { get { return pool; } set { pool = value; } }

    [SerializeField] private bool startOnEnable = true;
    public bool StartOnEnable { get { return startOnEnable; } set { startOnEnable = value; } }

    [SerializeField] private Transform firePoint;
    [SerializeField] private SpriteRenderer bodyRenderer;
    [SerializeField] private SpriteRenderer canonRenderer;

    [SerializeField] private Color baseColor;
    [SerializeField] private Color fullColor;


    [SerializeField,Range(0,1)] private float timeOffset = .2f;
    [SerializeField] private float waitDuration = 3;
    [SerializeField] private float flicker = 1;
    [SerializeField] private float flickerRate = 5;

    [SerializeField] private float bulletSpeed = 4;

    [SerializeField] private AnimationCurve kickbackCurve ;
    [SerializeField] private float kickBackDuration = 1;
    [SerializeField] private float kickBackAmmount = 1;


    private CodeAnimator codeAnimator = new CodeAnimator();

    private void OnEnable()
    {
        bodyRenderer.color = baseColor;
        if (startOnEnable)
            codeAnimator.StartWaitAndExecute(this, timeOffset * waitDuration, StartTimer, false);
    }

    

    [Button]
    public void StartTimer()
    {
        codeAnimator.StartAnimacion(this,
            x =>
            {
                bodyRenderer.color = Color.Lerp(baseColor, fullColor, x);
            }, DeltaTimeType.deltaTime, AnimationType.Simple, null, PreFireAnimation, waitDuration);
    }


    [Button]
    public void PreFireAnimation()
    {
        codeAnimator.StartAnimacion(this,
          x =>
          {
              bodyRenderer.color = Color.Lerp(fullColor, Color.white, Mathf.PingPong(x * flickerRate,1));
          }, DeltaTimeType.deltaTime, AnimationType.Simple, null, Fire, flicker);
    }

    [Button]
    public void Fire()
    {
        bodyRenderer.color = baseColor;
        Proyectile proyectile = pool.Dequeue().GetComponent<Proyectile>();

        proyectile.OnDead += DisableProyectile;

        proyectile.transform.rotation = Quaternion.Euler(0,0,  bodyRenderer.transform.position.GetAngle(firePoint.position));
        proyectile.transform.position = firePoint.transform.position;

        Vector2 startPosition = canonRenderer.transform.localPosition;
        proyectile.Initialize(bulletSpeed);

        codeAnimator.StartAnimacion(this,
        x =>
        {
            canonRenderer.transform.localPosition = startPosition + Vector2.left * kickBackAmmount * x;
        }, DeltaTimeType.deltaTime, AnimationType.Simple, kickbackCurve, StartTimer, flicker);
    }

    private void DisableProyectile(Proyectile proyectile)
    {
        proyectile.OnLifeSpanEnd -= proyectile.OnDead;
        pool.Enqueue(proyectile.poolable);
    }


}
