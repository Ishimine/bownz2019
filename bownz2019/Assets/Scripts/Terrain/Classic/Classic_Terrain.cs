﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class Classic_Terrain : MonoBehaviour
{
    public Transform glassFloorSpawnPoint;
    public FakeWaveTerrain topTerrain;
    public FakeWaveTerrain botTerrain;
    public WaveTerrain_SpikeSpawnerRandom spikeSpawner;

    public Vector2Int floorHeight = new Vector2Int(-6, 3);

    public CodeAnimatorCurve animatorCurve;
    private CodeAnimator codeAnimator = new CodeAnimator();
    public Vector2 timeBetweenFloorElevations = new Vector2(5, 20);

    /// <summary>
    /// Una vez alcanzado este valor. La velocidad entre aumentos de elevacion sera la minima
    /// </summary>
    [MinValue(1)]
    public int coinCountToMaxSpeed = 30;
    public int currentCoinCount;

    public bool IsAtMaxHeight => botTerrain.transform.localPosition.y >= floorHeight[1];
    public bool IsAtMinHeight => botTerrain.transform.localPosition.y <= floorHeight[0];

    private void Awake()
    {
        topTerrain.Initialize();
        botTerrain.Initialize();
        spikeSpawner.Initialize(botTerrain);
    }

    private void OnEnable()
    {
        this.AddObserver(OnGameMode_Ready, Notifications.GameMode_Ready);
        this.AddObserver(OnTerrain_AllDown_Response, Notifications.Terrain_AllDown);
        this.AddObserver(OnTerrain_StartTimer_Response, Notifications.Terrain_StartTimer); 
        this.AddObserver(OnTerrain_StopTimer_Response, Notifications.Terrain_StopTimer);
        this.AddObserver(OnTerrain_FloorUp_Response, Notifications.Terrain_FloorUp); 
        this.AddObserver(Terrain_AddGlassFloor_Response, Notifications.Terrain_AddGlassFloor);
    }

    private void OnDisable()
    {
        this.RemoveObserver(OnGameMode_Ready, Notifications.GameMode_Ready);
        this.RemoveObserver(OnTerrain_AllDown_Response, Notifications.Terrain_AllDown);
        this.RemoveObserver(OnTerrain_StartTimer_Response, Notifications.Terrain_StartTimer);
        this.RemoveObserver(OnTerrain_StopTimer_Response, Notifications.Terrain_StopTimer);
        this.RemoveObserver(OnTerrain_FloorUp_Response, Notifications.Terrain_FloorUp);
        this.RemoveObserver(Terrain_AddGlassFloor_Response, Notifications.Terrain_AddGlassFloor);
    }

    private void Terrain_AddGlassFloor_Response(object arg1, object arg2)
    {
        GlassFloor gFloor = glassFloorSpawnPoint.transform.GetComponentInChildren<GlassFloor>();
        if (gFloor == null)
        {
            //gFloor.DestroyFloor();
            gFloor = GameObjectLibrary.Instantiate("GlassFloor").GetComponent<GlassFloor>();
            gFloor.transform.SetParent(glassFloorSpawnPoint);
            gFloor.transform.localPosition = Vector3.zero;
            gFloor.Initialize(0);
        }
        gFloor.AddBlock();
    }

    private void OnTerrain_FloorUp_Response(object arg1, object arg2)
    {
        FloorUp(false);
    }

    private void OnTerrain_AllDown_Response(object arg1, object arg2)
    {
        FloorAllDown((bool)arg2);
    }

    private void OnTerrain_StopTimer_Response(object arg1, object arg2)
    {
        codeAnimator.Stop(this);
        FloorAllDown(false);
    }

    private void OnTerrain_StartTimer_Response(object arg1, object arg2)
    {
        StartElevationTimer();
        botTerrain.SplashFromCenter(.24f);
    }

    private void OnBallDeadStart_Response(object arg1, object arg2)
    {
        FloorAllDown(false);
    }

    private void OnGameMode_Go(object arg1, object arg2)
    {
        StartElevationTimer();
    }

    private void OnCoinPickedUp(object source, object value)
    {
        currentCoinCount++;
    }

    private void OnGameMode_Ready(object source, object value)
    {
        spikeSpawner.SpawnAll();
        currentCoinCount = 0;
    }

    [Button]
    public void StartElevationTimer()
    {
        float time = Mathf.Lerp(timeBetweenFloorElevations.y, timeBetweenFloorElevations.x, currentCoinCount / coinCountToMaxSpeed);
        codeAnimator.StartWaitAndExecute(this, time,()=> FloorUp(true), false);
    }

    [Button]
    public void FloorUp(bool startElevationTimerLater)
    {
        if (IsAtMaxHeight) return;
        FloorElevationChange(1, startElevationTimerLater, 1);
    }

   [Button]
    public void FloorDown(bool startElevationTimerLater)
    {
        if (IsAtMinHeight) return;
        FloorElevationChange(-1, startElevationTimerLater,1);
    }

    [Button]
    public void FloorAllUp(bool startElevationTimerLater)
    {
        if (IsAtMaxHeight) return;
        FloorElevationChange(floorHeight[1] - botTerrain.transform.position.y, startElevationTimerLater, 1);
    }

    [Button]
    public void FloorAllDown(bool startElevationTimerLater)
    {
        if (IsAtMinHeight) return;
        FloorElevationChange(floorHeight[0] - botTerrain.transform.position.y, startElevationTimerLater, 1);
    }

    public void FloorElevationChange(float ammount, bool startElevationTimerLater, float splashForce)
    {
        Vector3 startPos = botTerrain.transform.localPosition;
        Vector3 displacement = Vector3.up * ammount;
        codeAnimator.StartAnimacion(this, x =>
        {
            botTerrain.transform.localPosition = startPos + displacement * x;
        }, DeltaTimeType.deltaTime, AnimationType.Simple, animatorCurve.Curve, 
        ()=>
        {
            if (startElevationTimerLater) StartElevationTimer();
            botTerrain.SplashFromCenter(splashForce);
        }, animatorCurve.Time);
    }
}
