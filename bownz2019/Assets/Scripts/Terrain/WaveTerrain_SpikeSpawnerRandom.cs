﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;
public class WaveTerrain_SpikeSpawnerRandom : MonoBehaviour
{
    public GameObject spikePrefab;

    private List<FakeWaveNode> nodes;
    private List<FakeWaveNode> inactiveNodes;
    private List<FakeWaveNode> activeNodes;

    private Dictionary<FakeWaveNode, NodeSpike> spikeNodes = new Dictionary<FakeWaveNode, NodeSpike>();

    private Action onRandomSpikeSpawned;
    public Action OnRandomSpikeSpawned
    {
        get { return onRandomSpikeSpawned; }
        set { onRandomSpikeSpawned = value; }
    }

    public bool CanSpawnMoreSpikes => inactiveNodes.Count > 0;

    public void Initialize(FakeWaveTerrain terrain)
    {
        nodes = new List<FakeWaveNode>();
        nodes.AddRange(terrain.fakeWaveNodes);
        activeNodes = new List<FakeWaveNode>();
        inactiveNodes = new List<FakeWaveNode>(nodes);
    }

    [Button]
    public void SpawnRandom()
    {
        int index = UnityEngine.Random.Range(0, inactiveNodes.Count);
        SpawnAt(index);
        OnRandomSpikeSpawned?.Invoke();
    }

    public void SpawnAt(int index)
    {
        var selected = inactiveNodes[index];
        inactiveNodes.RemoveAt(index);
        activeNodes.Add(selected);
        if (!spikeNodes.ContainsKey(selected)) InstantiateSpike(selected);
        spikeNodes[selected].Show();
    }

    [Button]
    public void SpawnAll()
    {
        for (int i = inactiveNodes.Count-1; i >= 0; i--)
            SpawnAt(i);
    }

    [Button]
    public void HideAll()
    {
        for (int i = activeNodes.Count-1; i >= 0; i--)
        {
            spikeNodes[activeNodes[i]].Hide();
            inactiveNodes.Add(activeNodes[i]);
            activeNodes.Remove(activeNodes[i]);
        }
    }

    private GameObject InstantiateSpike(FakeWaveNode fakeWaveNode)
    {
        GameObject go = Instantiate(spikePrefab, fakeWaveNode.transform);
        go.transform.localPosition = Vector3.zero;
        go.transform.localRotation = Quaternion.Euler(0, 0, 0);
        var nodeSpike = go.GetComponent<NodeSpike>();
        spikeNodes.Add(fakeWaveNode, nodeSpike);
        nodeSpike.Initialize(fakeWaveNode.Owner);
        return go;
    }

    public WaveNode GetNextNode()
    {
        return null;
    }

    private void OnGameMode_Reset(object arg1, object arg2)
    {
        HideAll();
    }
}
