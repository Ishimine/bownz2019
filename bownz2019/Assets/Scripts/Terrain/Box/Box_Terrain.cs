﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;

public class Box_Terrain : MonoBehaviour
{
    [SerializeField] FakeWaveTerrain terrain = null;
    [SerializeField] WaveTerrain_SpikeSpawnerRandom spikeSpawner = null;
    [SerializeField] private EdgeBoxCollider2D edgeBoxCollider2D = null;

    [SerializeField] private Rect boxArea;

    [SerializeField] private float edgeThreshold = -.25f;

    [ShowInInspector, ReadOnly] private int currentCount = 0;
    [ShowInInspector, ReadOnly]
    private float CurrentTimeBetweenSpikes
    {
        get
        {
            return Mathf.Lerp(timeBetweenSpikes.y, timeBetweenSpikes.x, (float)currentCount / maxCountForMaxSpeed);
        }
    }

    public Vector2 timeBetweenSpikes;
    [MinValue(1)]
    public int maxCountForMaxSpeed;

    private CodeAnimator codeAnimator = new CodeAnimator();

    private void Awake()
    {
        terrain.Initialize();
        spikeSpawner.Initialize(terrain);
    }

    private void OnEnable()
    {
        this.AddObserver(OnGameMode_Ready, Notifications.GameMode_Ready);
        this.AddObserver(OnGameMode_Go, Notifications.GameMode_Go);
        this.AddObserver(OnGameMode_End, Notifications.GameMode_End);
        this.AddObserver(OnCoinPickedResponse, Notifications.OnCoinPickedUp);
    }

    private void OnDisable()
    {
        this.RemoveObserver(OnGameMode_Ready, Notifications.GameMode_Ready);
        this.RemoveObserver(OnGameMode_Go, Notifications.GameMode_Go);
        this.RemoveObserver(OnGameMode_End, Notifications.GameMode_End);
        this.RemoveObserver(OnCoinPickedResponse, Notifications.OnCoinPickedUp);
    }

    private void OnGameMode_Ready(object arg1, object arg2)
    {
        spikeSpawner.HideAll();
    }

    private void OnGameMode_End(object arg1, object arg2)
    {
        codeAnimator.Stop(this);
    }

    private void OnCoinPickedResponse(object sender, object value)
    {
        currentCount++; //Sumamos una moneda al contador
        var id = (int)value;

        if (id == 1) //Si la moneda agarrada es de tipo Dorada escondemos todos los pinches
            spikeSpawner.HideAll();
    }

    private void OnGameMode_Go(object arg1, object arg2)
    {
        currentCount = 0;
        StartSpawnTimer();
    }

    [Button]
    public void UpdateArea()
    {
        edgeBoxCollider2D.Area = new Rect(boxArea.position - Vector2.one * edgeThreshold / 2, boxArea.size + Vector2.one * edgeThreshold);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(.5f, 0, .5f, .5f);
        Gizmos.DrawCube(boxArea.center, boxArea.size);
    }

    private void StartSpawnTimer()
    {
        codeAnimator.StartWaitAndExecute(
            this, 
            CurrentTimeBetweenSpikes, 
           ()=>
           {
               if(spikeSpawner.CanSpawnMoreSpikes) spikeSpawner.SpawnRandom();
               StartSpawnTimer();
           },
            false);
    }
}
