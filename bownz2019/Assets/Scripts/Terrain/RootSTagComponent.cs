﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class RootSTagComponent : MonoBehaviour
{
    [SerializeField] private List<STag> tags = new List<STag>();
    public List<STag> Tags
    {
        get { return tags; }
        set { tags = value; }
    }

    public void CollectTags()
    {
        tags = new List<STag>();
        STagComponent[] sTagComponents = GetComponentsInChildren<STagComponent>();
        foreach (var item in sTagComponents)
        {
            foreach (var sTag in item.STags)
            {
                if (!tags.Contains(sTag))
                    tags.Add(sTag);
            }
        }
        
    }
}
