﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class TerrainWalls : MonoBehaviour
{
    public float correcion = .10f;
    public SpriteRenderer leftWall;
    public SpriteRenderer rightWall;

    [Button]
    public void ResetColliders()
    {
        BoxCollider2D[] colliders = GetComponentsInChildren<BoxCollider2D>();
        foreach (var item in colliders)
        {
            item.size = Vector2.one;
        }
    }

    public void RoundHeight()
    {
        SetHeight(leftWall.size.y,true);
    }

    [Button]
    public void SetHeight(float height, bool changeX = true)
    {
        height = Mathf.CeilToInt(height);

        leftWall.size = new Vector2(leftWall.size.x, height);
        rightWall.size = new Vector2(rightWall.size.x, height);
        PositionWalls(height);
    }
    
    [Button]
    public void PositionWalls(bool changeX = true)
    {
        PositionWalls(rightWall.size.y, changeX);
    }

    public void PositionWalls(float height, bool changeX = true)
    {
        height = Mathf.CeilToInt(height);
        leftWall.transform.localPosition = PlayArea.Area.width / 2 * Vector2.left + Vector2.left * leftWall.size.x / 2 + ((changeX)?Vector2.up * height/2 + Vector2.right * correcion:Vector2.zero);
        rightWall.transform.localPosition = PlayArea.Area.width / 2 * Vector2.right + Vector2.right * leftWall.size.x / 2 + ((changeX) ? Vector2.up * height/2 - Vector2.right * correcion : Vector2.zero);

        leftWall.transform.localPosition = new Vector3( (leftWall.transform.localPosition.x + .1f).SnapTo(.25f), leftWall.transform.localPosition.y, leftWall.transform.localPosition.z); 
        rightWall.transform.localPosition = new Vector3( (rightWall.transform.localPosition.x - .1f).SnapTo(.25f), rightWall.transform.localPosition.y, rightWall.transform.localPosition.z); 
    }
}
