﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StasisArea : MonoBehaviour
{
    public Transform targetPoint;

    public float force = 5;
    public ForceMode2D forceMode;

    public LayerMask targetLayer;

    public void OnTriggerStay2D(Collider2D collision)
    {
        if(targetLayer.Contains(collision.gameObject.layer))
            collision.attachedRigidbody.AddForce(collision.attachedRigidbody.position.GetDirection(targetPoint.position) * Time.deltaTime * force, forceMode);
    }
}
