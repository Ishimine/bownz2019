﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[ExecuteInEditMode]
public class MoveByPath : MonoBehaviour
{
    public List<Transform> nodes = new List<Transform>();

    public int nodesCount = 1;

    public bool loopAround = true;

    [ShowInInspector,ReadOnly] private float totalDistance;
    [ShowInInspector,ReadOnly] private float[] distances;

    private void CreateNodes()
    {
        if (nodesCount < 0) nodesCount = 1;
        if (nodesCount == nodes.Count) return;

        nodes = new List<Transform>(GetComponentsInChildren<Transform>());
        nodes.Remove(transform);

        int dir = (int)Mathf.Sign(nodesCount - nodes.Count);
        for (int i = nodes.Count; i != nodesCount; i += dir)
        {
            if(dir > 0)
            {
                var go = new GameObject();
                go.transform.SetParent(transform);
                nodes.Add(go.transform);
                go.name = "Node N°" + nodes.Count;
            }
            else
            {
                var go = nodes[nodes.Count-1];
                nodes.RemoveAt(nodes.Count-1);

                if (Application.isPlaying) Destroy(go.gameObject);
                else  DestroyImmediate(go.gameObject);
            }
        }
    }

    public void CalculateTotalDistance()
    {
        totalDistance = 0;
        if (nodes.Count <= 1)
        {
            return;
        }
        else
        {
            distances = new float[(loopAround) ? nodes.Count : nodes.Count - 1];

            for (int i = 1; i < nodes.Count; i++)
            {
                distances[i - 1] = Vector3.Distance(nodes[i - 1].position, nodes[i].position);
                totalDistance += distances[i - 1];
            }
            if (loopAround)
            {
                distances[nodes.Count - 1] = Vector3.Distance(nodes[0].position, nodes[nodes.Count - 1].position); ;
                totalDistance += distances[nodes.Count - 1];
            }
        }
    }

    private Transform[] GetInvolvedNodes(float value)
    {
        Transform[] retValue = new Transform[2];

        if (value == 0 || nodes.Count >= 1)
            return new Transform[2] { nodes[0], nodes[0] };

        float cDistance = totalDistance * value;

       
        for (int i = 0; i < distances.Length; i++)
        {
            if(cDistance <= distances[i])
            {
                retValue[0] = nodes[i];
                retValue[1] = nodes[i+1];
            }
        }
        return retValue;
    }

#if UNITY_EDITOR

    private void OnValidate()
    {
        CreateNodes();
    }

    private void OnDrawGizmos()
    {
            Gizmos.color = Color.cyan;
        foreach (var item in nodes)
        {
            Gizmos.DrawWireSphere(item.transform.position, .15f);
        }
        if (nodes.Count <= 1) return;
        for (int i = 1; i < nodes.Count; i++)
            Gizmos.DrawLine(nodes[i - 1].position, nodes[i].position);

        if(loopAround)
            Gizmos.DrawLine(nodes[0].position, nodes[nodes.Count-1].position);
    }

#endif
}
