﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class FakeWaveNode : MonoBehaviour
{
    public float sensitivity = 1;
    public Vector2 localDirection = Vector2.up;
    public FlashFX flash;

    private FakeWaveTerrain owner;
    public FakeWaveTerrain Owner
    {
        get { return owner; }
        set
        {
            owner = value;
            Initialize();
        }
    }

    private Vector2 targetPosition;
    public Vector2 TargetPosition
    {
        get { return targetPosition; }
        set { targetPosition = value; }
    }

    private void Initialize()
    {
        targetPosition = transform.position;
        if (owner.useDistanceSensitivity)
        {
            if (localDirection.x != 0) //Es una pared vertical
                sensitivity = Mathf.Lerp(1, owner.minSesitivity, Mathf.Abs(transform.position.y) / (PlayArea.Area.size.y / 2));
            else
                sensitivity = Mathf.Lerp(1, owner.minSesitivity, Mathf.Abs(transform.position.x) / (PlayArea.Area.size.x / 2));
        }
    }

    public Vector2 velocity;
   

    [Button]
    public float AddForce(float force, float dampProportion)
    {
        flash.Flash(dampProportion);
        velocity += localDirection * force * sensitivity;
        velocity = Vector2.ClampMagnitude(velocity, owner.maxVelocity);
        return sensitivity;
    }

    private void FixedUpdate()
    {
        Vector2 difference = (Vector3)targetPosition - transform.localPosition;
        if(velocity.sqrMagnitude < owner.stopThreshold && difference.sqrMagnitude < owner.stopThreshold)
        {
            velocity = Vector2.zero;
            transform.localPosition = targetPosition;
            return;
        }

        velocity += difference.normalized * Mathf.Lerp(0, owner.maxTensionSpeed, difference.sqrMagnitude / owner.maxLenght) * owner.speed;
        velocity *= owner.dampen;
        transform.localPosition += (Vector3)velocity;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(transform.localPosition, .14f);
    }
}
