﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using mattatz.Triangulation2DSystem.Example;
using mattatz.Triangulation2DSystem;
using UnityEditor;

public class MeshFromColliders : MonoBehaviour
{
    public Material terrainMaterial;
    public Material lineMaterial;

    [Range(1, 180)]  public float angle = 22.5f;
    [Range(0.2f, 2)]  public float threshold = 0.483f;


    public float poligonOptimizationTolerance = .25f;
    public float borderSize = .45f;
    public int cornerVertices = 5;

    public List<LineRenderer> lineRenderers;
    public PolygonCollider2D polyCol;
    public DemoMesh demoMesh;

    public bool useCompositeCollider = true;

    public Transform terrainRoot;

    [Button]
    public void CreateMeshFromLevelEditor(GameObject[] gos)
    {
        for (int i = 0; i < gos.Length; i++)
        {
            gos[i].GetComponent<Terrain_Module>().CreateMesh();
        }
    }

    [Button]
    public void CreateMeshFromLevelEditor(bool createLines, bool saveMesh, bool writeNow)
    {
        CreateMeshFromLevelEditor(terrainRoot, createLines, saveMesh,writeNow);
    }

    public void CreateLinesFromRoot(Transform levelRoot)
    {
        List<Vector2[]> paths = new List<Vector2[]>();
        CompositeCollider2D compositeCollider2D = levelRoot.gameObject.GetComponent<CompositeCollider2D>();
        bool destroy = false;
        if (compositeCollider2D == null)
        {
            destroy = true;
            compositeCollider2D = levelRoot.gameObject.AddComponent<CompositeCollider2D>();
        }
        for (int i = 0; i < compositeCollider2D.pathCount; i++)
        {
            Vector2[] points = new Vector2[compositeCollider2D.GetPathPointCount(i)];
            compositeCollider2D.GetPath(i, points);
            paths.Add(points);
        }
        transform.position = Vector3.zero;
        CreateLinesFrom(paths);

        if (!destroy) return;
        DestroyImmediate(compositeCollider2D);
        DestroyImmediate(levelRoot.GetComponent<Rigidbody2D>());
    }

    public void CreateMeshFromLevelEditor(Transform levelRoot, bool createLines, bool saveMesh, bool writeNow)
    {
        demoMesh.transform.position = Vector3.zero;
        demoMesh.Clear();
        List<Vector2[]> paths = new List<Vector2[]>();

        CompositeCollider2D compositeCollider2D = levelRoot.gameObject.GetComponent<CompositeCollider2D>();

        bool destroy = false;
        if (compositeCollider2D == null)
        {
            destroy = true;
            compositeCollider2D = levelRoot.gameObject.AddComponent<CompositeCollider2D>();
        }

        for (int i = 0; i < compositeCollider2D.pathCount; i++)
        {
            Vector2[] points = new Vector2[compositeCollider2D.GetPathPointCount(i)];
            compositeCollider2D.GetPath(i, points);
            paths.Add(points);
        }

        transform.position = transform.parent.position;
        CreateCombinedMesh(paths);
        if (createLines) CreateLinesFrom(paths);
        demoMesh.transform.localPosition = levelRoot.position - Vector3.forward * .1f;

#if UNITY_EDITOR
        if (saveMesh)
        {
            AssetDatabase.CreateAsset(demoMesh.MFilter.sharedMesh, "Assets/_Prefabs&SO/Prefabs/Terrains/Modular/Meshes/" + levelRoot.parent.name + "_Mesh.asset");
            if(writeNow) AssetDatabase.SaveAssets();
        }
#endif
        if (!destroy) return;

        DestroyImmediate(compositeCollider2D,true);
        DestroyImmediate(levelRoot.gameObject.GetComponent<Rigidbody2D>(),true);
    }

public void CreateMeshFromLevel(Transform levelRoot, bool createLines)
    {
        StartCoroutine(CreateMeshFromLevelRutine(levelRoot, createLines));
    }

    public IEnumerator CreateMeshFromLevelRutine(Transform levelRoot, bool createLines)
    {
        demoMesh.transform.position = Vector3.zero;
        demoMesh.Clear();
        List<Vector2[]> paths = new List<Vector2[]>();
        if (useCompositeCollider)
        {
            CompositeCollider2D compositeCollider2D = levelRoot.GetComponent<CompositeCollider2D>();
            for (int i = 0; i < compositeCollider2D.pathCount; i++)
            {
                Vector2[] points = new Vector2[compositeCollider2D.GetPathPointCount(i)];
                compositeCollider2D.GetPath(i, points);
                paths.Add(points);
            }
        }
        else
        {
            LoadBoxCollidersIntoPolygonCollider(CollectColliders(levelRoot));
            //    yield return null;
            ProcessPolygonCollider(polyCol);
            //    yield return null;
            for (int i = 0; i < polyCol.pathCount; i++)
            {
                paths.Add(polyCol.GetPath(i));
            }
            polyCol.isTrigger = true;
        }
        yield return null;
        transform.position = levelRoot.transform.position;
        CreateCombinedMesh(paths);
        if(createLines)   CreateLinesFrom(paths);
        demoMesh.transform.localPosition = levelRoot.transform.position -Vector3.forward * .1f;
      //  yield return null;
    }

    public List<BoxCollider2D> CollectColliders(Transform root)
    {
        List<BoxCollider2D> colliders = new List<BoxCollider2D>(root.GetComponentsInChildren<BoxCollider2D>());
        for (int i = colliders.Count-1; i >= 0; i--)
        {
            if (!colliders[i].gameObject.CompareTag("Terrain")) colliders.RemoveAt(i);
        }
        return colliders;
    }

    public void LoadBoxCollidersIntoPolygonCollider(List<BoxCollider2D> colliders)
    {
        polyCol.pathCount = colliders.Count;
        for (int i = 0; i < colliders.Count; i++)
        {
            polyCol.SetPath(i, colliders[i].GetVertex());
        }
    }

    public void ProcessPolygonCollider(PolygonCollider2D nPolyCol)
    {
        List<List<Vector2>> paths = ColliderMerger.UniteCollisionPolygons(nPolyCol.GetAllPaths(), .1f,10);   
        nPolyCol.SetPaths(paths);
    }   

    public void CreateCombinedMesh(List<List<Vector2>> paths)
    {
        List<Vector2[]> vectors = new List<Vector2[]>();
        for (int i = 0; i < paths.Count; i++)
            vectors.Add(paths[i].ToArray());
        CreateCombinedMesh(vectors);
    }

    public void CreateCombinedMesh(List<Vector2[]> paths)
    {
        Triangulation2D[] triangulation2Ds = new Triangulation2D[paths.Count];
        for (int i = 0; i < triangulation2Ds.Length; i++)
        {
            paths[i] = DouglasPeuckerLineApproximation.DouglasPeuckerReduction( new List<Vector2>(paths[i]), poligonOptimizationTolerance).ToArray();
            triangulation2Ds[i] = new Triangulation2D(new Polygon2D(paths[i]),angle,threshold);
        }
        demoMesh.SetMultipletTriangulation(triangulation2Ds);
    }

    public void CreateLinesFrom(List<Vector2[]> paths)
    {
        InitializeLineRenderers(paths.Count);
        for (int i = 0; i < paths.Count; i++)
        {
            List<Vector3> vectors = new List<Vector3>();
            for (int j = 0; j < paths[i].Length; j++)
            {
                vectors.Add(paths[i][j]);
            }
            vectors.Add(paths[i][0]);
            lineRenderers[i].positionCount = vectors.Count;
            lineRenderers[i].SetPositions(vectors.ToArray());
            lineRenderers[i].material = lineMaterial;
            lineRenderers[i].transform.localPosition = transform.parent.position;
        }
    }

    private void InitializeLineRenderers(int count)
    {
        lineRenderers = new List<LineRenderer>(GetComponentsInChildren<LineRenderer>());

        int sign = (int)Mathf.Sign(count - lineRenderers.Count);

        for (int i = lineRenderers.Count-1 ; i != count - sign; i += sign)
        {
            if (sign > 0)
            {
                GameObject go = new GameObject();
                go.transform.SetParent(transform);
                lineRenderers.Add(go.AddComponent<LineRenderer>());
            }
            else if (sign < 0)
            {
                if (Application.isPlaying)
                    DestroyImmediate(lineRenderers[i],true);
                    //Destroy(lineRenderers[i]);
                else
                    DestroyImmediate(lineRenderers[i]);
            }
        }

        for (int i = lineRenderers.Count-1; i >= 0; i--)
        {
            if (!lineRenderers[i])
            {
                lineRenderers.RemoveAt(i);
                continue;
            }

            lineRenderers[i].loop = false;
            lineRenderers[i].useWorldSpace = false;
            lineRenderers[i].widthMultiplier = borderSize;
            lineRenderers[i].numCornerVertices = cornerVertices;
        }
    }

    public void Build()
    {
        Build(new List<Vector2>(polyCol.GetPath(0)));
    }

    public void Build(List<Vector2> points)
    {
        Polygon2D polygon = Polygon2D.Contour(points.ToArray());

        Vertex2D[] vertices = polygon.Vertices;
        if (vertices.Length < 3) return; // error

        var triangulation = new Triangulation2D(polygon, angle);
       
        demoMesh.SetTriangulation(triangulation);

        demoMesh.SetMaterial(terrainMaterial);
    }
}
