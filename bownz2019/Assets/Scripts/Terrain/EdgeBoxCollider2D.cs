﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[RequireComponent(typeof(EdgeCollider2D))]
public class EdgeBoxCollider2D : MonoBehaviour
{
    [SerializeField] private Vector2 size;
    public Vector2 Size
    {
        get => size;
        set
        {
            size = value;
            RefreshCollider();
        }
    }
    [SerializeField] private Vector2 localPosition;
    public Vector2 LocalPosition
    {
        get => localPosition;
        set
        {
            localPosition = value;
            RefreshCollider();
        }
    }

    public Rect Area
    {
        set
        {
            localPosition = value.center;
            size = value.size;
            RefreshCollider();
        }
    }

    [SerializeField] EdgeCollider2D col;

    private void Awake()
    {
        col = GetComponent<EdgeCollider2D>();
    }

    [Button]
    public void RefreshCollider()
    {
        Vector2 extends = size / 2;
        col.points = new Vector2[]
        {
             new Vector2(-extends.x, extends.y),
             new Vector2(extends.x,  extends.y),
             new Vector2(extends.x,  -extends.y),
             new Vector2(-extends.x, -extends.y),
             new Vector2(-extends.x, extends.y)
        };
        col.offset = localPosition;
    }
}
