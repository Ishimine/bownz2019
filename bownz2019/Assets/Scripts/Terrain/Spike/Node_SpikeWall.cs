﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Node_SpikeWall : MonoBehaviour
{
    private SpikeButtonWall owner;
    public SpikeButtonWall Owner { get { return owner; } }

    protected CodeAnimator codeAnimator = new CodeAnimator();

    public void Initialize(SpikeButtonWall owner)
    {
        this.owner = owner;
    }

    public abstract void TurnOn();
    public abstract void TurnOff();
}
