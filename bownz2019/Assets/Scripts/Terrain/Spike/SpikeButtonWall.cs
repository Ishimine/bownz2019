﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeButtonWall : MonoBehaviour
{
    [SerializeField]private CodeAnimatorCurve curveIn = null;   public CodeAnimatorCurve CurveIn { get { return curveIn; } }
    [SerializeField] private CodeAnimatorCurve curveOut = null; public CodeAnimatorCurve CurveOut { get { return curveOut; } }
}
