﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spike_Terrain : MonoBehaviour
{
    public SpikeButtonWall spikeCoinWall;

}

public class SpikeCoinWall
{
    private int wallSize;
    public int WallSize
    {
        get { return wallSize; }
        set { wallSize = value; }
    }

    //public SpikeButton[] spikeButtons;

    public void Initialize()
    {

    }

    public void SetSpike(int index)
    {

    }

    public void SetButton(int index)
    {

    }

    public void AllSpike()
    {

    }

    public void AllButton()
    {

    }


}