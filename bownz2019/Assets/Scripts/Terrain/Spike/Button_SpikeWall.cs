﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button_SpikeWall : Node_SpikeWall
{
    public SpriteRenderer render;

    public override void TurnOff()
    {
        Color startColor = render.color;
        codeAnimator.StartAnimacion(this, x =>
        {
            //render.color = Color.Lerp(startColor, ,x);
        }, DeltaTimeType.deltaTime, AnimationType.Simple, Owner.CurveOut.Curve, Owner.CurveOut.Time);
    }

    public override void TurnOn()
    {
        codeAnimator.StartAnimacion(this, x =>
        {

        }, DeltaTimeType.deltaTime, AnimationType.Simple, Owner.CurveIn.Curve, Owner.CurveIn.Time);
    }
}
