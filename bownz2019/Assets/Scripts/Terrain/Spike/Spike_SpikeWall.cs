﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spike_SpikeWall : Node_SpikeWall
{
    public SpriteRenderer spikeRender;
    public SpriteRenderer baseRender;

    public override void TurnOff()
    {
        codeAnimator.StartAnimacion(this, x =>
        {

        }, DeltaTimeType.deltaTime, AnimationType.Simple, Owner.CurveOut.Curve, Owner.CurveOut.Time);
    }

    public override void TurnOn()
    {
        codeAnimator.StartAnimacion(this, x =>
        {

        }, DeltaTimeType.deltaTime, AnimationType.Simple, Owner.CurveIn.Curve, Owner.CurveIn.Time);
    }
}
