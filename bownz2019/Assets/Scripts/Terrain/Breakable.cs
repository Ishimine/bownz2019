﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Health))]
public class Breakable : MonoBehaviour
{
    private bool destroyOnDead = true;
    public bool DestroyOnDead
    {
        get { return destroyOnDead; }
        set { destroyOnDead = value; }
    }


    private Health health;
    public Health Health
    {
        get
        {
            if (health == null)  health = gameObject.GetComponent<Health>();
            return health;
        }
    }

    [SerializeField] private ParticleSystem particles;
    private CodeAnimator codeAnimator = new CodeAnimator();

    private Collider2D col;
    public Collider2D Col
    {
        get
        {
            if (col == null) col = GetComponent<Collider2D>();
            return col;
        }
        set { col = value; }
    }

    [SerializeField] private Sprite[] spriteLives = new Sprite[0];
    [SerializeField] private SpriteRenderer render;
    [SerializeField] private CodeAnimatorCurve animatorCurve;
   
    public void Initialize()
    {
        Col.enabled = true;
        gameObject.SetActive(true);
        render.enabled = false;
        gameObject.transform.localScale = Vector3.zero;
        render.enabled = true;
        if (spriteLives.Length == 0) Debug.LogError(this + " => Se necesita al menos 1 sprite para que el script funcione");
        Health.Initialize(spriteLives.Length);
        Health.OnChangeValue += OnChangeValue_Response;
        Show();
    }

    private void OnChangeValue_Response(int previousValue, int newValue)
    {
        if (newValue == 0)
        {
            Dead();
        }
        else
            render.sprite = spriteLives[newValue - 1];
    }

    private void Start()
    {
        Initialize();
    }

    private void OnDestroy()
    {
        Health.OnChangeValue -= OnChangeValue_Response;
    }

    private void Dead()
    {
        if (particles != null)
        {
            particles.Play();
            if (DestroyOnDead)
            {
                particles.transform.SetParent(null);
                Destroy(particles.gameObject, 5);
            }
        }
        Col.enabled = false;
        render.enabled = false;
        if (DestroyOnDead)
        {
            Destroy(gameObject,3);
            gameObject.SetActive(false);
        }
    }

    public void Reactivate()
    {
        Initialize();
    }

    private void Show()
    {
        codeAnimator.StartAnimacion(this,
           x =>
           {
               transform.localScale = Vector3.one * x;
           }, DeltaTimeType.deltaTime, AnimationType.Simple, animatorCurve.Curve, animatorCurve.Time);
    }

    public void PlayParticles()
    {
        particles.Play();
    }

}
