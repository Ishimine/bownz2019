﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bouncer : MonoBehaviour
{

    [SerializeField] private CodeAnimatorCurve codeAnimatorCurve;
    [SerializeField] private GameObject visual;
    [Range(0,2)]
    public float sizeExpantion = .2f;

    private CodeAnimator codeAnimator = new CodeAnimator();

    public LayerMask targetMasK;


    private void OnCollisionEnter2D(Collision2D collision)
    {
       if( targetMasK.Contains(collision.rigidbody.gameObject.layer))
        {
            Animate();
        }
    }

    private void Animate()
    {
        codeAnimator.StartAnimacion(this, x =>
        {
            visual.transform.localScale = Vector3.one + Vector3.one *x *  sizeExpantion;
        }, DeltaTimeType.deltaTime, AnimationType.Simple, codeAnimatorCurve.Curve, codeAnimatorCurve.Time);
    }

    private void OnDisable()
    {
        codeAnimator.Stop(this);
    }

}
