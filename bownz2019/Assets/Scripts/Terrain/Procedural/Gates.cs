﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class Gates : MonoBehaviour
{
    public enum Compatibility { None, Parcial, Perfect}

    public float gateHeight = 3;

    public Terrain_Module terrain_Module;

    [Header("Exits")]
    [SerializeField] private bool[] exits = new bool[3] { true, true, true };
    public bool[] Exit { get { return exits; } set { exits = value; } }


    [Header("Entrances")]
    [SerializeField] private bool[] entrances = new bool[3] { true, true, true };
    public bool[] Entrances { get { return entrances; } set { entrances = value; } }

   
    [Header("Colliders")]
    public BoxCollider2D[] entranceColliders;
    public BoxCollider2D[] exitsColliders;

    public ContactFilter2D filter;

    [SerializeField] private float edge = -.1f;


    public void Invert()
    {
        bool aux = exits[0];
        exits[0] = exits[2];
        exits[2] = aux;

        aux = entrances[0];
        entrances[0] = entrances[2];
        entrances[2] = aux;
    }

    public bool AreAllGatesOpen()
    {
        return AreAllEntranceOpen() && AreAllExitsOpen();
    }

    public bool AreAllEntranceOpen()
    {
        return AreAllOpen(entrances);
    }

    public bool AreAllExitsOpen()
    {
        return AreAllOpen(exits);
    }

    public bool AreAllOpen(bool[] gates)
    {
        for (int i = 0; i < gates.Length; i++)
        {
            if (!gates[i]) return false;
        }
        return true;
    }



    [Button]
    public void AutoPosition()
    {
        Vector2 size = new Vector2(PlayArea.Area.width / 3, gateHeight);
        Vector2 startPoint = terrain_Module.ModuleHeight * Vector2.up - Vector2.right * PlayArea.Area.width / 2 + size.x/2 * Vector2.right - size.y/2 * Vector2.up;
        Vector2 steps = Vector2.right * size.x; 
        for (int i = 0; i < exitsColliders.Length; i++)
        {
            exitsColliders[i].size = size + Vector2.one * edge;
            exitsColliders[i].transform.position = startPoint + steps * i;
        }
        startPoint = Vector2.left * PlayArea.Area.width / 2 + size.x/2 * Vector2.right + size.y/2 * Vector2.up;
        for (int i = 0; i < entranceColliders.Length; i++)
        {
            entranceColliders[i].size = size + Vector2.one * edge;
            entranceColliders[i].transform.position = startPoint + steps * i;
        }
    }

    [Button]
    public void AutoCheck()
    {
        Collider2D[] collider2Ds = new Collider2D[1];
        for (int i = 0; i < entranceColliders.Length; i++)
            entrances[i] = Physics2D.OverlapCollider(entranceColliders[i], filter, collider2Ds) == 0;

        for (int i = 0; i < exitsColliders.Length; i++)
            exits[i] = Physics2D.OverlapCollider(exitsColliders[i], filter, collider2Ds) == 0;
    }

    public static Compatibility AreCompatibles(Gates exit, Gates entrances/*, out bool[] compatibles*/)
    {
        int valid = 0;
        int invalid = 0;
        for (int i = 0; i < exit.exits.Length; i++)
        {
            if ((exit.exits[i] && entrances.entrances[i]))
                valid++;
            else
                invalid++;
        }

        if (valid == exit.exits.Length)
            return Compatibility.Perfect;
        else if (valid > 0)
            return Compatibility.Parcial;
        else
            return Compatibility.None;
    }

    public static Compatibility AreCompatibleAsInverted(Gates exit, Gates entrances/*, out bool[] compatibles*/)
    {
        int valid = 0;
        int invalid = 0;

        for (int i = 0; i < exit.exits.Length; i++)
        {
            if ((exit.exits[i] && entrances.entrances[2 - i]))
            {
                valid++;
            }
            else
            {
                invalid++;
            }
        }

        if (valid == exit.exits.Length)
            return Compatibility.Perfect;
        else if (valid > 0)
            return Compatibility.Parcial;
        else
            return Compatibility.None;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = new Color(0,.5f,1,.35f);
        
        for (int i = 0; i < exitsColliders.Length; i++)
        {
            Gizmos.DrawCube(exitsColliders[i].transform.position, exitsColliders[i].size);
        }

        Gizmos.color = new Color(0,1f,.5f,.35f);
        for (int i = 0; i < entranceColliders.Length; i++)
        {
            Gizmos.DrawCube(entranceColliders[i].transform.position, entranceColliders[i].size);
        }
    }

    public override string ToString()
    {
        return string.Format("Entrances Left:{0} Mid:{1} Right:{2}  |  Exits Left:{3} Mid:{4} Right:{5}", entrances[0], entrances[1],entrances[2],exits[0],exits[1],exits[2]);
    }
}
