﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[ExecuteInEditMode]
public class LevelConfiguration : MonoBehaviour
{
    [SerializeField] private PolygonCollider2D cameraConfiner = null;
    public PolygonCollider2D CameraConfiner
    {
        get { return cameraConfiner; }
        set { cameraConfiner = value; }
    }

    [SerializeField] private Transform startPlayerPosition = null;
    public Vector3 PlayerStartPosition => startPlayerPosition.position;

    [SerializeField] private Transform startCameraPosition = null;
    public Vector3 CameraStartPosition => startCameraPosition.position;

    [SerializeField] private Transform terrainRoot = null;
    [SerializeField] private Transform terrainRoofFloor = null;


    [SerializeField] private Vector2 playAreaSize = new Vector2(14, 22);
    public Vector2 PlayAreaSize
    {
        get { return playAreaSize; }
    }

    private void Update()
    {
        #if UNITY_EDITOR
        Vector2 difference = startPlayerPosition.position.GetDifference(startCameraPosition.position);

        if (difference.magnitude > playAreaSize.magnitude) //Si la posicion inicial de la camara es mas grande que el tamaño del nivel la camara se acerca
            startCameraPosition.position = startPlayerPosition.position + (Vector3)difference.normalized * playAreaSize.magnitude;
        #endif
    }

#if UNITY_EDITOR
    [Button]
    public void CameraConfinerWrapLevel()
    {
        Bounds bounds = new Bounds();
        bounds.size = Vector3.zero;
        bounds.center = Vector3.zero;

        List<Collider2D> colliders = new List<Collider2D>();
        terrainRoot.GetComponentsInChildren(colliders);
        colliders.AddRange(terrainRoofFloor.GetComponentsInChildren<Collider2D>());

        List<Vector2> vPoints = new List<Vector2>();
        foreach (Collider2D col in colliders)
        {
            vPoints.Add(col.bounds.min);
            vPoints.Add(col.bounds.center + new Vector3(col.bounds.extents.x, -col.bounds.extents.y));
            vPoints.Add(col.bounds.max);
            vPoints.Add(col.bounds.center + new Vector3(-col.bounds.extents.x, col.bounds.extents.y));
        }

        List<Point> points = new List<Point>();

        foreach (var v in vPoints)
            points.Add(new Point(v.x, v.y));

        points = ConvexHull.MakeHull(points) as List<Point>;

        vPoints.Clear();

        foreach (var p in points)
            vPoints.Add(new Vector2((float)p.x, (float)p.y));

        cameraConfiner.points = vPoints.ToArray();

    }
#endif

    #region Gizmos
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(1, .6f, .6f, .4f);
        Gizmos.DrawCube(transform.position, playAreaSize);

        Gizmos.color = new Color(1, .5f, 0, 1);
        Gizmos.DrawSphere(startPlayerPosition.position, 1);

        Gizmos.color = Color.cyan;
        Gizmos.DrawSphere(startCameraPosition.position, .5f);
    }
    #endregion


}
