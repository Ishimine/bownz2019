﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[ExecuteInEditMode]
public class Terrain_Wrapper : MonoBehaviour
{
    public Transform terrainConteiner;

    public Terrain_Module terrain;
    public TerrainWalls terrainWalls;

    [Button]
    public void WrapTerrain()
    {
        terrainWalls.RoundHeight();
        transform.localPosition = Vector3.zero;
        List<Collider2D> colliders = new List<Collider2D>();
        Bounds bounds = new Bounds();

        foreach (Transform item in terrainConteiner)
        {
            if (item != terrainConteiner)
            {
                var col = item.GetComponent<Collider2D>();
                if(col != null)  colliders.Add(col);
            }
        }

        foreach (var item in colliders)
        {
            bounds.Encapsulate(item.bounds);
        }

        terrain.ModuleHeight = bounds.size.y;

        foreach (Transform t in transform)
        {
            t.position += Vector3.up * -bounds.min.y;
        }

        terrainWalls.PositionWalls(true);
    }
}
