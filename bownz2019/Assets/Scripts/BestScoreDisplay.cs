﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BestScoreDisplay : MonoBehaviour
{
    [SerializeField] private Vector2 startPosition = new Vector2(0,-4);
    public Vector2 StartPosition { get { return startPosition; } set { startPosition = value; } }

    public TextMesh labelTxt;
    public TextMesh valueTxt;

    public CodeAnimatorCurve animationIn;
    public CodeAnimatorCurve animationOut;

    public Transform root;

    private CodeAnimator codeAnimator = new CodeAnimator();

    public void Show(float value, string label = "Best")
    {
        Show(value, StartPosition);
    }

    public void Show(float value, Vector2 position, string label = "Best")
    {
        gameObject.SetActive(false);
        labelTxt.text = label;
        valueTxt.text = value.ToString("0.");
        codeAnimator.StartAnimacion(this,
            x =>
            {
               // valueTxt.text = Mathf.Lerp(0,value, x).ToString("0.");
                root.transform.localScale = Vector3.one * animationIn.Curve.Evaluate(x);
            }, DeltaTimeType.deltaTime, AnimationType.Simple, null, animationIn.Time);
    }

    public void Hide()
    {
        float startSize = transform.localScale.x;
        codeAnimator.StartAnimacion(this,
            x =>
            {
                root.transform.localScale = Mathf.LerpUnclamped(startSize, 0, animationOut.Curve.Evaluate(x)) * Vector2.one;
            }, DeltaTimeType.deltaTime, AnimationType.Simple, null, animationOut.Time);
    }
}
