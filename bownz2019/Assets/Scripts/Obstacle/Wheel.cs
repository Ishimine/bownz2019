﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wheel : MonoBehaviour
{

    [SerializeField] private Rigidbody2D rb;
    public Rigidbody2D Rb           { get { return rb; } set { rb = value; } }

    [SerializeField] private CircleCollider2D col;
    public CircleCollider2D Col     { get { return col; } set { col = value; } }

    public ContactFilter2D filter;

    public bool goingRight = true;

    private Vector3 Dir => (goingRight) ? Vector2.right : Vector2.left;

    public float rayLenght = .2f;

    public float speed = 30;
    public float skin = .02f;

    RaycastHit2D[] hits = new RaycastHit2D[10];
    int count = 0;
    Vector2 startPoint;
    private void FixedUpdate()
    {
        rb.angularVelocity = -speed * Dir.x;
        startPoint = transform.position + Dir * (col.bounds.extents.x + skin);
        count = Physics2D.Raycast(startPoint, Dir, filter, hits, rayLenght);

   
        if (count == 1)
        {
            Debug.DrawRay(startPoint, Dir * rayLenght, Color.green);
            goingRight = !goingRight;
        }
        else
        {
            Debug.DrawRay(startPoint, Dir * rayLenght, Color.red);
        }
    }
}
