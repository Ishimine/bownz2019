﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestroy : MonoBehaviour
{
    public float duration = 3;

    public void Awake()
    {
        Destroy(gameObject, duration);
    }
}
