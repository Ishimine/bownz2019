﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;

public class PlayArea : MonoSingleton<PlayArea>
{
    public override PlayArea Myself => this;

    public Settings settings;
    
    private static Action<Rect> onAreaChange;
    public static Action<Rect> AreaChange
    {
        get { return onAreaChange; }
        set { onAreaChange = value; }
    }

    [ShowInInspector]
    private Rect area = new Rect(-6.5f,-11,13,22);
    public static Rect Area
    {
        get
        {
            return Instance.area;
        }
        set
        {
            Instance.area = value;
            AreaChange?.Invoke(Area);
        }
    }

    public void SetSize(Vector2 nSize)
    {
        Area = new Rect(area.position, nSize);
    }
    
    #if UNITY_EDITOR
    [Button]
    private void AreaChangeBroadcast()
    {
        AreaChange?.Invoke(Area);
    }
    #endif

    [Button]
    private void OnEnable()
    {
        area = new Rect(0, 0, Settings.Portrait_PlayAreaSize.x, Settings.Portrait_PlayAreaSize.y);
        Area = area.SetCenter(Vector2.zero);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireCube(area.center, area.size);
    }

    [SerializeField]  bool isLandscape;
    public void Update()
    {
        if(isLandscape != IsLandscape())
        {
            isLandscape = IsLandscape();
            AreaChange?.Invoke(area);
        }
    }

    [Button]
    public void SetCenter(Vector2 centro)
    {
        Area = area.SetCenter(centro);
    }
    
    private bool IsLandscape()
    {
        return GetScreenAspectRatio() > 1;
    }

    private float GetScreenAspectRatio()
    {
        return MainCameraManager.Current.aspect;
    }

    private void OnValidate()
    {
        if(!Application.isPlaying)
            ManualRefreshSize();
    }
    
    [Button]
    public void ManualRefreshSize()
    {
        if(settings != null)
            SetSize(settings.defaultPortrait_PlayAreaSize);
    }
        
}
