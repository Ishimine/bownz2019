﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Sirenix.OdinInspector;

public class Checkpoint : MonoBehaviour
{
    public UnityEventBool OnActivate;
    public UnityEventBool OnDeactivate;

    public static string NotificationActivated ="NotificationActivated";
    public static string NotificationDeactivated ="NotificationDeactivated";

    public static List<Checkpoint> Checkpoints { get; set; } = new List<Checkpoint>();

    public static List<Checkpoint> Active { get; set; } = new List<Checkpoint>();

    [SerializeField] private Transform respawnPoint;

    public Action<bool> OnStateChange { get; set; }
    public Action<Checkpoint> OnActivation { get; set; }

    [ReadOnly, ShowInInspector] private bool activated = false;
    public bool Activated
    {
        get { return activated; }
        protected set
        {
            if (activated == value) return;
            activated = value;
            OnStateChange?.Invoke(value);

            if(value)
                OnActivation?.Invoke(this);

            if (value)Active.Add(this); else Active.Remove(this);
        }
    }

#if UNITY_EDITOR
    private void OnValidate()
    {
        if (respawnPoint == null)
        {
            respawnPoint = new GameObject().transform;
            respawnPoint.name = "RespawnPoint";
            respawnPoint.SetParent(transform);
            respawnPoint.localPosition = Vector3.zero;
        }
    }
#endif

    public void Activate()
    {
        Activated = true;
        this.PostNotification(NotificationActivated, this);
    }

    public void Deactivate()
    {
        Activated = false;
        this.PostNotification(NotificationDeactivated, this);
    }

    private void OnEnable()
    {
        Checkpoints.Add(this);
    }

    private void OnDisable()
    {
        Checkpoints.Remove(this);
        Deactivate();
    }

    public Vector3 GetRespawnPoint()
    {
        return respawnPoint.position;
    }
}
