﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WayOnePlatformCheckPoint : Checkpoint
{
    public LayerMask targetLayer;

    public ShineOnContact shineOnContact;

    private void OnCollisionExit2D(Collision2D collision)
    {
        if(!Activated && targetLayer.Contains(collision.gameObject.layer))
        {
            Activate();
            if (shineOnContact != null) shineOnContact.Shine();
        }
    }
}
