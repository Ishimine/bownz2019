﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Particles_ReleaseAndPlay : MonoBehaviour
{
    public ParticleSystem particles;

   /* private Action<ParticleSystem> onTimeDone;
    public Action<ParticleSystem> OnTimeDone
    {
        get { return onTimeDone; }
        set { onTimeDone = value; }
    }*/

    public void ReleaseAndPlay()
    {
        Release();
        Play();
    }

    public void Release()
    {
        transform.SetParent(null);
    }

    public void Play()
    {
        particles.Play();
    }

}
