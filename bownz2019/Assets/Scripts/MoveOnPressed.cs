﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;

    [ExecuteInEditMode]
public class MoveOnPressed : MonoBehaviour, IPressed
{
    public Transform start;
    public Transform end;

    public Transform target;

    public float duration;
    public AnimationCurve animationCurve;

    private CodeAnimator codeAnimator = new CodeAnimator();

    public LineRenderer line;


   /* private void OnValidate()
    {
        if(!Application.isPlaying)
            OnEnable();
    }*/

    [ExecuteInEditMode]
    private void OnEnable()
    {
        if (target == null)
        {
            Debug.Log($"Target of object{name} is null");
            Transform current = transform.parent;
            while (current != null)
            {
                Debug.Log($"Parent{current.gameObject.name}");
                current = transform.parent;
            }
        }
        target.transform.position = start.position;
        line.positionCount = 2;
        line.SetPositions(new Vector3[] { start.position, end.position });
    }

    [Button]
    public void Pressed()
    {
        Vector2 targetPos = end.position;
        Vector2 startPost = target.position;
        SwapPositions();
        codeAnimator.StartAnimacion(this,
            x =>
            {
                target.transform.position = Vector3.Lerp(startPost, targetPos, x);
            }, DeltaTimeType.deltaTime, AnimationType.Simple, animationCurve, duration);
    }

    private void SwapPositions()
    {
        Transform t = start;
        start = end;
        end = t;
    }

}
