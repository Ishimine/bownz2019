﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class RemoveRigidVOdies : MonoBehaviour
{
    [Button]
    public void RemoveRigidbodies(GameObject[] gameObjects)
    {
        Rigidbody2D rb;
        foreach (var item in gameObjects)
        {
            rb = item.GetComponent<Terrain_Module>().wrapper.gameObject.GetComponent<Rigidbody2D>();
            if (rb != null)
                DestroyImmediate(rb,true);
        }
    }

}
