﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class StarUnlockFx : MonoBehaviour
{
    public float speed = 10;
    private CodeAnimator codeAnimator = new CodeAnimator();

    public float positionDisplacementAmmount = 5;
    public AnimationCurve positionCurve;

    public float lateralDisplacementAmmount = 3;
    public AnimationCurve lateralDisplacementCurve;

    public Rigidbody2D rb;

    public float startForce = 15;

    public float stopDistance = .5f;

    public float movementForce = 7;


    private IEnumerator rutine;
    public void Initialize(Vector2 startPosition, Vector2 targetPosition, Action callback)
    {
        float duration = Vector2.Distance(startPosition, targetPosition) / speed;
        transform.position = startPosition;
        gameObject.SetActive(true);
        Vector2 dir = startPosition.GetDirection(targetPosition);
        Vector2 lateralDir = (Vector2)(Quaternion.Euler(0, 0, dir.AsAngle()  + UnityEngine.Random.Range(-80,80)) * Vector2.up);

        rb.AddForce(-lateralDir * startForce, ForceMode2D.Impulse);

        rutine = GoToPoint(callback,targetPosition);

        StartCoroutine(rutine);
        /*     codeAnimator.StartAnimacion(this,
                 x =>
                 {
                     /*transform.position = Vector2.Lerp(startPosition, targetPosition, x) - 
                                          positionCurve.Evaluate(x) * dir * positionDisplacementAmmount + 
                                          lateralDir * lateralDisplacementCurve.Evaluate(x) * lateralDisplacementAmmount;
     
                 transform.position = new Vector2();
     
                 }, DeltaTimeType.deltaTime, AnimationType.Simple,  null, ()=> OnDone(callback), duration);*/
    }

    IEnumerator GoToPoint(Action callback, Vector2 targetPosition)
    {
        do
        {
            //rb.velocity /= 2; 
            rb.AddForce(transform.position.GetDirection(targetPosition) * movementForce, ForceMode2D.Impulse);
            yield return new WaitForFixedUpdate();
        } while (Mathf.Abs(Vector2.Distance(transform.position, targetPosition)) > stopDistance);
        rb.velocity = Vector2.zero;
        transform.position = targetPosition;
        OnDone(callback);
    }

    private void OnDone(Action callback)
    {
        callback?.Invoke();
        Destroy(gameObject, 3);
    }
}
