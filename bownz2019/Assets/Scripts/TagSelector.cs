﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using System;

public class TagSelector : MonoBehaviour
{
    [SerializeField] private Dropdown dropdown;
    public Dropdown Dropdown
    {
        get { return dropdown; }
        set { dropdown = value; }
    }

    public bool isAdd;

    [SerializeField] private Canvas_LevelConfiguration canvas_LevelConfiguration;
    public Canvas_LevelConfiguration Canvas_LevelConfiguration { get { return canvas_LevelConfiguration; } }

    private void Awake()
    {
        Initialize();
    }

    void Initialize()
    {
        List<STag> keys = new List<STag>(ProceduralModuleDatabase.Instance.STags);
        List<Dropdown.OptionData> dropdowns = new List<Dropdown.OptionData>();

        for (int i = 0; i < keys.Count; i++)
        {
            dropdowns.Add(new Dropdown.OptionData(keys[i].name));
        }
        Dropdown.options = dropdowns;
    }

    public void OnKeySelected(int index)
    {
        STag tag = ProceduralModuleDatabase.Instance.STags[index];

        if(isAdd)
            canvas_LevelConfiguration.SelectedForAdd(tag);
        else
            canvas_LevelConfiguration.SelectedForRemove(tag);
    }
}
