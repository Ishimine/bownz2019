﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball_OnTerrainCollitionVfxFromManualPaths : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D other)
    {
        OnTerrainCollitionVfxFromManualPaths onTerrainCollitionVfxFromManualPaths = 
            other.gameObject.GetComponentInParent<OnTerrainCollitionVfxFromManualPaths>();

        if (onTerrainCollitionVfxFromManualPaths == null) return;
        onTerrainCollitionVfxFromManualPaths.FromCollitionSource(other);
    }
}
