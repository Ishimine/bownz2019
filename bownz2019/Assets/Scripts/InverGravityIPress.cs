﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InverGravityIPress : MonoBehaviour, IPressed
{
    public void Pressed()
    {
        Physics2D.gravity = -Physics2D.gravity;
        this.PostNotification(Notifications.GravityChange);
    }
}
