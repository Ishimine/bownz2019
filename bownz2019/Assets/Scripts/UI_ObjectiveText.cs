﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class UI_ObjectiveText : MonoBehaviour
{
    [SerializeField] private Text current;
    [SerializeField] private Text target;

    private CodeAnimator codeAnimator = new CodeAnimator();

    public float duration = 2f;
    public AnimationCurve curve;

    public Color cNormal;
    public Color cCompleted;

    public void Initialize(bool isCompleted, string currentText, string targetText)
    {
        transform.localScale = Vector3.one;
        gameObject.SetActive(true);

        target.text = targetText;
        current.text = currentText;
        current.color = (isCompleted) ? cCompleted : cNormal;
        target.color = (isCompleted) ? cCompleted : cNormal;
    }

    public void StartAnimation(float currentValue, string format, Action postAction)
    {
        StartAnimation(currentValue, format, postAction, target.text);
    }

    public void StartAnimation(float currentValue, string format, Action postAction, string targetText)
    {
        current.text = 0.ToString(format);
        codeAnimator.StartAnimacion(this,
            x =>
            {
                //transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, x * 3);
                current.text = Mathf.Lerp(0, currentValue, curve.Evaluate(x)).ToString(format);
            }, DeltaTimeType.deltaTime, AnimationType.Simple, null, postAction, duration);
    }

  /*  public void TransitionToCompleted()
    {
        codeAnimator.StartAnimacion(this,
          x =>
          {
          
          }, DeltaTimeType.deltaTime, AnimationType.Simple, null, duration);
    }*/



}
