﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Sirenix.OdinInspector;

public class UI_LevelMedalButton : MonoBehaviour
{
    private int index;
    public int Index { get { return index; }
        set
        {
            index = value;
            txt.text = index.ToString();
        }
    }

    public Text txt;
    public Image_ColorShine top;
    public Image_ColorShine left;
    public Image_ColorShine right;
    public Image_ColorShine center;

    public Color centerColor;
    public Color enableColor;
    public Color disableColor;

    private Action<int> onPressed;
    public Action<int> OnPressed
    {
        get { return onPressed; }
        set { onPressed = value; }
    }


    private void Awake()
    {
        Initialize();
    }

    [Button]
    private void Initialize()
    {
        top.SetColor(disableColor);
        left.SetColor(disableColor);
        right.SetColor(disableColor);
        center.SetColor(disableColor);
    }

    public void SetProgress(bool top, bool left, bool right, bool center)
    {
        if(top) this.top.ShineTo(enableColor);
        if(left) this.left.ShineTo(enableColor);
        if(right) this.right.ShineTo(enableColor);
        if(center) this.center.ShineTo(centerColor);
    }

    public void Pressed()
    {
        OnPressed?.Invoke(index);
    }
}
