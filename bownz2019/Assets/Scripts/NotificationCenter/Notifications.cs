﻿using System;

public static class Notifications
{
    public const string GamePlay_Start ="GamePlay_Start";
    public const string Vfx_BackgroundSetLevel = "Vfx_BackgroundSetLevel";
    public const string Vfx_BackgroundSetEnable = "Vfx_BackgroundSetEnable";
    public const string Vfx_BloomSetEnable ="Vfx_BloomSetEnable";
    public const string Debug_WinLevel = "Debug_WinLevel";

    /// <summary>
    /// arg0 = int
    /// </summary>
    public static string ScoreUpdate => "ScoreUpdate";

    /// <summary>
    /// arg0 = TimeShift
    /// </summary>
    public static string TimeShiftRequest => "TimeShiftRequest";

    #region Health

    public static string Health_DamageReceived
    {
        get => "Health.DamageReceived";
    }

    public static string Health_DamageApplied
    {
        get => "Health.DamageApplied";
    }

    public static string BeforeHealthModify
    {
        get => "Health.BeforeHealthModify";
    }

    #endregion

    #region WrapFX

    public static string Wrappable_Start
    {
        get => "Wrappable.Start";
    }

    public static string Wrappable_Done
    {
        get => "Wrappable.Done";
    }

    #endregion

    #region Background/VFXs

    /// <summary>
    /// arg0 = BackgroundContactReactionData
    /// </summary>
    public static string BackgroundContactReaction
    {
        get => "BackgroundContactReaction";
    }

    #endregion

    #region PickUps

    /// <summary>
    /// arg0 = int
    /// </summary>
    public static string OnCoinPickedUp
    {
        get => "OnCoinPickedUp";
    }

    public static string OnFirstCoinPickedUp
    {
        get => "OnFirstCoinPickedUp";
    }

    public static string ShowStars
    {
        get => "ShowStars";
    }

    #endregion

    #region General

    public static string Activate => "Activate";
    public static string Deactivate => "Deactivate";

    public static string Ball_Teleporting_Start => "Ball_Teleporting_Start";
    public static string Ball_Teleporting_End => "Ball_Teleporting_End";

    public static string BackButtonPressed => "BackButtonPressed";
    public static string ExtraLifeGained => "ExtraLifeGained";

    public static string LevelButtonPressed => "LevelButtonPressed";

    public static string SettingsMenu_OpenClose => "SettingsMenu_OpenClose";

    public static string SettingsMenu_Enable => "SettingsMenu_Enable";

    public static string GravityChange => "GravityChange";

    public const string PauseChange = "PauseChange";
    public const string PauseRequest = "PauseRequest";

    #endregion



    #region GameMode

    /// <summary>
    /// Se envia Antes de iniciar el juego para que los objetos se preparen
    /// </summary>
    public static string GameMode_Ready
    {
        get => "GameMode_Ready";
    }

    public static string GameMode_Menu => "GameMode_Menu";

    /// <summary>
    /// Se envia al momento de iniciar la "partida"
    /// </summary>
    public static string GameMode_Go
    {
        get => "GameMode_Go";
    }

    public static string GameMode_End
    {
        get => "GameMode_End";
    }

    public static string GameMode_Reset
    {
        get => "GameMode_Reset";
    }

    public static string GameMode_Quit
    {
        get => "GameMode_Quit";
    }

    public static string GameMode_PlayerRespawn => "GameMode_PlayerRespawn";

    public static string GameModeRequest_Retry => "GameModeRequest_Retry";
    public static string GameModeRequest_Next => "GameModeRequest_Next";

    /// <summary>
    /// Enviar junto a un LevelRequest struct
    /// </summary>
    public static string LevelLoadRequest => "LevelLoadRequest";

    public static string GameModeRequest_BackToLevelSelection => "GameModeRequest_BackToLevelSelection";

    #endregion

    #region Level

    public static string LevelWon => "LevelWon";
    public static string WinCoinPickedUp => "WinCoinPickedUp";

    #endregion

    #region Swipe&Platforms

    /// <summary>
    /// Lo acompaña un bool para saber si se prende o se apaga.
    /// </summary>
    public static string SwipeSpawnerSetEnable => "SwipeSpawnerEnable";

    public static string SwipeSpawnerClear => "SwipeSpawnerClear";
    public static string SwipeSpawner_KillClosestAtRange => "SwipeSpawner_KillClosestAtRange";

    /// <summary>
    /// arg = SwipePlatform
    /// </summary>
    public static string SwipePlatform_Touched => "SwipePlatform_Touched";

    #endregion

    #region Ball

    public static string Ball_DeadEnd
    {
        get => "Ball_DeadEnd";
    }

    public static string Ball_DeadStart
    {
        get => "Ball_DeadStart";
    }

    public static string BallWaiting => "BallWaiting";

    #endregion

    #region Classic

    public static string Terrain_AllDown
    {
        get => "Terrain_AllDown";
    }

    public static string Terrain_FloorUp
    {
        get => "Terrain_FloorUp";
    }

    public static string Terrain_AddGlassFloor
    {
        get => "Terrain_AddGlassFloor";
    }


    public static string Terrain_StartTimer
    {
        get => "Terrain_StartTimer";
    }

    public static string Terrain_StopTimer
    {
        get => "Terrain_StopTimer";
    }

    #endregion

    #region Puzzle

    public static string Puzzle_PlayButton => "Puzzle_PlayButton";
    public static string Puzzle_ResetButton => "Puzzle_ResetButton";
    public static string Puzzle_StopButton => "Puzzle_StopButton";
    public static string Puzzle_Editing_State => "Puzzle_Editing_State";
    public static string Puzzle_Playing_State => "Puzzle_Playing_State";

    #endregion

    #region Input

    public static string OnFingerSet => "OnFingerSet";
    public static string OnFingerUp => "OnFingerUp";
    public static string OnFingerDown => "OnFingerDown";

    public const string InputActiveChange = "InputActiveChange";
    #endregion

    #region ProceduralLevels

    /// <summary>
    /// arg => struct ProceduralLevelRequest
    /// </summary>
    public static string ProceduralLevel_Request => "LoadProceduralLevelRequest";

    public static string ProceduralLevel_Created => "ProceduralLevel_Created";

    #endregion

    #region GamePlayModifier

    public static string Modifier_SlowMotionOnSwipe_Equipped => "Modifier_SlowMotionOnSwipe_Equipped";
    public static string Modifier_SlowMotionOnSwipe_Unequipped => "Modifier_SlowMotionOnSwipe_Unequipped";
    public static string Modifier_SlowMotionOnSwipe_ChargeUsed => "Modifier_SlowMotionOnSwipe_ChargeUsed";

    #endregion

    #region Shop

    public static string Shop_NextPage => "Shop_NextPage";
    public static string Shop_PreviousPage => "Shop_PreviousPage";
    public static string Shop_FirstPage => "Shop_FirstPage";
    public static string Shop_LastPage => "Shop_LastPage";
    public static string Shop_CanSelect => "Shop_CanSelect";
    public static string Shop_ProductDisplaySelected => "Shop_ProductDisplaySelected";
    public static string Shop_BuyButtonPressed => "Shop_BuyButtonPressed";
    public static string Shop_EquipButtonPressed => "Shop_EquipButtonPressed";
    
    public const string ShopOpenClose = "ShowOpenClose";

    public const string RewardPowerUpShield = "RewardPowerUpShield";
    public const string RewardPowerUpMagnet = "RewardPowerUpMagnet";
    public const string RewardSkipModule = "RewardSkipModule";

    #endregion

    #region GameCycle

    public static string GameCycle_EndScreen_Menu => "GameCycle_EndScreen_Menu";
    public static string GameCycle_EndScreen_Retry => "GameCycle_EndScreen_Retry";
    public static string GameCycle_EndScreen_Next => "GameCycle_EndScreen_Next";
                                                                                      
    public const string GameCycle_Playing_BackToMenu = "GameCycle_Playing_BackToMenu";

#endregion

    #region LevelSelection
    public static string LevelFoldoutInfoRequest => "LevelFoldoutInfoRequest";
    public static string LevelFoldoutInfoClose => "LevelFoldoutInfoClose";
    #endregion


    #region Debug
    public static string Debug_OpenClose => "Debug_OpenClose";

    public static string OnBallSizeChange => "OnBallSizeChange";

    public static string OnBallGravityChange => "OnBallGravityChange";

    public const string RemoteConfigFetched = "RemoteConfigFetched";

    public const string ShopClosed = "ShopClosed";     
    public const string ValidateBuy = "ValidateBuy";

    public const string HasEnoughCoins = "HasEnoughCoins";

    public const string TryToBuy = "TryToBuy";

    public const string StarCoinKeyPickedUp = "StarCoinKeyPickedUp";

    public const string PowerUpCanBuy = "PowerUpCanBuy";

    public const string GoToGiftBox = "GoToGiftBox";

    public const string ShowGiftUnlockSuccess = "ShowGiftUnlockSucess";

    public const string ShowGiftUnlockProgress = "ShowGiftUnlockProgress";

    public const string GiftButtonSetEnable = "GiftButton";

    public const string OnGiftBoxAnimationEnd = "OnGiftBoxAnimationEnd";
    public const string OnGiftButtonPressed = "OnGiftButtonPressed";

    public const string GoToMain = "GoToMain";

    public const string BackToMain = "GoToShop";

    public const string GoToShop = "GoToShop";

    public const string ShowPlayGamesBar = "ShowPlayGamesBar";

    public const string NextDailyDate = "NextDailyDate";
    public const string PreviousDailyDate = "PreviousDailyDate";
    public const string OnDailyDateChange = "OnDailyDateChange";
    public const string RewardPowerUpFromShop = "RewardPowerUpFromShop";

    #endregion



    #region Query

    public const string Query_DoesNextLevelExist = "Query_DoesNextLevelExist";

    #endregion

    #region LevelConstruction

    public const string OnLevelConstructionProgress = "OnLevelConstructionProgress";
    public const string OnLevelConstructionStart = "OnLevelConstructionStart";
    public const string OnLevelConstructionEnd = "OnLevelConstructionEnd";

    public const string ShowLoadBar = "ShowLoadBar";

    #endregion

}


