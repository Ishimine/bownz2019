﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class RemoteVariableDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);
        SerializedProperty useRemoteBool = property.FindPropertyRelative("useRemoteVariable");

        var amountRect = new Rect(position.x, position.y, 65, position.height);

        int selection = EditorGUI.Popup(amountRect, useRemoteBool.intValue,new string[] { "LOCAL","REMOTE" });

        useRemoteBool.boolValue = selection == 1;

        position.x += 65;

        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        var variable = new Rect(position.x, position.y, position.width - 65, position.height);

        if (useRemoteBool.boolValue)
        {
            EditorGUI.PropertyField(variable, property.FindPropertyRelative("key"), GUIContent.none);
        }
        else
        {
            EditorGUI.PropertyField(variable, property.FindPropertyRelative("defaultValue"), GUIContent.none);
        }
        EditorGUI.EndProperty();
    }
}

[CustomPropertyDrawer(typeof(RemoteString))]
public class RemoteStringDrawer : RemoteVariableDrawer { }

[CustomPropertyDrawer(typeof(RemoteBool))]
public class RemoteBoolDrawer : RemoteVariableDrawer { }

[CustomPropertyDrawer(typeof(RemoteFloat))]
public class RemoteFloatDrawer : RemoteVariableDrawer { }

[CustomPropertyDrawer(typeof(RemoteInt))]
public class RemoteIntDrawer : RemoteVariableDrawer { }