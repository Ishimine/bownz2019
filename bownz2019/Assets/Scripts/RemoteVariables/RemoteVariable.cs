﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Firebase.RemoteConfig;
using System.Linq;
[System.Serializable]
public abstract class RemoteVariable<T>
{
    [SerializeField]
    private bool useRemoteVariable = false;
    public bool UseRemoteVariable { get { return useRemoteVariable; } set { useRemoteVariable = value; } }

    [ShowIf("useRemoteVariable"), SerializeField]
    private string key;
    public string Key { get { return key; } set { key = value; } }

    [HideIf("useRemoteVariable"), SerializeField]
    public T defaultValue;
    public T DefaultValue { get { return defaultValue; } set { defaultValue = value; } }

    public T GetValue()
    {
        if (useRemoteVariable && FirebaseRemoteConfig.Keys.Contains(key))
            return GetRemoteValue();
        else
            return defaultValue;
    }

    public abstract T GetRemoteValue();
}

[System.Serializable]
public class RemoteString : RemoteVariable<string>
{
    public override string GetRemoteValue()
    {
        return FirebaseRemoteConfig.GetValue(Key).StringValue;
    }
}

[System.Serializable]
public class RemoteInt : RemoteVariable<int>
{
    public override int GetRemoteValue()
    {
        return (int)FirebaseRemoteConfig.GetValue(Key).LongValue;
    }
}

[System.Serializable]
public class RemoteFloat : RemoteVariable<float>
{
    public override float GetRemoteValue()
    {
        return (float)FirebaseRemoteConfig.GetValue(Key).DoubleValue;
    }
}

[System.Serializable]
public class RemoteBool : RemoteVariable<bool>
{
    public override bool GetRemoteValue()
    {
        return FirebaseRemoteConfig.GetValue(Key).BooleanValue;
    }
}
