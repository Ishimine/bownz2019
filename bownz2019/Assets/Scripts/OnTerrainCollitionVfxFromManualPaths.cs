﻿using System.Collections;
using System.Collections.Generic;
using Pooling.Poolers;
using UnityEngine;

public class OnTerrainCollitionVfxFromManualPaths : MonoBehaviour
{
    [SerializeField] private SetPooler vfxCollitionPool;
    [SerializeField] private  ExtractPaths pathsConteiner;

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (!collision.rigidbody.gameObject.CompareTag("Player")) return;


        TerrainCollitionVfx terrainCollitionVfx = vfxCollitionPool.Dequeue().GetComponent<TerrainCollitionVfx>();
        List<List<Vector2>> paths = new List<List<Vector2>>();
        for (int i = 0; i < pathsConteiner.lines.Count; i++)
        {
            Vector2[] aux = new Vector2[pathsConteiner.lines[i].positionCount];
            for (int j = 0; j < aux.Length; j++)
            {
                aux[j] = pathsConteiner.lines[i].GetPosition(j);
            }
            paths.Add(new List<Vector2>(aux));
        }
        terrainCollitionVfx.gameObject.SetActive((true));
        terrainCollitionVfx.transform.parent = transform;
        terrainCollitionVfx.OnAnimationEnd = ()=> vfxCollitionPool.Enqueue(terrainCollitionVfx.GetComponent<Poolable>());
        terrainCollitionVfx.Initialize(collision, paths);
    }


 public void FromCollitionSource(Collision2D collision)
    {

        TerrainCollitionVfx terrainCollitionVfx = vfxCollitionPool.Dequeue().GetComponent<TerrainCollitionVfx>();
        List<List<Vector2>> paths = new List<List<Vector2>>();
        for (int i = 0; i < pathsConteiner.lines.Count; i++)
        {
            Vector2[] aux = new Vector2[pathsConteiner.lines[i].positionCount];
            for (int j = 0; j < aux.Length; j++)
            {
                aux[j] = pathsConteiner.lines[i].GetPosition(j);
            }
            paths.Add(new List<Vector2>(aux));
        }
        terrainCollitionVfx.gameObject.SetActive((true));
        terrainCollitionVfx.transform.parent = transform;
        terrainCollitionVfx.OnAnimationEnd = ()=> vfxCollitionPool.Enqueue(terrainCollitionVfx.GetComponent<Poolable>());
        terrainCollitionVfx.Initialize(collision, paths);
    }
}
