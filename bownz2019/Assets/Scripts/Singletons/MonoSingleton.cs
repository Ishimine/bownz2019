﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MonoSingleton<T> : MonoBehaviour where T : MonoSingleton<T>
{
    private static List<MonoBehaviour> monoSingletons = new List<MonoBehaviour>();

    private static T instance;
    public static T Instance
    {
        get
        {
            if(instance == null)
            {
                Instance = FindObjectOfType<T>();
                if (instance == null) Debug.LogError("Singleton NO EXISTE " + typeof(T));
            }
            return instance;
        }
        set
        {
            instance = value;
            Debug.Log("<Color=green> SINGLETON Initialized: </color> <Color=blue> " + instance + " </color> ");
        }
    }

    public abstract T Myself
    {
        get;
    }

    protected virtual void Awake()
    {
        if(instance == null)
        {
            Instance = Myself;
            monoSingletons.Add(this);
        }
        else if (instance != null && instance != this)
        {
            Debug.Log("Instance: " + instance);
            Debug.LogError("<Color=red> "+ this + "  SINGLETON ALREADY EXIST CONFLICT </color> => "+instance.gameObject);
        }
    }

    public static void DestroyAllSingletons()
    {
        for (int i = monoSingletons.Count; i >= 0; i--)
        {
            Destroy(monoSingletons[i].gameObject);
        }
        monoSingletons.Clear();
    }

    private void OnDestroy()
    {
        if(monoSingletons.Contains(this))
            monoSingletons.Remove(this);

        instance = null;
    }
}
