﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif
public abstract class ScriptableSingleton<T> : BaseScriptableSingleton where T : ScriptableSingleton<T>
{
    public static List<ScriptableObject> scriptableSingletons = new List<ScriptableObject>();
    private static T instance;
    public static T Instance
    {
        get 
        {
            if (instance == null)
            {
#if UNITY_EDITOR
                Debug.LogError("ERROR ScriptableSingleton NO EXISTE " + typeof(T));
#endif
            }
            return instance;
        }
        set
        {
            instance = value;
            Debug.Log(" <Color=green> SCRIPTABLE_SINGLETON Initialized: </color> <Color=blue> " + instance + "</color> ");
        }
    }

    public abstract T Myself
    {
        get;
    }

    public override void InitializeSingleton()
    {
        if (instance == null)
        {
            Instance = Myself;
            scriptableSingletons.Add(this);
        }
        else if (instance != this)
        {
            Debug.LogError("<Color=red> " + this + "  SCRIPTABLE_SINGLETON ALREADY EXIST CONFLICT </color>");
        }
    }



    protected virtual void OnDisable()
    {
        instance = null;
    }
}

public abstract class BaseScriptableSingleton : ScriptableObject
{
    public abstract void InitializeSingleton();

    public virtual void Start()
    {

    }
}


public static class TypeNameExtensions
{
    public static string GetFriendlyName(this Type type)
    {
        string friendlyName = type.Name;
        if (type.IsGenericType)
        {
            int iBacktick = friendlyName.IndexOf('`');
            if (iBacktick > 0)
            {
                friendlyName = friendlyName.Remove(iBacktick);
            }
            friendlyName += "<";
            Type[] typeParameters = type.GetGenericArguments();
            for (int i = 0; i < typeParameters.Length; ++i)
            {
                string typeParamName = GetFriendlyName(typeParameters[i]);
                friendlyName += (i == 0 ? typeParamName : "," + typeParamName);
            }
            friendlyName += ">";
        }

        return friendlyName;
    }
}