﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class SingletonInitializer : MonoBehaviour 
{
    [AssetsOnly,AssetList(AutoPopulate = true)]
    public List<BaseScriptableSingleton> scriptableObjects; 

    private void Awake()
    {
        foreach (var item in scriptableObjects)
            item.InitializeSingleton();
    }

    private void Start()
    {
        foreach (var item in scriptableObjects)
            item.Start();
    }
}
