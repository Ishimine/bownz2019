﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IOnCollisionEnter2D
{
    void OnCollisionEnter2D(Collision2D collision);
}
