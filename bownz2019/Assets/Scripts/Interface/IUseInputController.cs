﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Touch;

public interface IUseInputController 
{
    void FingerTap(LeanFinger leanFinger);
    void FingerDown(LeanFinger leanFinger);
    void FingerSet(LeanFinger leanFinger);
    void FingerUp(LeanFinger leanFinger);
    void FingerSwipe(LeanFinger leanFinger);
    void FingerExpired(LeanFinger leanFinger);
    void Gesture(List<LeanFinger> leanFingers);
}
