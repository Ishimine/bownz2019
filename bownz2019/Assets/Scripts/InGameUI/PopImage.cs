﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopImage : PopMessage
{
    public SpriteRenderer render;

    protected override void ApplyColor(Color color)
    {
        render.color = color;
    }

    protected override void InitializeMessage(PopMessageRequest message)
    {
        PopSpriteRequest spriteMessage = (message as PopSpriteRequest);
        render.sprite = spriteMessage.sprite;
        render.transform.rotation = Quaternion.Euler(0,0, spriteMessage.rotation);
    }
}
