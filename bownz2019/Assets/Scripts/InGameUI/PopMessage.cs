﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PopMessage : MonoBehaviour
{
    public Vector2 forceRange = new Vector2(0, 3);
    public float dirRange = 3;

    [SerializeField] private float duration = 0.85f;
    [SerializeField] private AnimationCurve heightCurve = null;
    [SerializeField] private AnimationCurve sizeCurve = null;
    [SerializeField] private AnimationCurve colorCurve = null;

    private CodeAnimator codeAnimator = new CodeAnimator();

    private void Awake()
    {
        ApplyColor(Color.clear);
    }

    public void Show(PopMessageRequest message)
    {
        InitializeMessage(message);
        transform.position = message.position;
        Vector2 startPos = transform.position;
        float displacement = Random.Range(-dirRange, dirRange);
        float xPosValue;
        float heightValue;
        Color startColor = message.color;
        startColor.a = 0;
        float force = Random.Range(forceRange.x, forceRange.y);
        codeAnimator.StartAnimacion(this,
            x =>
            {
                ApplyColor(Color.Lerp(startColor, message.color, colorCurve.Evaluate(x)));
                xPosValue = startPos.x + Mathf.Lerp(0, displacement, x);
                heightValue = startPos.y + force * heightCurve.Evaluate(x);
                transform.localScale = Vector3.one * sizeCurve.Evaluate(x);
                transform.position = new Vector3(xPosValue, heightValue);
            },
            DeltaTimeType.deltaTime, AnimationType.Simple, null, () => Destroy(gameObject), duration);
    }

    protected abstract void InitializeMessage(PopMessageRequest message);
    protected abstract void ApplyColor(Color color);
}