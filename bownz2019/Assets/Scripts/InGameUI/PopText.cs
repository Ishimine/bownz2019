﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class PopText : PopMessage
{
    public TextMesh textMesh;

    protected override void ApplyColor(Color color)
    {
        textMesh.color = color;
    }

    protected override void InitializeMessage(PopMessageRequest message)
    {
        textMesh.text =  (message as PopTextRequest).txt;

    }
}
