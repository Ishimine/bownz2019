﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopTextManager : MonoSingleton<PopTextManager>
{
    public bool printDebug = true;

    public override PopTextManager Myself => this;

    public static void ShowPopMessage(PopMessageRequest popText)
    {
        Instance._ShowPopMessage(popText);
    }

    private void _ShowPopMessage(PopMessageRequest popText)
    {
        if (printDebug) Debug.Log("PopText: " + popText.ToString());
        Instantiate(GameObjectLibrary.Get(popText.PrefabID), null).GetComponent<PopMessage>().Show(popText);
    }

    public static PopMessageRequest PuzzleStarMessage(int count, Vector2 position)
    {
        switch (count)
        {
            case 1:
                return new PopTextRequest(position,ColorPallete.Wall, "GOOD");
            case 2:
                return new PopTextRequest(position, ColorPallete.NormalCoin,"GREAT!");
            case 3:
                return new PopTextRequest(position, ColorPallete.GoldenCoin, "PERFECT!!!");
            default:
                return new PopTextRequest(position, Color.red, "NULL");
        }
    }
}

[System.Serializable]
public class PopTextRequest : PopMessageRequest
{
    public string txt;

    public PopTextRequest(Vector2 position, Color color, string txt) : base(position, color)
    {
        this.txt = txt;
    }
    public override string PrefabID => "PopTextMsg_Prefab";
}


[System.Serializable]
public class PopSpriteRequest : PopMessageRequest
{
    public Sprite sprite;
    public float rotation = 0;

    public PopSpriteRequest(Vector2 position, Color color, Sprite sprite, float rotation = 0) : base(position, color)
    {
        this.sprite = sprite;
        this.rotation = rotation;
    }

    public override string PrefabID => "PopSpriteMsg_Prefab";
}

public abstract class PopMessageRequest
{
    public Vector2 position;
    public Color color;

    public abstract string PrefabID { get; }

    public PopMessageRequest(Vector2 position, Color color)
    {
        this.position = position;
        this.color = color;
    }
}
