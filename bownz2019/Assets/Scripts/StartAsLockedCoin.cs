﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartAsLockedCoin :StartAsCoin
{
    protected override void GameMode_PlayerRespawn_Response(object arg1, object arg2)
    {
        Pickable pickable = GetComponent<Pickable>();
        if (pickable != null) ShowForRespawn();
    }


    public int coinsToUnlock = 3;

    protected override void Show()
    {
        starCoin.LifeSpan = float.MaxValue;
        starCoin.Show(true, new LockedCoin(starCoin, coinsToUnlock));
        starCoin.LifeSpan = float.MaxValue;
    }
                  
    protected  void ShowForRespawn()
    {
        starCoin.LifeSpan = float.MaxValue;
        starCoin.Show(true, null);
        starCoin.LifeSpan = float.MaxValue;
    }
}
