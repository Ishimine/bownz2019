﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public abstract class LevelGameModeState : GameModeState
{
    protected static GameObject Player;

    protected static GameObjectDataBase LevelsDb { get; set; }

    public static LevelConfiguration CurrentLevelConfiguration { get; protected set; }

    public static GameObject CurrentLevelTerrain { get; set; }

    protected static LevelRequest CurrentLevelRequest { get; set; }

    public static int CurrentLevelIndex { get; set; }

    protected abstract string LevelsDbId
    { get; }

    protected override void InitializeElements()
    {
        base.InitializeElements();
        LevelsDb = (GameObjectDataBase)ScriptableDataBaseLibrary.Get(LevelsDbId);
    }
                                             
    public LevelProgress GetPlayerScore(int index, bool isChallenge)
    {
        string key = index.ToString();
        if(!LevelsDb.Elements.ContainsKey(key))
            Debug.LogError("No encontrada llave: " + key);
       
        string progressKey = isChallenge ? LevelsDb.Elements[key].GetComponent<Level_Objectives>().IdChallenge : LevelsDb.Elements[key].GetComponent<Level_Objectives>().Id;
        return LevelsProgress.Get(GameModeID, index, isChallenge);
    }

    public LevelProgress GetLevelObjective(int index, bool isChallenge)
    {
        string key = index.ToString();
        if(!LevelsDb.Elements.ContainsKey(key))
            Debug.LogError("No encontrada llave: " + key);
        return (isChallenge)? LevelsDb.Elements[key].GetComponent<Level_Objectives>().Challenge : LevelsDb.Elements[key].GetComponent<Level_Objectives>().Normal;
    }

    public virtual void LoadLevel(LevelRequest request)
    {
        CurrentLevelRequest = request;
        Debug.LogWarning("LOAD LEVEL");
        CurrentLevelIndex = request.Index;
        if (CurrentLevelTerrain != null)
        {
            Debug.LogWarning("DestroyImmediate LEVEL");
            Object.DestroyImmediate(CurrentLevelTerrain);
        }
        //Cargamos terreno
        CurrentLevelTerrain = Object.Instantiate(GetLevelFromDB(request.Index.ToString()), Elements[GameModeID][rootId].transform);
        CurrentLevelConfiguration = CurrentLevelTerrain.GetComponent<LevelConfiguration>();
    }

    private GameObject GetLevelFromDB(string key)
    {
        if (LevelsDb.Elements.ContainsKey(key))
            return LevelsDb.Elements[key];
        else
        {
            Debug.LogError(string.Format("NIVEL:{0} no encontrado ", key));
            return LevelsDb.Elements.Values.ElementAt(0);
        }
    }

    protected override void CloseThenSwitchState(State nState)
    {
        base.CloseThenSwitchState(nState);
        if (CurrentLevelTerrain != null) Object.Destroy(CurrentLevelTerrain);
    }

  
}
