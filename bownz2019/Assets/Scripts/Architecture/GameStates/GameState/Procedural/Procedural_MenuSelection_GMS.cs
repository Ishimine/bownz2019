﻿using System;
using System.Collections;
using System.Collections.Generic;
using Procedural;
using UnityEngine;

public class Procedural_MenuSelection_GMS : Procedural_GMS
{
    public override void Enter()
    {
        base.Enter();

        MenuSelectionCanvas.Set(
            "Procedural",
            () => CloseThenSwitchState(new Procedural_DailyLevel_Menu_GMS()),
            null);

        //SetElementsEnabled(false);
        Player.gameObject.SetActive(true);
        Canvas_LevelConfiguration.gameObject.SetActive(true);
        Canvas_LevelConfiguration.OpenManual();
        MainCameraManager.Instance.SwitchToStaticCamera();

        Player.TrailComponent.UsePlayerSelection = true;
        Player.SkinComponent.UsePlayerSelection = true;

        this.PostNotification(Notifications.SettingsMenu_OpenClose,true);
    }

    public override void Exit()
    {
        base.Exit();
        Canvas_LevelConfiguration.gameObject.SetActive(false);
        this.PostNotification(Notifications.SettingsMenu_OpenClose,false);
    }

    protected override void AddListeners()
    {
        base.AddListeners();
        this.AddObserver(LoadLevel_Request, Notifications.ProceduralLevel_Request);
    }

    protected override void RemoveListeners()
    {
        base.RemoveListeners();
        this.RemoveObserver(LoadLevel_Request, Notifications.ProceduralLevel_Request);
    }

    private void LoadLevel_Request(object arg1, object arg2)
    {
        MenuSelectionCanvas.Instance.gameObject.SetActive(false);
        SwitchState(new Procedural_LoadLevel_GMS((Blueprint)arg2));
    }

    protected override void OnBackButtonPressed()
    {
    }
}

/*
[System.Serializable]
public class ProceduralLevelRequest
{
    public string ID;
    public float Difficulty;
    public int Height = 50;
    public int Seed = 0;
    public Vector2 distanceBetweenModules = new Vector2(3,5);

    public STag[] addTags;
    public STag[] removeTags;

    public override string ToString()
    {
        return string.Format("Procedural Level ID:{0} Difficulty:{1} Height:{2} Seed{3} AddTags:{4} RemoveAdds:{5}", ID, Difficulty, Height, Seed, Content(addTags), Content(removeTags));
    }

    private string Content(STag[] tags)
    {
        string retValue ="";

        for (int i = 0; i < tags.Length; i++)
        {
            retValue += " " +tags[i] + " ";
        }
        return retValue;
    }
}*/