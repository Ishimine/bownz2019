﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Procedural_WinScreen_GMS : Procedural_GMS
{
    public Procedural_WinScreen_GMS(LevelProgress score)
    {
        WinScreen.Initialize(Objectives, score, LevelsProgress.Get(GameModeID, LevelIndex, Settings.IsChallengeMode));
        LevelsProgress.AddLevelProgress(LevelId, System.DateTime.Now.Year, false, score, true);
    }

    public override void Enter()
    {
        base.Enter();
        WinScreen.Show();
        this.PostNotification(Notifications.SwipeSpawnerClear);
    }

    protected override void AddListeners()
    {
        base.AddListeners();
        this.AddObserver(OnRequestLevelSelection_Response, Notifications.GameModeRequest_BackToLevelSelection);
        this.AddObserver(OnRequestRetry_Response, Notifications.GameModeRequest_Retry);
        this.AddObserver(OnRequestNext_Response, Notifications.GameModeRequest_Next);
    }

    protected override void RemoveListeners()
    {
        base.RemoveListeners();
        this.RemoveObserver(OnRequestLevelSelection_Response, Notifications.GameModeRequest_BackToLevelSelection);
        this.RemoveObserver(OnRequestRetry_Response, Notifications.GameModeRequest_Retry);
        this.RemoveObserver(OnRequestNext_Response, Notifications.GameModeRequest_Next);
    }

    private void OnRequestNext_Response(object arg1, object arg2)
    {
        SwitchState(new Procedural_DailyLevel_Menu_GMS());
    }

    private void OnRequestRetry_Response(object arg1, object arg2)
    {
        SwitchState(new Procedural_LoadLevel_GMS(blueprint));
    }

    private void OnRequestLevelSelection_Response(object arg1, object arg2)
    {
        SwitchState(new Procedural_DailyLevel_Menu_GMS());
    }

    //Nulifica el accionar del boton back
    protected override void OnGameModeQuit() { }
}
