﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Procedural_Playing_GMS : Procedural_GMS
{
    private List<SwipePlatform> touchedPlatforms;
    public List<SwipePlatform> TouchedPlatforms
    {
        get { return touchedPlatforms; }
        set { touchedPlatforms = value; }
    }

    private HashSet<CoinType> pickedStars;
    public HashSet<CoinType> PickedStars
    {
        get { return pickedStars; }
        set { pickedStars = value; }
    }

    private float startTime=0;
    private float endTime = 0;

    int deathCount;

    public override void Enter()
    {
        base.Enter();

        Level.gameObject.SetActive(true);
        Player.transform.position = PlayArea.Area.center;
        Player.gameObject.SetActive(true);

        touchedPlatforms = new List<SwipePlatform>();
        pickedStars = new HashSet<CoinType>();

        //ProceduralTerrain.transform.position = PlayArea.Area.center + Vector2.down * PlayArea.Area.height / 2;
        this.PostNotification(Notifications.GameMode_Go);

        deathCount = 0;
        Settings.UiInteraction = true;
                                                  
        SlowMotionWhileSwipe_Modificator.isInfinite = true;
        SlowMotionWhileSwipe_Modificator.Deactivate();

    }

    public override void Exit()
    {
        base.Exit();
        Level.gameObject.SetActive(false);
    }

    protected override void AddListeners()
    {
        base.AddListeners();

        this.AddObserver(OnBallDeadStartResponse, Notifications.Ball_DeadStart);
        this.AddObserver(OnBallDeadEndResponse, Notifications.Ball_DeadEnd);
        this.AddObserver(OnWinCoinPickedUp, Notifications.WinCoinPickedUp);
        this.AddObserver(OnSwipePlatform_Touched_Response, Notifications.SwipePlatform_Touched);
        this.AddObserver(OnCoinPickedUp, Notifications.OnCoinPickedUp);

        this.AddObserver(OnPowerUp_Shield, Notifications.RewardPowerUpShield);
        this.AddObserver(OnPowerUp_Magnet, Notifications.RewardPowerUpMagnet);
        this.AddObserver(OnSkipModule_Reward, Notifications.RewardSkipModule);
        //this.AddObserver(OnRetry_Request, Notifications.GameModeRequest_Retry);
    }


 
    protected override void RemoveListeners()
    {
        base.RemoveListeners();

        this.RemoveObserver(OnBallDeadStartResponse, Notifications.Ball_DeadStart);
        this.RemoveObserver(OnBallDeadEndResponse, Notifications.Ball_DeadEnd);
        this.RemoveObserver(OnWinCoinPickedUp, Notifications.WinCoinPickedUp);
        this.RemoveObserver(OnSwipePlatform_Touched_Response, Notifications.SwipePlatform_Touched);
        this.RemoveObserver(OnCoinPickedUp, Notifications.OnCoinPickedUp);

        this.RemoveObserver(OnPowerUp_Shield, Notifications.RewardPowerUpShield);
        this.RemoveObserver(OnPowerUp_Magnet, Notifications.RewardPowerUpMagnet);
        this.RemoveObserver(OnSkipModule_Reward, Notifications.RewardSkipModule);

        //this.RemoveObserver(OnRetry_Request, Notifications.GameModeRequest_Retry);
    }

    private void OnPowerUp_Magnet(object arg1, object arg2)
    {
        GameObjectLibrary.Instantiate("Magnet").GetComponent<Magnet>().EquipTo(Player.gameObject);
    }

    private void OnSkipModule_Reward(object arg1, object arg2)
    {
        TravelTo travelTo = GameObjectLibrary.Instantiate("TravelTo").GetComponent<TravelTo>();

        Debug.Log("Player point is null" +(Player == null));
        Debug.Log("Respawn point is null"+ (Level.CurrentCp == null));
        Debug.Log("GetNextCheckpoint point is null" +(Level.GetNextCheckpoint() == null));

        Checkpoint nextCp = Level.GetNextCheckpoint();

        Rect rect = new Rect();

        rect.min = Level.CurrentCp.GetRespawnPoint() + Vector3.left * PlayArea.Area.width/2;
        rect.max = nextCp.GetRespawnPoint() + Vector3.right * PlayArea.Area.width/2;

        void PostAction()
        {
            List<StarCoin> starCoins = Level.GetStarsInsideRect(rect);
            foreach (StarCoin starCoin in starCoins)
            {
                starCoin.gameObject.AddComponent<GoToObject>().Initialize(Player.transform, true);
            }
            nextCp.Activate();
        }    


        travelTo.Initialize(Player, nextCp.GetRespawnPoint(), Level.CurrentCp.GetRespawnPoint());
        travelTo.Execute(null, PostAction);
    }

    private void OnPowerUp_Shield(object arg1, object arg2)
    {
        Shield.EquipTo(Player);
    }

    private void OnBallDeadEndResponse(object arg1, object arg2)
    {
        if (Checkpoint.Checkpoints.Count == 0 || Checkpoint.Active.Count < 1)
        {
            Player.transform.position = Vector3.zero;
            deathCount = 0;
        }
        else
            Player.transform.position = Checkpoint.Active[Checkpoint.Active.Count - 1].GetRespawnPoint();

        Player.GetComponent<BallStateMachine>().AppearToLiving();
        this.PostNotification(Notifications.GameMode_PlayerRespawn);
    }

    private void OnBallDeadStartResponse(object sender, object arg2)
    {
        deathCount++;
    }

    private void OnWinCoinPickedUp(object sender, object arg2)
    {
        endTime = Time.time;

        ///Notificaciones
        this.PostNotification(Notifications.SwipeSpawnerSetEnable, false);
        this.PostNotification(Notifications.LevelWon);
        this.PostNotification(Notifications.GameMode_End);

        ///Ball
        Player.Disappear(GameMode_Win);
    }

    private void OnSwipePlatform_Touched_Response(object sender, object arg2)
    {
        touchedPlatforms.Add((SwipePlatform)sender);
    }

    private void OnCoinPickedUp(object sender, object tipo)
    {
        if ((int)tipo == 0)
        {
            var coin = (CoinType)sender;
            pickedStars.Add(coin);
            //PopTextManager.ShowPopMessage(PopTextManager.PuzzleStarMessage(pickedStars.Count, coin.Position));
        }
    }

    private void GameMode_Win()
    {
        this.PostNotification(Notifications.GameMode_Reset);
        SwitchState(
                new Procedural_WinScreen_GMS(                    
                new LevelProgress(
                    touchedPlatforms.Count,
                    endTime - startTime,
                    deathCount,
                    pickedStars.Count
                    )));
    }

    protected override void OnGameModeQuit()
    {
        base.OnGameModeQuit();
        //  SwitchState(new Procedural_MenuSelection_GMS());
        SwitchState(new Procedural_DailyLevel_Menu_GMS());
    }

}
