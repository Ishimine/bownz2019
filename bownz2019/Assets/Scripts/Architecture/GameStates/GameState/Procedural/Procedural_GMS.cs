﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Procedural;

public abstract class Procedural_GMS : GameModeState
{
    public override string[] GameModePrefabsKeys =>new string[]
        {
            "Ball",
            "Canvas_Procedural_LevelSelection",
            "Canvas_Level_WinScreen",
            "Canvas_DailyProcedural",
            "Canvas_InGame_Level",
            "ProceduralLevel"
        };

    public override string GameModeID => "Procedural";
    protected static Blueprint blueprint;

    public static string LevelId { get; protected set; }

    private static Canvas_LevelConfiguration canvas_LevelConfiguration;
    protected static Canvas_LevelConfiguration Canvas_LevelConfiguration
    {
        get { return canvas_LevelConfiguration; }
        set { canvas_LevelConfiguration = value; }
    }

    public static LevelProgress Objectives => new LevelProgress();

    protected static ProceduralLevel Level { get; set; }
    protected static BallStateMachine Player { get; set; }

    protected int StarCount { get; set; } = -1;


    private static Canvas_Level_WinScreen winScreen;
    protected static Canvas_Level_WinScreen WinScreen { get { return winScreen; } set { winScreen = value; } }

    protected override void InitializeElements()
    {
        base.InitializeElements();
        Level = Elements[GameModeID].Values.First(x => x.gameObject.GetComponent<ProceduralLevel>() != null).GetComponent<ProceduralLevel>();

        Player = Elements[GameModeID].Values.First(x => x.gameObject.GetComponent<BallStateMachine>() != null).GetComponent<BallStateMachine>();

        canvas_LevelConfiguration = Elements[GameModeID].Values.First(x => x.gameObject.GetComponent<Canvas_LevelConfiguration>() != null).GetComponent<Canvas_LevelConfiguration>();

        winScreen = Elements[GameModeID].Values.First(x => x.gameObject.GetComponent<Canvas_Level_WinScreen>() != null).GetComponent<Canvas_Level_WinScreen>();

    }
}
