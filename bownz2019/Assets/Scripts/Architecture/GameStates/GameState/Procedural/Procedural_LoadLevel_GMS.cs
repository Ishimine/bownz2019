﻿using System;
using System.Collections;
using System.Collections.Generic;
using Procedural;
using UnityEngine;

public class Procedural_LoadLevel_GMS : Procedural_GMS
{
    public Procedural_LoadLevel_GMS(Blueprint blueprint)
    {
        Procedural_GMS.blueprint = blueprint;
    }

    public Procedural_LoadLevel_GMS(Blueprint blueprint, string levelId)
    {
        Procedural_GMS.blueprint = blueprint;
        LevelId = levelId;
    }

    public override void Enter()
    {
        base.Enter();
        Level.gameObject.SetActive(false);
        UI_ScreenTransitioner.Out(InitializeTerrainAndCamera);
    }

    private void InitializeTerrainAndCamera()
    {
        MainCameraManager.Instance.SwitchToFollowCamera(Player.transform, FollowCamera.FollowType.Portrait);
        Level.gameObject.SetActive(true);
        Level.CreateLevel(blueprint, TransitionIn);

    }

    private void TransitionIn()
    {
        Player.gameObject.SetActive(true);
        UI_ScreenTransitioner.In(PostAction);
    }

    public override void Exit()
    {
        base.Exit();
        MenuSelectionCanvas.Instance.gameObject.SetActive(false);
    }

    private void PostAction()
    {
        this.PostNotification(Notifications.GameMode_Ready);
        this.PostNotification(Notifications.ShowStars);
    }

   

    private void StartPlaying()
    {
        SwitchState(new Procedural_Playing_GMS());
    }

    protected override void AddListeners()
    {
        base.AddListeners();
        this.AddObserver(OnBallWaiting_Response, Notifications.BallWaiting);

        SwipePlatform.OnAnyAliveState -= OnAnyAliveState_Response;
        SwipePlatform.OnAnyAliveState += OnAnyAliveState_Response;
    }

    protected override void RemoveListeners()
    {
        base.RemoveListeners();
        this.RemoveObserver(OnBallWaiting_Response, Notifications.BallWaiting);
        SwipePlatform.OnAnyAliveState -= OnAnyAliveState_Response;
    }

    private void OnAnyAliveState_Response(SwipePlatform obj)
    {
        StartPlaying();
    }

    private void OnBallWaiting_Response(object arg1, object arg2)
    {
        this.PostNotification(Notifications.SwipeSpawnerSetEnable, true);
    }
}
