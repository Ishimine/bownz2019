﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using Procedural;

public class Procedural_DailyLevel_Menu_GMS : Procedural_GMS
{
    private Canvas_DailyProcedural canvas_Daily;

    protected Canvas_DailyProcedural Canvas_Daily
    {
        get
        {
            if(canvas_Daily == null)                canvas_Daily = Elements[base.GameModeID].Values.First(x => x.gameObject.GetComponent<Canvas_DailyProcedural>() != null).GetComponent<Canvas_DailyProcedural>();
            return canvas_Daily;
        }
    }
    public override void Enter()    
    {
        base.Enter();
   
        canvas_Daily = Elements[GameModeID].Values.First(x => x.gameObject.GetComponent<Canvas_DailyProcedural>() != null).GetComponent<Canvas_DailyProcedural>();

        Player.gameObject.SetActive(true);
        MainCameraManager.Instance.SwitchToStaticCamera();

        Player.gameObject.SetActive(false);
        Player.transform.position = Vector3.zero;
        Player.TrailComponent.UsePlayerSelection = true;
        Player.SkinComponent.UsePlayerSelection = true;

        canvas_Daily.gameObject.SetActive(true);
        
        Level.gameObject.SetActive(false);
    }

    public override void Exit()
    {
        canvas_Daily.gameObject.SetActive(false);
        base.Exit();
        Canvas_LevelConfiguration.gameObject.SetActive(false);
    }

    protected override void AddListeners()
    {
        base.AddListeners();
        this.AddObserver(LoadLevel_Request, Notifications.ProceduralLevel_Request);
    }

    protected override void RemoveListeners()
    {
        base.RemoveListeners();
        this.RemoveObserver(LoadLevel_Request, Notifications.ProceduralLevel_Request);
    }

    private void LoadLevel_Request(object arg1, object arg2)
    {
     
    }

    protected override void OnBackButtonPressed()
    {
    }
}

public struct ProceduralLevelRequest
{
    public readonly Blueprint Blueprint;
    public readonly string LevelId;

    public ProceduralLevelRequest(Blueprint blueprint, string levelId)
    {
        Blueprint = blueprint;
        LevelId = levelId;
    }
}

