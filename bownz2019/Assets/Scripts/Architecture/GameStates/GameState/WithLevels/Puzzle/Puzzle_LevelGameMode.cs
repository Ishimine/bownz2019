﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Puzzle_LevelGameMode : LevelGameModeState
{
    public override string[] GameModePrefabsKeys => new string[]
    {
        "Ball",
        "Canvas_Grid_LevelSelection",
        "Canvas_Puzzle_InGame",
        "Canvas_Puzzle_WinScreen",
    };

    public override string GameModeID => "Puzzle";
    protected override string LevelsDbId => "Puzzle_Levels";

    static protected UI_LevelSelectionGrid menuSelection;

    static private LevelConfiguration levelConfiguration;
    static protected LevelConfiguration LevelConfiguration => levelConfiguration;
    static protected Canvas_Level_WinScreen winScreen;
    static protected BallStateMachine ball;

    static private Level_Objectives objectives;
    static protected LevelProgress Objectives => (IsChallengeMode)? objectives.Challenge : objectives.Normal;

    private static bool isChallengeMode = false;
    public static bool IsChallengeMode
    {
        get { return isChallengeMode; }
        set { isChallengeMode = value; }
    }

    protected override void InitializeElements()
    {
        base.InitializeElements();

        GameObject go;
        Player = Elements[GameModeID].Values.First(x => x.gameObject.GetComponent<BallStateMachine>() != null);
        go = Elements[GameModeID].Values.First(x => x.gameObject.GetComponent<UI_LevelSelectionGrid>() != null);

        winScreen = Elements[GameModeID].Values.First(x => x.gameObject.GetComponent<Canvas_Level_WinScreen>() != null).GetComponent<Canvas_Level_WinScreen>();

        if (go != null)
        {
            menuSelection = go.GetComponent<UI_LevelSelectionGrid>();
            menuSelection.MyGameMode = this;
        }

        ball = Player.GetComponent<BallStateMachine>();
    }

    public override void LoadLevel(LevelRequest request)
    {
        base.LoadLevel(request);

        //Cargamos tamaño del area de juego objetivo
        levelConfiguration = CurrentLevelTerrain.GetComponent<LevelConfiguration>();
        objectives = CurrentLevelTerrain.GetComponent<Level_Objectives>();

        if (levelConfiguration != null)
        {
            PlayArea.Instance.SetSize(levelConfiguration.PlayAreaSize);
            PlayArea.Instance.SetCenter(Vector2.zero);
        }
        //Aplicamos camara Follow adecuada
        MainCameraManager.Instance.SwitchToStaticCamera();

    }

    private GameObject GetLevelFromDB(string key)
    {
        if (LevelsDb.Elements.ContainsKey(key))
            return LevelsDb.Elements[key];
        else
        {
            Debug.LogError(string.Format("NIVEL:{0} no encontrado ", key));
            return LevelsDb.Elements.Values.ElementAt(0);
        }
    }

    protected override void OnBackButtonPressed()
    {
        base.OnBackButtonPressed();
        SwitchState(new Puzzle_MenuSelection_GMS());
    }

    protected override void CloseThenSwitchState(State nState)
    {
        base.CloseThenSwitchState(nState);
        if (CurrentLevelTerrain != null) Object.Destroy(CurrentLevelTerrain);
    }

    protected virtual Vector2 GetAreaSize()
    {
        return Settings.Portrait_PlayAreaSize;
    }

}
