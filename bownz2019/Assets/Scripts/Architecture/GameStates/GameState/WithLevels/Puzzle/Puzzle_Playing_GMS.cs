﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puzzle_Playing_GMS : Puzzle_LevelGameMode
{
    public bool inTransition = false;

    private HashSet<SwipePlatform> touchedPlatforms;
    public HashSet<SwipePlatform> TouchedPlatforms
    {
        get { return touchedPlatforms; }
        set { touchedPlatforms = value; }
    }

    private HashSet<CoinType> pickedStars;
    public HashSet<CoinType> PickedStars
    {
        get { return pickedStars; }
        set { pickedStars = value; }
    }

    private float startTime;
    private float endTime;

    public override void Enter()
    {
        base.Enter();
        this.PostNotification(Notifications.GameMode_Go);
        
        pickedStars = new HashSet<CoinType>();
        touchedPlatforms = new HashSet<SwipePlatform>();

        ball.SwitchState(new BallStates.Living(ball));

        this.PostNotification(Notifications.SwipeSpawnerSetEnable, false);
        this.PostNotification(Notifications.GameMode_Go);
        this.PostNotification(Notifications.Puzzle_Playing_State);
        
        startTime = Time.time;
        inTransition = false;
        Settings.UiInteraction = true;
    }

    public override void Exit()
    {
        base.Exit();
        ball.useFloatAnimation = true;
    }

    private CodeAnimator codeAnimator = new CodeAnimator();
    protected override void AddListeners()
    {
        base.AddListeners();
        this.AddObserver(OnBallDeadStartResponse, Notifications.Ball_DeadStart);
        this.AddObserver(OnBallDeadEndResponse, Notifications.Ball_DeadEnd);
        this.AddObserver(OnWinCoinPickedUp, Notifications.WinCoinPickedUp);
        this.AddObserver(OnCoinPickedUp, Notifications.OnCoinPickedUp);

        this.AddObserver(OnPuzzle_ResetButton_Response, Notifications.Puzzle_ResetButton);
        this.AddObserver(OnPuzzle_StopButton_Response, Notifications.Puzzle_StopButton);

        this.AddObserver(OnSwipePlatform_Touched_Response, Notifications.SwipePlatform_Touched);
    }

    protected override void RemoveListeners()
    {
        base.RemoveListeners();
        this.RemoveObserver(OnBallDeadStartResponse, Notifications.Ball_DeadStart);
        this.RemoveObserver(OnBallDeadEndResponse, Notifications.Ball_DeadEnd);
        this.RemoveObserver(OnWinCoinPickedUp, Notifications.WinCoinPickedUp);
        this.RemoveObserver(OnCoinPickedUp, Notifications.OnCoinPickedUp);

        this.RemoveObserver(OnPuzzle_ResetButton_Response, Notifications.Puzzle_ResetButton);
        this.RemoveObserver(OnPuzzle_StopButton_Response, Notifications.Puzzle_StopButton);

        this.RemoveObserver(OnSwipePlatform_Touched_Response, Notifications.SwipePlatform_Touched);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="tipo">0 = Normal | 1 = Golden</param>
    private void OnCoinPickedUp(object sender, object tipo)
    {
        if ((int)tipo == 0)
        {
            var coin = (CoinType)sender;
            pickedStars.Add(coin);
            PopTextManager.ShowPopMessage(PopTextManager.PuzzleStarMessage(pickedStars.Count, coin.Position));
        }
    }

    private void OnSwipePlatform_Touched_Response(object sender, object touchingObject)
    {
        touchedPlatforms.Add((SwipePlatform)sender);
    }

    private void OnPuzzle_StopButton_Response(object arg1, object arg2)
    {
        SwitchState(new Puzzle_Editing_GMS());
    }

    private void OnPuzzle_ResetButton_Response(object sender, object arg2)
    {
        this.PostNotification(Notifications.SwipeSpawnerClear);
        OnPuzzle_StopButton_Response(sender, arg2);
    }

    private void OnWinCoinPickedUp(object sender, object arg2)
    {
        inTransition = true;

        endTime = Time.time;

        this.PostNotification(Notifications.SwipeSpawnerSetEnable, false);
        this.PostNotification(Notifications.LevelWon);
        this.PostNotification(Notifications.GameMode_End);

        //Ball
        Player.GetComponent<BallStateMachine>().Disappear();

        GameObject go = new GameObject();
        Empty proxy = go.AddComponent<Empty>();
        codeAnimator.StartWaitAndExecute(proxy, 1, GameMode_Win, false);
        UnityEngine.Object.Destroy(go, 5);
    }

    private void OnBallDeadStartResponse(object arg1, object arg2)
    {
    }

    private void OnBallDeadEndResponse(object arg1, object arg2)
    {
        SwitchState(new Puzzle_Editing_GMS());
    }

    private void GameMode_Win()
    {
        Debug.Log("Notifications.GameMode_Reset");
        this.PostNotification(Notifications.GameMode_Reset);
        LevelProgress score = new LevelProgress(touchedPlatforms.Count, endTime - startTime,  0, pickedStars.Count);
        inTransition = false;
    }

    protected override void OnGameModeQuit()
    {
        base.OnGameModeQuit();
        SwitchState(new Puzzle_MenuSelection_GMS());
    }
}


