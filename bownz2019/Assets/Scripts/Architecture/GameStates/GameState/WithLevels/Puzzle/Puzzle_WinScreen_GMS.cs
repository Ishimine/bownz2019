﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puzzle_WinScreen_GMS : Puzzle_LevelGameMode
{
    public Puzzle_WinScreen_GMS(LevelProgress score)
    {
        winScreen.Initialize(Objectives, score, GetCurrentLevelPlayerScore());
    }

    public override void Enter()
    {
        base.Enter();
        this.PostNotification(Notifications.SwipeSpawnerClear);
        winScreen.Show();
    }

    public override void Exit()
    {
        base.Exit();
    }

    protected override void AddListeners()
    {
        base.AddListeners();
        this.AddObserver(OnRequestLevelSelection_Response, Notifications.GameModeRequest_BackToLevelSelection);
        this.AddObserver(OnRequestRetry_Response, Notifications.GameModeRequest_Retry);
        this.AddObserver(OnRequestNext_Response, Notifications.GameModeRequest_Next);
    }

    protected override void RemoveListeners()
    {
        base.RemoveListeners();
        this.RemoveObserver(OnRequestLevelSelection_Response, Notifications.GameModeRequest_BackToLevelSelection);
        this.RemoveObserver(OnRequestRetry_Response, Notifications.GameModeRequest_Retry);
        this.RemoveObserver(OnRequestNext_Response, Notifications.GameModeRequest_Next);
    }

    private void OnRequestNext_Response(object arg1, object arg2)
    {
        SwitchState(new Puzzle_LevelSelected_GMS(new LevelRequest(GameModeID, CurrentLevelIndex + 1)));
    }

    private void OnRequestRetry_Response(object arg1, object arg2)
    {
        SwitchState(new Puzzle_LevelSelected_GMS());
    }

    private void OnRequestLevelSelection_Response(object arg1, object arg2)
    {
        SwitchState(new Puzzle_MenuSelection_GMS());
    }

    //Nulifica el accionar del boton back
    protected override void OnGameModeQuit() { }
}
