﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puzzle_Editing_GMS : Puzzle_LevelGameMode
{
    public override void Enter()
    {
        base.Enter();
        this.PostNotification(Notifications.SwipeSpawnerSetEnable, false);

        ResetBall();
        ResetAllPickables();

        this.PostNotification(Notifications.ShowStars);
        this.PostNotification(Notifications.Puzzle_Editing_State);
    }

    private void ResetBall()
    {
        Player.transform.position = LevelConfiguration.PlayerStartPosition; //Reseteanis ka oisucuib de la pelota

        ball.useFloatAnimation = false;
        ball.AppearToWaiting(() => this.PostNotification(Notifications.SwipeSpawnerSetEnable, true));
    }

    private void ResetAllPickables()
    {
        var pickables = GameObject.FindObjectsOfType<Pickable>();           //Reseteamos todos los objetos pickeables (estrellas primordialmente)
        foreach (var item in pickables)
            item.Active = true;
    }

    public override void Exit()
    {
        base.Exit();

        ball.useFloatAnimation = true;
    }

    protected override void AddListeners()
    {
        base.AddListeners();

        this.AddObserver(PlayButton_Response, Notifications.Puzzle_PlayButton);
        this.AddObserver(ResetButton_Response, Notifications.Puzzle_ResetButton);

        SwipePlatform.OnFailedCreation += OnSwipePlatformFailedCreation_Response;
    }


    protected override void RemoveListeners()
    {
        base.RemoveListeners();

        this.RemoveObserver(PlayButton_Response, Notifications.Puzzle_PlayButton);
        this.RemoveObserver(ResetButton_Response, Notifications.Puzzle_ResetButton);

        SwipePlatform.OnFailedCreation -= OnSwipePlatformFailedCreation_Response;
    }

    private void PlayButton_Response(object arg1, object arg2)
    {
        SwitchState(new Puzzle_Playing_GMS());
    }

    private void ResetButton_Response(object arg1, object arg2)
    {
        this.PostNotification(Notifications.SwipeSpawnerClear);
    }

    private void OnSwipePlatformFailedCreation_Response(SwipePlatform swipePlatform)
    {
        this.PostNotification(Notifications.SwipeSpawner_KillClosestAtRange, new KillClosestAtRangeRequest(swipePlatform.StartWorldPosition, 3));
    }

   
}
