﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class Puzzle_MenuSelection_GMS : Puzzle_LevelGameMode
{
    public override void Enter()
    {
        base.Enter();

        MenuSelectionCanvas.Set(
          "Puzzle",
         () => CloseThenSwitchState(new Landscape_MenuSelection_GMS()),
         () => CloseThenSwitchState(new Procedural_DailyLevel_Menu_GMS()));
        ball.DefaultStartPosition = Vector2.zero;

        SetElementsEnabled(false);
        menuSelection.Initialize(LevelsDb.Elements.Count);
        menuSelection.gameObject.SetActive(true);
        menuSelection.SetPage(0);
        MainCameraManager.Instance.SwitchToStaticCamera();

        SwipeSpawner.DieOnTouch = false;
        SwipeSpawner.LifeSpan = Settings.SwipePlatform_PuzzleLifeSpan;
        SwipeSpawner.MaxCount = Settings.SwipePlatform_PuzzleMaxCount;
        SwipeSpawner.MaxSimultaneousSwipes = Settings.PuzzleMaxSimultaneousSwipes;
        Inventory.SaveToDisk();
    }

    public override void Exit()
    {
        base.Exit();
        MenuSelectionCanvas.Instance.gameObject.SetActive(false);
    }

    protected override void CloseThenSwitchState(State state)
    {
        SwipeSpawner.DieOnTouch = true;
        SwipeSpawner.LifeSpan = Settings.SwipePlatform_LifeSpan;
        SwipeSpawner.MaxCount = Settings.SwipePlatform_MaxCount;
        SwipeSpawner.MaxSimultaneousSwipes = Settings.MaxSimultaneousSwipes;
        base.CloseThenSwitchState(state);
    }

    protected override void AddListeners()
    {
        base.AddListeners();
        this.AddObserver(OnLevelRequestedResponse, Notifications.LevelLoadRequest);
    }

    protected override void RemoveListeners()
    {
        base.RemoveListeners();
        this.RemoveObserver(OnLevelRequestedResponse, Notifications.LevelLoadRequest);
    }

    private void OnLevelRequestedResponse(object arg1, object levelRequest)
    {
        LevelRequest request = (LevelRequest)levelRequest;
        MenuSelectionCanvas.Instance.gameObject.SetActive(false);
        UI_ScreenTransitioner.OutIn(()=>SwitchState(new Puzzle_LevelSelected_GMS(request)));
    }

    //Nulifica el accionar del boton back
    protected override void OnGameModeQuit() { }
}
