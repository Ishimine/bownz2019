﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puzzle_LevelSelected_GMS : Puzzle_LevelGameMode
{
    private LevelRequest levelRequested;

    //Use this for level reset
    public Puzzle_LevelSelected_GMS()
    {
        levelRequested = new LevelRequest(GameModeID, CurrentLevelIndex);
    }

    public Puzzle_LevelSelected_GMS(LevelRequest levelRequest)
    {
        levelRequested = levelRequest;
    }

    public override void Enter()
    {
        base.Enter();
        LoadLevel(levelRequested);
        SetElementsEnabled(true);
        this.PostNotification(Notifications.GameMode_Ready);
        Player.transform.position = LevelConfiguration.PlayerStartPosition;
        menuSelection.gameObject.SetActive(false);

        ball.SwitchState(new BallStates.Awaking(ball));
        SwitchState(new Puzzle_Editing_GMS());
    }

    protected override void AddListeners()
    {
        base.AddListeners();
      /*  SwipePlatform.OnAnyAliveState -= StartMode;
        SwipePlatform.OnAnyAliveState += StartMode;*/
    }

    protected override void RemoveListeners()
    {
        base.RemoveListeners();
    //    SwipePlatform.OnAnyAliveState -= StartMode;
    }

    //Nulifica el accionar del boton back
    protected override void OnGameModeQuit() { }
}
