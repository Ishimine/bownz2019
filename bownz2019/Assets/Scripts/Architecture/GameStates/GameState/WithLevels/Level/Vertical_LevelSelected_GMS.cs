﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
/// <summary>
/// Se activa cuando un nivel es seleccionado (posiblemente desde una pantalla de seleccion de nivel)
/// </summary>
public class Vertical_LevelSelected_GMS : VerticalLevelGameMode
{
    private LevelRequest levelRequested;

    //Use this for level reset
    public Vertical_LevelSelected_GMS()
    {
        levelRequested = CurrentLevelRequest;
    }

    public Vertical_LevelSelected_GMS(LevelRequest levelRequest)
    {
        levelRequested = levelRequest;
        LevelIndex = levelRequest.Index;
    }

    public override void Enter()
    {
        base.Enter();


        LoadLevel(levelRequested);
        Ball.DefaultStartPosition = CurrentLevelConfiguration.PlayerStartPosition;
        SetElementsEnabled(true);
        this.PostNotification(Notifications.GameMode_Ready);
        MenuSelection.gameObject.SetActive(false);
        this.PostNotification(Notifications.ShowStars);

        CanvasInGame.SetObjective(Objectives);
        MainCameraManager.SetPosition(CurrentLevelConfiguration.CameraStartPosition);

        InputManager.SetActive(true);
    }

    protected override void AddListeners()
    {
        base.AddListeners();

        SwipePlatform.OnAnyAliveState -= StartMode;
        SwipePlatform.OnAnyAliveState += StartMode;

        this.AddObserver(OnBallWaiting_Response, Notifications.BallWaiting);
    }

    protected override void RemoveListeners()
    {
        base.RemoveListeners();
        SwipePlatform.OnAnyAliveState -= StartMode;
        this.RemoveObserver(OnBallWaiting_Response, Notifications.BallWaiting);
    }

    private void OnBallWaiting_Response(object arg1, object arg2)
    {
        this.PostNotification(Notifications.SwipeSpawnerSetEnable, true);
    }

    private void StartMode(SwipePlatform swipePlatform)
    {
        SwitchState(new Vertical_Playing_GMS());
    }

    //Nulifica el accionar del boton back
    protected override void OnBackButtonPressed()
    {
    }
}
