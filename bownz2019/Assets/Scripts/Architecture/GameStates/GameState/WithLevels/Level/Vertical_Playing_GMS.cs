﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vertical_Playing_GMS : VerticalLevelGameMode
{
    private List<SwipePlatform> touchedPlatforms;
    public List<SwipePlatform> TouchedPlatforms
    {
        get { return touchedPlatforms; }
        set { touchedPlatforms = value; }
    }

    private HashSet<CoinType> pickedStars;
    public HashSet<CoinType> PickedStars
    {
        get { return pickedStars; }
        set { pickedStars = value; }
    }

    private float startTime;
    private float endTime;
    private int deathCount;

    public override void Enter()
    {
        base.Enter();
        this.PostNotification(Notifications.GameMode_Go);

        pickedStars = new HashSet<CoinType>();
        touchedPlatforms = new List<SwipePlatform>();
        startTime = Time.time;
        deathCount = 0;
        Settings.UiInteraction = true;

        if (Settings.IsChallengeMode)
        {
            SlowMotionWhileSwipe_Modificator.isInfinite = false;
            SlowMotionWhileSwipe_Modificator.Activate();
        }
        else
        {
            SlowMotionWhileSwipe_Modificator.isInfinite = true;
            SlowMotionWhileSwipe_Modificator.Deactivate();
        }
        MainCameraManager.SetPosition(CurrentLevelConfiguration.CameraStartPosition);
    }


    private CodeAnimator codeAnimator = new CodeAnimator();

    public override void Exit()
    {
        base.Exit();
        TimeManager.Resume();

        CurrentLevelTerrain.gameObject.SetActive(false);
    }

    protected override void AddListeners()
    {
        base.AddListeners();
        this.AddObserver(OnBallDeadStartResponse, Notifications.Ball_DeadStart);
        this.AddObserver(OnBallDeadEndResponse, Notifications.Ball_DeadEnd);
        this.AddObserver(OnWinCoinPickedUp, Notifications.WinCoinPickedUp);
        this.AddObserver(OnSwipePlatform_Touched_Response, Notifications.SwipePlatform_Touched);
        this.AddObserver(OnCoinPickedUp, Notifications.OnCoinPickedUp);
        this.AddObserver(OnBackToMainMenu, Notifications.GameCycle_Playing_BackToMenu);
        this.AddObserver(OnRetry_Request, Notifications.GameModeRequest_Retry);

    }

    protected override void RemoveListeners()
    {
        base.RemoveListeners();
        this.RemoveObserver(OnBallDeadStartResponse, Notifications.Ball_DeadStart);
        this.RemoveObserver(OnBallDeadEndResponse, Notifications.Ball_DeadEnd);
        this.RemoveObserver(OnWinCoinPickedUp, Notifications.WinCoinPickedUp);
        this.RemoveObserver(OnSwipePlatform_Touched_Response, Notifications.SwipePlatform_Touched);
        this.RemoveObserver(OnCoinPickedUp, Notifications.OnCoinPickedUp);
        this.RemoveObserver(OnBackToMainMenu, Notifications.GameCycle_Playing_BackToMenu);
        this.RemoveObserver(OnRetry_Request, Notifications.GameModeRequest_Retry);
    }
 

    private void OnRetry_Request(object arg1, object arg2)
    {
        Restart();
    }

    private void OnBackToMainMenu(object arg1, object arg2)
    {
        Debug.Log("OnBackToMainMenu");
        OnBackButtonPressed();
    }

    private void OnCoinPickedUp(object sender, object tipo)
    {
        if ((int)tipo == 0)
        {
            var coin = (CoinType)sender;
            pickedStars.Add(coin);
            PopTextManager.ShowPopMessage(PopTextManager.PuzzleStarMessage(pickedStars.Count, coin.Position));
        }
    }

    private void OnSwipePlatform_Touched_Response(object sender, object touchingObject)
    {
        touchedPlatforms.Add((SwipePlatform)sender);
    }

    private void OnWinCoinPickedUp(object arg1, object arg2)
    {
        endTime = Time.time;

        ///Notificaciones
        this.PostNotification(Notifications.SwipeSpawnerSetEnable, false);
        this.PostNotification(Notifications.LevelWon);
        this.PostNotification(Notifications.GameMode_End);

        ///Ball
        Player.GetComponent<BallStateMachine>().Disappear();
        GameObject go = new GameObject();
        Empty proxy = go.AddComponent<Empty>();
        codeAnimator.StartWaitAndExecute(proxy, 1, GameMode_Win, false);
        UnityEngine.Object.Destroy(go,5);


    }

    private void OnBallDeadStartResponse(object arg1, object arg2)
    {
        deathCount++;
        MainCameraManager.Instance.Pause();
    }

    private void OnBallDeadEndResponse(object arg1, object arg2)
    {
        if ((Checkpoint.Checkpoints.Count == 0 || Checkpoint.Active.Count < 1) && pickedStars.Count == 0)
        {
            Restart();
        }
        else
        {
            this.PostNotification(Notifications.GameMode_End);
            MainCameraManager.Instance.Resume();
            Player.gameObject.SetActive(false);
            Player.transform.position = (Checkpoint.Active.Count-1 >= 0)?Checkpoint.Active[Checkpoint.Active.Count-1].GetRespawnPoint():(Vector3)Ball.DefaultStartPosition;
            Player.gameObject.SetActive(true);
            Player.GetComponent<BallStateMachine>().AppearToLiving();
            this.PostNotification(Notifications.GameMode_PlayerRespawn);    
        }
    }

    private void Restart()
    {
        this.PostNotification(Notifications.GameMode_Reset);
        SwitchState(new Vertical_LevelSelected_GMS());
    }

    private void GameMode_Win()
    {
        Debug.Log("Notifications.GameMode_Reset");
        this.PostNotification(Notifications.GameMode_Reset);
        LevelProgress score = new LevelProgress(touchedPlatforms.Count, endTime - startTime, deathCount, pickedStars.Count);
        SwitchState(new Vertical_WinScreen_GMS(score));
    }

    protected override void OnGameModeQuit()
    {
        base.OnGameModeQuit();
        SwitchState(new Vertical_MenuSelection_GMS());
    }


}
