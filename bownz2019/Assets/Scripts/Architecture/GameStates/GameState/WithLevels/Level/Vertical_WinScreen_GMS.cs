﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Vertical_WinScreen_GMS : VerticalLevelGameMode
{
    public Vertical_WinScreen_GMS(LevelProgress score)
    {



        WinScreen.Initialize(Objectives, score, GetCurrentLevelPlayerScore());
        SaveCurrentLevelPlayerScore(score);
    }

    public override void Enter()
    {
        base.Enter();
        WinScreen.Show();
        this.PostNotification(Notifications.SwipeSpawnerClear);
    }

    protected override void AddListeners()
    {
        base.AddListeners();
        this.AddObserver(OnRequestLevelSelection_Response, Notifications.GameModeRequest_BackToLevelSelection);
        this.AddObserver(OnRequestRetry_Response, Notifications.GameModeRequest_Retry);
        this.AddObserver(OnRequestNext_Response, Notifications.GameModeRequest_Next);
        this.AddObserver(OnQuery_DoesNextLevelExist,Notifications.Query_DoesNextLevelExist );
    }

    protected override void RemoveListeners()
    {
        base.RemoveListeners();
        this.RemoveObserver(OnRequestLevelSelection_Response, Notifications.GameModeRequest_BackToLevelSelection);
        this.RemoveObserver(OnRequestRetry_Response, Notifications.GameModeRequest_Retry);
        this.RemoveObserver(OnRequestNext_Response, Notifications.GameModeRequest_Next);
        this.RemoveObserver(OnQuery_DoesNextLevelExist,Notifications.Query_DoesNextLevelExist );
    }

    private void OnQuery_DoesNextLevelExist(object arg1, object arg2)
    {
        if(LevelsDb.Elements.ContainsKey((CurrentLevelIndex+1).ToString()))
            ((BaseException)arg2).FlipToggle();
    }

    private void OnRequestNext_Response(object arg1, object arg2)
    {
        SwitchState(new Vertical_LevelSelected_GMS(new LevelRequest(Settings.VerticalLevelId,CurrentLevelIndex+1)));
    }

    private void OnRequestRetry_Response(object arg1, object arg2)
    {
        SwitchState(new Vertical_LevelSelected_GMS());
    }

    private void OnRequestLevelSelection_Response(object arg1, object arg2)
    {
        SwitchState(new Vertical_MenuSelection_GMS());
    }

    //Nulifica el accionar del boton back
    protected override void OnGameModeQuit() { }
}
