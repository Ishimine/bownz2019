﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class Landscape_MenuSelection_GMS : VerticalLevelGameMode
{
    public override void Enter()
    {
        MenuSelectionCanvas.Set(
        "Levels Landscape",
       () => CloseThenSwitchState(new Portrait_MenuSelection_GMS()),
       () => CloseThenSwitchState(new Puzzle_MenuSelection_GMS()));

        UsePortraitLevels = false;
        SwitchState(new Vertical_MenuSelection_GMS());
    }
}
