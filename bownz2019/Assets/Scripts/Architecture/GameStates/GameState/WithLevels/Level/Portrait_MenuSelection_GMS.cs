﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class Portrait_MenuSelection_GMS : VerticalLevelGameMode
{
    public override void Enter()
    {
        MenuSelectionCanvas.Set(
          "Levels Portrait",
         () => CloseThenSwitchState(new Box_MenuSelection_GMS()),
         () => CloseThenSwitchState(new Landscape_MenuSelection_GMS()));


        UsePortraitLevels = true;
        SwitchState(new Vertical_MenuSelection_GMS());
    }
}
