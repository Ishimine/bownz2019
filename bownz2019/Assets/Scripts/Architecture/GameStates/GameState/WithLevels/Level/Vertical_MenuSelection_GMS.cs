﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class Vertical_MenuSelection_GMS : VerticalLevelGameMode
{
    public override void Enter()
    {
        base.Enter();
                                           
        FoldoutInfo.gameObject.SetActive(true);
        MenuSelectionCanvas.Instance.gameObject.SetActive(true);

        SetElementsEnabled(false);
        MenuSelection.Initialize(LevelsDb.Elements.Count);
        MenuSelection.gameObject.SetActive(true);
        MenuSelection.SetPage(0);
        MainCameraManager.Instance.SwitchToStaticCamera();

        FoldoutInfo.gameObject.SetActive(true);
        Inventory.SaveToDisk();

        this.PostNotification(Notifications.SettingsMenu_Enable, true);
    }

    public override void Exit()
    {
        base.Exit();
        MenuSelectionCanvas.Instance.gameObject.SetActive(false);
        FoldoutInfo.gameObject.SetActive(false);
            
        this.PostNotification(Notifications.SettingsMenu_Enable, false);
        this.PostNotification(Notifications.LevelFoldoutInfoClose);

    }

    protected override void CloseThenSwitchState(State state)
    {
        MainCameraManager.Instance.SwitchToStaticCamera();
        base.CloseThenSwitchState(state);
    }

    protected override void AddListeners()
    {
        base.AddListeners();
        this.AddObserver(OnLevelRequestedResponse, Notifications.LevelLoadRequest);
    }

    protected override void RemoveListeners()
    {
        base.RemoveListeners();
        this.RemoveObserver(OnLevelRequestedResponse, Notifications.LevelLoadRequest);
    }

    private void OnLevelRequestedResponse(object arg1, object levelRequest)
    {
        LevelRequest request = (LevelRequest)levelRequest;
        MenuSelectionCanvas.Instance.gameObject.SetActive(false);
        UI_ScreenTransitioner.OutIn(()=>SwitchState(new Vertical_LevelSelected_GMS(request)));
    }

    //Nulifica el accionar del boton back
    protected override void OnGameModeQuit() { }
}
