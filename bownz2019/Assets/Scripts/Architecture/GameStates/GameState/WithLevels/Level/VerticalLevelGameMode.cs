﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class VerticalLevelGameMode : LevelGameModeState
{
    public override string[] GameModePrefabsKeys => new string[]
    {
        "Ball",
        "Canvas_Grid_LevelSelection",
        "Canvas_InGame_Level",
        "Canvas_Level_WinScreen",
        "Canvas_ObjectiveFoldout"
    };

    public override string GameModeID => (UsePortraitLevels)? "Portrait" : "Landscape";

    protected override string LevelsDbId => (UsePortraitLevels)?"Portrait_Levels": "Landscape_Levels";

    public static bool UsePortraitLevels = false;

    protected static BallStateMachine Ball;
    protected static UI_LevelSelectionGrid MenuSelection;
    protected static Canvas_Level_WinScreen WinScreen;

    private static Level_Objectives objectives;
    protected static LevelProgress Objectives => (Settings.IsChallengeMode)? objectives.Challenge: objectives.Normal;

    protected static Canvas_LevelFoldoutInfo FoldoutInfo;
    protected static Canvas_InGame_Explorer CanvasInGame;

    protected override void InitializeElements()
    {
        base.InitializeElements();

        GameObject go;
        Player = Elements[GameModeID].Values.First(x => x.gameObject.GetComponent<BallStateMachine>() != null);
        Ball = Player.GetComponent<BallStateMachine>();
        go = Elements[GameModeID].Values.First(x => x.gameObject.GetComponent<UI_LevelSelectionGrid>() != null);
        WinScreen = Elements[GameModeID].Values.First(x => x.gameObject.GetComponent<Canvas_Level_WinScreen>() != null).GetComponent<Canvas_Level_WinScreen>();
        FoldoutInfo = Elements[GameModeID].Values.First(x => x.gameObject.GetComponent<Canvas_LevelFoldoutInfo>() != null).GetComponent<Canvas_LevelFoldoutInfo>();
        CanvasInGame = Elements[GameModeID].Values.First(x => x.gameObject.GetComponent<Canvas_InGame_Explorer>() != null).GetComponent<Canvas_InGame_Explorer>();
        if (go != null)
        {
            MenuSelection = go.GetComponent<UI_LevelSelectionGrid>();
            MenuSelection.MyGameMode = this;
        }
    }

    public override void LoadLevel(LevelRequest request)
    {
        base.LoadLevel(request);

        //Cargamos tamaño del area de juego objetivo
        if(CurrentLevelConfiguration != null) PlayArea.Instance.SetSize(CurrentLevelConfiguration.PlayAreaSize);

        objectives = CurrentLevelTerrain.GetComponent<Level_Objectives>();

        //Aplicamos camara Follow adecuada
        FollowCamera.FollowType followType = (CurrentLevelConfiguration.PlayAreaSize.x > CurrentLevelConfiguration.PlayAreaSize.y) ? FollowCamera.FollowType.Landscape : FollowCamera.FollowType.Portrait;
        MainCameraManager.Instance.SwitchToFollowCamera(Player.transform, followType);

        //Aplicamos el "Confiner" en caso de que el nivel tenga uno
        if (CurrentLevelConfiguration != null && CurrentLevelConfiguration.CameraConfiner != null) MainCameraManager.Instance.SetColliderConfiner(CurrentLevelConfiguration.CameraConfiner);
    }

    private GameObject GetLevelFromDB(string key)
    {
        if (LevelsDb.Elements.ContainsKey(key))
            return LevelsDb.Elements[key];
        else
        {
            Debug.LogError(string.Format("NIVEL:{0} no encontrado ", key));
            return LevelsDb.Elements.Values.ElementAt(0);
        }
    }

    protected override void CloseThenSwitchState(State nState)
    {
        base.CloseThenSwitchState(nState);
        if (CurrentLevelTerrain != null) Object.Destroy(CurrentLevelTerrain);
    }

    protected virtual Vector2 GetAreaSize()
    {
        return Settings.Portrait_PlayAreaSize;
    }

}
