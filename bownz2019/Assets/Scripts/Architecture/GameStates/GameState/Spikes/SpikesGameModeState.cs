﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikesGameModeState : GameModeState
{
    public override string[] GameModePrefabsKeys => new string[]
    {
        "Ball",
        "Spikes_Terrain",
        "Camera_Killbox",
        "BackgroundDinamic",
        "StarManager",
        "StarTargetFX",
        "UI_Score"
    };

    public override string GameModeID => "SpikesGameModeState";

    protected override void OnGameModeQuit()
    {
        base.OnGameModeQuit();
        SwitchState(new Spikes_MenuSelection_GMS());
    }
}
