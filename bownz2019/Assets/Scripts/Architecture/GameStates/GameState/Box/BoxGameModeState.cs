﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class BoxGameModeState : GameModeState
{
    public override string[] GameModePrefabsKeys => new string[]
    {
        "Ball",
        "Box_ScoreManager",
        "BackgroundDinamic",
        "BestScoreDisplay",
        "Box_Terrain",
        "Camera_Killbox",
        "FirstStarCoin",
        "StarManager",
        "StarManagerSpecial",
        "UI_Score"
    };

    protected BallStateMachine ball;
    public override string GameModeID => "BoxGameModeState";
    static protected BestScoreDisplay bestScoreDisplay;
    static protected SpecialBoxSpawnConfig specialBoxSpawnConfig;

    protected override void InitializeElements()
    {
        base.InitializeElements();
        ball = Elements[GameModeID].Values.First(x => x.gameObject.GetComponent<BallStateMachine>() != null).GetComponent<BallStateMachine>();
        bestScoreDisplay = Elements[GameModeID].Values.First(x => x.gameObject.GetComponent<BestScoreDisplay>() != null).GetComponent<BestScoreDisplay>();
        specialBoxSpawnConfig = Elements[GameModeID].Values.First(x => x.gameObject.GetComponent<SpecialBoxSpawnConfig>() != null).GetComponent<SpecialBoxSpawnConfig>();
    }


    protected override void OnGameModeQuit()
    {
        base.OnGameModeQuit();
        SwitchState(new Box_MenuSelection_GMS());
    }
}
