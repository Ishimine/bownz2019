﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Box_Playing_GMS : BoxGameModeState
{
    public const float MaxTimeScale = 2.5f;
    public const int StarsToReachMaxScale = 300;
    public const float AccelerationSteps = (MaxTimeScale - 1) / StarsToReachMaxScale;
    public const int accelerationDelta = 10;

    int coinsCount = 0;

    Queue<KeyValuePair<BallSkin, GameObject>> extraLives;

    private const int CoinsToIncreaceAtMaxSpeed = 100;
    private Vector2 minMaxBetweenHeightIncreace = new Vector2(12, 5);

    public int currentScore;

    public override void Enter()
    {
        base.Enter();
        this.PostNotification(Notifications.GameMode_Go);
        Settings.UiInteraction = true;

        TimeManager._ResetAll();
        coinsCount = 0;
        extraLives = new Queue<KeyValuePair<BallSkin, GameObject>>();

        bestScoreDisplay.Hide();
    }

    protected override void AddListeners()
    {
        base.AddListeners();
        this.AddObserver(OnBallDeadStartResponse, Notifications.Ball_DeadStart);
        this.AddObserver(OnBallDeadEndResponse, Notifications.Ball_DeadEnd);
        this.AddObserver(OnCoinPickedUp_Response, Notifications.OnCoinPickedUp);
        this.AddObserver(OnScoreUpdate_Response, Notifications.ScoreUpdate);
        this.AddObserver(OnExtraLifeGained_Response, Notifications.ExtraLifeGained);
    }

    protected override void RemoveListeners()
    {
        base.RemoveListeners();
        this.RemoveObserver(OnBallDeadStartResponse, Notifications.Ball_DeadStart);
        this.RemoveObserver(OnBallDeadEndResponse, Notifications.Ball_DeadEnd);
        this.RemoveObserver(OnCoinPickedUp_Response, Notifications.OnCoinPickedUp);
        this.RemoveObserver(OnScoreUpdate_Response, Notifications.ScoreUpdate);
        this.RemoveObserver(OnExtraLifeGained_Response, Notifications.ExtraLifeGained);
    }

    private void OnExtraLifeGained_Response(object star, object arg2)
    {

        ExtraLifePopUp extraLifePopUp = GameObjectLibrary.Instantiate("ExtraLifePopUp", null).GetComponent<ExtraLifePopUp>();
        extraLifePopUp.transform.position = ball.transform.position;


        KeyValuePair<BallSkin, GameObject> skinConfig = new KeyValuePair<BallSkin, GameObject>(
            Body_Conteiner.GetRandomOwnedSkin(),
            GameObjectLibrary.Get("Trail" + Inventory.GetRandomOwnedTrailIndex()));

        extraLives.Enqueue(skinConfig);

        extraLifePopUp.SetSkin(skinConfig.Key);
        extraLifePopUp.SetTrail(skinConfig.Value);

        UI_Messenger.Show(new ValueChangeMsg(MsgPosition.Top, .75f, extraLives.Count - 1, extraLives.Count, "0.", SpriteLibrary.Get("Heart")));
    }


    public override void Exit()
    {
        base.Exit();
        Inventory.Add(Inventory.IDs.Coin, currentScore, true);
    }

    private void OnScoreUpdate_Response(object arg1, object arg2)
    {
        currentScore = (int)arg2;
    }

    private void OnCoinPickedUp_Response(object arg1, object idType)
    {
        coinsCount++;
        if ((coinsCount % accelerationDelta == 0) && TimeManager.BaseTimeScale < MaxTimeScale)
        {
            float accelerationAmmount = AccelerationSteps * accelerationDelta;
            TimeManager._ModifyFlat_BaseTime(accelerationAmmount);
            //UI_Messenger.Show(new Toast_Msg(string.Format("{0}Coins! SPEED↑↑↑", coinsCount), MsgPosition.Top, 2f, SpriteLibrary.Get("SpeedUp_Arrow")));
        }
    }

    private void OnBallDeadStartResponse(object arg1, object arg2)
    {
        if (extraLives.Count == 0)
            this.PostNotification(Notifications.GameMode_End);
    }

    private void OnBallDeadEndResponse(object arg1, object arg2)
    {
        if (extraLives.Count == 0)
        {
            Inventory.Add(Inventory.IDs.Coin, currentScore, true);
            if (PlayerPrefs.GetInt("BestScore_Box", 0) < currentScore) PlayerPrefs.SetInt("BestScore_Box", currentScore);
            currentScore = 0;

            this.PostNotification(Notifications.Terrain_AllDown, false);
            TimeManager._ResetAll();
            this.PostNotification(Notifications.GameMode_Reset);
            SwitchState(new Box_MenuSelection_GMS());

        }
        else
        {
            UI_Messenger.Show(new ValueChangeMsg(MsgPosition.Top, .75f, extraLives.Count + 1, extraLives.Count, "0.", SpriteLibrary.Get("Heart")));
            KeyValuePair<BallSkin, GameObject> skinAndTrail = extraLives.Dequeue();
            ball.TrailComponent.SetTrailPrefab(skinAndTrail.Value);
            ball.SkinComponent.SetSkin(skinAndTrail.Key);
            ball.transform.position = ball.DefaultStartPosition;
            ball.AppearToLiving();
        }
    }

}
