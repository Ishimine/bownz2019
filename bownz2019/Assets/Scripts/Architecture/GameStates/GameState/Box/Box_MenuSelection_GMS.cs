﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Box_MenuSelection_GMS : BoxGameModeState
{
    public override void Enter()
    {
        base.Enter();

        MenuSelectionCanvas.Set(
            "BOX",
            () => CloseThenSwitchState(new Classic_MenuSelection_GMS()),
            () => CloseThenSwitchState(new Portrait_MenuSelection_GMS()));

        /*MenuSelectionCanvas.Set(
         "BOX",
         () => CloseThenSwitchState(new Classic_MenuSelection_GMS()),
         () => CloseThenSwitchState(new Procedural_DailyLevel_Menu_GMS()));*/

        ball.DefaultStartPosition = Vector2.zero;

        SetElementsEnabled(true);
        this.PostNotification(Notifications.GameMode_Ready);

        PlayArea.Instance.SetSize(Settings.Portrait_PlayAreaSize);
        MenuSelectionCanvas.Instance.gameObject.SetActive(true);

        ball.TrailComponent.UsePlayerSelection = true;
        ball.SkinComponent.UsePlayerSelection = true;

        bestScoreDisplay.Show(PlayerPrefs.GetInt("BestScore_Box", 0));

        Inventory.SaveToDisk();

        specialBoxSpawnConfig.SetAs_Box();
    }

    public override void Exit()
    {
        base.Exit();
        MenuSelectionCanvas.Instance.gameObject.SetActive(false);
    }

    protected override void AddListeners()
    {
        base.AddListeners();
        SwipePlatform.OnAnyAliveState += StartMode;
        this.AddObserver(OnBallAliveResponse, Notifications.BallWaiting);
    }

    protected override void RemoveListeners()
    {
        base.RemoveListeners();
        SwipePlatform.OnAnyAliveState -= StartMode;
        this.RemoveObserver(OnBallAliveResponse, Notifications.BallWaiting);
    }

    private void OnBallAliveResponse(object arg1, object arg2)
    {
        this.PostNotification(Notifications.SwipeSpawnerSetEnable, true);
    }

    private void StartMode(SwipePlatform swipePlatform)
    {
        SwitchState(new Box_Playing_GMS());
    }

    //Nulifica el accionar del boton back
    protected override void OnGameModeQuit() { }
}
