﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameModeElements = System.Collections.Generic.Dictionary<string, System.Collections.Generic.Dictionary<string, UnityEngine.GameObject>>;

public abstract class GameModeState : GameState
{ 
    public abstract string[] GameModePrefabsKeys { get; }

    protected const string rootId = "_Root";

    /// <summary>
    /// Un diccionario que utiliza como llave los nombres de los modos de juego y como valor un diccionario de objetos del modo.
    /// </summary>
    private static GameModeElements elements = new GameModeElements();
    protected static GameModeElements Elements
    {
        get { return elements; }
        set { elements = value; }
    }

    protected static GameObject Root;

    /// <summary>
    /// Si el nombre de un modo de juego ESTA en la lista significa que el modo ya fue Inicializado con exito
    /// </summary>
    private HashSet<string> initialized = new HashSet<string>();
    protected HashSet<string> Initialized
    {
        get { return initialized; }
    }

    private static int levelIndex = -1;
    public static int LevelIndex { get => levelIndex; protected set => levelIndex = value;}

    public abstract string GameModeID
    {
        get;
    }
                   
    protected LevelProgress GetCurrentLevelPlayerScore()
    {
        return LevelsProgress.Get(GameModeID, LevelIndex, Settings.IsChallengeMode);
    }
                                    
    protected void SaveCurrentLevelPlayerScore(LevelProgress score)
    {
        LevelsProgress.AddLevelProgress(GameModeID, LevelIndex, Settings.IsChallengeMode, score,true);
    }

    public override void Enter()
    {
        base.Enter();

        if (!Elements.ContainsKey(GameModeID)) //Inicializamos el diccionario del modo de juego dentro del diccionario Maestro
        {
            Elements.Add(GameModeID, new Dictionary<string, GameObject>());
            Debug.Log("Initialized mode elements dictionary => " + GameModeID);

            //Agregamos el objeto ROOT que va a contener a todos los objetos del modo de juego.
            Root = new GameObject();
            Root.name = GameModeID + rootId;
            Elements[GameModeID].Add(rootId, Root);
        }
        if (!Initialized.Contains(GameModeID)) InitializeElements();

    }

    protected virtual void InitializeElements()
    {
        Initialized.Add(GameModeID);
        foreach (var item in GameModePrefabsKeys)
        {
            if (!Elements[GameModeID].ContainsKey(item))
            {
                Elements[GameModeID].Add(item, GameObjectLibrary.Instantiate(item, Elements[GameModeID][rootId].transform));
            }
        }
    }

    protected void SetElementsEnabled(bool value)
    {
        foreach (var item in Elements[GameModeID].Values)
        {
            item.SetActive(value);
        }
        var root = Elements[GameModeID][rootId];
        root.gameObject.SetActive(true);
        if (value)
            root.name = root.name.ToUpper();
        else
            root.name = root.name.ToLower();
    }

    protected virtual void CloseThenSwitchState(State nState) 
    {
        SetElementsEnabled(false);
        this.PostNotification(Notifications.SwipePlatform_Touched, false);
        SwitchState(nState);
    }

    protected override void AddListeners()
    {
        base.AddListeners();
        this.AddObserver(OnBackButtonPressed, Notifications.BackButtonPressed);
        this.AddObserver(OnPauseRequested, Notifications.PauseRequest);
    }

    protected override void RemoveListeners()
    {
        base.RemoveListeners();
        this.RemoveObserver(OnBackButtonPressed, Notifications.BackButtonPressed);
        this.RemoveObserver(OnPauseRequested, Notifications.PauseRequest);
    }

  

    /// <summary>
    /// Se ejecuta cuando se presiona el boton back Ejecuta OnGameModeQuit() y luego envia una notificacion del tipo "Notifications.GameMode_Quit()"
    /// Sobreescribir y agregar SwitchState(Estado de seleccion en menu);
    /// </summary>
    /// <param name="arg1"></param>
    /// <param name="arg2"></param>
    protected void OnBackButtonPressed(object arg1, object arg2)
    {
        OnBackButtonPressed();
    }

    protected virtual void OnBackButtonPressed()
    {
        OnGameModeQuit();
        this.PostNotification(Notifications.SwipeSpawnerSetEnable, false);
        this.PostNotification(Notifications.GameMode_Quit);
    }

    //Accion a tomar cuando se toca el boton Back
    protected virtual void OnGameModeQuit()
    {
        this.PostNotification(Notifications.GameMode_End);
        this.PostNotification(Notifications.GameMode_Reset);
    }
            
    protected void OnPauseRequested(object arg1, object arg2)
    {
        Debug.Log("Pause Requested: " + (bool)arg2);
        Pause((bool) arg2);
    }

    private static bool inPause = false;
    public static bool InPause => inPause;

    protected void Pause(bool value)
    {
        if (inPause == value)
        {
            Debug.Log("Deny");
            return;
        }

        inPause = value;

        if (value)
        {
            Debug.Log("Pause");
            TimeManager.Pause();
            InputManager.SetActive(false);
        }
        else
        {
            Debug.Log("Resume");
            TimeManager.Resume();
            InputManager.SetActive(true);
        }

        this.PostNotification(Notifications.PauseChange, value);
    }
}
