﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Classic_MenuSelection_GMS : ClassicGameModeState
{
    public override void Enter()
    {
        base.Enter();
        PlayArea.Instance.SetSize(Settings.Portrait_PlayAreaSize);

        MenuSelectionCanvas.Set(
            "CLASSIC",
            () => CloseThenSwitchState(new OptionsMenuSelectionState()),
            () => CloseThenSwitchState(new Box_MenuSelection_GMS()));
        WrapManager.WrapX = true;
        WrapManager.WrapY = false;

        ball.TrailComponent.UsePlayerSelection = true;
        ball.SkinComponent.UsePlayerSelection = true;
        ball.SkinComponent.SetSkin(BallSkin.PlayerSelectedSkin);
        ball.TrailComponent.SetTrailPrefab(TrailBall.PlayerSelection);
        ball.DefaultStartPosition = Vector2.zero;

        SetElementsEnabled(true);
        this.PostNotification(Notifications.GameMode_Ready);
        MenuSelectionCanvas.Instance.gameObject.SetActive(true);
        MainCameraManager.Instance.SwitchToStaticCamera();

        ball.useWrap = true;

        bestScoreDisplay.Show(PlayerPrefs.GetInt("BestScore_Classic", 0));
        Inventory.SaveToDisk();

        specialBoxSpawnConfig.SetAs_Classic();
    }

    public override void Exit()
    {
        base.Exit();
        MenuSelectionCanvas.Instance.gameObject.SetActive(false);
        bestScoreDisplay.gameObject.SetActive(false);
    }

    protected override void AddListeners()
    {
        base.AddListeners();
        SwipePlatform.OnAnyAliveState += StartMode;
        this.AddObserver(OnBallAliveResponse, Notifications.BallWaiting);
    }

    protected override void RemoveListeners()
    {
        base.RemoveListeners();
        SwipePlatform.OnAnyAliveState -= StartMode;
        this.RemoveObserver(OnBallAliveResponse, Notifications.BallWaiting);
    }

    private void OnBallAliveResponse(object arg1, object arg2)
    {
        this.PostNotification(Notifications.SwipeSpawnerSetEnable, true);
    }

    private void StartMode(SwipePlatform swipePlatform)
    {
        SwitchState(new Classic_Playing_GMS());
    }

    protected override void OnBackButtonPressed()
    {
    }

    //Nulifica el accionar del boton back
    protected override void OnGameModeQuit() { }
}
