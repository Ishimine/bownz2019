﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ClassicGameModeState : GameModeState
{
    public override string[] GameModePrefabsKeys => new string[]
    {
        "Ball",
        "Classic_Terrain",
        "BestScoreDisplay",
      //  "Classic_EndScreen",
        "Camera_Killbox",
        "Box_ScoreManager",
        "BackgroundDinamic",
        "FirstStarCoin",
    //    "StarManager",
        "StarManagerSpecial",
        "UI_Score",
        "WrapController"
    };

    public override string GameModeID => "ClassicGameModeState";

    static protected BallStateMachine ball;
    static protected BestScoreDisplay bestScoreDisplay;
    static protected SpecialBoxSpawnConfig specialBoxSpawnConfig;

    protected override void InitializeElements()
    {
        base.InitializeElements();
        ball = Elements[GameModeID].Values.First(x => x.gameObject.GetComponent<BallStateMachine>() != null).GetComponent<BallStateMachine>();
        bestScoreDisplay = Elements[GameModeID].Values.First(x => x.gameObject.GetComponent<BestScoreDisplay>() != null).GetComponent<BestScoreDisplay>();
        specialBoxSpawnConfig = Elements[GameModeID].Values.First(x => x.gameObject.GetComponent<SpecialBoxSpawnConfig>() != null).GetComponent<SpecialBoxSpawnConfig>();
    }

    protected override void OnGameModeQuit()
    {
        base.OnGameModeQuit();
        SwitchState(new Classic_MenuSelection_GMS());
    }

    protected override void CloseThenSwitchState(State nState)
    {
        base.CloseThenSwitchState(nState);
        ball.useWrap = true;
    }
}
