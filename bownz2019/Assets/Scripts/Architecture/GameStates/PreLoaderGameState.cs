﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreLoaderGameState : GameState
{
    public override void Enter()
    {
        base.Enter();
        UI_ScreenTransitioner.Out(OnTransitionOut_Done);
    }

    private void OnTransitionOut_Done()
    {
        Debug.Log("OnTransitionOut_Done");
        SwitchState(new InitMenuSelection());
    }
}
