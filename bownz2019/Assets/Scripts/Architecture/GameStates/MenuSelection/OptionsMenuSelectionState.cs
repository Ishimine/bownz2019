﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionsMenuSelectionState : GameState
{
    public override void Enter()
    {
        base.Enter();
        MenuSelectionCanvas.Set(
           "OPTIONS",
          () => SwitchState(new CreditsMenuSelectionState()),
          () => SwitchState(new Classic_MenuSelection_GMS()));
        MainCameraManager.Instance.SwitchToStaticCamera();
    }
}
