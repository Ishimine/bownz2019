﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditsMenuSelectionState : GameState
{
    public override void Enter()
    {
        base.Enter();
        MenuSelectionCanvas.Set(
           "CREDITS",
           null,
           () => SwitchState(new OptionsMenuSelectionState()));
        MainCameraManager.Instance.SwitchToStaticCamera();
    }
}
