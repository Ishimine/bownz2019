﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class Spikes_MenuSelection_GMS : SpikesGameModeState
{
    public override void Enter()
    {
        base.Enter();
       
        /*MenuSelectionCanvas.Set(
         "SPIKES",
         () => CloseThenSwitchState(new Vertical_MenuSelection_GMS()),
         () => CloseThenSwitchState(new ShopMenuSelectionState()));*/

        SetElementsEnabled(true);
        MainCameraManager.Instance.SwitchToStaticCamera();
        Inventory.SaveToDisk();
    }

    protected override void AddListeners()
    {
        base.AddListeners();
        SwipePlatform.OnAnyAliveState += StartMode;
    }

    protected override void RemoveListeners()
    {
        base.RemoveListeners();
        SwipePlatform.OnAnyAliveState -= StartMode;
    }

    private void StartMode(SwipePlatform swipePlatform)
    {
        MenuSelectionCanvas.Instance.gameObject.SetActive(false);
        throw new NotImplementedException();
        //Owner.SwitchState(new SpikesGameModeState());
    }
}
