﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectionMenuSelectionState : GameModeState
{
    public override string[] GameModePrefabsKeys => new string[]
    {
        "Canvas_QuickSkinSelector",
        "Collection_Shelf",
        "QuickSkinSelector",
    };

    public override string GameModeID => "Collection";

    public override void Enter()
    {
        base.Enter();
        SetElementsEnabled(true);
     /*   MenuSelectionCanvas.Set(
           "COLLECTION",
           ()=> CloseThenSwitchState(new ShopMenuSelectionState()),
           null);*/
    }

    public override void Exit()
    {
        base.Exit();
        SetElementsEnabled(false);
    }
}
