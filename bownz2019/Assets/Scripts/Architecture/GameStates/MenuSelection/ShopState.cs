﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using Lean.Touch;

public class ShopState : CSM<RootCsm>
{
    public ShopState(CompositeStateMachine superState) : base(superState)
    {
        inGameShop = Instantiate<InGameShop>("InGameShop");
        canvas_Shop_UI = Instantiate<Canvas_Shop_UI>("Canvas_Shop_UI");
    }

    public string GameModeId => "Shop";
    static InGameShop inGameShop;
    static Canvas_Shop_UI canvas_Shop_UI;

    ProductDisplay selectedProductDisplay;
    Product SelectedProduct => selectedProductDisplay.Product;

    private bool canSelect = false;

    protected override void Enter()
    {
        base.Enter();
        
        this.PostNotification(Notifications.SwipeSpawnerSetEnable,false);
        inGameShop.StartCoroutine(WaitAndExecute());
        canSelect = true;
    }

    IEnumerator WaitAndExecute()
    {
        yield return new WaitForEndOfFrame();
        inGameShop.FirstPage();
    }

    protected override void AddListeners()
    {
        base.AddListeners();
        this.AddObserver(Shop_NextPage_Response, Notifications.Shop_NextPage);
        this.AddObserver(Shop_PreviousPage_Response, Notifications.Shop_PreviousPage);
        this.AddObserver(Shop_FirstPage_Response, Notifications.Shop_FirstPage);
        this.AddObserver(Shop_LastPage_Response, Notifications.Shop_LastPage);

        this.AddObserver(OnFingerUp_Response, Notifications.OnFingerUp);
        this.AddObserver(CanSelect_Response, Notifications.Shop_CanSelect);
        this.AddObserver(BackToMain, Notifications.BackToMain);

    }
    protected override void RemoveListeners()
    {
        base.RemoveListeners();
        this.RemoveObserver(Shop_NextPage_Response, Notifications.Shop_NextPage);
        this.RemoveObserver(Shop_PreviousPage_Response, Notifications.Shop_PreviousPage);
        this.RemoveObserver(Shop_FirstPage_Response, Notifications.Shop_FirstPage);
        this.RemoveObserver(Shop_LastPage_Response, Notifications.Shop_LastPage);

        this.RemoveObserver(OnFingerUp_Response, Notifications.OnFingerUp);
        this.RemoveObserver(CanSelect_Response, Notifications.Shop_CanSelect);
        this.RemoveObserver(BackToMain, Notifications.BackToMain);

    }

    private void BackToMain(object arg1, object arg2)
    {
        Owner.BackToMain();
    }


    private void CanSelect_Response(object arg1, object arg2)
    {
        canSelect = (bool)arg2;
    }

    private void OnFingerUp_Response(object arg1, object arg2)
    {
        if (!canSelect) return;

        LeanFinger finger = (LeanFinger)arg2;
        Collider2D[] hits = Physics2D.OverlapPointAll(finger.GetWorldPosition(-20, null));

        ProductDisplay selectedProduct;
        foreach (var item in hits)
        {
            selectedProduct = item.transform.GetComponent<ProductDisplay>();
            if (selectedProduct != null && selectedProduct.IsButton)
            {
                SelectProductDisplay(selectedProduct);
                break;
            }
        }
    }

    private void SelectProductDisplay(ProductDisplay display)
    {
        canSelect = false;
        Debug.Log("Product Selected: " + display.Product);
        selectedProductDisplay = display;
        display.Selected();
        inGameShop.SetSelectedProduct(display);
        canvas_Shop_UI.UpdateCostTag(display);
    }

    private void Shop_NextPage_Response(object arg1, object arg2)
    {
        inGameShop.NextPage();
    }

    private void Shop_PreviousPage_Response(object arg1, object arg2)
    {
        inGameShop.PreviousPage();
    }

    private void Shop_FirstPage_Response(object arg1, object arg2)
    {
        inGameShop.FirstPage();
    }

    private void Shop_LastPage_Response(object arg1, object arg2)
    {
        inGameShop.LastPage();
    }

  
}
