﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitMenuSelection : GameState
{
    public override void Enter()
    {
        base.Enter();
        UnityEngine.Object.Instantiate(GameObjectLibrary.Get("MenuSelectionCanvas"), null);

        //SwitchState(new Classic_MenuSelection_GMS());
        SwitchState(new Procedural_DailyLevel_Menu_GMS());
        UI_ScreenTransitioner.In();
    }

    IEnumerator WaitAndExecute(Action action)
    {
        yield return new WaitForSeconds(1);
        action?.Invoke();
    }
}
