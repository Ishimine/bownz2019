﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : ScriptableSingleton<GameManager>
{
    private static bool isPlaying;
    public static bool IsPlaying
    {
        get { return isPlaying; }
        set { isPlaying = value; }
    }

    public override GameManager Myself => this;



}

