﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameState : State
{
    public override void Enter()
    {
        base.Enter();
        Debug.Log("<color=green> ↓↓↓ Entered </color> to <color=blue>" + this + "</color>");
    }

    public override void Exit()
    {
        base.Exit();
        Debug.Log("<color=brown> ↑↑↑  Exit </color> from <color=blue>" + this+ "</color>");
    }
}
