﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptableStateMachine<T> : ScriptableObject where T: ScriptableState
{
    private T currentState;
    public T CurrentState
    {
        get { return currentState; }
        set { currentState = value; }
    }

    public void SwitchState<B>() where B : T
    {
        SwitchState<B>(null);
    }

    public void SwitchState<B>(object parameter) where B : T
    {
        SwitchState<B>(new object[] { parameter });
    }

    public void SwitchState<B>(object[] parametros) where B : T
    {
        if (currentState != null && currentState.GetType() == (typeof(B)))
            return;

        List<object> tempParameters = new List<object>();
        tempParameters.Add(this);
        if (parametros != null && parametros.Length > 0)
        {
            for (int i = 0; i < parametros.Length; i++)
                tempParameters.Add(parametros[i]);
        }

        if (currentState != null)
            currentState.Exit();

        currentState = (T)Activator.CreateInstance(typeof(B), tempParameters.ToArray());

        if (currentState != null)
            currentState.Enter();
    }
}
