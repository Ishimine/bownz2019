﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class GameStateMachine : MonoStateMachine
{
  /*  [InfoBox("$GetCurrentState", InfoMessageType = InfoMessageType.None)]
    [ShowInInspector] string state;

    [SerializeField] private bool logToSocialPlay = true;

    Firebase.FirebaseApp app;

    private static bool firebaseInitilized;
    public static bool FirebaseInitilized { get { return firebaseInitilized; } }

    private string GetCurrentState()
    {
        if (CurrentState == null)
            return "None";
        return CurrentState.ToString();
    }

    private void Start()
    {
     //   if(logToSocialPlay) SocialPlayManager.Initialize();

        Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
            var dependencyStatus = task.Result;
            if (dependencyStatus == Firebase.DependencyStatus.Available)
            {
                // Create and hold a reference to your FirebaseApp,
                // where app is a Firebase.FirebaseApp property of your application class.
                //   app = Firebase.FirebaseApp.DefaultInstance;
                // Set a flag here to indicate whether Firebase is ready to use by your app.
                app = Firebase.FirebaseApp.DefaultInstance;
                firebaseInitilized = true;
            }
            else
            {
                UnityEngine.Debug.LogError(System.String.Format(
                  "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                // Firebase Unity SDK is not safe to use here.
                firebaseInitilized = false;
            }
        });

        SwitchState(new PreLoaderGameState());
    }
    */
}

