﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class Shield : MonoBehaviour
{
    Health health;
    public ParticleSystem pSystem;

    private CodeAnimator codeAnimator = new CodeAnimator();

    [SerializeField] private CodeAnimatorCurve animatorCurve;

    private void Awake()
    {
        gameObject.transform.localScale = Vector3.zero;
        this.AddObserver(OnValidateBuy, Notifications.ValidateBuy + MagnetReward.Id);
    }

    private void OnValidateBuy(object sender, object arg)
    {
        Argument<bool, string> validator = (Argument<bool, string>)arg;
        validator.Item0 = false;
    }

    private void OnDestroy()
    {
        this.RemoveObserver(OnValidateBuy, Notifications.ValidateBuy + ShieldReward.Id);
    }

    private void OnEnable()
    {
        Show();
    }

    public void Show()
    {
        codeAnimator.StartAnimacion(this,
           x =>
           {
               transform.localScale = Vector3.one * x;
           }, DeltaTimeType.deltaTime, AnimationType.Simple, animatorCurve.Curve, animatorCurve.Time);
    }

    private void OnDisable()
    {
        codeAnimator.Stop(this);
    }

    public void EquipTo(GameObject go)
    {

        Shield otherShield = go.GetComponentInChildren<Shield>();
        if (otherShield != null) otherShield.Unequip(false);

        gameObject.SetActive(true);
        health = go.GetComponent<Health>();
        transform.SetParent(health.transform);
        transform.localPosition = Vector3.zero;
        RegisterTo(health);


        this.AddObserver(OnGameModeEnd, Notifications.GameMode_End);

    }

    private void RegisterTo(Health health)
    {
        this.AddObserver(OnHealthModified_Response, Notifications.BeforeHealthModify,health);
    }

    private void Unregister()
    {
        this.RemoveObserver(OnHealthModified_Response, Notifications.BeforeHealthModify, health);
    }

    private void OnHealthModified_Response(object arg1, object arg2)
    {
        HealthException healthException = arg2 as HealthException;
        if(healthException.Ammount < 0)
        {
            healthException.Ammount = 0;
            Unequip(true);
        }
    }

    [Button]
    private void Play()
    {
        pSystem.Play();
    }

    public void Unequip(bool applyTemporalInvulnerability )
    {
        pSystem.transform.SetParent(null);
        pSystem.Play();
        Destroy(pSystem.gameObject,5);

        transform.SetParent(null);
        Unregister();

        Destroy(gameObject);

        this.RemoveObserver(OnGameModeEnd, Notifications.GameMode_End);

        if (applyTemporalInvulnerability)  health.gameObject.AddComponent<TemporalInvulnerability>().SetDuration(.75f).ResetTime();
    }

    private void OnGameModeEnd(object arg1, object arg2)
    {
        Unequip(false);
    }

    public static void EquipTo(BallStateMachine ball)
    {
        Shield shield = GameObjectLibrary.Instantiate("Shield", null).GetComponent<Shield>();
        shield.EquipTo(ball.gameObject);
    }
    
    

}
