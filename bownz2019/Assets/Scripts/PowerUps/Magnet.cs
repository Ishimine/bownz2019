﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;
using Sirenix.OdinInspector;

[RequireComponent(typeof(CircleCollider2D))]
public class Magnet : MonoBehaviour
{
    public float smoothTime = .2f;
    public List<StarCoin> stars = new List<StarCoin>();
    public List<Vector2> vel = new List<Vector2>();

    public bool unequipAtDead = true;

    private Health health;
    [Button]
    public void EquipTo(GameObject go)
    {
        gameObject.SetActive(true);
        transform.position = go.transform.position;
        transform.parent = go.transform;

        health = go.GetComponent<Health>();
        if (unequipAtDead && health != null)
        {
            health.OnDead -= UnequiAndDestroy;
            health.OnDead += UnequiAndDestroy;
        }
        this.AddObserver(OnGameEnd,Notifications.GameMode_End);
    }

    private void Awake()
    {
        this.AddObserver(OnValidateBuy, Notifications.ValidateBuy + MagnetReward.Id);
    }


    private void OnValidateBuy(object sender, object arg)
    {
        Debug.Log("Invalidate Magnet buy");
        Argument<bool, string> validator = (Argument<bool, string>)arg;
        validator.Item0 = false;
    }

    private void OnDestroy()
    {
        this.RemoveObserver(OnGameEnd,Notifications.GameMode_End);
        this.RemoveObserver(OnValidateBuy, Notifications.ValidateBuy + MagnetReward.Id);
    }

    private void OnGameEnd(object arg1, object arg2)
    {
        UnequiAndDestroy(health);
    }
   
    private void UnequiAndDestroy(Health obj)
    {
        if(obj != null) obj.OnDead -= UnequiAndDestroy;
        Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        StarCoin starCoin = collision.gameObject.GetComponent<StarCoin>();
        if (starCoin != null && !stars.Contains(starCoin) && starCoin.Pickable.Active)
        {
            starCoin.StopFloating();
            Add(starCoin);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        StarCoin starCoin = collision.gameObject.GetComponent<StarCoin>();
        if (starCoin != null && stars.Contains(starCoin))
        {
            Remove(starCoin);
            starCoin.StartFloating();
        }
    }

    private void Add(StarCoin starCoin)
    {
        stars.Add(starCoin);
        vel.Add(Vector2.zero);

        starCoin.OnPickedUp -= PickedUpResponse;
        starCoin.OnPickedUp += PickedUpResponse;

        starCoin.OnHideStart -= OnHideStartResponse;
        starCoin.OnHideStart += OnHideStartResponse;
    }

    private void OnHideStartResponse(StarCoin starCoin)
    {
        starCoin.OnHideStart -= OnHideStartResponse;
        Remove(starCoin);
    }

    private void PickedUpResponse(StarCoin starCoin)
    {
        starCoin.OnPickedUp -= PickedUpResponse;
        Remove(starCoin);
    }

    private void Remove(StarCoin starCoin)
    {
        if (stars.Contains(starCoin)) return;

        vel.RemoveAt(stars.IndexOf(starCoin));
        stars.Remove(starCoin);
    }

    private void FixedUpdate()
    {
        for (int i = 0; i < stars.Count; i++)
        {
            Vector2 v = vel[i];
            stars[i].transform.position = Vector2.SmoothDamp(stars[i].transform.position, transform.position, ref v, smoothTime);
            vel[i] = v;
        }
    }

  
    private void OnDisable()
    {
        ClearAll();
    }

  
    private void ClearAll()
    {
        vel.Clear();
        stars.Clear();
    }
}
