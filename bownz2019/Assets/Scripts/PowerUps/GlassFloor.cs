﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;

public class GlassFloor : MonoBehaviour
{
    [SerializeField] private Breakable[] bBlocks;

    private List<Breakable> activo;
    private List<Breakable> inactivo;

    public void Initialize(int activeModules)
    {
        activo = new List<Breakable>();
        inactivo = new List<Breakable>(bBlocks);

        foreach (var item in inactivo)
        {
            item.DestroyOnDead = false;
            item.gameObject.SetActive(false);
        }

        activeModules = Mathf.Min(activeModules, bBlocks.Length);
        gameObject.SetActive(true);

        foreach (var item in bBlocks)
            item.Health.OnDead = OnBlockDead;

        for (int i = 0; i < activeModules; i++)
            AddBlock();
    }

    public void AddBlock()
    {
        Breakable block;
        if (inactivo.Count == 0)
        {
            block = activo.GetRandom();
            block.PlayParticles();
        }
        else
        {
            block = inactivo.GetRandom();
            inactivo.Remove(block);
            activo.Add(block);
            block.Initialize();
        }
    }

    public void DestroyFloor()
    {
        foreach (var item in bBlocks)
        {
            if(item != null) item.Health.Damage(item.Health.Current);
        }
    }

    private void OnBlockDead(Health health)
    {
        Breakable block = health.gameObject.GetComponent<Breakable>();
        inactivo.Add(block);
        activo.Remove(block);

        if (inactivo.Count == activo.Count)
        {
            transform.SetParent(null);
            Destroy(gameObject);
        }
    }

    private void OnDisable()
    {
        this.RemoveObserver(OnGameMode_End_Response, Notifications.GameMode_End);
    }

    private void OnEnable()
    {
        this.AddObserver(OnGameMode_End_Response, Notifications.GameMode_End);
    }

    private void OnGameMode_End_Response(object arg1, object arg2)
    {
        DestroyFloor();
    }
}
