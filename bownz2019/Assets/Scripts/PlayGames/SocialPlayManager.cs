﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using System;

public static class SocialPlayManager
{
    public static bool AskForLog = true;

    private static Action<bool> onSocialPlayLogIn;
    public static Action<bool> OnSocialPlayLogIn { get { return onSocialPlayLogIn; } set { onSocialPlayLogIn = value; } }

    private static bool achievementsLoaded = false;
    private static IAchievement[] achievements;

    public static void LoadAchievements()
    {
        Social.LoadAchievements(
            achievements =>
            {
                DoOnMainThread.ExecuteOnMainThread.Enqueue(() =>
                {
                    SocialPlayManager.achievements = achievements;
                    achievementsLoaded = true;
                    if (IsAchievementCompleted(Achievement_EarlyAdopter))
                    {
                        Settings.HasRemoveAds = true;
                    }
                });
            });
    }

    public static void Initialize()
    {
        Debug.Log("<color=brown> INITIALIZE </color> " + " SocialPlayManager ");
        PlayerPrefs.SetInt("ShouldLogInSocialPlay", 0);
        if (Application.platform == RuntimePlatform.Android && !Social.localUser.authenticated)
        {
            Debug.Log("PlayServicesManager.Initialize()");
            PlayServicesManager.Initialize();
        }
        else
            DoOnMainThread.ExecuteOnMainThread.Enqueue(() => SocialPlayManager.OnSocialPlayLogIn?.Invoke(false));
    }

    public static bool IsAchievementCompleted(string id)
    {
        if (!achievementsLoaded) return false;

        for (int i = 0; i < achievements.Length; i++)
        {
            if (achievements[i].id == id)
            {
                return achievements[i].percentCompleted == 100;
            }
        }
        return false;
    }

    public static string Achievement_EarlyAdopter
    {
        get
        {
            switch (Application.platform)
            {
                case RuntimePlatform.Android:
                    return ""/*GPGSIds.achievement_early_adopter*/;
                case RuntimePlatform.IPhonePlayer:
                    return "";
                default:
                    return "";
            }
        }
    }

    public static string Achievement_YourFirstDifference
    {
        get
        {
            switch (Application.platform)
            {
                case RuntimePlatform.Android:
                    return "";
                case RuntimePlatform.IPhonePlayer:
                    return "";
                default:
                    return "";
            }
        }
    }

}
