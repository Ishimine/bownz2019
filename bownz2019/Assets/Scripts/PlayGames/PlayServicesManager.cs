﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;
using System;

public static class PlayServicesManager
{
    private static Action<string> onAuthCode;
    public static Action<string> OnAuthCode     { get { return onAuthCode; } set { onAuthCode = value; } }

    private static bool requestAuthCode = true;
    public static bool RequestAuthCode          { get { return requestAuthCode; } set { requestAuthCode = value; } }

    public static void Initialize()
    {
        UI_Messenger.Show(new Toast_Msg("Logging...", MsgPosition.Top, 2f, null));

        PlayGamesClientConfiguration config;

        PlayGamesClientConfiguration.Builder builder = new PlayGamesClientConfiguration.Builder();

        if (Settings.UseGooglePlayCloudSaving)    builder.EnableSavedGames();
        if (requestAuthCode)            builder.RequestServerAuthCode(false);

        config = builder.Build();

        PlayGamesPlatform.InitializeInstance(config);
        // Activate the Google Play Games platform
        PlayGamesPlatform.DebugLogEnabled = true;
        PlayGamesPlatform.Activate();
        Social.Active.Authenticate(Social.localUser, OnAuthenticateHandler);
    }

    private static void OnAuthenticateHandler(bool sucess, string message)
    {
        Debug.Log("<color=green> OnAuthenticateHandler: </color>" + message);
        DoOnMainThread.ExecuteOnMainThread.Enqueue(() =>
        {
            if (sucess)
            {
                Debug.Log("PlayServices Authenticated Sucess");
                SocialPlayManager.LoadAchievements();
                PlayerPrefs.SetInt("ShouldLogInSocialPlay", 1);
                OnAuthCode?.Invoke(PlayGamesPlatform.Instance.GetServerAuthCode());
            }
            else
            {
                Debug.Log("PlayServices Authenticated Failed");
            }
            SocialPlayManager.OnSocialPlayLogIn?.Invoke(sucess);

        });
    }
}
