﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GoogleMobileAds.Api;
using GooglePlayGames;

public class UI_SocialLeaderboard : MonoBehaviour
{
    public Button button;

	public void Pressed()
    {
        if (Social.localUser.authenticated)
        {
            if(Application.platform == RuntimePlatform.Android)
            {
                ((PlayGamesPlatform)Social.Active).ShowLeaderboardUI();
            }
            //Social.ShowLeaderboardUI(SocialPlayManager.Leaderboard_DifferencesFound);
        }
        else
        {
            Debug.Log("SocialLeaderboard: Player not authenticated");
            PlayServicesManager.Initialize();
        }
    }

    public void ForcePressed()
    {
            ((PlayGamesPlatform)Social.Active).ShowLeaderboardUI();
    }

    private void SetInteractive(bool value)
    {
        button.interactable = value;
    }

    private CodeAnimator codeAnimator = new CodeAnimator();

    private IEnumerator rutine;

    private void OnEnable()
    {
        //   rutine = ConectionCheck();

    }

    private void OnDisable()
    {
        
    }

   /* IEnumerator ConectionCheck()
    {
        while(true)
        {
            yield return new WaitForSeconds(3);
            if()
        }
    }*/

}
