﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_SocialAchivementsButton : MonoBehaviour
{

    public void Pressed()
    {
        if (Social.localUser.authenticated)
        {
            Social.ShowAchievementsUI();
        }
        else
        {
            Debug.Log("SocialAchivement: Player not authenticated");
            PlayServicesManager.Initialize();
        }
    }

}
