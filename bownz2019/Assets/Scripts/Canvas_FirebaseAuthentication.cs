﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Auth;
using System;
using Sirenix.OdinInspector;
using System.Text.RegularExpressions;
using UnityEngine.UI;

public class Canvas_FirebaseAuthentication : MonoBehaviour
{
    private FirebaseAuth Auth => FirebaseStarter.Auth;

    private string cEMail;
    public string EMail         { get => cEMail;    set { cEMail = value;       } }

    private string cPassword;
    public string CPassword     { get => cPassword; set { cPassword = value;    } }

    public string displayName;
    public string emailAddress;
    private Uri photoUrl;

    private Action<bool> onLogStateChange;
    public Action<bool> OnLogStateChange { get { return onLogStateChange; } set { onLogStateChange = value; } }

    private Action<bool> onMailValidation;
    private Action onMailValidationFailed;
    private Action onMailValidationCanceled;
    private Action onMailWrongFormat;

    public Action<bool> onTryToCreateUser;

    public  InputField mail;

    private void Awake()
    {
        //InitializeFirebase();

        //PlayServicesManager.OnAuthCode += LogWithPlayServicesAuth;
    }

   
    bool IsValidEmail(string email)
    {
        Regex regex = new Regex(
            @"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*" + 
            "@"+ 
            @"((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))$");
        return regex.IsMatch(email);
    }

    [Button]
    public void ValidateMail(string nMail)
    {
        if (!IsValidEmail(nMail))
        {
            OnMailValidation(false);
            onMailWrongFormat?.Invoke();
            Debug.Log("Wrong format");
        }
        else
        {
            DoOnMainThread.ExecuteOnMainThread.Enqueue(
             () =>
             {
                 Debug.Log("ValidateMail Started");
                 Auth.FetchProvidersForEmailAsync(nMail).ContinueWith(task =>
                 {
                     Debug.Log("FetchProvidersForEmailAsync Done");
                     if (task.IsCompleted)
                     {
                         Debug.Log("Completed: " + task.Result);
                         List<string> providers = new List<string>(task.Result);
                         Debug.Log("Count:" + providers.Count);
                         if (providers.Count > 0)
                             OnMailValidation(false);
                         else
                             OnMailValidation(true);
                     }
                     else if (task.IsCanceled)
                     {
                         Debug.Log("Canceled");
                         onMailValidationCanceled?.Invoke();
                         OnMailValidation(false);
                     }
                     else if (task.IsFaulted)
                     {
                         Debug.Log("Faulted" + task.Exception);
                         onMailValidationFailed?.Invoke();
                         OnMailValidation(false);
                     }
                 });
             });
        }
    }

    private void OnMailValidation(bool isMailValid)
    {
        onMailValidation?.Invoke(isMailValid);

        if (isMailValid)
            Debug.Log("Mail is valid");
        else
            Debug.Log("Mail is Invalid");
    }

    public bool IsValidPassword(string password)
    {
        return password.Length >= 6;
    }



    [Button]
    public void TryCreateUser(string eMail, string password)
    {
        emailAddress = eMail;
        cPassword = password;

        onMailValidation += CreateUser_MailValidation;
        ValidateMail(eMail);
    }

    private void CreateUser_MailValidation(bool isMailValid)
    {
        onMailValidation -= CreateUser_MailValidation;
        if (isMailValid)
            CreateUser(emailAddress, cPassword);
        else
            onTryToCreateUser?.Invoke(false);
    }

    private void CreateUser(string eMail, string password)
    {
        DoOnMainThread.ExecuteOnMainThread.Enqueue(
            ()=>
        {
            Auth.CreateUserWithEmailAndPasswordAsync(eMail, password).ContinueWith(task => {

                if(task.IsCompleted)
                {
                    onTryToCreateUser?.Invoke(true);
                }
                if (task.IsCanceled)
                {
                    Debug.LogError("CreateUserWithEmailAndPasswordAsync was canceled.");
                    return;
                }
                if (task.IsFaulted)
                {
                    Debug.LogError("CreateUserWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                    return;
                }

                // Firebase user has been created.
                Firebase.Auth.FirebaseUser newUser = task.Result;
                Debug.LogFormat("Firebase user created successfully: {0} ({1})",
                    newUser.DisplayName, newUser.UserId);
            }); }
        );
    }

    [Button]
    public void SignIn(string eMail, string password)
    {
        DoOnMainThread.ExecuteOnMainThread.Enqueue(
          () =>
          {
              Auth.SignInWithEmailAndPasswordAsync(eMail, password).ContinueWith(task =>
              {
                  if (task.IsCanceled)
                  {
                      Debug.LogError("SignInWithEmailAndPasswordAsync was canceled.");
                      return;
                  }
                  if (task.IsFaulted)
                  {
                      Debug.LogError("SignInWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                      return;
                  }
                  
                  Firebase.Auth.FirebaseUser newUser = task.Result;
                  Debug.LogFormat("User signed in successfully: {0} ({1})",
                      newUser.DisplayName, newUser.UserId);
                  
                  //FirebaseStarter.OnSignStateChange?.Invoke(true);
              });
          });
    }

    [Button]
    public void SignOut()
    {
        DoOnMainThread.ExecuteOnMainThread.Enqueue(
          () =>
          {
              Auth.SignOut();
          });
    }

    public void SignIn()
    {
        SignIn(cEMail, cPassword);
    }

    public void LogWithPlayServicesAuth(string authCode)
    {
        Debug.Log("authCode: " + authCode);
        Firebase.Auth.Credential credential = Firebase.Auth.PlayGamesAuthProvider.GetCredential(authCode);
        LogWithExternalCredential(credential);
    }

    public void LogWithExternalCredential(Firebase.Auth.Credential credential)
    {
        Debug.Log("credential: " + credential);
        Auth.SignInWithCredentialAsync(credential).ContinueWith(task => {
            if (task.IsCanceled)
            {
                Debug.LogError("SignInWithCredentialAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                Debug.LogError("SignInWithCredentialAsync encountered an error: " + task.Exception);
                return;
            }

            Firebase.Auth.FirebaseUser newUser = task.Result;
            Debug.LogFormat("User signed in successfully: {0} ({1})", newUser.DisplayName, newUser.UserId);
        });
    }
}
