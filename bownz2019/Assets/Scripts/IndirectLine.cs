﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

                           [ExecuteInEditMode]

public class IndirectLine : MonoBehaviour
{
    public int angleSteps = 45;
    public int startAngle = 0;

    public float stopDistance = 1;

    public float snapStep = .25f;
    //private float lenghtSteps;
    public List<Vector2> waypoints = new List<Vector2>();

    public float lenghtStepsSubdivition = 5;
    public float maxSimultaneousSteps = 3;

    public LineRenderer lineRenderer;
    IEnumerator rutine;

    public Transform startPos;
    public Transform endPos;

    public bool startRandomAngle = false;

    [Button]
    public void SetStartAndEnd(Transform startPos, Transform endPos)
    {
        this.startPos = startPos;
        this.endPos = endPos;
    }

    [Button]
    public void CalculateWaypoints()
    {
        CalculateWaypoints(startPos.position, endPos.position);
    }
   
    public void CalculateWaypoints(Vector2 startPos, Vector2 endPos)
    {
        waypoints.Clear();
        Vector2 currentPosition = startPos;
        waypoints.Add(transform.InverseTransformPoint(currentPosition));

        Vector2 startDir = startPos.GetDirection(endPos);

        int lastAngle = ((startRandomAngle)?UnityEngine.Random.Range(0,360/angleSteps):0) + startAngle;
        while (currentPosition != endPos)
        {
            currentPosition = GetNextPosition(currentPosition, endPos, ref lastAngle, startDir);
            waypoints.Add(transform.InverseTransformPoint(currentPosition));
        }
        RemoveRepeated();
        ApplyToLine();
    }

    private void RemoveRepeated()
    {
        for (int i = waypoints.Count-2; i >= 0; i--)
        {
            if(waypoints[i] == waypoints[i+1])
                waypoints.RemoveAt(i + 1);
        }
    }

    [Button]
    public void ApplyToLine()
    {
        lineRenderer.positionCount = waypoints.Count;
        List<Vector3> vectors = new List<Vector3>();

        for (int i = 0; i < waypoints.Count; i++)
        {
            if(i != 0 && i != waypoints.Count-1) 
                waypoints[i] = waypoints[i].SnapTo(snapStep);

            vectors.Add(waypoints[i]);
        }
        lineRenderer.SetPositions(vectors.ToArray());
    }

    private float GetLenghtSteps(Vector2 startPos, Vector2 endPos, bool useMaxDistance)
    {
        if(useMaxDistance)
            return Mathf.Max(Mathf.Abs(startPos.x - endPos.x), Mathf.Abs(startPos.y - endPos.y)) / lenghtStepsSubdivition;
        else
            return Mathf.Min(Mathf.Abs(startPos.x - endPos.x), Mathf.Abs(startPos.y - endPos.y)) / lenghtStepsSubdivition;
    }

    private Vector2 GetNextPosition(Vector2 currentPosition, Vector2 endPos,ref int lastAngle, Vector2 startDir)
    {
        int nAngle = lastAngle/* + angleSteps * ((int)UnityEngine.Random.Range(0, (360 / angleSteps)) + 1)*/;
        Vector2 dir = (Vector2)(Quaternion.Euler(0, 0, nAngle) * Vector2.up);
        while (lastAngle == nAngle || lastAngle == (Mathf.Repeat(nAngle + 180,360)) || !GoesCloser(dir, currentPosition, endPos)) 
        {
            nAngle += angleSteps;
            dir = (Vector2)(Quaternion.Euler(0, 0, nAngle) * Vector2.up);
        }  
        float length =  UnityEngine.Random.Range(1, ((Vector2.Distance(currentPosition, endPos)/2) / stopDistance)+1);
        lastAngle = nAngle;
        currentPosition += dir * length;

        if ((startDir.x > 0 && currentPosition.x > endPos.x) || (startDir.x < 0 && currentPosition.x < endPos.x)) currentPosition.x = endPos.x;
        if ((startDir.y > 0 && currentPosition.y > endPos.y) || (startDir.y < 0 && currentPosition.y < endPos.y))  currentPosition.y = endPos.y;

        if (Math.Abs(Vector2.Distance(currentPosition, endPos)) <= (stopDistance)) currentPosition = endPos;
        return currentPosition;
    }

    private bool GoesCloser(Vector2 angle, Vector2 currentPosition, Vector2 endPos)
    {
        return Math.Abs(Vector2.Distance(currentPosition, endPos)) >= Math.Abs(Vector2.Distance(currentPosition + angle * stopDistance/2, endPos));
    }

}

