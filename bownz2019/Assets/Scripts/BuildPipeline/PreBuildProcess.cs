﻿#if UNITY_EDITOR

using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEngine;

namespace BuildPipeline
{
    public class PreBuildProcess : IPostprocessBuildWithReport, IPreprocessBuildWithReport
    {
        public int callbackOrder { get { return 0; } }

        public void OnPreprocessBuild(BuildReport report)
        {
       /*     Debug.Log("START => OnPreprocessBuild for target " + report.summary.platform + " at path " + report.summary.outputPath);
            Resources.Load<ProceduralModuleDatabase>("_ProceduralModuleDB").InitializeDatabase();
//            Resources.Load<LevelsObjectiveDB>("/Libraries/LevelsObjectiveDB").ProcessObjectives();
            Debug.Log("END => OnPreprocessBuild for target " + report.summary.platform + " at path " + report.summary.outputPath);*/
        }

        public void OnPostprocessBuild(BuildReport report)
        {
            Debug.Log("MyCustomBuildProcessor.OnPostprocessBuild for target " + report.summary.platform + " at path " + report.summary.outputPath);
        }
    }
}
 #endif