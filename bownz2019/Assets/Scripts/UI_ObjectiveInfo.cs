﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

public class UI_ObjectiveInfo : MonoBehaviour
{
    public UI_Medal medal;

    public Text playerScoreText;  
    public Text targetScoreText;  
    public Text sucessScoreText;
    public Text separtor;

    public Image bg;

    public Color cCompletedBg;
    public Color cIncompletedBg;

    [Button]
    public void Show(float playerValue, float targetValue, string format)
    {
        Debug.Log("playerValue: " + playerValue);
        Debug.Log("targetValue: " + targetValue);
        if (playerValue <= targetValue)
        {
            Show(playerValue.ToString(format));
        }
        else
        {
            Show(playerValue > 10000 ? "-" : playerValue.ToString(format), targetValue.ToString(format));
        }
    }
    [Button]

    private void Show(string playerValue, string targetValue)   

    {
            playerScoreText.text = playerValue;
            targetScoreText.text = targetValue;
            SetCompleted(false);
    }
    [Button]

    private void Show(string sucessValue)
    {
        sucessScoreText.text = sucessValue;
        SetCompleted(true);
    }

    [Button]
    private void SetCompleted(bool value)
    {
        sucessScoreText.gameObject.SetActive(value);
        playerScoreText.gameObject.SetActive(!value);
        targetScoreText.gameObject.SetActive(!value);
        separtor.gameObject.SetActive(!value);
        bg.color = (value) ? cCompletedBg : cIncompletedBg;

        if (value)
            medal.SetComplete();
        else
            medal.SetIncomplete();
    }
}
