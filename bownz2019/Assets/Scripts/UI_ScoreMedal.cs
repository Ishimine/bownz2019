﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

public class UI_ScoreMedal : MonoBehaviour
{
    private CodeAnimator codeAnimator = new CodeAnimator();

    public CanvasGroup canvasGroup;

    public Image[] renderers;

    public Color cComplete = Color.yellow;
    public Color cIncomplete = Color.gray;

    public GameObject[] enableByCompletionObjects;

    public AnimationCurve fadeInCurve;
    public AnimationCurve scaleInCurve;
    public float duration = 1;

    public void Initialize()
    {
        canvasGroup.alpha = 0;
    }

    public void SetUnlocked(bool value)
    {
        if (value) SetCompleted();
        else SetIncompleted();
    }

    [Button]
    private void SetIncompleted()
    {
        SetColor(cIncomplete);
    }

    [Button]
    private void SetCompleted()
    {
        SetColor(cComplete);
    }

    private void SetColor(Color c)
    {
        foreach (Image render in renderers)
            render.color = c;
    }

    [Button]
    public void Animation_AlreadyCompleted()
    {
        EnableObjectsByCompletion(true);
        SetCompleted();
        codeAnimator.StartAnimacion(this,
            x =>
            {
                canvasGroup.alpha = fadeInCurve.Evaluate(x);
            }, DeltaTimeType.deltaTime, AnimationType.Simple, null, duration);
    }

    [Button]
    public void Animation_JustCompleted()
    {
        EnableObjectsByCompletion(true);
        SetCompleted();
        codeAnimator.StartAnimacion(this,
            x =>
            {
                canvasGroup.alpha = x;
                transform.localScale = Vector3.one * scaleInCurve.Evaluate(x);
            }, DeltaTimeType.deltaTime, AnimationType.Simple, null, duration);
    }

    private void EnableObjectsByCompletion(bool value)
    {
        foreach (GameObject go in enableByCompletionObjects)
            go.SetActive(value);
    }

    [Button]    
    public void Animation_NotCompleted()
    {
        EnableObjectsByCompletion(false);
        SetIncompleted();
        codeAnimator.StartAnimacion(this,
            x =>
            {
                canvasGroup.alpha = x;
            }, DeltaTimeType.deltaTime, AnimationType.Simple, null, duration);
    }
}
