﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoSingleton<MusicManager>
{
    public override MusicManager Myself => this;

    public SoundPlay mainTrack;

    private void Start()
    {
        mainTrack.Play();
    }

    IEnumerator WaitAndPlay()
    {
        yield return new WaitForSeconds(5);

    }

    public static void PlayMainTrack()
    {
        Instance.mainTrack.Play();
    }

    public static void StopMainTrack()
    {
       Instance.mainTrack.Stop();
    }
}
