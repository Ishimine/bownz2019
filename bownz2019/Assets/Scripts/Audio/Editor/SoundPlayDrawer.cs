﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.AnimatedValues;

[CustomPropertyDrawer(typeof(SoundPlay))]
[CanEditMultipleObjects]
public class SoundPlayDrawer : PropertyDrawer
{
    string[] options;
    string key;
    int selected;
    int oldSelected;

    private AnimBool showParameters = new AnimBool(true);

   /* public override void OnEnable()
    {
        base.OnEnable();
        showParameters = new AnimBool(true);
        showParameters.valueChanged.AddListener(Repaint);
    }*/

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {  
        // Using BeginProperty / EndProperty on the parent property means that
        // prefab override logic works on the entire property.
        EditorGUI.BeginProperty(position, label, property);

        key = property.FindPropertyRelative("key").stringValue;
        GUILayout.BeginHorizontal();
        GUILayout.Label(property.name, GUILayout.MinWidth(50), GUILayout.MaxWidth(120));
        key = GUILayout.TextField(key, GUILayout.Width(100));
        if (GUILayout.Button("-", GUILayout.Width(15), GUILayout.Height(15)))
        {
            key = "";
        }

        options = SoundLibrary.GetKeysFrom(key).ToArray();
        selected = EditorGUILayout.Popup(oldSelected, options, GUILayout.MaxWidth(110), GUILayout.MinWidth(60));
        if (selected != oldSelected)
        {
            oldSelected = selected;
            key = options[selected];
        }
      
        property.FindPropertyRelative("key").stringValue = key;


        GUILayout.EndHorizontal();
        EditorGUI.EndProperty();
    } 
}
