﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class MasterAudioSource : MonoSingleton<MasterAudioSource>
{
    public override MasterAudioSource Myself =>this;
    private List<AudioSource> audioSources;

    public void Initialize(int initialSources)
    {
        for (int i = 0; i < initialSources; i++)
        {
            CreateAudioSource();
        }
    }

    AudioSource source;

    private void Start()
    {
        Initialize(12);
    }

    public void Play(string key)
    {
        Sound s = GetSound(key);
        if (s != null) Play(s);
        else Debug.LogWarning("ERROR 10: Audio no encontrado keyCode =" + key); 
    }

    public Sound GetSound(string key)
    {
        return SoundLibrary.Get(key);
    }

    public void Play(Sound s)
    {
        source = GetAudioSource();
        source.loop = s.loop;
        source.clip = s.clip;
        source.volume = s.volume;
        source.outputAudioMixerGroup = s.mixerGroup;
        source.Play();
    }

    public void Stop(string key)
    {
        Sound s = GetSound(key);
        source = FindPlayingAudioSource(s);
        if (source != null)
        {
            source.Stop();
            Debug.Log("Source: " + source);
        }
        else
        {
            Debug.Log("Source is NULL");
        }
    }

    private AudioSource FindPlayingAudioSource(Sound s)
    {
        AudioSource audioSource = audioSources.Find(X => 
        ((X.isPlaying) && X.clip == s.clip));
        return audioSource;
    }

    private AudioSource GetAudioSource()
    {
        for (int i = 0; i < audioSources.Count; i++)
        {
            if (!audioSources[i].isPlaying) return audioSources[i];
        }
        return CreateAudioSource();
    }

    private AudioSource CreateAudioSource()
    {
        if (audioSources == null) audioSources = new List<AudioSource>();
        AudioSource source = new GameObject("AudioSource N" + (audioSources.Count + 1)).AddComponent<AudioSource>();
        source.gameObject.transform.SetParent(this.transform);
        audioSources.Add(source);
        return source;
    }

}
