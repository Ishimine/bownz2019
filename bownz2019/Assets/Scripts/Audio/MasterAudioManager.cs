﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class MasterAudioManager
{
    private static MasterAudioSource instance;
    private static MasterAudioSource Instance
    {
        get
        {
            if (instance == null)                Initialize();
            return instance;
        }
    }

    private static void Initialize()
    {
        instance = new GameObject("MasterAudioSource").AddComponent<MasterAudioSource>();
        instance.Initialize(12);
    }

    public static void Play(Sound s)
    {
        Instance.Play(s); 
    }

    public static void Play(string key)
    {
        Instance.Play(key);
    }

    public static void Stop(string key)
    {
        Instance.Stop(key);
    }
}
