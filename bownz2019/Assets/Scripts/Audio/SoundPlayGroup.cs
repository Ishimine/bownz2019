﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SoundPlayGroup
{
    public SoundPlay[] sounds;


    public void PlayRandom()
    {
        if (sounds.Length > 0)
        {
            sounds[Random.Range(0,sounds.Length)].Play();
        }
        else
            Debug.LogWarning("CUIDADO, tratando de reproducir un grupo de audios random. Pero el grupo esta vacio");
    }

    public void PlayAll()
    {
        foreach (var item in sounds)
            item.Play();
    }

    public void StopAll()
    {
        foreach (var item in sounds)
            item.Stop();
    }

}
