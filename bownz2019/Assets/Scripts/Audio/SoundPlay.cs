﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SoundPlay 
{
    public string key;

    public SoundPlay(string key)
    {
        this.key = key;
    }

    public void Play()
    {
        if(!string.IsNullOrEmpty(key))
            MasterAudioManager.Play(key);
    }

    public void Stop()
    {
        if (!string.IsNullOrEmpty(key))
            MasterAudioManager.Stop(key);
    }
}
