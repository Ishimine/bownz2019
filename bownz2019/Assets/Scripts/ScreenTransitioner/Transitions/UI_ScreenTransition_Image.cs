﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_ScreenTransition_Image : UI_ScreenTransition
{
    [SerializeField] private Color inColor = Color.clear; 
    [SerializeField] private Color outColor = Color.black;
    [SerializeField] private Image img = null;

    [SerializeField] CodeAnimatorCurve curveInimg = null;
    [SerializeField] CodeAnimatorCurve curveOutimg = null;
                                               
    private Color startColor = Color.clear;

    

    protected override void InStep(float progress)
    {
        img.color = Color.Lerp(startColor, inColor, progress);
    }
    
    protected override void OutStep(float progress)
    {
        img.color = Color.Lerp(startColor, outColor, progress);
    }
  
    protected override void InStart_SB()
    {
        startColor = img.color;
        img.raycastTarget = true;
    }

    protected override void OutStart_SB()
    {
        img.raycastTarget = true;
        startColor = img.color;
    }

    protected override void InEnd_SB()
    {
        img.raycastTarget = false;
    }

    protected override void OutEnd_SB()
    {
        img.raycastTarget = false;
    }
}
