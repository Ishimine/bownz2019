﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Sirenix.OdinInspector;

public abstract class UI_ScreenTransition : MonoBehaviour
{
    IEnumerator rutine;

    public float durationIn = .5f;
    public float durationOut = .5f;
    private bool swipeSpawnerState;

    #region Public

    [Button]
    public void In(Action postAction = null)
    {
        if (rutine != null) StopCoroutine(rutine);
        rutine = StepRoutine(durationIn, InStep, InStart, InEnd, postAction);
        StartCoroutine(rutine);
    }

    [Button]
    public void Out(Action postAction = null)
    {
        if (rutine != null) StopCoroutine(rutine);
        rutine = StepRoutine(durationIn, OutStep, OutStart, OutEnd, postAction);
        StartCoroutine(rutine);
    }

    IEnumerator StepRoutine(float duration, Action<float> stepMethod, Action onStartMethod, Action onEndMethod, Action postAction = null)
    {
        onStartMethod?.Invoke();
        yield return null; 
        float time = 0;
        do
        {
            time += Time.deltaTime;
            stepMethod?.Invoke(time/duration);
            yield return null; 
        } while (time < duration);
        onEndMethod?.Invoke();
        postAction?.Invoke();
    }

    #endregion

    #region Private
    /// <summary>
    /// Se ejecuta al INICIAR la animacion de ENTRADA
    /// </summary>
    private void InStart()
    {
        UI_ScreenTransitioner.OnInStart?.Invoke();
        InStart_SB();
    }
    /// <summary>
    /// Se ejecuta al TERMINAR la animacion de ENTRADA
    /// </summary>
    private void InEnd()
    {
        UI_ScreenTransitioner.OnInEnd?.Invoke();
        InEnd_SB();
    }


    /// <summary>
    /// Se ejecuta al INICIAR la animacion de SALIDA
    /// </summary>
    private void OutStart()
    {
        swipeSpawnerState = SwipeSpawner.IsActive;

        Settings.UiInteraction = false;

        this.PostNotification(Notifications.SwipeSpawnerSetEnable, false);
        UI_ScreenTransitioner.OnOutStart?.Invoke();
        OutStart_SB();
    }

    /// <summary>
    /// Se ejecuta al TERMINAR la animacion de SALIDA
    /// </summary>
    private void OutEnd()
    {
        //this.PostNotification(Notifications.SwipeSpawnerSetEnable, swipeSpawnerState);
        UI_ScreenTransitioner.OnOutEnd?.Invoke();
        OutEnd_SB();
    }
    #endregion

    #region Sandbox/Template
    /// <summary>
    /// Se ejecuta al INICIAR la transicionde ENTRADA a escena
    /// </summary>
    /// <param name="progress"></param>
    protected abstract void InStart_SB();
    /// <summary>
    /// Se ejecuta al TERMINAR la transicionde ENTRADA a escena
    /// </summary>
    /// <param name="progress"></param>
    protected abstract void InEnd_SB();
    /// <summary>
    /// Step de progreso de animacion ENTRANDO a ESCENA
    /// </summary>
    /// <param name="progress"></param>
    protected abstract void InStep(float progress);
    /// <summary>
    /// Se ejecuta al INICIA la transicionde SALIDA a escena
    /// </summary>
    /// <param name="progress"></param>
    protected abstract void OutStart_SB();
    /// <summary>
    /// Se ejecuta al TERMINAR la transicionde SALIDA a escena
    /// </summary>
    /// <param name="progress"></param>
    protected abstract void OutEnd_SB();
    /// <summary>
    /// Step de progreso de animacion SALIENDO de ESCENA
    /// </summary>
    /// <param name="progress"></param>
    protected abstract void OutStep(float progress);
    #endregion


}
