﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Sirenix.OdinInspector;

public class UI_ScreenTransitioner : MonoSingleton<UI_ScreenTransitioner>
{
    public override UI_ScreenTransitioner Myself => this;

    [SerializeField] private GameObject defaultTransition = null;

    private UI_ScreenTransition currentTransition;
    public UI_ScreenTransition CurrentTransition
    {
        get
        {
            if (currentTransition == null) SetTransition(defaultTransition);
            return currentTransition;
        }
    }

    public static Action OnOutStart { get; set; }

    public static Action OnOutEnd { get; set; }

    public static Action OnInStart { get; set; }

    public static Action OnInEnd { get; set; }


    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        OnOutEnd += ()=> this.PostNotification(Notifications.ShowLoadBar, true);
        OnInStart += ()=> this.PostNotification(Notifications.ShowLoadBar, false);
    }


    private void OnDestroy()
    {
        OnOutEnd -= ()=> this.PostNotification(Notifications.ShowLoadBar, true);
        OnInStart -= ()=> this.PostNotification(Notifications.ShowLoadBar, false);
    }

    public void SetTransition(GameObject screenTransitionPrefab)
    {
        if (currentTransition != null)
        {
            Destroy(currentTransition.gameObject);
        }
        currentTransition = Instantiate(screenTransitionPrefab, null).GetComponent<UI_ScreenTransition>();
        currentTransition.transform.localScale = Vector3.one;
        currentTransition.gameObject.name = "_CurrentTransition";
    }

    [Button]
    public void TransitionOut(Action postAction = null)
    {
        CurrentTransition.Out(postAction);
    }

    [Button]
    public void TransitionIn(Action postAction = null)
    {
        CurrentTransition.In(postAction);
    }

    public static void Out(Action postAction = null)
    {
        Debug.Log("OUT");
        Instance.TransitionOut(postAction);
    }

    public static void In(Action postAction = null)
    {
        Instance.TransitionIn(postAction);
    }

    public static void OutIn(Action middleAction = null, Action postAction = null)
    {
        Out(
            () =>
            {
                middleAction?.Invoke();
                In(postAction);
            });
    }
}
