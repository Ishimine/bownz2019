﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class BallsCollectionShelf : MonoBehaviour
{
    public GameObject ballprefab;

    public int ballsPerShelft = 5;
    public Vector2 starOffset;
    public float shelftHeight = 1;
    public float ballSize;

    private void Awake()
    {
    }

    private void OnEnable()
    {
        Initialize(Body_Conteiner.GetSkins(Inventory.GetAllOwnedSkinsIndexes()));
    }

    [Button]
    void Initialize(BallSkin[] skins)
    {
        int shelfs = Mathf.CeilToInt((float)skins.Length / ballsPerShelft);
        Vector2 startCenterPosition = PlayArea.Area.center + Vector2.down * (PlayArea.Area.height / (float)skins.Length) + Vector2.up * shelftHeight*2;
        float xDistance = PlayArea.Area.width / ballsPerShelft;
        startCenterPosition -= Vector2.right * (xDistance) * (float)ballsPerShelft / 2 + Vector2.left * xDistance/2;

        GameObject cBall;
        BallStateMachine ballStateMachine;

        int i = 0;
        for (int x = 0; x < ballsPerShelft; x++)
        {
            for (int y = 0; y < shelfs; y++)
            {
                if (i >= skins.Length) break;

                cBall = Instantiate(ballprefab, transform);
                cBall.transform.position = startCenterPosition + new Vector2(x * xDistance, y * shelftHeight);

                ballStateMachine = cBall.GetComponent<BallStateMachine>();
                ballStateMachine.ballSize = ballSize;
                ballStateMachine.AppearToPosing();
                i++;

                ballStateMachine.SkinComponent.UsePlayerSelection = false;
                ballStateMachine.SkinComponent.SetSkin(skins[i]);
            }
        }
    }
}
