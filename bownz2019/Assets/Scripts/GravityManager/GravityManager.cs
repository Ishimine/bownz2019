﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;

[CreateAssetMenu(menuName = "Manager/Gravity")]
public class GravityManager: ScriptableSingleton<GravityManager>
{
    public override GravityManager Myself => this;

    private  Vector2 defaultGravity;
    public  Vector2 DefaultGravit
    {
        get => Physics2D.gravity;
        set => Physics2D.gravity = value; 
    }

    public void RestoreGravity()
    {
        ChangeGravity(defaultGravity);
    }

    public void ChangeGravity(Vector2 targetValue)
    {
        Physics2D.gravity = targetValue;
        this.PostNotification(Notifications.GravityChange);
    }

    public override void InitializeSingleton()
    {
        base.InitializeSingleton();
        defaultGravity = Physics2D.gravity;
        this.RemoveObserver(OnGameMode_EndResponse, Notifications.GameMode_End);
        this.AddObserver(OnGameMode_EndResponse, Notifications.GameMode_End);
    }


    public void OnGameMode_EndResponse(object o, object o1)
    {
        ChangeGravity(defaultGravity);
    }
}
