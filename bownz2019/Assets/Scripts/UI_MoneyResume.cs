﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
                using Sirenix.OdinInspector;
public class UI_MoneyResume : MonoBehaviour
{
    public Text resultText;
    public Text fadeText;
    public Text currentElementText;

    public enum Calculation { Multiplication, Divition,Addition,Subtraction }

    private CodeAnimator codeAnimator;

    private Queue<MoneyResumeElement> queue = new Queue<MoneyResumeElement>();

    public float timeIn = .3f;
    public float timeStay = .45f;

    public Action OnElementStart { get; set; }
    public Action OnElementEnd { get; set; }
    public Action OnAnimationStart { get; set; }
    public Action OnAnimationEnd { get; set; }

    public AnimationCurve resultTransitionCurve;

    public AnimationCurve positionCurve;
    private float heightDisplacement;
    private float currentValue;
    public float CurrentValue => currentValue;

    private string format = "0.";
    private bool inAnimation = false;

    [SerializeField] private RectTransform center;
    private Vector3 StartPosition => center.position;

    private IEnumerator rutine;

    [Button]
    private void Awake()
    {
        heightDisplacement = currentElementText.rectTransform.rect.height;

        fadeText.text = string.Empty;
        resultText.text = string.Empty;
        currentElementText.text = string.Empty;
    }

                           
    public void AddElements(MoneyResumeElement[] element)
    {
        foreach (MoneyResumeElement item in element)
        {
            AddElement(item);
        }
    }

    [Button]
    public void AddElement(MoneyResumeElement element)
    {
        queue.Enqueue(element);
        Next();
    }

    private void OnDisable()
    {
         if(rutine != null) StopCoroutine(rutine);
    }

    private void Next()
    {
        if(inAnimation) return;

        Debug.Log("NEXT");
        if (queue.Count > 0)
        {
            MoneyResumeElement element = queue.Dequeue();
            float previousValue = currentValue;
            currentValue = GetCalculation(element.TipeOfCalculation).Invoke(currentValue, element.Value);
            ElementStart(element.Description, previousValue, currentValue);
        }
        else
        {
            if(rutine != null) StopCoroutine(rutine);
            rutine = AnimationTransition("");
            StartCoroutine(rutine);
            OnAnimationEnd?.Invoke();
        }
    }


    private Func<float, float, float> GetCalculation(Calculation calculation)
    {
        switch (calculation)
        {
            case Calculation.Multiplication:
                return Multiplication;
            case Calculation.Divition:                
                return Divition;
            case Calculation.Addition:                
                return Addition;
            case Calculation.Subtraction:                
                return Subtraction;

            default:
                throw new ArgumentOutOfRangeException(nameof(calculation), calculation, null);
        }
    }

    private float Multiplication(float previousValue, float inValue)
    {
        return previousValue * inValue;
    }
                                             
    private float Divition(float previousValue, float inValue)
    {
        return previousValue / inValue;
    }

    private float Addition(float previousValue, float inValue)
    {
        return previousValue + inValue;
    }

    private float Subtraction(float previousValue, float inValue)
    {
        return previousValue - inValue;
    }

    public void ElementStart(string description, float previous, float nValue)
    {
        inAnimation = true;
        OnElementStart?.Invoke();

        if(rutine != null) StopCoroutine(rutine);

        rutine = AnimationFull(description, previous, nValue);
        StartCoroutine(rutine);
    }

    IEnumerator AnimationFull(string nDescription, float previouse, float nValue)
    {
        Debug.Log("AnimationFull");
        OnAnimationStart?.Invoke();
        yield return AnimationTransition(nDescription);
        yield return AnimationResult(previouse, nValue);
        ElementDone();
    }        


    IEnumerator AnimationTransition(string nValue)
    {

   Debug.Log("AnimationTransition");
        Vector3 inPosition = StartPosition + Vector3.up * heightDisplacement;
        Vector3 outPosition = StartPosition + Vector3.down * heightDisplacement;

        fadeText.text = currentElementText.text;
        currentElementText.text = nValue;

        fadeText.rectTransform.position = StartPosition;
        currentElementText.rectTransform.position = inPosition;

        float x = 0;
        do
        {
            x += Time.deltaTime / timeIn;

            fadeText.color = Color.Lerp(Color.white, Color.clear, x);
            currentElementText.color = Color.Lerp(Color.clear,Color.white, x);
            fadeText.rectTransform.position = Vector3.Lerp(StartPosition, outPosition,positionCurve.Evaluate(x));
            currentElementText.transform.position = Vector3.Lerp(inPosition, StartPosition,positionCurve.Evaluate(x));
            yield return null;
        } while (x <= 1);
    }
                                         
    IEnumerator AnimationResult(float previouseValue, float targetValue)
    {
        Debug.Log("AnimationResult");
        float x = 0;
        do
        {
            x += Time.deltaTime / timeStay;
            resultText.text = Mathf.Lerp(previouseValue, currentValue, resultTransitionCurve.Evaluate(x)).ToString(format);
            yield return null;
        } while (x <= 1);
        
    }

    public void ElementDone()
    {
        inAnimation = false;
        OnElementEnd?.Invoke();
        Next();
    }

    public struct MoneyResumeElement
    {
        public string Description;
        public float Value;
        public Calculation TipeOfCalculation;

        public MoneyResumeElement(string description, float value, Calculation tipeOfCalculation)
        {
            Description = description;
            Value = value;
            TipeOfCalculation = tipeOfCalculation;
        }
    }

    public void Clear()
    {
        resultText.text = string.Empty;
        fadeText.text = string.Empty;
        currentElementText.text = string.Empty;
        currentValue = 0;
        queue.Clear();
    }
}





