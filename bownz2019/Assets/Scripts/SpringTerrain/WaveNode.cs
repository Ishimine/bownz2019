﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class WaveNode : MonoBehaviour
{
    [SerializeField] private Vector2 targetPos;
    public Vector2 TargetPos { get { return targetPos; } set { targetPos = value; } }

    [SerializeField] private Vector2 velocity;
    [SerializeField] private Vector2 currentPos;

    public float elastisity = 1;
    public float speed = 1;
    public float k = .025f;
    public float minTension = .05f;

    public float tension = .1f;
    public float dampening = .1f;


    [Button]
    public void Impact(Vector2 force)
    {
        velocity += force;
    }

    public void Impact(Vector2 dir, float force)
    {
        Impact(dir * force);
    }

    private void OnEnable()
    {
        currentPos = targetPos;
    }

    public void FixedUpdate()
    {
        Vector2 dir = targetPos - currentPos;
        float sqrMagnitude = dir.sqrMagnitude;
        dir.Normalize();
        currentPos += velocity;
        velocity += (k * dir * tension * (sqrMagnitude + minTension) * speed - velocity * dampening) * elastisity;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawSphere(currentPos, .3f);
    }
}
