﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteByStyle : MonoBehaviour
{
    public SpriteRenderer render;
    public string id;

    public void Refresh()
    {
        render.sprite =  StyleManager.GetSprite(id);

    }

    private void OnEnable()
    {
        Refresh();
    }
}
