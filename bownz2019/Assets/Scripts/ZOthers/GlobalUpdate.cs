﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GlobalUpdate : MonoBehaviour
{
    private static Action globalUpdateEvent;
    public static Action GlobalUpdateEvent
    {
        get { return globalUpdateEvent; }
        set { globalUpdateEvent = value; }
    }

    private static Action globalFixedUpdateEvent;
    public static Action GlobalFixedUpdateEvent
    {
        get { return globalFixedUpdateEvent; }
        set { globalFixedUpdateEvent = value; }
    }

    private static Action globalLateUpdateEvent;
    public static Action GlobalLateUpdateEvent
    {
        get { return globalLateUpdateEvent; }
        set { globalLateUpdateEvent = value; }
    }

    private static GlobalUpdate instance = null;

    public void Awake()
    {
        Application.targetFrameRate = 60;

        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);

        globalUpdateEvent = null;
        globalFixedUpdateEvent = null;
        globalLateUpdateEvent = null;
    }

    private void Update()
    {
        GlobalUpdateEvent?.Invoke();
    }

    private void LateUpdate()
    {
        GlobalLateUpdateEvent?.Invoke();
    }

    private void FixedUpdate()
    {
        GlobalFixedUpdateEvent?.Invoke();
    }
}
