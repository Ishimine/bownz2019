﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class UnityExtentions
{
    /// <summary>
    /// Extension method to check if a layer is in a layermask
    /// </summary>
    /// <param name="mask"></param>
    /// <param name="layer"></param>
    /// <returns></returns>
    public static bool Contains(this LayerMask mask, int layer)
    {
        return mask == (mask | (1 << layer));
    }

    public static Vector2 GetDirection(this Vector2 startPos, Vector2 endPos)
    {
        return GetDifference(startPos, endPos).normalized;
    }

    public static Vector3 GetDirection(this Vector3 startPos, Vector3 endPos)
    {
        return GetDifference(startPos, endPos).normalized;
    }

    public static float GetMagnitud(this Vector2 startPos, Vector2 endPos)
    {
        return GetDifference(startPos, endPos).magnitude;
    }

    public static float GetMagnitud(this Vector3 startPos, Vector3 endPos)
    {
        return GetDifference(startPos, endPos).magnitude;
    }

    public static float GetSqrMagnitud(this Vector2 startPos, Vector2 endPos)
    {
        return GetDifference(startPos, endPos).sqrMagnitude;
    }

    public static float GetSqrMagnitud(this Vector3 startPos, Vector3 endPos)
    {
        return GetDifference((Vector2)startPos, (Vector2)endPos).sqrMagnitude;
    }

    public static Vector2 GetDifference(this Vector2 startPos, Vector2 endPos)
    {
        return (endPos - startPos);
    }

    public static Vector3 GetDifference(this Vector3 startPos, Vector3 endPos)
    {
        return GetDifference((Vector2)startPos, (Vector2)endPos);
    }

    public static float GetAngle(this Vector2 startPos, Vector2 endPos)
    {
        Vector2 dif = GetDirection(startPos, endPos);
        float angle = Mathf.Atan2(dif.y, dif.x) * Mathf.Rad2Deg;
        return angle;
    }

    public static float AsAngle(this Vector2 source)
    {
        Vector2 normalized = source.normalized;
        float angle = Mathf.Atan2(normalized.y, normalized.x) * Mathf.Rad2Deg;
        return angle;
    }

    public static float GetAngle(this Vector3 startPos, Vector3 endPos)
    {
        return GetAngle((Vector2)startPos, (Vector2)endPos);
    }
                   
    public static float GetRandomBetweenXY(this Vector2 value)
    {
        return Mathf.Lerp(value.x, value.y, Random.Range(0, 1f));
    }


    public static Vector2 GetRandomBetween(this Vector2 startPos, Vector3 endpos)
    {
        return Vector2.Lerp(startPos, endpos, Random.Range(0, 1f));
    }

    public static Vector2 GetRandomBetweenAsRect(this Vector2 startPos, Vector3 endpos)
    {
        return new Vector2(Mathf.Lerp(startPos.x, endpos.x, Random.Range(0f, 1f)), Mathf.Lerp(startPos.y, endpos.y, Random.Range(0f, 1f)));
    }

    public static Vector2 ClampPositionToView(this Camera camera, Vector2 value)
    {
        float hSize = GetHorizontalSize(camera);
        return new Vector2(Mathf.Clamp(value.x, -hSize, hSize), Mathf.Clamp(value.y, -camera.orthographicSize, camera.orthographicSize)) + (Vector2)camera.transform.position;
    }

    public static Vector2 Clamp(this Vector2 value, Vector2 min, Vector2 max)
    {
        return new Vector2(Mathf.Clamp(value.x, min.x, max.x), Mathf.Clamp(value.y, min.y, max.y));
    }

    public static bool IsInsideCameraView(this Camera camera, Vector2 value)
    {
        value -= (Vector2)camera.transform.position;
        return Mathf.Abs(value.y) < camera.orthographicSize && Mathf.Abs(value.x) < camera.GetHorizontalSize();
    }

    public static float GetHorizontalSize(this Camera camera)
    {
        return camera.aspect * camera.orthographicSize * 2;
    }

    public static Vector2 GetOrtographicSize(this Camera camera)
    {
        return new Vector2(camera.GetHorizontalSize(), camera.orthographicSize * 2); 
    }

    public static bool ContainsInLocalSpace(this BoxCollider2D boxCollider2D, Vector2 worldSpacePoint)
    {
        worldSpacePoint = boxCollider2D.transform.InverseTransformPoint(worldSpacePoint);
        return Mathf.Abs(worldSpacePoint.x) <= boxCollider2D.size.x / 2 && Mathf.Abs(worldSpacePoint.y) <= boxCollider2D.size.y / 2;
    }


    public static Vector2 GetCropping(this Vector2 fromResolution, float toAspect)
    {
        Vector2 dif = Vector2.zero;
        float aspect = Camera.main.aspect;
        if (toAspect > aspect)
        {
            //Debug.Log("Horizontal");
            float targetHeight = fromResolution.x / toAspect;
            dif.y = (fromResolution.y - targetHeight) / 2;
        }
        else
        {
            //Debug.Log("Vertical");
            float targetWidth = fromResolution.y * toAspect;
            dif.x = (fromResolution.x - targetWidth) / 2;
        }
        return dif;
    }

    public static Rect SetCenter(this ref Rect rect, Vector2 newPos)
    {
        rect.Set(newPos.x - rect.width / 2, newPos.y - rect.height / 2, rect.width, rect.height);
        return rect;
    }


    public static bool LineIntersection(Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4, ref Vector2 intersection)
    {

        float Ax, Bx, Cx, Ay, By, Cy, d, e, f, num/*,offset*/;

        float x1lo, x1hi, y1lo, y1hi;

        Ax = p2.x - p1.x;

        Bx = p3.x - p4.x;

        // X bound box test/
        if (Ax < 0)
        {
            x1lo = p2.x;
            x1hi = p1.x;
        }
        else
        {
            x1hi = p2.x;
            x1lo = p1.x;
        }

        if (Bx > 0)
        {
            if (x1hi < p4.x || p3.x < x1lo) return false;
        }
        else
        {
            if (x1hi < p3.x || p4.x < x1lo) return false;
        }

        Ay = p2.y - p1.y;
        By = p3.y - p4.y;

        // Y bound box test//
        if (Ay < 0)
        {
            y1lo = p2.y;
            y1hi = p1.y;
        }
        else
        {
            y1hi = p2.y;
            y1lo = p1.y;
        }

        if (By > 0)
        {
            if (y1hi < p4.y || p3.y < y1lo) return false;
        }
        else
        {
            if (y1hi < p3.y || p4.y < y1lo) return false;
        }

        Cx = p1.x - p3.x;
        Cy = p1.y - p3.y;
        d = By * Cx - Bx * Cy;  // alpha numerator//
        f = Ay * Bx - Ax * By;  // both denominator//
        // alpha tests//
        if (f > 0)
        {
            if (d < 0 || d > f) return false;
        }
        else
        {
            if (d > 0 || d < f) return false;
        }
        e = Ax * Cy - Ay * Cx;  // beta numerator//
        // beta tests //
        if (f > 0)
        {
            if (e < 0 || e > f) return false;
        }
        else
        {
            if (e > 0 || e < f) return false;
        }
        // check if they are parallel
        if (f == 0) return false;
        // compute intersection coordinates //
        num = d * Ax; // numerator //

        //    offset = same_sign(num,f) ? f*0.5f : -f*0.5f;   // round direction //

        //    intersection.x = p1.x + (num+offset) / f;
        intersection.x = p1.x + num / f;

        num = d * Ay;

        //    offset = same_sign(num,f) ? f*0.5f : -f*0.5f;
        //    intersection.y = p1.y + (num+offset) / f;
        intersection.y = p1.y + num / f;
        return true;
    }

    public static Vector2 PerpendicularClockwise(this Vector2 vector2)
    {
        return new Vector2(vector2.y, -vector2.x);
    }

    public static Vector2 PerpendicularCounterClockwise(this Vector2 vector2)
    {
        return new Vector2(-vector2.y, vector2.x);
    }

    public static T GetRandom<T>(this T[] value)
    {
        return value[Random.Range(0, value.Length)];
    }

    public static T GetRandom<T>(this List<T> value)
    {
        return value[Random.Range(0, value.Count)];
    }

    public static List<T> GetRandoms<T>(this List<T> value, int count, bool canRepeat)
    {
        if(!canRepeat && count > value.Count)
            Debug.LogError("Count is Bigger than the list, but we cannot repeat values. Configuration is imposible to fulfil");

        List<T> iterationList = new List<T>(value);
        List<T> retValue = new List<T>();

        while (retValue.Count < count)
        {
            T element = iterationList.GetRandom();
            if (!canRepeat) iterationList.Remove(element);
            retValue.Add(element);
        }
        return retValue;
    }

    public static T GetLast<T>(this List<T> value)
    {
        return value[Random.Range(0, value.Count-1)];
    }
      
    public static T GetLast<T>(this T[] value)
    {
        return value[Random.Range(0, value.Length-1)];
    }

    public static T[] GetElements<T>(this T[] value, int[] indexes) where T : Object
    {
        T[] rValue = new T[indexes.Length];
        for (int i = 0; i < indexes.Length; i++)
        {
            rValue[i] = value[indexes[i]];
        }
        return rValue;
    }

    public static T[] GetElements<T>(this List<T> value, int[] indexes) where T : Object
    {
        return GetElements(value.ToArray(),indexes);
    }

    public static Vector2[] GetVertex(this BoxCollider2D box)
    {
        Vector2[] retValue = new Vector2[4];
        retValue[0] = new Vector2(box.bounds.min.x - box.edgeRadius, box.bounds.min.y - box.edgeRadius);
        retValue[1] = new Vector2(box.bounds.min.x - box.edgeRadius, box.bounds.max.y + box.edgeRadius);
        retValue[2] = new Vector2(box.bounds.max.x + box.edgeRadius, box.bounds.max.y + box.edgeRadius);
        retValue[3] = new Vector2(box.bounds.max.x + box.edgeRadius, box.bounds.min.y - box.edgeRadius);
        return retValue;
    }

    public static List<List<Vector2>> GetAllPaths(this PolygonCollider2D collider)
    {
        List<List<Vector2>> polygons = new List<List<Vector2>>();
        for (int i = 0; i < collider.pathCount; i++)
        {
            polygons.Add(new List<Vector2>(collider.GetPath(i)));
        }
        return polygons;
    }

    public static void SetPaths (this PolygonCollider2D collider2D, List<List<Vector2>> nPaths)
    {
        collider2D.pathCount = nPaths.Count;
        for (int i = 0; i < collider2D.pathCount; i++)
        {
            collider2D.SetPath(i, nPaths[i].ToArray());
        }
    }
    public static float SnapTo(this float value, float snap)
  {
      return Mathf.Round(value / snap) * snap;
  }
    public static Vector3 SnapTo(this Vector3 value, float snap)
    {
        Vector3 retValue = value;
        retValue.x = retValue.x.SnapTo(snap);
        retValue.y = retValue.y.SnapTo(snap);
       retValue.z =  retValue.z.SnapTo(snap);
        return retValue;
    }

  public static Vector2 SnapTo(this Vector2 value, float snap)
    {
        Vector2 retValue = value;
        retValue.x = retValue.x.SnapTo(snap);
        retValue.y = retValue.y.SnapTo(snap);
        return retValue;
    }

  public static int DifferenceXtoY(this Vector2Int value)
  {
      return value.y - value.x;
  }

}
