﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DoOnMainThread : MonoSingleton<DoOnMainThread>
{
    public readonly static Queue<Action> executeOnMainThread = new Queue<Action>();
    public static Queue<Action> ExecuteOnMainThread
    {
        get
        {
            return executeOnMainThread;
        }
    }

    public override DoOnMainThread Myself => this;

    public void Update()
    {
        // dispatch stuff on main thread
        while (ExecuteOnMainThread.Count > 0)
        {
            ExecuteOnMainThread.Dequeue()?.Invoke();
        }
    }
}