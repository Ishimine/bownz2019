﻿using System.Collections.Generic;
using Pooling.Poolers;
using UnityEngine;

public class OnTerrainCollitionVfx : MonoBehaviour
{
    [SerializeField] private SetPooler vfxCollitionPool;
    [SerializeField] private  CompositeCollider2D col;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!collision.rigidbody.gameObject.CompareTag("Player")) return;
        TerrainCollitionVfx terrainCollitionVfx = vfxCollitionPool.Dequeue().GetComponent<TerrainCollitionVfx>();
        List<List<Vector2>> paths = new List<List<Vector2>>();
        for (int i = 0; i < col.pathCount; i++)
        {
            Vector2[] aux = new Vector2[col.GetPathPointCount(i) + 1];   //El 1 extra esta para LOOPEAR la linea. Sin embargo la linea YA deberia llegar loopeada, porque previamente ya hago esto en otro lado. PERO POR ALGUNA RAZON NO FUNCIONA ASI QUE LO HAGO OTRA VEZ ACA.
            col.GetPath(i, aux);
            aux[aux.Length - 1] = aux[0];
            paths.Add(new List<Vector2>(aux));
        }
        terrainCollitionVfx.gameObject.SetActive((true));
        terrainCollitionVfx.transform.parent = transform;
        terrainCollitionVfx.OnAnimationEnd = ()=> vfxCollitionPool.Enqueue(terrainCollitionVfx.GetComponent<Poolable>());
        terrainCollitionVfx.Initialize(collision, paths);
    }
}
