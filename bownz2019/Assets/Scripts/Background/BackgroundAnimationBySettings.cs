﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Sirenix.OdinInspector;


public class BackgroundAnimationBySettings : BackgroundAnimation
{
    [SerializeField] private Transform parent;
    public WrapMode defaultWrapMode = WrapMode.PingPong;

    [TabGroup("General")] public float generalSpeed = 1;
    [TabGroup("General")] public Vector2 generalDirection = Vector2.one;
    [TabGroup("General")] public Vector2 offsetByPosition = Vector2.one;


    [TabGroup("Size")] public float baseSize = .3f;
    [TabGroup("Size")] public AnimationCurve sizeCurve = AnimationCurve.EaseInOut(0, 0, 1, 1);
    [TabGroup("Size")] public float sizeSpeed = 1;
    [TabGroup("Size")] public Vector2 sizeOffsetByPosition = Vector2.one;


    [TabGroup("Rotation")] public AnimationCurve rotationCurve = AnimationCurve.EaseInOut(0, 0, 1, 1);
    [TabGroup("Rotation")] public float baseRotation = 45f;
    [TabGroup("Rotation")] public float rotationSpeed = 1;
    [TabGroup("Rotation")] public Vector2 rotationOffsetByPosition = Vector2.one;

    [TabGroup("Position")] public AnimationCurve positionCurve = AnimationCurve.EaseInOut(0, 0, 1, 1);
    [TabGroup("Position")] public float positionSpeed = 1;
    [TabGroup("Position")] public Vector2 positionOffsetByPosition = Vector2.one;
    [TabGroup("Position")] public Vector2 animationPositionDirection = Vector2.zero;

    [TabGroup("Position")] public AnimationCurve basePositionOffsetCurve = AnimationCurve.EaseInOut(0, 0, 1, 1);
    [TabGroup("Position")] public Vector2 basePositionOffset = Vector3.zero;
    [TabGroup("Position")] public Vector2 basePositionDirection = Vector3.zero;


    public Vector2[,] defaultPositions;

    public override void SetDefaultPosition(Transform[,] nodes)
    {
        defaultPositions = new Vector2[nodes.GetLength(0), nodes.GetLength(1)];
        for (int x = 0; x < nodes.GetLength(0); x++)
        {
            for (int y = 0; y < nodes.GetLength(1); y++)
            {
                defaultPositions[x, y] = nodes[x, y].localPosition;
            }
        }
    }
        
    public override void UpdateAnimation(Transform[,] nodes)
    {
        if (Application.isEditor)
            ChangeEvaluateTipe(defaultWrapMode);

        for (int x = 0; x < nodes.GetLength(0); x++)
        {
            for (int y = 0; y < nodes.GetLength(1); y++)
            {
                nodes[x, y].transform.localScale =Vector3.one * baseSize * (1 + sizeCurve.Evaluate(generalSpeed * Time.time  * sizeSpeed+ sizeOffsetByPosition.x
                    * nodes[x, y].transform.localPosition.x + sizeOffsetByPosition.y * nodes[x, y].transform.localPosition.y) * baseSize);

                nodes[x, y].transform.rotation = Quaternion.Euler(0, 0, baseRotation + rotationCurve.Evaluate(generalSpeed * Time.time + rotationOffsetByPosition.x
                    * nodes[x, y].transform.localPosition.x + rotationOffsetByPosition.y * nodes[x, y].transform.localPosition.y) * rotationSpeed);

                nodes[x, y].transform.localPosition = defaultPositions[x, y] + basePositionDirection * basePositionOffsetCurve.Evaluate(basePositionOffset.x * x + basePositionOffset.y * y) + animationPositionDirection *  positionCurve.Evaluate(generalSpeed * Time.time + positionOffsetByPosition.x * nodes[x, y].transform.localPosition.x + positionOffsetByPosition.y * nodes[x, y].transform.localPosition.y) * positionSpeed;
            }
        }
    }
    
   [Button]
    public void ChangeEvaluateTipe(WrapMode wrapMode)
    {
        sizeCurve.preWrapMode = wrapMode;
        rotationCurve.preWrapMode = wrapMode;
        positionCurve.preWrapMode = wrapMode;
        basePositionOffsetCurve.preWrapMode = wrapMode;

        sizeCurve.postWrapMode = wrapMode;
        rotationCurve.postWrapMode = wrapMode;
        positionCurve.postWrapMode = wrapMode;
        basePositionOffsetCurve.postWrapMode = wrapMode;
    }

}

