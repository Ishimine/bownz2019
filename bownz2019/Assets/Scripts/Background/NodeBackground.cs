﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class NodeBackground : MonoBehaviour
{
    public bool usePlayAreaPosition = true;
    public GameObject prefab;

    public float defaultSize = .15f;
    public float defaultRotation = 45;

    [SerializeField] private Vector2 density = new Vector2(1,1);
   private Vector2Int nodesCount = new Vector2Int(10, 20);
    public Vector2Int NodesCount
    {
        get { return nodesCount; }
        set { nodesCount = value; }
    }

    [SerializeField] private Vector2 size = new Vector2(14, 24);
    public Vector2 Size
    {
        get { return PlayArea.Area.size; }
        set { size = value; }
    }

    public bool intercalarFilas;
    private Transform[,] nodes = new Transform[0,0];

    public BackgroundAnimation bgAnimation;

    private void Awake()
    {
        CreateNodes();
    }

    [Button]
    public void CreateNodes()
    {
        nodesCount = new Vector2Int((int)(size.x * density.x), (int)(size.y * density.y));
        DestroyCurrentNodes();
        Vector2 distance = new Vector2(1 / density.x, 1 / density.y);

        nodes = new Transform[nodesCount.x, nodesCount.y];
        for (int x = 0; x < nodesCount.x; x++)
        {
            for (int y = 0; y < nodesCount.y; y++)
            {
                nodes[x,y] = Instantiate(prefab, transform).transform;
                nodes[x, y].transform.localPosition = distance / 2 + new Vector2(distance.x * x, distance.y * y);
                if(intercalarFilas)
                    nodes[x, y].transform.localPosition += Vector3.right * distance.x / 2 * ((y % 2 == 0) ? 1 : 0);

                nodes[x, y].name = string.Format("Node X:{0} Y:{1}",x,y);
                nodes[x, y].transform.localScale = Vector3.one * defaultSize;
                nodes[x, y].transform.rotation = Quaternion.Euler(0, 0, defaultRotation);
            }
        }
        if (usePlayAreaPosition)    transform.position = PlayArea.Area.position;//-new Vector3(nodesCount.x * distance.x, nodesCount.y * distance.y)/2;

        if (nodes.GetLength(0) == 0 || nodes.GetLength(1) == 0)
            Debug.LogWarning("NodeBckground is 0");
        else
            bgAnimation.SetDefaultPosition(nodes);
    }

    private void Update()
    {
        bgAnimation.UpdateAnimation(nodes);
    }

    [Button]
    public void DestroyCurrentNodes()
    {
        foreach (var item in nodes)
        {
            if(Application.isPlaying)        Destroy(item.gameObject);
            else DestroyImmediate(item.gameObject);
        }
        nodes = new Transform[0, 0];

        int childCount = transform.childCount;

        for (int i = childCount-1; i >= 0; i--)
        {
            if (Application.isPlaying) Destroy(transform.GetChild(i).gameObject);
            else DestroyImmediate(transform.GetChild(i).gameObject);
        }
    }

    public Vector2Int GetClosestNode(Vector2 worldPos)
    {
        Vector2Int min = new Vector2Int(); ;
        float minDistance = float.MaxValue;
        float aux;
        for (int x = 0; x < nodes.GetLength(0); x++)
        {
            for (int y = 0; y < nodes.GetLength(1); y++)
            {
                aux = (nodes[x,y].transform.position.GetSqrMagnitud(worldPos));
                if (aux < minDistance)
                {
                    minDistance = aux;
                    min.x = x;
                    min.y = y;
                }
            }
        }
        return min;
    }
}



