﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelPlayAreaToBackgroundBridge : MonoBehaviour
{
    [SerializeField] private LevelConfiguration levelPlayAreaSize = null;
    [SerializeField] private NodeBackground nodeBackground = null;

    private void Awake()
    {
        nodeBackground.Size = levelPlayAreaSize.PlayAreaSize;
    }
}
