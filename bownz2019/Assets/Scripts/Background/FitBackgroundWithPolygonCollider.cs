﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class FitBackgroundWithPolygonCollider : MonoBehaviour
{
    [SerializeField] protected PolygonCollider2D col;
    [SerializeField] protected NodeBackground nodeBackground;

    [SerializeField] private Background background;
    [SerializeField] private PolygonCollider2D cameraConfiner;


    private void OnEnable()
    {
        Fit();
    }

    public virtual void Fit()
    {
        /*var bounds = new Bounds(col.points[0], Vector3.zero);
        foreach (var point in col.points)
            bounds.Encapsulate(point);

        nodeBackground.transform.position = bounds.center - bounds.extents;
        nodeBackground.Size = bounds.size;*/

        background.SetPositionAndSize(new Rect(cameraConfiner.transform.TransformPoint(cameraConfiner.bounds.min), cameraConfiner.bounds.size));
    }
}
