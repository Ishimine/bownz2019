﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BackgroundAnimation : MonoBehaviour
{
    public abstract void UpdateAnimation(Transform[,] nodes);
    public abstract void SetDefaultPosition(Transform[,] nodes);
}