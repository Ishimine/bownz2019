﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class BackgroundAnimationGamePlayReactive : BackgroundAnimation
{
    public NodeBackground nodeBackground;
    private Color defaultColor;
    [SerializeField]  private Color terrainColor;

    private float defaultSize;
    public float activatedSize = .55f;

    public SpriteRenderer[,] renders;
    public float[,] acumulativeValue;
    public float[,] lerpValue;
    public float[,] acumulativeVelocity;
    public float[,] flatVelocity;
    public Color[,] nodeColor;
    public float acumulativeSmoothTime;
    public float flatSmoothTime = .15f;

    public float[,] targetSize;
    public Vector2[,] defaultPositions;
    public Vector2[,] velocity;
    public Color[,] colors;


    public float forcePropagationDamp = .9f;
    public float forcePropagationMinimum = .1f;

    public bool usePosition = false;
    public bool useColor = false;
    public bool useSize = true;

    public float minCalculationThreshold = .05f;

    public float timeBetweenPropagation = .1f;

    private void OnEnable()
    {
        this.AddObserver(OnCollitionReaction, Notifications.BackgroundContactReaction);
    }

    private void OnDisable()
    {
        this.RemoveObserver(OnCollitionReaction, Notifications.BackgroundContactReaction);
    }

    private void OnCollitionReaction(object sender, object data)
    {
        BackgroundContactReactionData reactionData = (BackgroundContactReactionData)data;
        StartPropagationFrom(reactionData.point, reactionData.color, reactionData.force);
    }

    public override void SetDefaultPosition(Transform[,] nodes)
    {
        defaultPositions = new Vector2[nodes.GetLength(0), nodes.GetLength(1)];
        renders = new SpriteRenderer[nodes.GetLength(0), nodes.GetLength(1)];
        acumulativeValue = new float[nodes.GetLength(0), nodes.GetLength(1)];
        acumulativeVelocity = new float[nodes.GetLength(0), nodes.GetLength(1)];
        flatVelocity = new float[nodes.GetLength(0), nodes.GetLength(1)];
        targetSize = new float[nodes.GetLength(0), nodes.GetLength(1)];
        nodeColor = new Color[nodes.GetLength(0), nodes.GetLength(1)];
        lerpValue = new float[nodes.GetLength(0), nodes.GetLength(1)];

        for (int x = 0; x < nodes.GetLength(0); x++)
        {
            for (int y = 0; y < nodes.GetLength(1); y++)
            {
                defaultPositions[x, y] = nodes[x, y].localPosition;
                renders[x, y] = nodes[x, y].GetComponent<SpriteRenderer>();
            }
        }
        defaultColor = renders[0, 0].color;
        defaultSize = renders[0, 0].transform.localScale.x;
    }

    public override void UpdateAnimation(Transform[,] nodes)
    {
        if (nodes.Length == 0) return;

        if (usePosition)
        {
            for (int x = 0; x < nodes.GetLength(0); x++)
            {
                for (int y = 0; y < nodes.GetLength(1); y++)
                {
                    nodes[x, y].localPosition = Vector2.SmoothDamp(nodes[x, y].localPosition, defaultPositions[x, y], ref velocity[x, y], flatSmoothTime);
                }
            }
        }

        for (int x = 0; x < nodes.GetLength(0); x++)
        {
            for (int y = 0; y < nodes.GetLength(1); y++)
            {
                if (acumulativeValue[x, y] < minCalculationThreshold)
                {
                    if (acumulativeValue[x, y] != 0)
                    {
                        acumulativeValue[x, y] = 0;
                        renders[x, y].color = defaultColor;
                        renders[x, y].transform.localScale = Vector3.one * defaultSize;
                    }
                    continue;
                }

                acumulativeValue[x, y] = Mathf.SmoothDamp(acumulativeValue[x, y], 0, ref acumulativeVelocity[x, y], acumulativeSmoothTime);
                lerpValue[x, y] = Mathf.SmoothDamp(lerpValue[x, y], acumulativeValue[x, y], ref flatVelocity[x, y], flatSmoothTime);

                if (useColor)
                    renders[x, y].color = Color.Lerp(defaultColor, nodeColor[x, y], acumulativeValue[x, y]);

                if (useSize)
                    renders[x, y].transform.localScale = Vector3.one * defaultSize  + Vector3.one * lerpValue[x, y] * activatedSize;
            }
        }
    }

    [Button]
    public void StartPropagationFrom(Vector2 worldPosition,Color color, float force)
    {
        StartPropagationFrom(nodeBackground.GetClosestNode(worldPosition), color, force);
    }

    [Button]
    public void StartPropagationFrom(Vector2Int centerIndex, Color color, float force)
    {
        if (acumulativeValue == null) return;
        StartCoroutine(PropagateColorFX(centerIndex, color, force));
    }

    private IEnumerator PropagateColorFX(Vector2Int center, Color color, float force)
    {
        HashSet<Vector2Int> alreadyVisited = new HashSet<Vector2Int>();

        acumulativeValue[center.x, center.y] = 1;

        Vector2Int lengths = new Vector2Int(renders.GetLength(0), renders.GetLength(1));

        List<Vector2Int> currentList;
        int count = 1;
        do
        {
            currentList = GetAreaFromCenter(center, ref lengths, count);
            count++;
            foreach (var item in currentList)
            {
                if (!alreadyVisited.Contains(item))
                {
                    if(acumulativeValue[item.x, item.y] < force)
                        acumulativeValue[item.x, item.y] = force;

                    if (lerpValue[item.x, item.y] < force)
                    {
                        nodeColor[item.x, item.y] = color;
                    }
                    alreadyVisited.Add(item);
                }
            }
            force *= forcePropagationDamp;
            //Debug.Log(string.Format("Counter N°{0} | Force:  {1}", count,force));
            yield return new WaitForSeconds(timeBetweenPropagation);
        } while (currentList.Count > 0 && force > forcePropagationMinimum);
    }

    public List<Vector2Int> GetAreaFromCenter(Vector2Int center, ref Vector2Int lengths, int distance)
    {
        List<Vector2Int> rValue = new List<Vector2Int>();

        Vector2Int min, max;
        min = center - Vector2Int.one * distance;
        max = center + Vector2Int.one * distance;

        min.Clamp(Vector2Int.zero, lengths - Vector2Int.one);
        max.Clamp(Vector2Int.zero, lengths - Vector2Int.one);

        Vector2Int current = new Vector2Int();
        for (int x = min.x; x <= max.x; x++)
        {
            int y;
            for (int pass = 0; pass < 2; pass++)
            {
                y = (pass == 0) ? min.y : max.y;

                current = new Vector2Int(x, y);

                if (!rValue.Contains(current))
                {
                    rValue.Add(current);
                }
               
            }
        }

        for (int y = min.y; y <= max.y; y++)
        {
            int x;
            for (int pass = 0; pass < 2; pass++)
            {
                x = (pass == 0) ? min.x : max.x;
         
                current = new Vector2Int(x, y);

                if (!rValue.Contains(current))
                {
                    rValue.Add(current);
                }
             
            }
        }

        return rValue;
    }

    public void PropagateExplotion(Vector2Int index, Vector2 force)
    {
        
    }


}

public struct BackgroundContactReactionData
{
    public Vector2 point;
    public Color color;
    public float force;
}