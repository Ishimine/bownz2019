﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;
using Pooling.Poolers;

public class SpecialStarsManager : MonoBehaviour
{
    [SerializeField] private CoinSpawnConfig coinSpawnConfig = null;
    [SerializeField] private SetPooler inactivePool = null;
    [SerializeField] private ShineMark mark = null;

    List<StarCoin> activeStars = new List<StarCoin>();
    [SerializeField] private SpawnFromBoxCollider2D spawnFromBox = null;

    [SerializeField] private Vector2 defaultPosition = Vector2.up;
    private Vector2 nextPosition = Vector2.zero;

    [SerializeField] Timer timer = null;

    public ContactFilter2D evadeFilter;
    public float evadeRatio = 1;

    [SerializeField] private Vector2 minMaxTime = new Vector2(5,12);

    public float translationSpeed = 3;
    public float lifeSpan = 20;

    private void Awake()
    {
        timer.OnTimeDone = SpawnStar;
    }

    private void StartTimer()
    {
        timer.StartTimer(UnityEngine.Random.Range(minMaxTime.x, minMaxTime.y));
    }

    private void StopTimer()
    {
        timer.StopTimer();
    }

    private void OnEnable()
    {
        this.AddObserver(OnGameModeReady_Response, Notifications.GameMode_Ready);
        this.AddObserver(OnGameModeGo_Response, Notifications.GameMode_Go);
        this.AddObserver(OnGameModeEnd_Response, Notifications.GameMode_End);
    }
    private void OnDisable()
    {
        this.RemoveObserver(OnGameModeReady_Response, Notifications.GameMode_Ready);
        this.RemoveObserver(OnGameModeGo_Response, Notifications.GameMode_Go);
        this.RemoveObserver(OnGameModeEnd_Response, Notifications.GameMode_End);
    }

    private void OnGameModeGo_Response(object arg1, object arg2)
    {
        Initialize();
    }

    private void OnGameModeReady_Response(object arg1, object arg2)
    {
    }

    private void OnGameModeEnd_Response(object sender, object value)
    {
        DisableAll();
        StopTimer();
    }

    private void DisableAll()
    {
        foreach (var item in activeStars)
        {
            inactivePool.EnqueueScript(item.GetComponent<Poolable>());
            //item.gameObject.SetActive(false);
        }
    }

    public void Initialize()
    {
        spawnFromBox.GetPosition(out nextPosition, evadeRatio, evadeFilter);
        StartTimer();
    }

    private void NextStar()
    {
        mark.Shine(nextPosition, SpawnStar);
    }

    [Button]
    public void SpawnStar()
    {
        StarCoin star = inactivePool.DequeueScript<StarCoin>();
        star.gameObject.SetActive(true);
        star.gameObject.transform.SetParent(transform);
        star.LifeSpan = lifeSpan;

        star.OnHideDone -= OnHideDoneResponse;
        star.OnHideDone += OnHideDoneResponse;

        star.Show(true, coinSpawnConfig.GetCoinType(star), nextPosition);

        ConstantTraslation constantTraslation = star.gameObject.AddComponent<ConstantTraslation>();
        constantTraslation.SetDirection( Vector2.left * star.transform.position.x);
        constantTraslation.SetSpeed(translationSpeed);

        star.OnHideDone += x => Destroy(constantTraslation);

        bool isValid = spawnFromBox.GetPosition(out nextPosition, evadeRatio, evadeFilter);
        activeStars.Add(star);
    }


    private void OnHideDoneResponse(StarCoin star)
    {
        star.OnHideDone -= OnHideDoneResponse;  

        if (activeStars.Contains(star)) activeStars.Remove(star);

        inactivePool.EnqueueScript(star.GetComponent<Poolable>());
        StartTimer();
    }
}