﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnFromBoxCollider2D : SpawnArea
{
    public BoxCollider2D[] colliders;

    public override Vector2 GetPosition()
    {
        return GetRandomPointInsideBounds(colliders.GetRandom().bounds);
    }

    public Vector2 GetPosition(Collider2D colToSpawn, Collider2D[] colsToEvade, ContactFilter2D filter = new ContactFilter2D())
    {
        Collider2D[] results = new Collider2D[10];
        Vector2 pos;
        int attemps = 0;
        int maxAttemps = 20;
        do
        {
            attemps++;
            pos = GetRandomPointInsideBounds(colliders.GetRandom().bounds);
        } while (IsValidPosition(pos, colToSpawn, colsToEvade, filter) && attemps < maxAttemps);
        return pos;
    }

    public bool GetPosition(out Vector2 retPos, float evadeRatio, ContactFilter2D filter)
    {
        doesOverlap = false;
        res = new List<Collider2D>();

        Vector2 pos = GetRandomPointInsideBounds(colliders.GetRandom().bounds);
        count = Physics2D.OverlapCircleNonAlloc(pos, evadeRatio, results);
        res.Clear();
        res.AddRange(results);
        retPos = pos;
        return count > 0;
    }

    public bool GetPosition(out Vector2 retPos, Collider2D colToSpawn, Collider2D[] colsToEvade, ContactFilter2D filter = new ContactFilter2D())
    {
        Collider2D[] results = new Collider2D[10];
        Vector2 pos;
        int attemps = 0;
        int maxAttemps = 20;
        bool isValid;
        do
        {
            attemps++;
            pos = GetRandomPointInsideBounds(colliders.GetRandom().bounds);
            isValid = IsValidPosition(pos, colToSpawn, colsToEvade, filter);
        } while (isValid && attemps < maxAttemps);
        retPos = pos;
        return isValid;
    }


    int count;
    bool doesOverlap;
    Vector2 origPosition;
    List<Collider2D> res;
    Collider2D[] results = new Collider2D[10];
    private bool IsValidPosition(Vector2 pos, Collider2D colToSpawn, Collider2D[] colsToEvade, ContactFilter2D filter = new ContactFilter2D())
    {
        doesOverlap = false;
        origPosition = colToSpawn.transform.position;
        res = new List<Collider2D>();

        colToSpawn.transform.position = GetRandomPointInsideBounds(colliders.GetRandom().bounds);
        count = colToSpawn.OverlapCollider(filter, results);
        res.Clear();
        res.AddRange(results);
        for (int i = 0; i < colsToEvade.Length; i++)
        {
            doesOverlap = res.Contains(colsToEvade[i]);
            if (doesOverlap) break;
        }

        return !doesOverlap;
    }


    public Vector2 GetRandomPointInsideBounds(Bounds bounds)
    {
        return new Vector2(Random.Range(bounds.min.x, bounds.max.x), Random.Range(bounds.min.y, bounds.max.y));
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = new Color(0, 1, 0, .4f);
        for (int i = 0; i < colliders.Length; i++)
        {
            Gizmos.DrawCube((Vector2)colliders[i].transform.position + colliders[i].offset, colliders[i].size);
        }
    }
}
