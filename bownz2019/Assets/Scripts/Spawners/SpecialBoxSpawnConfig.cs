﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SpecialBoxSpawnConfig : CoinSpawnConfig
{
    private List<Type> enableTypes = new List<Type>();

    public void SetAs_Classic()
    {
        Clear();
        Add<ShieldCoin>();
        Add<ExtraBallCoin>();
        Add<GlassFloorCoin>();
    }

    public void SetAs_Box()
    {
        Clear();
        Add<ShieldCoin>();
        Add<ExtraBallCoin>();
    }


    private void Awake()
    {
        Clear();
        Add<ShieldCoin>();
    }
    public void Clear()
    {
        enableTypes = new List<Type>();
    }

    public void Remove<T>() where T : CoinType
    {
        Type type = typeof(T);
        if (enableTypes.Contains(type))
            enableTypes.Remove(type);
    }

    public void Add<T>() where T : CoinType
    {
        Type type = typeof(T);
        if(!enableTypes.Contains(type))
            enableTypes.Add(type);
    }


    public override CoinType GetCoinType(StarCoin starCoin)
    {
       // int min = (SlowMotionWhileSwipe_Modificator.isInfinite) ? 2 : 1;
     //   int min = 4;
     //   int value = UnityEngine.Random.Range(min, 7);

        return (CoinType)Activator.CreateInstance(enableTypes.GetRandom(), starCoin as object);
  /*      switch (value)
        {
            default:
            //case 0: return new GoldenCoin(starCoin);
         //   case 1: return new SlowMotionCoin(starCoin);
        //    case 2: return new RedCoin(starCoin);
       //     case 3: return new BlueCoin(starCoin);
            case 4: return new ExtraBallCoin(starCoin);
            case 5: return new ShieldCoin(starCoin);
            case 6: return new GlassFloorCoin(starCoin);
        }*/
    }

    public override void Reset()
    {
    }
}