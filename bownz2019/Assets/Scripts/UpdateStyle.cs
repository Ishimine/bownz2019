﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class UpdateStyle : MonoBehaviour
{
    public TerrainToSpriteShape TerrainToSpriteShape;
    public MeshFromColliders MeshFromColliders;
    public CorrectRotation correctRotation;
    public AutoPositionRoofAndFloor AutoPositionRoofAndFloor;
    public LevelConfiguration LevelConfiguration;
    public Transform terrain;
    public ExtractPaths extractPaths;

    [Button]
    public void UpdateMeshAndShape(bool writeNow)
    {
        DestroyImmediate(terrain.GetComponent<CompositeCollider2D>(),true);
        DestroyImmediate(terrain.GetComponent<Rigidbody2D>(),true);


AutoPositionRoofAndFloor?.RepositionRoofAndFloor();
#if UNITY_EDITOR
        LevelConfiguration?.CameraConfinerWrapLevel();
    #endif
        correctRotation?.Correct();
        TerrainToSpriteShape.CreateSpriteShape();
        MeshFromColliders.CreateMeshFromLevelEditor(true,true, writeNow);
        extractPaths?.Extract();
    }

    [Button, ButtonGroup("B")]
    private void ShowAll()
    {
        MeshFromColliders.gameObject.SetActive(true);
        TerrainToSpriteShape.Show();
    }

    [Button, ButtonGroup("B")]
    private void HideAll()
    {
        MeshFromColliders.gameObject.SetActive(false);
        TerrainToSpriteShape.Hide();
    }


    [Button, ButtonGroup("A")]
    private void ShowAbstract()
    {
        MeshFromColliders.gameObject.SetActive(true);
        TerrainToSpriteShape.Hide();
    }

    [Button, ButtonGroup("A")]
    private void ShowNormal()
    {
        MeshFromColliders.gameObject.SetActive(false);
        TerrainToSpriteShape.Show();
    }
}
