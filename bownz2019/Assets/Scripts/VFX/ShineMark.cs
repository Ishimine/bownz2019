﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Sirenix.OdinInspector;

public class ShineMark : MonoBehaviour
{

    private CodeAnimator codeAnimator = new CodeAnimator();

    [SerializeField] private CodeAnimatorCurve animatorCurve = null;

    public float targetRotation = 360;

    public float targetSize = .5f;

    [Button]
    public void Shine(Vector2 position, Action postAction = null)
    {
        transform.position = position;

        float startRotation = transform.rotation.z;

        Quaternion difRotation = Quaternion.Euler(0, 0, targetRotation);

        codeAnimator.StartAnimacion(this, x =>
        {
            transform.rotation = Quaternion.Euler(0, 0, startRotation + targetRotation * x);
            transform.localScale = Vector3.one * animatorCurve.Curve.Evaluate(x) * targetSize;
        }, DeltaTimeType.deltaTime, AnimationType.Simple, null, postAction, animatorCurve.Time);
    }

    private void OnEnable()
    {
        
    }

    private void OnDisable()
    {
        
    }
}
