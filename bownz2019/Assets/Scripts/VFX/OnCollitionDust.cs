﻿using System.Collections;
using System.Collections.Generic;
using Pooling.Poolers;
using UnityEngine;

public class OnCollitionDust : MonoBehaviour
{
    public bool useOnlyWithContactColor = true; 
    public Color defaultDustColor = Color.grey;
    public SetPooler pooler;

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (!other.enabled) return;

        ContactColor contactColor = other.collider.gameObject.GetComponent<ContactColor>();
        if (useOnlyWithContactColor && !contactColor) return;

        Color pColor = (contactColor == null) ? defaultDustColor : contactColor.Color;
        CollitionDust dust = GetDust();
        dust.Play(other.GetContact(0).point, (other.GetContact(0).point.GetDirection( other.transform.position)).AsAngle(), pColor, 4, ()=> RemoveDust(dust));
    }

    private CollitionDust GetDust()
    {
        CollitionDust dust = pooler.Dequeue().GetComponent<CollitionDust>();
        dust.transform.SetParent(transform); 
        dust.gameObject.SetActive(true);
        return dust;
    }

    private void RemoveDust(Component dust)
    {
        pooler.Enqueue(dust.gameObject.GetComponent<Poolable>());
    }
}
