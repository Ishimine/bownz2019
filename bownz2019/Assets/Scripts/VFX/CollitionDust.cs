﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollitionDust : MonoBehaviour
{
    public ParticleSystem[] particles;
  
    private readonly CodeAnimator codeAnimator = new CodeAnimator();

    public void Play(Vector2 targetPosition, float angle, Color pColor, float duration, Action callback)
    {
       foreach (var p in particles)
        {
            SetParticlesColor(p, pColor);
        }
       // transform.LookAt(contact.point, transform.right);
        transform.rotation = Quaternion.Euler(0,0,angle);
        transform.position = targetPosition;
        codeAnimator.StartWaitAndExecute(this, duration, callback, false);
    }

    private void SetParticlesColor(ParticleSystem particle, Color pColor)
    {
        var main = particle.main;
        main.startColor = pColor;
    }

    private void OnEnable()
    {
        foreach (var p in particles)
        {
            p.Play();
        }
    }
}
