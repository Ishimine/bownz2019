﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Sirenix.OdinInspector;

public class UI_Medal_ScoreCounter : MonoBehaviour
{
    [TabGroup("Dependencies"),SerializeField] private UI_ScoreMedal medal;
    [TabGroup("Dependencies"), SerializeField] private Text currentText;
    [TabGroup("Dependencies"), SerializeField] private Text targetText;

    [TabGroup("Animation"), SerializeField] private float duration = 1.2f;
    [TabGroup("Animation"), SerializeField] private AnimationCurve textAnimCurve;
    [TabGroup("Animation"), SerializeField] private Color cCompleted;
    [TabGroup("Animation"), SerializeField] private Color cIncompleted = Color.grey;

    public string format = "0.0";

    public Action OnEnd { get; set; }
    public Action OnStart { get; set; }
    private CodeAnimator codeAnimator = new CodeAnimator();
    private float targetValue;
    private float previousValue;
    private float currentValue;

    //Inicia animacion, se activa cuando la pantalla YA esta desplegada.
    public void Initialize(float nPreviousValue, float nCurrentValue, float nTargetValue)
    {
        Debug.Log(gameObject.name +
                  $"Submited Score Previous:{nPreviousValue} Current:{nCurrentValue} Target:{nTargetValue}");

        previousValue = nPreviousValue;
        currentValue = nCurrentValue;
        targetValue = nTargetValue;

        currentText.color = Color.white;
        targetText.color = Color.white;

        currentText.text = 0.ToString(format);
        targetText.text = targetValue.ToString(format);

        medal.Initialize();
    }


    public void StartAnimation()
    {
        OnStart?.Invoke();

        if (previousValue <= targetValue)
            Animation_AlreadyCompleted();
        else if (currentValue <= targetValue)
            Animation_JustCompleted();
        else
            Animation_NotCompleted();
    }

    private void AnimateText(float startValue, float targetValue, Action postAction)
    {
        codeAnimator.StartAnimacion(this,
                    x =>
                    {
                        currentText.text = Mathf.Lerp(startValue, targetValue, textAnimCurve.Evaluate(x)).ToString(format);
                    }, DeltaTimeType.deltaTime, AnimationType.Simple, null, postAction, duration);
    }


    #region AlreadyCompleted

    public void Animation_AlreadyCompleted()
    {
        float startValue = 0;
        float targetValue = (previousValue < currentValue) ? previousValue : currentValue;

        if (targetValue != startValue)
            AnimateText(startValue, targetValue, AnimationEnd_AlreadyCompleted);
        else
            AnimationEnd_AlreadyCompleted();
    }
    private void AnimationEnd_AlreadyCompleted()
    {
        medal.Animation_AlreadyCompleted();
        codeAnimator.StartAnimacion(this,
            x =>
            {
                currentText.color = Color.Lerp(Color.white, cCompleted, x);
                targetText.color = Color.Lerp(Color.white, cCompleted, x);
            }, DeltaTimeType.deltaTime, AnimationType.Simple, null, .5f);
        OnEnd?.Invoke();
    }

    #endregion

    #region NotCompleted
    private void Animation_NotCompleted()
    {
        float startValue = (previousValue<10000)?previousValue:0;
        float targetValue = currentValue;

        if (targetValue != startValue)
            AnimateText(startValue, targetValue, AnimationEnd_NotCompleted);
        else
            AnimationEnd_NotCompleted();
    }
    private void AnimationEnd_NotCompleted()
    {
        codeAnimator.StartAnimacion(this,
        x =>
        {
            currentText.color = Color.Lerp(Color.white, cIncompleted, x);
        }, DeltaTimeType.deltaTime, AnimationType.Simple, null, .5f);

        medal.Animation_NotCompleted();
        OnEnd?.Invoke();
    }
    #endregion

    #region JustCompleted
    private void Animation_JustCompleted()
    {
        float startValue = 0;
        float targetValue = currentValue;

        AnimateText(startValue, targetValue, AnimationEnd_JustCompleted);
        
    }

    private void AnimationEnd_JustCompleted()
    {
        medal.Animation_JustCompleted();
        codeAnimator.StartAnimacion(this,
            x =>
            {
                currentText.color = Color.Lerp(Color.white, cCompleted, x);
                targetText.color = Color.Lerp(Color.white, cCompleted, x);
            }, DeltaTimeType.deltaTime, AnimationType.Simple, null, .5f);
        OnEnd?.Invoke();
    }
    #endregion
}
