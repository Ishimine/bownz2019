﻿using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class AutoPositionRoofAndFloor : MonoBehaviour
{

    public Transform terrain;

    public Transform roof;
    public Transform floor;


    public float roofOffset;
    public float floorOffset;

    private void OnValidate()
    {

    }

    [Button]
    private void ResetRoofAndFlor()
    {
roof.transform.position = Vector3.zero;
        floor.transform.position = Vector3.zero;
    }


[Button]
    public void RepositionRoofAndFloor()
    {

        ResetRoofAndFlor();

        List<Collider2D> boxColliders = new List<Collider2D>();
        terrain.GetComponentsInChildren(boxColliders);
        List<Vector2> vPoints = new List<Vector2>();

        foreach (Collider2D col in boxColliders)
        {
            if (col.transform.parent.gameObject.tag == "Roof" || col.transform.gameObject.layer.Equals(LayerMask.NameToLayer("Obstacle")))
            {
                Debug.Log(col.transform.parent.name + " Continued");
                continue;
            }
            else
            {
                Debug.Log("Name: " + col.transform.parent.gameObject);
            Bounds bounds = col.bounds;
            vPoints.Add(bounds.min);
            vPoints.Add(bounds.center + new Vector3(bounds.extents.x, -bounds.extents.y));
            vPoints.Add(col.bounds.max);
            vPoints.Add(col.bounds.center + new Vector3(-bounds.extents.x, bounds.extents.y));
            }
        }
                                 
        Bounds b = new Bounds();
        foreach (Vector2 point in vPoints)  
        {
            b.Encapsulate(point);
        }

        roof.transform.position = new Vector3(0,b.max.y + roofOffset);
        floor.transform.position = new Vector3(0,b.min.y + floorOffset);
    }
}
