﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Sirenix.OdinInspector;

public class UI_ObjectiveDisplay : MonoBehaviour
{
    [SerializeField] private UI_ObjectiveStar objectiveStar;
    [SerializeField] private UI_ObjectiveText objectiveText;

    public string format = "0.";

    private Action onAnimationDone;
    public Action OnAnimationDone           { get { return onAnimationDone; } set { onAnimationDone = value; } }

    private Action onSucessAnimationDone;
    public Action OnSucessAnimationDone     { get { return onSucessAnimationDone; } set { onSucessAnimationDone = value; } }

    private Action onFailAnimationDone;
    public Action OnFailAnimationDone       { get { return onFailAnimationDone; } set { onFailAnimationDone = value; } }


    private CodeAnimator codeAnimator = new CodeAnimator();
    public AnimationCurve curveIn;
    public float durationIn = .3f;

    public AnimationCurve curveOut;
    public float durationOut = .3f;

    public void Initialize()
    {
        objectiveStar.transform.localScale = Vector3.zero;
        objectiveText.transform.localScale = Vector3.zero;
    }

    [Button]
    public void StartAnimation(float previousValue, float currentValue, float targetValue)
    {
        objectiveStar.Initialize(false);
        objectiveText.Initialize(false, 0.ToString(format), targetValue.ToString(format));

        codeAnimator.StartAnimacion(this, x =>
        {
            transform.localScale = Vector3.one * x;
        }, DeltaTimeType.deltaTime, AnimationType.Simple, curveIn, ()=> SelectAnimation(previousValue, currentValue, targetValue) , durationIn);
    }

    private void SelectAnimation(float previousValue, float currentValue, float targetValue)
    {
        if (previousValue <= targetValue) //El objetivo fue previamente cumplido
        {
            Animation_AlreadyCompleted((currentValue<previousValue)?currentValue:previousValue, targetValue);
        }
        else if (currentValue <= targetValue) //El objetivo acaba de ser cumplido
        {
            Animation_JustCompleted(currentValue, targetValue);
        }
        else    //El objetivo NO fue cumplido
        {
            Animation_NotCompleted(currentValue, targetValue);
        }
    }

    private void Animation_AlreadyCompleted(float currentValue, float targetValue)
    {
        objectiveText.Initialize(true, "0", targetValue.ToString(format));
        objectiveStar.Initialize(true);
        objectiveStar.OnAnimationDone?.Invoke();
        SucessDone();
    }

    private void Animation_JustCompleted(float currentValue, float targetValue)
    {
        objectiveStar.Initialize(false);
        objectiveStar.OnAnimationDone += SucessDone;
        objectiveText.Initialize(false, "0", targetValue.ToString(format));
        objectiveText.StartAnimation(currentValue, format, () => objectiveStar.TransitionToCompleted(), targetValue.ToString(format));
    }

    private void Animation_NotCompleted(float currentValue, float targetValue)
    {
        objectiveStar.Initialize(false);
        objectiveStar.OnAnimationDone += FailDone;
        objectiveText.Initialize(false, "0", targetValue.ToString(format));
        objectiveText.StartAnimation(currentValue, format, ()=> objectiveStar.TransitionToFail(), targetValue.ToString(format));
        //objectiveText.StartAnimation(currentValue, format, () => objectiveStar.Dissapear(true), targetValue.ToString(format));
    }

    private void SucessDone()
    {
        objectiveStar.OnAnimationDone -= SucessDone;
        OnSucessAnimationDone?.Invoke();
        Done();
    }

    private void FailDone()
    {
        objectiveStar.OnAnimationDone -= FailDone;
        OnFailAnimationDone?.Invoke();
        Done();
    }

    private void Done()
    {
        Vector3 startSize = transform.localScale;
        codeAnimator.StartAnimacion(this,
            x =>
            {
                transform.localScale = Vector3.LerpUnclamped(startSize, Vector3.zero, x);
            }, DeltaTimeType.deltaTime, AnimationType.Simple, curveOut,
            ()=>
            {
                objectiveText.gameObject.SetActive(false);
                OnAnimationDone?.Invoke();
            },durationOut);
    }
}
