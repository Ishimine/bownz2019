﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnEnableReporter : MonoBehaviour
{
    public Reporter reporter;


    private void OnEnable()
    {
        reporter.SetEnable(true);
    }
    private void OnDisable()
    {
        reporter.SetEnable(false);
    }
}
