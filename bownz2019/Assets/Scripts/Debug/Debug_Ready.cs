﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Debug_Ready : MonoBehaviour
{
    public Image img;
    public Text txt;

    public void SetReady(bool value)
    {
        if(value)
        {
            img.color = Color.green;
            txt.text = "READY";
        }
        else
        {
            img.color = Color.grey;
            txt.text = "NOT READY";
        }
    }
}
