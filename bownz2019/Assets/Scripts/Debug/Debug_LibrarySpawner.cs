﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class Debug_LibrarySpawner : MonoBehaviour
{
    
    [Button]
    public void InstantiateFromLibrary(string id)
    {
        Instantiate(GameObjectLibrary.Get(id));
    }
}
