﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Debug_IsInterstitialReady : MonoBehaviour
{
    public Debug_Ready debug_Ready;

    private void OnEnable()
    {
        AdsManager.InterstitialLoadedEvent -= OnInterstitialLoadedEvent;
        AdsManager.InterstitialLoadedEvent += OnInterstitialLoadedEvent;

        AdsManager.BannerFailedToLoadEvent -= OnBannerFailedToLoadEvent;
        AdsManager.BannerFailedToLoadEvent += OnBannerFailedToLoadEvent;

        debug_Ready.SetReady(AdsManager.IsVideoReady());
    }

    private void OnDisable()
    {
        AdsManager.BannerFailedToLoadEvent -= OnBannerFailedToLoadEvent;
        AdsManager.InterstitialLoadedEvent -= OnInterstitialLoadedEvent;
    }

    private void OnInterstitialLoadedEvent()
    {
        debug_Ready.SetReady(true);
    }

    private void OnBannerFailedToLoadEvent()
    {
        debug_Ready.SetReady(false);
    }
}