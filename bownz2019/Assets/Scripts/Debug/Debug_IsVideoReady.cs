﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Debug_IsVideoReady : MonoBehaviour
{
    public Debug_Ready debug_Ready;

    private void OnEnable()
    {
        AdsManager.BannerLoadedEvent -= OnBannerLoadedEvent;
        AdsManager.BannerLoadedEvent += OnBannerLoadedEvent;

        AdsManager.BannerFailedToLoadEvent -= OnBannerFailedToLoadEvent;
        AdsManager.BannerFailedToLoadEvent += OnBannerFailedToLoadEvent;

        debug_Ready.SetReady(AdsManager.IsVideoReady());
    }

    private void OnDisable()
    {
        AdsManager.BannerFailedToLoadEvent -= OnBannerFailedToLoadEvent;
        AdsManager.BannerLoadedEvent -= OnBannerLoadedEvent;
    }

    private void OnBannerLoadedEvent()
    {
        debug_Ready.SetReady(true);
    }

    private void OnBannerFailedToLoadEvent()
    {
        debug_Ready.SetReady(false);
    }
}
