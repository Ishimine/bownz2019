﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CoinSpawnConfig : MonoBehaviour
{
    public abstract void Reset();
    public abstract CoinType GetCoinType(StarCoin starCoin);
}