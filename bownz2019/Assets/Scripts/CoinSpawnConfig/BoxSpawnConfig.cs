﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxSpawnConfig : CoinSpawnConfig
{
    public int showGoldenEvery = 5;
    int counter;
    public override void Reset()
    {
        counter = 0;
        showGoldenEvery = 5;
    }

    public override CoinType GetCoinType(StarCoin starCoin)
    {
        counter++;
        if (counter == showGoldenEvery)
        {
            showGoldenEvery++;
            counter = 0;
            return new GoldenCoin(starCoin);
        }
        else
            return new NormalCoin(starCoin);
    }
}