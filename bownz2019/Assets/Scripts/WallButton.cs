﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;
using Sirenix.OdinInspector;

public class WallButton : MonoBehaviour
{
    public Action onPressed;

    public Transform buttonRoot;

    public GameObject[] targets;

    private CodeAnimator codeAnimator = new CodeAnimator();

    public float buttonHeight = 1;

    [SerializeField] private float animationDuration = .3f;
    [SerializeField] private AnimationCurve curve;

    private List<IPressed> currentTargets = new List<IPressed>();
    public List<IndirectLine> indirectLines;
    public GameObject indirectLinePrefab;


    public bool useAutoline = false;

    private void Start()
    {
        Initialize();
    }
    [Button]
    public void Initialize()
    {
        for (int i = 0; i < targets.Length; i++)
        {
            IPressed current = targets[i].GetComponent<IPressed>();
            if (current != null) currentTargets.Add(current);
            
            
        }

        if (useAutoline)
        {
            InstantiateLines();
        }
    }

    [Button]
    private void InstantiateLines()
    {
        DestroyCurrentLines();
        for (int i = 0; i < targets.Length; i++)
        {
            IndirectLine line = Instantiate(indirectLinePrefab, transform).GetComponent<IndirectLine>();
            indirectLines.Add(line);
            line.SetStartAndEnd(transform, targets[i].transform);
            if (useAutoline) line.CalculateWaypoints();
        }
    }

    [Button]
    public void CalculateLines()
    {
        for (int i = 0; i < indirectLines.Count; i++)
        {
            indirectLines[i].SetStartAndEnd(transform, targets[i].transform);
             indirectLines[i].CalculateWaypoints();
        }
    }

    private void DestroyCurrentLines()
    {
        for (int i = indirectLines.Count-1; i >= 0; i--)
        {
            if(!indirectLines[i]) continue;

            if(Application.isPlaying)
                Destroy(indirectLines[i].gameObject);
            else
                DestroyImmediate(indirectLines[i].gameObject);
        }
        indirectLines = new List<IndirectLine>();
    }

    public void RegisterTarget(IPressed pressed)
    {
        if (currentTargets.Contains(pressed)) Debug.LogError("El elemento YA existe en la lista");
        currentTargets.Add(pressed);
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.isTrigger || !collision.attachedRigidbody.gameObject.tag.Equals("Player")) return;

        Pressed();
    }

    [Button]
    public void Pressed()
    {
        foreach (var item in currentTargets) item.Pressed();

        codeAnimator.StartAnimacion(this,
            x =>
            {
                buttonRoot.localScale = new Vector3(1, buttonHeight - x , 1);
            }, DeltaTimeType.deltaTime, AnimationType.Simple, curve, animationDuration);
    }
}

public interface IPressed
{
    void Pressed();
}
