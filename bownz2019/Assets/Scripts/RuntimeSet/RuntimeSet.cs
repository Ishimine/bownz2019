﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu]
public class RuntimeSet : ScriptableObject
{
    [ShowInInspector,ReadOnly]
    private HashSet<GameObject> elements = new HashSet<GameObject>();

    public void Add(GameObject nElement)
    {
        if(!elements.Contains(nElement))
            elements.Add(nElement);
    }

    public void Remove(GameObject oElement)
    {
        if(elements.Contains(oElement))
            elements.Remove(oElement);
    }

    public bool Contains(GameObject oElement)
    {
       return elements.Contains(oElement);
    }
}
