﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RuntimeAutoAdd : MonoBehaviour
{
    public RuntimeSet[] runtimeSets;

    public void Add()
    {
        foreach (var item in runtimeSets)
            item.Add(gameObject);
    }

    public void Remove()
    {
        foreach (var item in runtimeSets)
            item.Remove(gameObject);
    }

    private void OnEnable()
    {
        Add();
    }

    private void OnDisable()
    {
        Remove();
    }
}
