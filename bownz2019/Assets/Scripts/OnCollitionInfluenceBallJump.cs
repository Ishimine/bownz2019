﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnCollitionInfluenceBallJump : MonoBehaviour
{
    [SerializeField] LineRenderer lineRenderer;
    [SerializeField] SpriteRenderer renderer;
    public Transform targetPointMark;
    public float targetHeight = 10;
    public Collider2D col;

    private Vector3 targetPoint;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            ApplyJump(other.GetComponent<Rigidbody2D>());
        }
    }

    private void Awake()
    {
        UpdateTargetPoint();
    }

    private void OnValidate()
    {
        UpdateTargetPoint();
    }

    private void UpdateTargetPoint()
    {
        targetPoint = transform.position + transform.up * targetHeight;
        targetPointMark.transform.position = targetPoint;
        UpdateLineRenderer();
    }

    private void UpdateLineRenderer()
    {
        lineRenderer.positionCount = 2;
        Vector3[] vectors = new Vector3[2];
        vectors[0] = Vector3.zero;
        vectors[1] = transform.InverseTransformPoint(targetPoint);
        lineRenderer.SetPositions(vectors);
        lineRenderer.widthMultiplier = renderer.size.x;
    }

    private void ApplyJump(Rigidbody2D oRb)
    {
        UpdateTargetPoint();
        Transform transform1 = transform;
        Vector2 dir = oRb.transform.position.GetDirection(targetPoint);
        float gravity = (Physics2D.gravity.y * oRb.gravityScale);
        Vector2 velocity =  Mathf.Sqrt(Math.Abs(gravity * targetHeight*2)) * dir;
        oRb.velocity = velocity;
    }

    private void OnDrawGizmos()
    {
        Transform transform1 = transform;
        Vector3 position = transform1.position;
        Vector3 up = transform1.up;
        DrawArrow.ForDebug(position, position + up * targetHeight);
        Gizmos.DrawWireSphere(position + up * targetHeight,.3f);
    }
}
