﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DouglasPeuckerLineApproximation : MonoBehaviour
{
    /// <summary>
    /// Uses the Douglas Peucker algorithm to reduce the number of points.
    /// </summary>
    /// <param name="Points">The points.</param>
    /// <param name="Tolerance">The tolerance.</param>
    /// <returns></returns>
    public static List<Vector2> DouglasPeuckerReduction (List<Vector2> Points, float tolerance)
    {
        if (Points == null || Points.Count < 3)
            return Points;

        int firstPoint = 0;
        int lastPoint = Points.Count - 1;
        List<int> pointIndexsToKeep = new List<int>();

        //Add the first and last index to the keepers
        pointIndexsToKeep.Add(firstPoint);
        pointIndexsToKeep.Add(lastPoint);

        //The first and the last point cannot be the same
        while (Points[firstPoint].Equals(Points[lastPoint]))
        {
            lastPoint--;
        }

        DouglasPeuckerReduction(Points, firstPoint, lastPoint, tolerance, ref pointIndexsToKeep);

        List<Vector2> returnPoints = new List<Vector2>();
        pointIndexsToKeep.Sort();
        foreach (int index in pointIndexsToKeep)
        {
            returnPoints.Add(Points[index]);
        }

        return returnPoints;
    }

    /// <summary>
    /// Douglases the peucker reduction.
    /// </summary>
    /// <param name="points">The points.</param>
    /// <param name="firstPoint">The first point.</param>
    /// <param name="lastPoint">The last point.</param>
    /// <param name="tolerance">The tolerance.</param>
    /// <param name="pointIndexsToKeep">The point index to keep.</param>
    private static void DouglasPeuckerReduction(List<Vector2> points, int firstPoint, int lastPoint, float tolerance, ref List<int> pointIndexsToKeep)
    {
        float maxDistance = 0;
        int indexFarthest = 0;

        for (int index = firstPoint; index < lastPoint; index++)
        {
            float distance = PerpendicularDistance (points[firstPoint], points[lastPoint], points[index]);
            if (distance > maxDistance)
            {
                maxDistance = distance;
                indexFarthest = index;
            }
        }

        if (maxDistance > tolerance && indexFarthest != 0)
        {
            //Add the largest point that exceeds the tolerance
            pointIndexsToKeep.Add(indexFarthest);

            DouglasPeuckerReduction(points, firstPoint,
            indexFarthest, tolerance, ref pointIndexsToKeep);
            DouglasPeuckerReduction(points, indexFarthest,
            lastPoint, tolerance, ref pointIndexsToKeep);
        }
    }

    /// <summary>
    /// The distance of a point from a line made from point1 and point2.
    /// </summary>
    /// <param name="pt1">The PT1.</param>
    /// <param name="pt2">The PT2.</param>
    /// <param name="p">The p.</param>
    /// <returns></returns>
    public static float PerpendicularDistance
        (Vector2 Point1, Vector2 Point2, Vector2 Point)
    {
        //Area = |(1/2)(x1y2 + x2y3 + x3y1 - x2y1 - x3y2 - x1y3)|   *Area of triangle
        //Base = v((x1-x2)²+(x1-x2)²)                               *Base of Triangle*
        //Area = .5*Base*H                                          *Solve for height
        //Height = Area/.5/Base

        float area = Mathf.Abs(.5f * (Point1.x * Point2.y + Point2.x *
        Point.y + Point.x * Point1.y - Point2.x * Point1.y - Point.x *
        Point2.y - Point1.x * Point.y));
        float bottom = Mathf.Sqrt(Mathf.Pow(Point1.x - Point2.x, 2) +
        Mathf.Pow(Point1.y - Point2.y, 2));
        float height = area / bottom * 2;

        return height;

        //Another option
        //float A = Point.x - Point1.x;
        //float B = Point.y - Point1.y;
        //float C = Point2.x - Point1.x;
        //float D = Point2.y - Point1.y;

        //float dot = A * C + B * D;
        //float len_sq = C * C + D * D;
        //float param = dot / len_sq;

        //float xx, yy;

        //if (param < 0)
        //{
        //    xx = Point1.x;
        //    yy = Point1.y;
        //}
        //else if (param > 1)
        //{
        //    xx = Point2.x;
        //    yy = Point2.y;
        //}
        //else
        //{
        //    xx = Point1.x + param * C;
        //    yy = Point1.y + param * D;
        //}

        //float d = DistanceBetweenOn2DPlane(Point, new Point(xx, yy));
    }
}
