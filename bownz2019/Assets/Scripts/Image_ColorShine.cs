﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Image_ColorShine : MonoBehaviour
{
    public Image img;
    public AnimationCurve curve;
    public float duration = .5f;

    private CodeAnimator codeAnimator = new CodeAnimator();

    public void SetColor(Color color)
    {
        img.color = color;
    }

    public void ShineTo(Color targetColor)
    {
        ShineTo(targetColor, Color.white);
    }

    public void ShineTo(Color targetColor,Color shineColor)
    {
        codeAnimator.StartAnimacion(this,
            x =>
            {
                img.color = Color.Lerp(shineColor, targetColor, x);
            }, DeltaTimeType.deltaTime, AnimationType.Simple, curve,
            () =>
            {
                img.color = Color.clear;
            }, duration);
    }
}
