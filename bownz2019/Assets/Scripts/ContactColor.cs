﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContactColor : MonoBehaviour
{
    [SerializeField] private Color color;
    public Color Color
    {
        get => color;
        set { color = value; }
    }
}
