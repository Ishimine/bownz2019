﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Canvas_LevelSelection_Adventure : MonoBehaviour
{
    public GameObject buttonPrefab;
    public Transform levelsGrid;
    public UI_Button_Id[] buttons;

    private string gameModeId;
    public string GameModeId
    {
        get { return gameModeId; }
        set
        {
            gameModeId = value;
            UpdateGameModeId();
        }
    }

    private void Awake()
    {
        Initialize();
    }

    public void UpdateGameModeId()
    {
        foreach (UI_Button_Id b in buttons)
        {
        }
    }

    public void Initialize()
    {
        buttons = new UI_Button_Id[AdventureLevelCatalog.AdventureLevels.Length];
        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i] = Instantiate(buttonPrefab, levelsGrid).GetComponent<UI_Button_Id>();
          
        }
    }


    public void ButtonPresed(int index)
    {
        this.PostNotification(Notifications.LevelButtonPressed, index);
    }
}
