﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Canvas_PlayGames : MonoBehaviour
{
    public UI_ConteinerOpenClose conteiner;
    private void OnEnable()
    {
        this.AddObserver(OnShowBar, Notifications.ShowPlayGamesBar);
    }

    private void OnDisable()
    {
        this.RemoveObserver(OnShowBar, Notifications.ShowPlayGamesBar);
    }

    private void OnShowBar(object arg1, object arg2)
    {
        bool value = (bool) arg2;
        conteiner.OpenOrClose(value);
    }

    private void Start()
    {
        LogIn();
    }

    public void LogIn()
    {
        SocialPlayManager.Initialize();
    }

}
