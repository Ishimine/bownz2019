﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
public class VirtualCameraAspectFitter : CameraAspectFitter
{
    public CinemachineVirtualCamera virtualCamera;

    public override void RefreshCamSize(float nSize)
    {
        base.RefreshCamSize(nSize);
        virtualCamera.m_Lens.OrthographicSize = nSize;
    }
}
