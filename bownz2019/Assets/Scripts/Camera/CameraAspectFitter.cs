﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;
using Cinemachine;

public class CameraAspectFitter : MonoBehaviour
{
    public ScreenOrientation defaultOrientation = ScreenOrientation.Portrait;
    public bool changeGameAspectBasedOnPlayArea = true;

    Vector2 camSize;

    public float offsetProportion = 1;

    public bool updatePosition = true;

    [Range(-.5f,.5f), SerializeField, OnValueChanged("Start")]private float verticalDisplacement = 0;
    [SerializeField] private float difBetweenCamHeightAndPlayArea;

    private void Awake()
    {
        Screen.autorotateToLandscapeLeft = false;
        Screen.autorotateToLandscapeRight = false;
        Screen.autorotateToPortrait = false;
        Screen.autorotateToPortraitUpsideDown = false;
        Screen.orientation = defaultOrientation;
    }

    private void OnEnable()
    {
        ManualUpdateCamSize();
        PlayArea.AreaChange -= OnPlayAreaChangeResponse;
        PlayArea.AreaChange += OnPlayAreaChangeResponse;
        OnPlayAreaChangeResponse(PlayArea.Area);
    }

    private void OnDisable()
    {
        PlayArea.AreaChange -= OnPlayAreaChangeResponse;
    }

    private void OnPlayAreaChangeResponse(Rect area)
    {
        if (changeGameAspectBasedOnPlayArea)
            RefreshCameraAspectRatio(area.size);
        ManualUpdateCamSize();
        RefreshCamPosition(area);
    }

    public void Start()
    {
        ManualUpdateCamSize();
        RefreshCamPosition(PlayArea.Area);
    }

    [Button]
    public void RefreshCamPosition(Rect area)
    {
        if(updatePosition)
            MainCameraManager.Current.transform.position = (Vector3)area.center + Vector3.up * difBetweenCamHeightAndPlayArea * verticalDisplacement + new Vector3(0, 0, transform.position.z);
    }

    public virtual void RefreshCamSize(float nSize)
    {
        MainCameraManager.SetSize(nSize * offsetProportion);
        difBetweenCamHeightAndPlayArea = (MainCameraManager.Current.orthographicSize*2) - PlayArea.Area.height;
    }

    [ExecuteInEditMode]
    private void Update()
    {
        //if (Application.isEditor && !Application.isPlaying) ManualUpdateCamSize();
    }

    [Button]
    public float ManualUpdateCamSize()
    {
        CalculateScreenSize(PlayArea.Area.size);
        RefreshCamSize(camSize.y / 2);
        RefreshCamPosition(PlayArea.Area);
        return camSize.y / 2;
    }

    public Vector2 CalculateScreenSize(Vector2 playableArea)
    {
        MainCameraManager.Current.orthographicSize = 100;
        Vector2 ortographicSize = MainCameraManager.Current.GetOrtographicSize();
        float scale = Mathf.Max(playableArea.x / ortographicSize.x, playableArea.y / ortographicSize.y);
        camSize.y = ortographicSize.y * scale;
        camSize.x = ortographicSize.x * scale;
        return camSize;
    }

    public void RefreshCameraAspectRatio(Vector2 size)
    {
        //Debug.Log("Size:" + size);
        if (size.x > size.y)
        {
            //Debug.Log("Landscape");
            Screen.orientation = ScreenOrientation.Landscape;
        }
        else
        {
            //Debug.Log("Portrait");
            Screen.orientation = ScreenOrientation.Portrait;
        }

     /*   Screen.orientation = (size.x > size.y) ? ScreenOrientation.Landscape : ScreenOrientation.Portrait;
        Debug.Log("Screen.Orientation = " + Screen.orientation);*/
    }
}