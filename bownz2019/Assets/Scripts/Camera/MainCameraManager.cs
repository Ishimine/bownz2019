﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Sirenix.OdinInspector;
using Cinemachine;

public class MainCameraManager : MonoSingleton<MainCameraManager>
{
    public Camera staticCamera;
    public Camera followCamera;

    public override MainCameraManager Myself => this;

    public BoxCollider2D col;
    public Camera currentCamera;

    public static Camera Current
    {
        get => Instance.currentCamera;
    }

    public static BoxCollider2D CurrentCollider
    {
        get => Instance.col;
    }

    private static Action<Vector2> onCameraSizeChange = null;
    public static Action<Vector2> OnCameraSizeChange
    {
        get { return onCameraSizeChange; }
        set { onCameraSizeChange = value; }
    }

    public static void SetSize(float nSize)
    {
        Current.orthographicSize = nSize;
        OnCameraSizeChange?.Invoke(Current.GetOrtographicSize());
    }

    private FollowCamera.FollowType cFollowType;
    private Transform cTarget;

    public void Pause()
    {
        cFollowType = FollowCamera.GetFollowType();
        cTarget = FollowCamera.GetTarget();
        FollowCamera.SetFollow(null, cFollowType);
    }

    public void Resume()
    {
        FollowCamera.SetFollow(cTarget, cFollowType);
    }

    public void SwitchToStaticCamera()
    {
        SwitchToCamera(staticCamera);
    }

    public void SwitchToFollowCamera(Transform target, FollowCamera.FollowType followType)
    {
        SwitchToCamera(followCamera);
        FollowCamera.SetFollow(target, followType);
    }

    private void SwitchToCamera(Camera camera)
    {
        if (currentCamera != null) currentCamera.gameObject.SetActive(false);
        currentCamera = camera;
        col = camera.gameObject.GetComponent<BoxCollider2D>();
        currentCamera.gameObject.SetActive(true);
    }

    public void SetColliderConfiner(PolygonCollider2D col)
    {
        FollowCamera.Instance.CurrentConfiner.m_BoundingShape2D = col;
        FollowCamera.Instance.CurrentConfiner.InvalidatePathCache();
    }

    public static void SetPosition(Vector3 pos)
    {
        Current.transform.position = new Vector3(pos.x, pos.y, Current.transform.position.z);
    }

    private void OnEnable()
    {
        
    }

    private void OnDisable()
    {
        
    }
}
