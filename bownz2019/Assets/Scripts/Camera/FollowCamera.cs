﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class FollowCamera : MonoSingleton<FollowCamera>
{
    public override FollowCamera Myself => this;
    public enum FollowType {Portrait, Landscape };

    private CinemachineVirtualCamera currentCamera = null;
    [SerializeField]  private CinemachineVirtualCamera vCam_Portrait = null;
    [SerializeField]  private CinemachineVirtualCamera vCam_Landscape = null;

    public CinemachineConfiner CurrentConfiner => CurrentCamera.GetComponent<CinemachineConfiner>();
    public CinemachineVirtualCamera CurrentCamera => currentCamera;
    public CameraAspectFitter aspectFitter;

    public static Transform GetTarget()
    {
        return Instance.CurrentCamera.Follow;
    }

    public static FollowType GetFollowType()
    {
        return (Instance.CurrentCamera == Instance.vCam_Landscape)?FollowType.Landscape:FollowType.Portrait;

    }
    public static void SetFollow(Transform target, FollowType followType)
    {
        Instance.vCam_Landscape.gameObject.SetActive(followType == FollowType.Landscape);
        Instance.vCam_Portrait.gameObject.SetActive(followType == FollowType.Portrait);

        Instance.currentCamera = (followType == FollowType.Landscape) ? Instance.vCam_Landscape : Instance.vCam_Portrait;
        SetFollowTarget(target);
    }

    private static void SetFollowTarget(Transform target)
    {
        var camera = Instance.CurrentCamera;
        camera.Follow = target;
        camera.gameObject.SetActive(true);

        camera.m_Lens.OrthographicSize = Instance.aspectFitter.ManualUpdateCamSize();
    }

    private void OnDisable()
    {
        vCam_Portrait.gameObject.SetActive(false);
        vCam_Landscape.gameObject.SetActive(false);
    }

}
