﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;

public class StarTargetFX : MonoBehaviour
{
    [SerializeField] private AnimationCurve curveA = new AnimationCurve();
    [SerializeField] private AnimationCurve curveB = new AnimationCurve();
    [SerializeField] private float frecuency = 1;

    [SerializeField] private CodeAnimatorCurve curveIn = new CodeAnimatorCurve();
    [SerializeField] private CodeAnimatorCurve curveOut = new CodeAnimatorCurve();

    [SerializeField] private SpriteRenderer renderA = null;
    [SerializeField] private SpriteRenderer renderB = null;

    [SerializeField, Range(0,2)] private float sizeAmmount = 1.5f;

    [SerializeField] private float animationOffset = .5f;
    private CodeAnimator codeAnimator = new CodeAnimator();

    private void Awake()
    {
        transform.localScale = Vector3.zero;
        transform.position = StarCoin.DefaultPosition;
    }

  /*  private void OnEnable()
    {
        //this.AddObserver(OnGameOnResponse, Notifications.GameMode_Go);
    }

    private void OnDisable()
    {
        //this.RemoveObserver(OnGameOnResponse, Notifications.GameMode_Go);
    }

    private void OnGameOnResponse(object sender, object value)
    {
        //Show();
    }*/

    [Button]
    public void Show()
    {
        Vector3 startScale = transform.localScale;
        Vector3 difference = Vector3.one - startScale;
        Color startColorA = renderA.color;
        Color startColorB = renderB.color;
        codeAnimator.StartAnimacion(this,
            x =>
            {
                renderA.color = Color.Lerp(startColorA, Color.white, x);
                renderB.color = Color.Lerp(startColorB, Color.white, x);
                transform.localScale = startScale + difference * x;
            }, DeltaTimeType.deltaTime, AnimationType.Simple, curveIn.Curve, StartSizeAnimation, curveIn.Time);
    }

    [Button]
    public void StartSizeAnimation()
    {
        float time = 0;
        codeAnimator.StartInfiniteAnimation(this,
            delta =>
            {
                time += delta * frecuency;
                renderA.transform.localScale = Vector3.one * (1 + sizeAmmount * curveA.Evaluate(time));
                renderB.transform.localScale = Vector3.one * (1 + sizeAmmount * curveB.Evaluate(time + animationOffset));
            }, DeltaTimeType.deltaTime);
    }

    [Button]
    public void Hide()
    {
        Vector3 startScale = transform.localScale;
        Vector3 difference = startScale;
        Color startColorA = renderA.color;
        Color startColorB = renderB.color;
        codeAnimator.StartAnimacion(this,
           x =>
           {
               renderA.color = Color.Lerp(startColorA, Color.clear, x);
               renderB.color = Color.Lerp(startColorB, Color.clear, x);
               transform.localScale = startScale - difference * x;
           }, DeltaTimeType.deltaTime, AnimationType.Simple, curveOut.Curve, curveOut.Time);
    }
}
