﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartAsNormalCoin : StartAsCoin
{
    protected override void Show()
    {
        starCoin.LifeSpan = float.MaxValue;
        starCoin.Show(true, new NormalCoin(starCoin));
        starCoin.LifeSpan = float.MaxValue;
    }
}

