﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartAsSlowMotionCoin : StartAsCoin
{
    protected override void Show()
    {
        starCoin.LifeSpan = float.MaxValue;
        starCoin.Show(true, new SlowMotionCoin(starCoin));
        starCoin.LifeSpan = float.MaxValue;
    }
}
