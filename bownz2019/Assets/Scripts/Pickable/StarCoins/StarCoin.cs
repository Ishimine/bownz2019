﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;

public class StarCoin : MonoBehaviour
{
    [TabGroup("Dependencies"), SerializeField]  private SpriteRenderer render_A;
    public SpriteRenderer Render_A => render_A;
    [TabGroup("Dependencies"), SerializeField] private SpriteRenderer render_B;
    public SpriteRenderer Render_B => render_B;
    [TabGroup("Dependencies"), SerializeField] private SpriteRenderer render_C;
    public SpriteRenderer Render_C => render_C;

    private GameObject sparkles = null;

    public GameObject pickUpParticlesPrefab;

    public static Action<StarCoin> OnAnyHideDone { get; set; }

    public Action<StarCoin> OnHideDone { get; set; }

    public static Action<StarCoin> OnAnyHideStart { get; set; }

    public Action<StarCoin> OnHideStart { get; set; }

    public static Action<StarCoin> OnAnyPickedUp { get; set; }

    public Action<StarCoin> OnPickedUp { get; set; }

    public static Action<StarCoin> OnAskForNext { get; set; }

    private readonly CodeAnimator floatAnimator = new CodeAnimator();
    private readonly CodeAnimator codeAnimator = new CodeAnimator();

    public bool showOnStart = false;

    [TabGroup("Audio")] public SoundPlay sFX_PickedUpGoldenStar = null;
    [TabGroup("Audio")] public SoundPlayGroup sFX_PickedUpStar = null;

    [TabGroup("Animation")] [SerializeField] private AnimationCurve floatAnimationCurve = null;
    [TabGroup("Animation")] [SerializeField] private float animationFrecuency = 1;
    [TabGroup("Animation")] [SerializeField] private float floatHeight = .5f;

    public CodeAnimatorCurve curveIn = null;
    public CodeAnimatorCurve curveOut = null;

    [SerializeField] private Pickable pickable = null;
    public Pickable Pickable => pickable;

    [ShowInInspector, ReadOnly] string type => (currentType == null)?"None":currentType.ToString();

    private CoinType currentType;
    public CoinType CoinType => currentType;
        
    [SerializeField] private float lifeSpan = 3;
    public float LifeSpan { get => lifeSpan; set => lifeSpan = value; }

    private Action<float, float> onRemainingTimeUpdate;
    public Action<float, float> OnRemainingTimeUpdate
    {
        get { return onRemainingTimeUpdate; }
        set { onRemainingTimeUpdate = value; }
    }

    
    public readonly static Vector3 DefaultPosition = new Vector3(0, 5);

    /// <summary>
    /// Comportamientos de la estrella
    /// Aparece y emite un sonido
    /// Desaparece y emite un sonido
    /// Es agarrada, emite un sonido y desaparece
    /// Flota
    /// </summary>
    bool isHiding = false;

    private Vector2 startPosition;

    public void Awake()
    {
        transform.localScale = Vector3.zero;
        //codeAnimator.Stop(this);
        pickable.OnPicked -= PickedUp;
        pickable.OnPicked += PickedUp;
    }
    
    public void SwitchType(CoinType newType)
    {
        currentType?.Exit();
        currentType = newType;
        currentType?.Enter();
    }
    private void Start()
    {
        startPosition = transform.position;
    }

    internal void StopFloating()
    {
        floatAnimator.Stop(this);
    }

    [Button]
    public void StartFloating()
    {
        float time = 0;
        float speed = animationFrecuency * Mathf.PI;
        Vector3 height = Vector3.up * floatHeight;

        Vector3 startPosition = transform.localPosition;
        floatAnimator.StartInfiniteAnimation(this,
            deltaTime =>
            {   
                time += deltaTime * speed;
                OnRemainingTimeUpdate?.Invoke(time, lifeSpan);
                //transform.Translate(Mathf.Sin(time) * deltaTime * height, Space.Self);
                transform.localPosition = startPosition + height * Mathf.Sin(Time.time);
                if (lifeSpan > 0 && lifeSpan < (time / speed))
                    Hide();
            }, DeltaTimeType.fixedDeltaTime);
    }

    [Button]
    public void Show(bool resetSize, CoinType pickedUpStrategy)
    {
        Debug.Log("Previous Strategy: " + currentType);
        if (pickedUpStrategy != null) SwitchType(pickedUpStrategy);
        isHiding = false;
        if (resetSize) transform.localScale = Vector3.zero;
        StartFloating();
        Vector3 startSize = transform.localScale;
        Vector3 differenceScale = Vector3.one - startSize;
        codeAnimator.StartAnimacion(this,
            x =>
            {
                transform.localScale = startSize + differenceScale * x;
            }, DeltaTimeType.deltaTime, AnimationType.Simple, curveIn.Curve, curveIn.Time);
    }


    public void Show(bool resetSize, CoinType pickedUpStrategy, Vector3 position)
    {
        startPosition = position;
        transform.position = position;
        Show(resetSize, pickedUpStrategy);
    }

    public void PickedUp(Pickable pickable, GameObject picker)
    {
        OnPickedUp?.Invoke(this);
        OnAnyPickedUp?.Invoke(this);
        currentType.Execute(picker);

        if (!isHiding)
            Hide();

        SpawnParticles();
    }

    private void SpawnParticles()
    {
        Instantiate(pickUpParticlesPrefab, null).transform.position = transform.position;
    }

    public void Hide()
    {
        if (sparkles != null)
            Destroy(sparkles);

        isHiding = true;
        OnHideStart?.Invoke(this);
        OnAnyHideStart?.Invoke(this);
        Vector3 startSize = transform.localScale;
        Vector3 differenceScale = startSize;
        codeAnimator.StartAnimacion(this,
          x =>
          {
              transform.localScale = startSize - differenceScale * x;
          }, DeltaTimeType.deltaTime, AnimationType.Simple, curveOut.Curve, HideDone, curveOut.Time);
        StopFloating();
    }

    private void HideDone()
    {
        OnHideDone?.Invoke(this);
        OnAnyHideDone?.Invoke(this);
        transform.position = startPosition;
        currentType.OnHideDone();

        SwitchType(new NullCoin(this));
    }

    private void OnEnable()
    {
        this.AddObserver(OnGameMode_EndResponse, Notifications.GameMode_End);

        currentType?.OnEnable();
    }

    private void OnDisable()
    {
        codeAnimator.Stop(this);
        floatAnimator.Stop(this);
        this.RemoveObserver(OnGameMode_EndResponse, Notifications.GameMode_End);
    }

    private void OnGameMode_EndResponse(object arg1, object arg2)
    {
        Hide();
    }

    [Button]
    public void AddSparks()
    {
        if(sparkles == null)
            sparkles = Instantiate(GameObjectLibrary.Get("Sparkle"), transform);
    }

    private void OnDestroy()
    {
        currentType?.Exit();
    }
}

public abstract class CoinType
{
    protected StarCoin owner;
    public StarCoin Owner => owner;

    public abstract Color FillColor { get; }

    public CoinType(StarCoin owner)
    {
        this.owner = owner;

        owner.Render_A.color = FillColor;
    }

    public virtual void OnHideDone() { }
    public Vector2 Position => owner.transform.position;
    public abstract void Execute(GameObject picker);

    public virtual void Enter()
    {
        owner.Pickable.Active = true;
    }
    public virtual void Exit() { }
    public virtual void OnEnable() { }
}

public class NullCoin : CoinType
{
    public NullCoin(StarCoin owner) : base(owner)
    {
    }

    public override void Enter()
    {
        base.Enter();
        owner.Pickable.Active = false;
    }

    public override Color FillColor => Color.clear;

    public override void Execute(GameObject picker)
    {
        Debug.LogWarning("Se ejecuto una  moneda NULA");
    }
}

public class NormalCoin : CoinType
{
    public override Color FillColor => ColorPallete.NormalCoin;

    public NormalCoin(StarCoin owner) : base(owner)
    {
        if(owner == null)
            Debug.LogError("Owner is null");
    }

    public override void Enter()
    {
        base.Enter();

        owner.Render_A.sprite = SpriteLibrary.Get("StarCenter");
        owner.Render_B.sprite = SpriteLibrary.Get("StarOutline");
     // owner.Render_A.sprite = SpriteLibrary.Get("Coin_Golden");
      //owner.Render_B.sprite = null;
        owner.Render_C.sprite = null;
    }

    public override void Execute(GameObject picker)
    {
        this.PostNotification(Notifications.BackgroundContactReaction, new BackgroundContactReactionData() {
            point = owner.transform.position,
            color = ColorPallete.BackgroundFxCoin,
            force = 2 });
        this.PostNotification(Notifications.OnCoinPickedUp, 0);
        owner.sFX_PickedUpStar.PlayRandom();
    }

    internal void SetActive()
    {
        owner.Pickable.Active = true;
    }
}

public class GoldenCoin : CoinType
{
    public override Color FillColor => ColorPallete.GoldenCoin;

    public GoldenCoin(StarCoin owner) : base(owner)
    {
    }

    public override void Enter()
    {
        base.Enter();
        owner.AddSparks();
        owner.Render_A.sprite = SpriteLibrary.Get("StarCenter");
        owner.Render_B.sprite = SpriteLibrary.Get("StarOutline");
        owner.Render_C.sprite = null;
    }

    public override void Execute(GameObject picker)
    {
        this.PostNotification(Notifications.BackgroundContactReaction, new BackgroundContactReactionData() {
            point = owner.transform.position,
            color = ColorPallete.BackgroundFxSuperCoin,
            force = 3 });
        this.PostNotification(Notifications.OnCoinPickedUp, 1);

        TimeManager._StartTimeShift(Settings.SlowMotionAmmount, Settings.SlowMotionDuration, false);
        owner.sFX_PickedUpGoldenStar.Play();
    }
}

public class SlowMotionCoin : CoinType
{
    public override Color FillColor => ColorPallete.GreenCoin;

    public SlowMotionCoin(StarCoin owner) : base(owner) { }

    public override void Enter()
    {
        base.Enter();
        owner.Render_A.sprite = SpriteLibrary.Get("StarCenter");
        owner.Render_B.sprite = SpriteLibrary.Get("StarOutline");
        owner.Render_C.sprite = null;
    }

    public override void Execute(GameObject picker)
    {
        

        this.PostNotification(Notifications.BackgroundContactReaction, new BackgroundContactReactionData() {
            point = owner.transform.position,
            color = ColorPallete.GreenBackgroundFx,
            force = 3 });
        this.PostNotification(Notifications.OnCoinPickedUp, 2);
        owner.sFX_PickedUpStar.PlayRandom();
        SlowMotionWhileSwipe_Modificator.Activate();
    }
}

public class RedCoin : CoinType
{
    public override Color FillColor => ColorPallete.RedCoin;

    public RedCoin(StarCoin owner) : base(owner)
    {
    }

    public override void Enter()
    {
        base.Enter();
        owner.Render_A.sprite = SpriteLibrary.Get("StarCenter");
        owner.Render_B.sprite = SpriteLibrary.Get("StarOutline");
        owner.Render_C.sprite = null;
    }

    public override void Execute(GameObject picker)
    {
        this.PostNotification(Notifications.BackgroundContactReaction, new BackgroundContactReactionData() {
            point = owner.transform.position,
            color = ColorPallete.RedCoin,
            force = 3 });
        this.PostNotification(Notifications.OnCoinPickedUp, 3);

        TimeManager._ModifyFlat_BaseTime(Settings.RedCoinFlatAcceleration);

        PopTextManager.ShowPopMessage(new PopSpriteRequest(Position, FillColor, SpriteLibrary.Get("TimeDown")));
        owner.sFX_PickedUpStar.PlayRandom();
    }
}

public class BlueCoin : CoinType
{
    public override Color FillColor => ColorPallete.BlueCoin;

    public BlueCoin(StarCoin owner) : base(owner)
    {
    }

    public override void Enter()
    {
        base.Enter();
        owner.Render_A.sprite = SpriteLibrary.Get("StarCenter");
        owner.Render_B.sprite = SpriteLibrary.Get("StarOutline");
        owner.Render_C.sprite = null;
    }
    public override void Execute(GameObject picker)
    {



        this.PostNotification(Notifications.BackgroundContactReaction, new BackgroundContactReactionData() {
            point = owner.transform.position,
            color = ColorPallete.AsBackgroundColor(FillColor),
            force = 3 });

        this.PostNotification(Notifications.OnCoinPickedUp, 4);

        float deaccelerationValue = Math.Min(Settings.BlueCoinFlatDeacceleration, TimeManager.BaseTimeScale - 1);
        TimeManager._ModifyFlat_BaseTime(-deaccelerationValue);

        PopTextManager.ShowPopMessage(new PopSpriteRequest(Position, FillColor, SpriteLibrary.Get("TimeUp")));
        owner.sFX_PickedUpStar.PlayRandom();
    }
}

public class ExtraBallCoin : CoinType
{
    public override Color FillColor => Color.white;

    public ExtraBallCoin(StarCoin owner) : base(owner)
    {
    }

    public override void Enter()
    {
        base.Enter();
        owner.Render_A.sprite = SpriteLibrary.Get("HeartIcon");
        owner.Render_B.sprite = SpriteLibrary.Get("PowerUpBubble");
        owner.Render_C.sprite = null;
    }

    public override void Execute(GameObject picker)
    {

        this.PostNotification(Notifications.BackgroundContactReaction, new BackgroundContactReactionData()
        {
            point = owner.transform.position,
            color = ColorPallete.AsBackgroundColor(FillColor),
            force = 3
        });

        this.PostNotification(Notifications.OnCoinPickedUp, 5);
        this.PostNotification(Notifications.ExtraLifeGained);

        PopTextManager.ShowPopMessage(new PopSpriteRequest(Position, FillColor, SpriteLibrary.Get("A new friend!")));
        
        owner.sFX_PickedUpGoldenStar.Play();
    }

    public override void Exit()
    {
        base.Exit();
    }
}


public class ShieldCoin : CoinType
{
    public override Color FillColor => Color.white;

    ConstantColorRotation constantColorRotation;
    Shield shield;

    public ShieldCoin(StarCoin owner) : base(owner)
    {

    }
    public override void Enter()
    {
        base.Enter();

        owner.Render_A.sprite = SpriteLibrary.Get("ShieldIcon");
        owner.Render_B.sprite = SpriteLibrary.Get("PowerUpBubble");
        owner.Render_C.sprite = null;

        owner.AddSparks();
        constantColorRotation = owner.Render_A.gameObject.AddComponent<ConstantColorRotation>();
        constantColorRotation.SetRenders(new SpriteRenderer[] { owner.Render_A });
    }

    public override void Execute(GameObject picker)
    {
        this.PostNotification(Notifications.BackgroundContactReaction, new BackgroundContactReactionData()
        {
            point = owner.transform.position,
            color = ColorPallete.AsBackgroundColor(FillColor),
            force = 3
        });

        this.PostNotification(Notifications.OnCoinPickedUp, 6);

        PopTextManager.ShowPopMessage(new PopSpriteRequest(Position, FillColor, null));
        shield = GameObjectLibrary.Instantiate("Shield", owner.transform).GetComponent<Shield>();
        shield.EquipTo(picker);

        owner.sFX_PickedUpGoldenStar.Play();
        shield = null;
    }

    public override void Exit()
    {
        base.Exit();
        if (shield != null) UnityEngine.Object.Destroy(shield.gameObject);
        UnityEngine.Object.Destroy(constantColorRotation, .5f);
    }
}

public class GlassFloorCoin : CoinType
{
    public override Color FillColor => Color.white;

    public GlassFloorCoin(StarCoin owner) : base(owner)
    {

    }

    public override void Enter()
    {
        base.Enter();

        owner.Render_A.sprite = SpriteLibrary.Get("GlassFloorIcon");
        owner.Render_B.sprite = SpriteLibrary.Get("PowerUpBubble");
        owner.Render_C.sprite = null;
    }

    public override void Execute(GameObject picker)
    {

        this.PostNotification(Notifications.BackgroundContactReaction, new BackgroundContactReactionData()
        {
            point = owner.transform.position,
            color = ColorPallete.AsBackgroundColor(FillColor),
            force = 3
        });

        this.PostNotification(Notifications.OnCoinPickedUp, 7);
        this.PostNotification(Notifications.Terrain_AddGlassFloor);

        PopTextManager.ShowPopMessage(new PopSpriteRequest(Position, FillColor, null));
        owner.sFX_PickedUpGoldenStar.Play();
    }
}

public class LockedCoin : CoinType
{
    public override Color FillColor => ColorPallete.LockedCoin;
    private readonly int cointsToOpen = 0;
    private int count = 0;

    public LockedCoin(StarCoin owner, int cointsToOpen) : base(owner)
    {
        if(owner == null)
            Debug.LogError("Owner is Null");

        this.cointsToOpen = cointsToOpen;
    }

    public override void Execute(GameObject picker)
    {
        
    }

    public override void Enter()
    {
        base.Enter();
        //owner.AddSparks();
        owner.Render_A.sprite = SpriteLibrary.Get("StarCenter");
        owner.Render_B.sprite = SpriteLibrary.Get("StarOutline");
        owner.Render_C.sprite = null;
        owner.Pickable.Active = false;
        this.AddObserver(CoinPickUp, Notifications.OnCoinPickedUp);
        this.AddObserver(PlayerRespawn, Notifications.GameMode_PlayerRespawn);
        Debug.Log("LockedCoin <color=blue> Enter </color>");
    }

    private void CoinPickUp(object sender, object arg)
    {
        if ((int) arg != 0) return;

        count++;
        Action callback = null;
            
        if(count == cointsToOpen)
            callback = ()=> owner.SwitchType(new GoldenCoin(owner));
                                             
        Vector2 startPos = ((CoinType)sender).Position;
        Vector2 targetPos = owner.transform.position;

        GameObjectLibrary.Instantiate("StarUnlockFx").GetComponent<StarUnlockFx>().Initialize(startPos, targetPos, callback);
    }

    public override void Exit()
    {
        base.Exit();
        owner.Pickable.Active = true;
        this.RemoveObserver(CoinPickUp, Notifications.OnCoinPickedUp);
        this.RemoveObserver(PlayerRespawn, Notifications.GameMode_PlayerRespawn);
    }

    private  void PlayerRespawn(object sender, object arg)
    {
        owner.Pickable.Active = false;
    }
}

public class KeyCoin : CoinType
{
    public override Color FillColor => ColorPallete.KeyCoin;
    private int count = 0;

    public KeyCoin(StarCoin owner) : base(owner)
    {
        if(owner == null)
            Debug.LogError("Owner is Null");
    }

    public override void Enter()
    {
        base.Enter();
        owner.LifeSpan = float.MaxValue;
        owner.AddSparks();
        owner.Render_A.sprite = SpriteLibrary.Get("StarCenter");
        owner.Render_B.sprite = SpriteLibrary.Get("StarOutline");
        owner.Render_C.sprite = SpriteLibrary.Get("StarKey");
        owner.Render_C.color = ColorPallete.KeyCoinIcon;
        owner.Pickable.Active = true;
        Debug.Log("LockedCoin <color=blue> Enter </color>");
        this.AddObserver(OnGameMode_Go,Notifications.GameMode_Go);
    }

    public override void Exit()
    {
        base.Exit();
        owner.Render_C.color = Color.white;
        owner.Render_C.sprite = null;
        this.RemoveObserver(OnGameMode_Go,Notifications.GameMode_Go);
    }

    private void OnGameMode_Go(object arg1, object arg2)
    {
        owner.Pickable.Active = true;
    }

    public override void Execute(GameObject picker)
    {
        this.PostNotification(Notifications.StarCoinKeyPickedUp,this);
        owner.sFX_PickedUpStar.PlayRandom();
    }
}