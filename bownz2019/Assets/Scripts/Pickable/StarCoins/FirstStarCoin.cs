﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstStarCoin : MonoBehaviour
{
    private StarCoin starCoin;
    private StarTargetFX targetFX;

    private void OnEnable()
    {
        starCoin = GetComponent<StarCoin>();
        this.AddObserver(OnGameModeReadyResponse, Notifications.GameMode_Ready);
        this.AddObserver(OnGameModeGoResponse, Notifications.GameMode_Go);
    }

    private void OnDisable()
    {
        this.RemoveObserver(OnGameModeReadyResponse, Notifications.GameMode_Ready);
        this.RemoveObserver(OnGameModeGoResponse, Notifications.GameMode_Go);
    }

    private void OnGameModeGoResponse(object arg1, object arg2)
    {
        targetFX.Hide();
    }

    private void OnGameModeReadyResponse(object arg1, object arg2)
    {
        starCoin.LifeSpan = float.MaxValue;
        starCoin.Show(true, new NormalCoin(starCoin), StarCoin.DefaultPosition);

        starCoin.OnPickedUp = (coin) => this.PostNotification(Notifications.OnFirstCoinPickedUp);
        if(targetFX == null) targetFX = GameObjectLibrary.Instantiate("StarTargetFX", transform).GetComponent<StarTargetFX>();
        targetFX.transform.localPosition = Vector3.zero;
        targetFX.Show();
    }
}
    