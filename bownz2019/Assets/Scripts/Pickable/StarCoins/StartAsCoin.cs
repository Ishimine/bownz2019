﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class StartAsCoin : MonoBehaviour
{
    public StarCoin starCoin;

    private void OnEnable()
    {
        this.AddObserver(OnGameMode_Go_Response, Notifications.GameMode_Go);
        this.AddObserver(GameMode_PlayerRespawn_Response, Notifications.GameMode_PlayerRespawn);
        this.AddObserver(ShowStars_Response, Notifications.ShowStars);
    }

    protected virtual void GameMode_PlayerRespawn_Response(object arg1, object arg2)
    {
        Pickable pickable = GetComponent<Pickable>();
        //if (pickable != null && pickable.Active) Show();

        if (starCoin.CoinType is NormalCoin normal)
        {
            normal.SetActive();
        }
    }

    private void OnDisable()
    {
        this.RemoveObserver(GameMode_PlayerRespawn_Response, Notifications.GameMode_PlayerRespawn);
        this.RemoveObserver(OnGameMode_Go_Response, Notifications.GameMode_Go);
        this.RemoveObserver(ShowStars_Response, Notifications.ShowStars);
    }

    private void ShowStars_Response(object arg1, object arg2)
    {
        Show();
        
    }

    protected virtual void OnGameMode_Go_Response(object arg1, object arg2)
    {
        if(starCoin.CoinType is NormalCoin normal)
        {
            normal.SetActive();
        }
        //Show();
    }

    protected abstract void Show();


    private void OnDestroy()
    {
    }
}
