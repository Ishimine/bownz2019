﻿using System;
using System.Collections;
using System.Collections.Generic;
using Pooling.Poolers;
using UnityEngine;
using Sirenix.OdinInspector;

[RequireComponent(typeof(CoinSpawnConfig))]
public class StarsManager : MonoBehaviour
{
    [ShowInInspector,ReadOnly] private bool active = false;

    [SerializeField] bool spawnAtAwake = false;
    [SerializeField] private CoinSpawnConfig coinSpawnConfig = null;
    [SerializeField] private SetPooler inactivePool = null;
    [SerializeField] private ShineMark mark = null;

    List<StarCoin> activeStars = new List<StarCoin>();

    [SerializeField] private SpawnFromBoxCollider2D spawnFromBox = null;

    [SerializeField] private Vector2 defaultPosition = Vector2.up;
    private Vector2 nextPosition = Vector2.zero;

    public ContactFilter2D evadeFilter;
    public float evadeRatio = 1;

    private void Awake()
    {
        Initialize();
        if (spawnAtAwake)
            SpawnStar();
    }

    private void OnEnable()
    {
        this.AddObserver(OnGameModeEndResponse, Notifications.GameMode_End);
        this.AddObserver(OnGameModeReadyResponse, Notifications.GameMode_Ready);
        this.AddObserver(OnFirstCoinPickedUp_Response, Notifications.OnFirstCoinPickedUp);
    }

    private void OnDisable()
    {
        this.RemoveObserver(OnGameModeEndResponse, Notifications.GameMode_End);
        this.RemoveObserver(OnGameModeReadyResponse, Notifications.GameMode_Ready);
        this.RemoveObserver(OnFirstCoinPickedUp_Response, Notifications.OnFirstCoinPickedUp);
    }

    private void OnFirstCoinPickedUp_Response(object arg1, object arg2)
    {
        SpawnStar();
    }

    private void OnGameModeReadyResponse(object arg1, object arg2)
    {
        active = true;
    }

    private void OnGameModeEndResponse(object sender, object value)
    {
        active = false;
        DisableAll();
    }

    private void DisableAll()
    {
        foreach (var item in activeStars)
        {
            inactivePool.EnqueueScript(item.GetComponent<Poolable>());
            //item.gameObject.SetActive(false);
        }
    }

    public void Initialize()
    {
        nextPosition = defaultPosition;
    }

    [Button]
    public void SpawnStar() 
    {
        StarCoin star =  inactivePool.DequeueScript<StarCoin>();
        star.gameObject.SetActive(true);
        star.gameObject.transform.SetParent(transform);
        //star.transform.position = ;

        star.OnHideStart -= OnHideStartResponse;
        star.OnHideDone -= OnHideDoneResponse;
        star.OnHideStart += OnHideStartResponse;
        star.OnHideDone += OnHideDoneResponse;


        star.Show(true, coinSpawnConfig.GetCoinType(star), nextPosition);

        bool isValid = spawnFromBox.GetPosition(out nextPosition, evadeRatio , evadeFilter);

        if (isValid)
            mark.Shine(nextPosition);
        else
            Debug.LogError("TRYING TO SPAWN IN INVALID AREA");

        activeStars.Add(star);
    }

    [Button]
    private void OnHideDoneResponse(StarCoin star)
    {
        star.OnHideDone -= OnHideDoneResponse;

        if(activeStars.Contains(star))
            activeStars.Remove(star);
        inactivePool.EnqueueScript(star.GetComponent<Poolable>());

    }

    private void OnHideStartResponse(StarCoin star)
    {
        star.OnHideStart -= OnHideStartResponse;

        if (active)
            SpawnStar();
    }
}

