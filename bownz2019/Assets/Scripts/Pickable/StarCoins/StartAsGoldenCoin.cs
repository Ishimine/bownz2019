﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(StarCoin))]
public class StartAsGoldenCoin : StartAsCoin
{
    protected override void Show()
    {
        starCoin.LifeSpan = float.MaxValue;
        starCoin.Show(true, new GoldenCoin(starCoin));
        starCoin.LifeSpan = float.MaxValue;
    }
}
