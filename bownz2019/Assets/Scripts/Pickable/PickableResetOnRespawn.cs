﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickableResetOnRespawn : MonoBehaviour
{
    Pickable[] components;

    private void Awake()
    {
        components = GetComponents<Pickable>();
    }

    private void OnEnable()
    {
        this.AddObserver(OnRespawn_Response, Notifications.GameMode_PlayerRespawn);
    }

    private void OnDisable()
    {
        
        this.RemoveObserver(OnRespawn_Response, Notifications.GameMode_PlayerRespawn);
    }

    private void OnRespawn_Response(object arg1, object arg2)
    {
        foreach (var item in components)
            item.ResetActiveState();
    }
}
