﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinPickable : TriggerPickable
{


  

    protected override void PickUp(GameObject picker)
    {
        base.PickUp(picker);
        this.PostNotification(Notifications.WinCoinPickedUp);
    }
}
