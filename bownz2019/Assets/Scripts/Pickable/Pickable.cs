﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Pickable : MonoBehaviour
{
    public bool activateOnGameStart = false;


    [field: SerializeField]
    public bool Active { get; set; } = false;

    private static Action<Pickable,GameObject> onAnyPicked;
    public static Action<Pickable, GameObject> OnAnyPicked
    {
        get { return onAnyPicked; }
        set { onAnyPicked = value; }
    }

    public Action<Pickable, GameObject> OnPicked { get; set; }

    private Action onPickedDone;
    public Action OnPickedDone
    {
        get { return onPickedDone; }
        set { onPickedDone = value; }
    }

    private void OnEnable()
    {
        this.AddObserver(OnGameMode_Go_Response, Notifications.GameMode_Go);
        //ResetActiveState();
    }

    private void OnDisable()
    {
        this.RemoveObserver(OnGameMode_Go_Response, Notifications.GameMode_Go);
    }

  private void OnGameMode_Go_Response(object arg1, object arg2)
  {
      ResetActiveState();
  }

  public void ResetActiveState()
  {
        Active = activateOnGameStart;
  }
    protected virtual void PickUp(GameObject picker)
    {
        Active = false;
        OnPickedDone?.Invoke();
        OnPicked?.Invoke(this, picker);
        OnAnyPicked?.Invoke(this, picker);
    }
}
