﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.U2D;
using UnityEngine.U2D;
using UnityEngine.U2D.SpriteShapeClipperLib;
using Sirenix.OdinInspector;
using System;
using System.Reflection;

public class SpriteShapeFromTerrain : MonoBehaviour
{
    public SpriteShapeRenderer spriteShape;
    public SpriteShapeController spriteShapeController;

    float tolerance = .5f;

    [Button]
    public void ApplyPath(LineRenderer lineRenderer)
    {
        Debug.Log("A ApplyPath");
        Vector3[] pointsArray = new Vector3[lineRenderer.positionCount];
        lineRenderer.GetPositions(pointsArray);
        List<Vector2> points = new List<Vector2>();
        for (int i = 0; i < pointsArray.Length; i++) points.Add(pointsArray[i]);
        ApplyPath(points, false);
    }

    public void ApplyPath(List<Vector2> points,  bool optimizePath = true)
    {
        Debug.Log("B ApplyPath");
        //if (optimizePath) points = OptimizePath(points);
        RemoveRepeated(points);
        List<Vector3> points2 = new List<Vector3>();
        for (int i = 0; i < points.Count; i++) points2.Add(points[i]); 
        ApplyPath(points2);
    }

    private void RemoveRepeated(List<Vector2> points)
    {
        Debug.Log("RemoveRepeated");
        for (int i = points.Count-1; i > 0; i--)
            if (points[i] == points[i - 1]) points.RemoveAt(i);
    }

    const float minDistance = .2f;

    private List<Vector2> OptimizePath(List<Vector2> points)
    {
        Debug.Log("OptimizePath");

        if (points.Count < 4) return points;
        List<Vector2> retValue = new List<Vector2>();
        Vector2 lastPoint = points[0];
        retValue.Add(lastPoint);
        for (int i = 1; i < points.Count; i++)
        {
            if(Vector2.Distance(lastPoint, points[i])> minDistance)
            {
                lastPoint = points[i];
                retValue.Add(points[i]);
            }
        }

        if (Vector2.Distance(retValue[retValue.Count - 1], points[points.Count - 1]) > minDistance)
            retValue[retValue.Count - 1] = points[points.Count - 1];
        else
            retValue.Add(points[points.Count - 1]);
        return retValue;
    }

    private void ApplyPath(List<Vector3> points)
    {
        Debug.Log("C ApplyPath");
        spriteShapeController.spline.Clear();
        Spline spline = spriteShapeController.spline;

        int count = points.Count;
        if (points[0] == points[points.Count-1]) count -= 1;

        for (int x = 0, i = count - 1; i >= 0; i--, x++)
        {
            spline.InsertPointAt(x, points[i]);
            spline.SetCorner(x,true);
        }
        spriteShapeController.RefreshSpriteShape();
    }

    private float FindDistanceToSegment(Vector2 pt, Vector2 p1, Vector2 p2, out Vector2 closest)
    {
        float dx = p2.x - p1.x;
        float dy = p2.y - p1.y;
        if ((dx == 0) && (dy == 0))
        {
            // It's a point not a line segment.
            closest = p1;
            dx = pt.x - p1.x;
            dy = pt.y - p1.y;
            return Mathf.Sqrt(dx * dx + dy * dy);
        }

        // Calculate the t that minimizes the distance.
        float t = ((pt.x - p1.x) * dx + (pt.y - p1.y) * dy) /
            (dx * dx + dy * dy);

        // See if this represents one of the segment's
        // end points or a point in the middle.
        if (t < 0)
        {
            closest = new Vector2(p1.x, p1.y);
            dx = pt.x - p1.x;
            dy = pt.y - p1.y;
        }
        else if (t > 1)
        {
            closest = new Vector2(p2.x, p2.y);
            dx = pt.x - p2.x;
            dy = pt.y - p2.y;
        }
        else
        {
            closest = new Vector2(p1.x + t * dx, p1.y + t * dy);
            dx = pt.x - closest.x;
            dy = pt.y - closest.y;
        }
        return Mathf.Sqrt(dx * dx + dy * dy);
    }

    
}
