﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using Sirenix.OdinInspector;

public class UI_StarOrb : MonoBehaviour
{
    private CodeAnimator codeAnimator = new CodeAnimator();

    public AnimationCurve sizeCurve;
    public float duration = .35f;

    public Transform starConteiner;

    public Image rStarCenter;
    public Image rBackground;

    public Color cBG_Completed;
    public Color cBG_Incompleted;

    [Button]
    public void SetCompleted(bool isCompleted)
    {
        rBackground.color = (isCompleted) ? cBG_Completed : cBG_Incompleted;
        starConteiner.localScale = (isCompleted) ? Vector3.one : Vector3.zero;
    }

    [Button]
    public void TransitionToCompleted()
    {
        starConteiner.gameObject.SetActive(true);
        Vector3 startSize = starConteiner.localScale;
        Color startColor = rBackground.color;
        codeAnimator.StartAnimacion(this,
            x =>
            {
                rBackground.color = Color.Lerp(startColor, cBG_Completed, x);
                starConteiner.localScale = Vector3.LerpUnclamped(startSize, Vector3.one, sizeCurve.Evaluate(x));
            }, DeltaTimeType.deltaTime, AnimationType.Simple, null, duration);
    }
}
