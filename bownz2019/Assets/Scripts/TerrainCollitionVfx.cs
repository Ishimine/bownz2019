﻿ using System;
 using System.Collections.Generic;
 using Sirenix.OdinInspector;
 using UnityEngine;

 public class TerrainCollitionVfx : MonoBehaviour
{
    [TabGroup("Nodes")] public AnimationCurve distanceFalloff;
    [TabGroup("Nodes")] public AnimationCurve scaleFalloff;
    [TabGroup("Nodes")] public float distancePerNode = .2f;
    [TabGroup("Nodes")] public float scale = .5f;
    [TabGroup("Nodes")] public int nodeLevels = 3;

    public List<TcNode> nodes = new List<TcNode>();
    public List<Vector2> path = new List<Vector2>();

    public GameObject prefab;

    public Action OnAnimationEnd { get; set; }

    [TabGroup("Search")] public float resolution = .5f;
    [TabGroup("Search")] public float breakDistance = .6f;

    private Vector2 testPoint;

    private float totalDistance;

    private readonly CodeAnimator codeAnimator = new CodeAnimator();
    [TabGroup("Animation")] public AnimationCurve alphaCurve;
    [TabGroup("Animation")] public AnimationCurve heightCurve;
    [TabGroup("Animation")] public AnimationCurve falloffCurve;
    [TabGroup("Animation")] public AnimationCurve sizeCurve;
    //[TabGroup("Animation")] public AnimationCurve particleSizeCurve;
    [TabGroup("Animation")] public float offset;
    [TabGroup("Animation")] public float magnitude = 3;
    [TabGroup("Animation")] public float duration = 3;

    SpriteRenderer render;

    private void Awake()
    {
        CreateNodes(nodeLevels);
    }

    public void Initialize(Collision2D collision, List<List<Vector2>> paths)
    {
        List<Vector2> closestPath = GetPathClosestToCollision(collision, paths, out PathVector closestPathVector);

        if(closestPath == null)
        {
            OnAnimationEnd.Invoke();
            return;
        }

        Initialize(collision.GetContact(0).point, closestPath, closestPathVector);
    }

    [Button]
    public void Initialize(Vector2 position, LineRenderer lineRenderer)
    {
        Vector3[] positions = new Vector3[lineRenderer.positionCount];
        lineRenderer.GetPositions(positions);
        List<Vector2> pos = new List<Vector2>();
        for (int i = 0; i < positions.Length; i++)
        {
            pos.Add(positions[i]);
        }
        Initialize(position, pos);
    }

    public void Initialize(Vector2 point, List<Vector2> nPath)
    {
        PathVector centerVector = GetClosesPointInPath(point, nPath, totalDistance,  resolution, breakDistance);
        Initialize(point,nPath,centerVector);
    }

    public void Initialize(Vector2 point, List<Vector2> nPath, PathVector centerVector)
    {
        transform.position = Vector3.zero;
        AdaptToPath(nPath, centerVector);
        Animate();
    }

    [Button]
    public void Animate()
    {
        int center = Mathf.CeilToInt((float)nodes.Count / 2) - 1;
        int levels = center;
        float waitStep = (1 / (float)levels) * offset;
        ResetAllNodes(levels);
        float nDuration = duration + levels * waitStep; 
        codeAnimator.StartAnimacion(this,
            x =>
            {
                float a = Mathf.Lerp(0,1,x / (1-waitStep * levels)); 
                nodes[center].Value = heightCurve.Evaluate(a) * magnitude;
                nodes[center].Alpha = alphaCurve.Evaluate(a);
                nodes[center].Scale = sizeCurve.Evaluate(a);
               
                for (int i = 1; i <= levels; i++)
                {
                    float b = Mathf.Lerp(0,1, (x - waitStep * i) / (1+waitStep * i));
                    nodes[center + i].Value = heightCurve.Evaluate(b) * magnitude * falloffCurve.Evaluate((float)i / levels);
                    nodes[center + i].Alpha = alphaCurve.Evaluate(b);
                    nodes[center + i].Scale = sizeCurve.Evaluate(b);
                    nodes[center - i].Value = heightCurve.Evaluate(b) * magnitude * falloffCurve.Evaluate((float)i / levels);
                    nodes[center - i].Alpha = alphaCurve.Evaluate(b);
                    nodes[center - i].Scale = sizeCurve.Evaluate(b);
                }
            }, DeltaTimeType.deltaTime, AnimationType.Simple, null, OnAnimationEnd, nDuration);
    }

    private void ResetAllNodes(int levels)
    {
        nodes[levels].ResetNode();
        for (int i = 1; i <= levels; i++)
        {
            nodes[levels + i].ResetNode();
            nodes[levels - i].ResetNode();
        }
    }

    private void AdaptToPath(List<Vector2> nPath, PathVector centerVector)
    {
        path = nPath;
        UpdateTotalDistance();
        float nodeTimeStep = RealDistanceToTimeDistance(distancePerNode, totalDistance);
        int levels = Mathf.CeilToInt((float)nodes.Count / 2) - 1;

        ApplyNode(nPath, centerVector.Time, levels, levels);
        for (int i = 1; i <= levels; i++)
        {
            for (int j = 0; j < 2; j++)
            {
                int sign = (j == 0) ? 1 : -1;
                float time = centerVector.Time + i * sign * nodeTimeStep * distanceFalloff.Evaluate((float)i / levels);
                ApplyNode(nPath, time, levels + i * sign, levels);
            }
        }
    }

    private void ApplyNode(List<Vector2> nPath, float centerTime, int index, int centerIndex)
    {
        centerTime = Mathf.Repeat(centerTime, 1); 
        nodes[index].Position = GetPointOnPath(centerTime, nPath, totalDistance);
        nodes[index].transform.rotation = Quaternion.Euler(0, 0, GetRotationOnPoint(centerTime, nPath, totalDistance).AsAngle() - 90 + 180);
        nodes[index].VisualScale = Vector2.one * scale * scaleFalloff.Evaluate(((float)Mathf.Abs(centerIndex - index) /    centerIndex));
    }

    private static Vector2 GetRotationOnPoint(float time, List<Vector2> nPath, float totalDistance)
    {
        return GetSegmentVector(time, nPath, totalDistance).PerpendicularCounterClockwise();
    }

    private static Vector2 GetSegmentVector(float time, List<Vector2> nPath, float totalDistance)
    {
        float targetDistance = totalDistance * time;
        float accumulativeDistance = 0;
        int index = 0;
        while (accumulativeDistance < targetDistance)
        {
            float segmentDistance = Vector2.Distance(nPath[index], nPath[index + 1]);
            if ((accumulativeDistance + segmentDistance) >= targetDistance)
            {
                return nPath[index].GetDirection(nPath[index+1]);
            }
            accumulativeDistance += segmentDistance;
            index++;
        }
        return Vector2.zero;
    }

    private void CreateNodes(int count)
    {
        CreateNewNode();
        for (int i = 0; i < count; i++)
        for (int j = 0; j < 2; j++)
            CreateNewNode();
    }

    // ReSharper disable once UnusedMethodReturnValue.Local
    private TcNode CreateNewNode()
    {
        TcNode instance = Instantiate(prefab).GetComponent<TcNode>();
        nodes.Add(instance);
        instance.Alpha = 0;
        instance.transform.parent = transform;
        return instance;
    }

    [Button]
    private void CreatePathFromTransform(Transform[] transforms)
    {
        path.Clear();
        for (int i = 0; i < transforms.Length; i++)
        {
            path.Add(transforms[i].position);
        }
    }

    [Button]
    private void TestGetClosestPointInPath(Transform nTransform, float steps = .05f, float nBreakDistance = .1f)
    {
        testPoint = GetClosesPointInPath(nTransform.position, path, totalDistance, steps, nBreakDistance).Position;
    }

           
    public static List<List<Vector2>> GetPathsThatOverlapsLine(Vector2 segmentStart, Vector2 segmentEnd, List<List<Vector2>> paths)
    {
        List<List<Vector2>> retList = new List<List<Vector2>>();
        Vector2 intersection = Vector2.one;

        for (int i = 0; i < paths.Count; i++)
        {
            for (int j = 0; j < paths[i].Count-1; j++)
            {
                if (UnityExtentions.LineIntersection(segmentStart, segmentEnd, paths[i][j], paths[i][j + 1],
                    ref intersection))
                {
                    retList.Add(paths[i]);
                    break;
                }
            }
        }
        return retList;
    }

    private struct Segment
    {
        public Vector2 Start { get; set; }
        public Vector2 End { get; set; }
        public Segment(Vector2 start, Vector2 end)
        {
            Start = start;
            End = end;
        }
    }

    private static List<Segment> GetSegmentsThatOverlapsLine(Vector2 segmentStart, Vector2 segmentEnd, IReadOnlyList<List<Vector2>> paths)
    {
        List<Segment> retList = new List<Segment>();
        Vector2 intersection = Vector2.one;
        for (int i = 0; i < paths.Count; i++)
        {
            for (int j = 0; j < paths[i].Count-1; j++)
            {
                if (UnityExtentions.LineIntersection(segmentStart, segmentEnd, paths[i][j], paths[i][j + 1],
                    ref intersection))
                {
                    retList.Add(new Segment(paths[i][j], paths[i][j + 1]));
                    break;
                }
            }
        }
        return retList;
    }

    private struct UniqueSegment
    {
        public int Id { get; }
        public Vector2 Start { get; }
        public Vector2 End { get; }
        public float Time { get; set; }
        public UniqueSegment(int id, Vector2 start, Vector2 end, float time)
        {
            Id = id;
            Start = start;
            End = end;
            Time = time;
        }

        public float Distance()
        {
            return Vector2.Distance(Start,End);
        }
    }

    public static List<Vector2> GetPathClosestToCollision(Collision2D collision, List<List<Vector2>> paths, out PathVector closestPathVector)
    {
        const float extraLenght = .5f;
        ContactPoint2D contact = collision.GetContact(0);
        Vector2 dir = -contact.normal; /*(contact.rigidbody.position - contact.point).normalized;*/
        Vector2 start = contact.point - dir * extraLenght;
        Vector2 end = contact.point + dir * extraLenght;
        List<List<Vector2>> closestPaths = GetPathsThatOverlapsLine(start, end, paths);

        if(closestPaths.Count == 0)
        {
            Debug.LogWarning("ClosestPaths.Count: " + closestPaths.Count);
            closestPathVector = new PathVector();
            return null;
        }

        List<UniqueSegment> segments = new List<UniqueSegment>();
        Vector2 intersection = Vector2.zero;
        closestPathVector = new PathVector();
        Vector2 normal = collision.GetContact(0).normal;

        //Buscamos los segmentos que "Cortan" la linea de impacto
        for (int i = 0; i < closestPaths.Count; i++)
        {
            for (int j = 0; j < closestPaths[i].Count-1; j++)
            {
                if (UnityExtentions.LineIntersection(start, end, closestPaths[i][j], closestPaths[i][j + 1], ref intersection))
                {
                    segments.Add(new UniqueSegment(i,closestPaths[i][j], closestPaths[i][j + 1],0));
                }
            }
        }

        if (segments.Count == 0)
        {
            Debug.LogWarning("segments.Count: " + segments.Count);
            closestPathVector = new PathVector();
            return null;
        }

        float minDistance = float.MaxValue;
        int minId = 0;
        UniqueSegment minSegment = new UniqueSegment();
        for (int i = 0; i < segments.Count; i++)
        {
            PathVector currentPathVector = GetClosesPointInSegment(contact.point, segments[i]);

            float currentDistance = Vector2.Distance(currentPathVector.Position, contact.point);
            if (!(currentDistance < minDistance)) continue;

            closestPathVector = currentPathVector;
            minDistance = currentDistance;
            minId = segments[i].Id;
            minSegment = segments[i];
        }

        float totalDistance = GetTotalDistance(closestPaths[minId]);

        float proportion = minSegment.Distance() / totalDistance;
        float accumulativeDistance = 0;
    
        for (int i = 0; i < closestPaths[minId].Count; i++)
        {
            if (closestPaths[minId][i] == minSegment.Start && closestPaths[minId][i+1] == minSegment.End)
                break;

            accumulativeDistance += Vector2.Distance(closestPaths[minId][i],closestPaths[minId][i+1]);
        }
        float accumulativeTime = accumulativeDistance / totalDistance;

        closestPathVector.Time = accumulativeTime + closestPathVector.Time * proportion;

        return closestPaths[minId];
        //return GetPathWithClosestPoint(contact.point, closestPaths);
    }

    private static PathVector GetClosesPointInSegment(Vector2 contactPoint,UniqueSegment uniqueSegment)
    {
        const float resolution = .2f;
        float step = resolution / uniqueSegment.Distance();

        float lastDistance = float.MaxValue;

        uniqueSegment.Time = 0;
        Vector2 closestPoint = Vector2.zero;

        while (uniqueSegment.Time <= 1)
        {
             Vector2 currentPoint = Vector2.Lerp(uniqueSegment.Start, uniqueSegment.End, uniqueSegment.Time);
             float currentDistance = Vector2.Distance(currentPoint, contactPoint);
            
             if(currentDistance >= lastDistance) break;

             closestPoint = currentPoint;
             lastDistance = currentDistance;
             uniqueSegment.Time += step;
        }

        return new PathVector(closestPoint,uniqueSegment.Time);
    }


    private static List<Vector2> GetPathWithClosestPoint(Vector2 source, IReadOnlyList<List<Vector2>> paths)
    {
        float minDistance = float.MaxValue;
        List<Vector2> minPath = null;

        for (int i = 0; i < paths.Count; i++)
        {
            float currentDistance = Vector2.Distance(GetClosesPointInPath(source, paths[i], GetTotalDistance(paths[i])).Position, source);
            if (!(currentDistance < minDistance)) continue;
            
            minDistance = currentDistance;
            minPath = paths[i];
        }
        return minPath;
    }

    private static PathVector GetClosesPointInPath(Vector2 source, IReadOnlyList<Vector2> nPath, float totalDistance, float steps = .05f, float nBreakDistance = .1f)
    {
        float currentTime = 0;
        Vector2 minPoint = Vector2.zero;
        steps = RealDistanceToTimeDistance(steps, totalDistance );
        PathVector retValue = new PathVector(Vector2.zero, 0);
        float endTime = 1;
        float minDistance = float.MaxValue;
        do
        {
            Vector2 currentPoint = GetPointOnPath(currentTime, nPath, totalDistance);
            float currentDistance = Vector2.Distance(source, currentPoint);
            if (currentDistance < minDistance)
            {
                minDistance = currentDistance;
                minPoint = currentPoint;
                endTime = currentTime;
                if (currentDistance < nBreakDistance)
                {
                    return retValue.Set(minPoint,endTime);
                }
            }
            currentTime += steps;
        } while (currentTime <= 1);
        return retValue.Set(minPoint,endTime);
    }

    private static float RealDistanceToTimeDistance(float steps, float totalDistance)
    {
        steps = steps / totalDistance;
        return steps;
    }


    private static Vector2 GetPointOnPath(float time, IReadOnlyList<Vector2> path, float totalDistance)
    {
        if (path.Count < 2) Debug.LogError("El path contiene menos de 2 puntos");
        if (time > 1) Debug.LogError("Time mayor a 1");

        float accumulativeDistance = FindSegmentFrom(time, path, totalDistance, out int index, out float currentSegmentDistance);
        float accumulativeTime = accumulativeDistance / totalDistance;
        float currentSegmentProportion = currentSegmentDistance / totalDistance;
        float timeDifference = time - accumulativeTime;
        float currentSegmentTime = timeDifference / currentSegmentProportion;
     
        Vector2 retValue = Vector2.Lerp(path[index], path[index + 1], currentSegmentTime);
        return retValue;
    }

    private static float FindSegmentFrom(float time, IReadOnlyList<Vector2> path, float totalDistance, out int index,
        out float currentSegmentDistance)
    {
        float targetDistance = totalDistance * time;
        float accumulativeDistance = 0;
        index = 0;
        currentSegmentDistance = Vector2.Distance(path[index], path[index + 1]);
        while (targetDistance > accumulativeDistance)
        {
            currentSegmentDistance = Vector2.Distance(path[index], path[index + 1]);
            if ((accumulativeDistance + currentSegmentDistance) > targetDistance)
            {
                break;
            }

            accumulativeDistance += currentSegmentDistance;
            index++;
        }
        return accumulativeDistance;
    }

    private static float GetTotalDistance(IReadOnlyList<Vector2> nPath)
    {
        float value = 0;
        for (int i = 0; i < nPath.Count - 1; i++)
        {
            value += Vector2.Distance(nPath[i], nPath[i + 1]);
        }
        return value;
    }

    private void UpdateTotalDistance()
    {
        totalDistance = GetTotalDistance(path);
    }

    private void OnDrawGizmos()
    {
        if (testPoint != Vector2.zero)
            Gizmos.DrawSphere(testPoint, .5f);
        for (int i = 0; i < path.Count-1; i++)
        {
            Gizmos.DrawLine(path[i], path[i + 1]);
        }
    }
}

 public struct PathVector
 {
     public Vector2 Position;
     public float Time;

     public PathVector(Vector2 position, float time)
     {
         Position = position;
         Time = time;
     }

     public PathVector Set(Vector2 nPos, float nTime)
     {
         Position = nPos;
         Time = nTime;
         return this;
     }
 }


