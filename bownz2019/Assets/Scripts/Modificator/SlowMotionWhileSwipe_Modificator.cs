﻿using System;
using System.Collections;
using System.Collections.Generic;
using Lean.Touch;
using UnityEngine;

public class SlowMotionWhileSwipe_Modificator : MonoBehaviour, IUseInputController
{
    private static SlowMotionWhileSwipe_Modificator instance;
    public static SlowMotionWhileSwipe_Modificator Instance
    {
        get
        {
            if(instance == null)
                instance = GameObjectLibrary.Instantiate("SlowMotionSwipe_Modifier", null).GetComponent<SlowMotionWhileSwipe_Modificator>();
            return instance;
        }
        set { instance = value; }
    }

    public float duration = .5f;

    bool active = false;
    private const int reloadCharges = 5;

    private int charges;
    public int Charges
    {
        get
        {
            if (isInfinite)
                return 666;
            return charges;
        }
        set
        {
            if (!isInfinite)
                charges = value;
            else
                charges = 1;
            this.PostNotification(Notifications.Modifier_SlowMotionOnSwipe_ChargeUsed, charges);

            active=(charges > 0);
        }
    }

    [SerializeField] private bool basedOnVirtualTimeScale = true;

    private float ammount = .3f;
    public float Ammount
    {
        get
        {
            return (basedOnVirtualTimeScale) ? ammount : TimeManager.BaseTimeScale * ammount;
        }
    }

    public static void Deactivate()
    {
        Instance.Charges = 0;
    }

    public static void Activate()
    {
        Instance.Charges += reloadCharges;
    }

    public static bool isInfinite = false;

    public void ToggleInfinite()
    {
        isInfinite = !isInfinite;
    }

    private void OnEnable()
    {
        this.AddObserver(BackToNormal, Notifications.SwipePlatform_Touched);
        this.PostNotification(Notifications.Modifier_SlowMotionOnSwipe_Equipped);
        InputManager.RegisterController(this);
    }

    private void OnDisable()
    {
        this.RemoveObserver(BackToNormal, Notifications.SwipePlatform_Touched);
        this.PostNotification(Notifications.Modifier_SlowMotionOnSwipe_Unequipped);
        if(Application.isPlaying) InputManager.UnregisterController(this);
        if (Application.isPlaying) ResetTimeScale();
        Debug.LogWarning("OnDisable " + this);
    }

    private void BackToNormal(object arg1, object arg2)
    {
        ResetTimeScale();
    }

    private void OnDestroy()
    {
        Debug.LogWarning("Destroyed " + this);
        instance = null;
    }

    public void FingerTap(LeanFinger leanFinger)
    {
    }

    public void FingerDown(LeanFinger leanFinger)
    {
        if (Charges > 0)
        {
            TimeManager._StartTimeShift(Ammount, duration, true);
            Charges--;
        }
    }

    public void FingerSet(LeanFinger leanFinger)
    {
    }

    public void FingerUp(LeanFinger leanFinger)
    {
        if (charges >= 0 && active)
        {
            ResetTimeScale();
            if (charges == 0)
                active = false;
        }
    }

    private void ResetTimeScale()
    {
        TimeManager._StartTimeShiftBack();
    }

    public void FingerSwipe(LeanFinger leanFinger)
    {
    }

    public void FingerExpired(LeanFinger leanFinger)
    {
    }

    public void Gesture(List<LeanFinger> leanFingers)
    {
    }
}
