﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class TerrainToSpriteShape : MonoBehaviour
{
    [SerializeField] private Pooling.Poolers.SetPooler pool;

    public bool applyOnStart = false;

    public Transform[] terrainRoots;

    private List<SpriteShapeFromTerrain> spriteShapes = new List<SpriteShapeFromTerrain>();

    private void Start()
    {
        if (applyOnStart && Settings.CurrentStyle == Settings.Style.Normal)
            StartCoroutine(WaitAndCreate());
    }

    public void Show()
    {
        pool.gameObject.SetActive(true);
    }

    public void Hide()
    {
        pool.gameObject.SetActive(false);
    }

    IEnumerator WaitAndCreate()
    {
        yield return null;
        CreateSpriteShape();
    }

    [Button]
    public void CreateSpriteShape()
    {
        ClearCurrentShapes();

        for (int i = 0; i < terrainRoots.Length; i++)
        {
            CreateSpriteShape(terrainRoots[i]);
        }
    }

    public void CreateSpriteShape(Transform transform)
    {
        Debug.Log("Name: " + transform.name);

        Rigidbody2D rb = transform.gameObject.GetComponent<Rigidbody2D>();
        CompositeCollider2D compositeCollider2D = transform.gameObject.GetComponent<CompositeCollider2D>();

        bool destroyRb = !rb;
        bool destroyCol = !compositeCollider2D;

        if (!rb) rb = transform.gameObject.AddComponent<Rigidbody2D>();
        if (!compositeCollider2D) compositeCollider2D = transform.gameObject.AddComponent<CompositeCollider2D>();

        rb.isKinematic = true;
        CreateSpriteShape(compositeCollider2D);

        if(destroyCol) DestroyNow(compositeCollider2D);
        if (destroyRb) DestroyNow(rb);
    }

    [Button]
    public void CreateSpriteShape(CompositeCollider2D compositeCollider2D)
    {
        for (int i = 0; i < compositeCollider2D.pathCount; i++)
        {
            Vector2[] path = new Vector2[compositeCollider2D.GetPathPointCount(i)];
            compositeCollider2D.GetPath(i, path);
            SpriteShapeFromTerrain current;

            if (Application.isPlaying)
                current = pool.Dequeue().GetComponent<SpriteShapeFromTerrain>();
            else
                current = Instantiate(pool.prefab, null).GetComponent<SpriteShapeFromTerrain>();

            current.ApplyPath(new List<Vector2>(path), true);
            current.transform.SetParent(pool.transform);
            current.gameObject.SetActive(true);
            spriteShapes.Add(current);
        }
    }

    public void ClearCurrentShapes()
    {
        if (Application.isPlaying)
            EnqueuChildren();
        else
            DestroyChildren();

        spriteShapes.Clear();
    }

    private void EnqueuChildren()
    {
        for (int i = spriteShapes.Count - 1; i >= 0; i--)
            pool.Enqueue(spriteShapes[i].gameObject.GetComponent<Poolable>());
    }

    private void DestroyChildren()
    {
        List<Transform> children = new List<Transform>();
        foreach (Transform item in pool.transform)
            children.Add(item);

        for (int i = children.Count - 1; i >= 0; i--)
            DestroyImmediate(children[i].gameObject);
    }

    private static void DestroyNow(Object obj)
    {
        if (Application.isPlaying)
            DestroyImmediate(obj,true);
//            Destroy(obj);
        else
            DestroyImmediate(obj,true);
    }
}
