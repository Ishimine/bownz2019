﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PSHelf_Manual : ProductShelf
{
    [SerializeField] private Transform[] placements = null;

    public override int Capacity => placements.Length;

    protected override void PositionProduct(ProductDisplay productDisplay, int index)
    {
        productDisplay.PositionAt(placements[index].position, Vector2.up);
    }
}
