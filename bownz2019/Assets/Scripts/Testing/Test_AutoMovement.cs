﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test_AutoMovement : MonoBehaviour
{
    [SerializeField] private bool active = true;
    public Vector2 speed = new Vector2(100, 50);

    public float frecuency;

    float startValue = Mathf.PI / 2;

    public SoundPlay testSound;

    private void Update()
    {
        if (!active) return;
        transform.Translate(Time.deltaTime * speed * Mathf.Sin(Time.time * frecuency + startValue));
    }

    public void PlaySound()
    {

    }
}
