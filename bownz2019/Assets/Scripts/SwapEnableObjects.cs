﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwapEnableObjects : MonoBehaviour
{
    public GameObject[] gos;

    public void Swap()
    {
        foreach (GameObject item in gos)
            item.SetActive(!item.activeSelf);
    }
}
