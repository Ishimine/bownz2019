﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Firebase;
using Firebase.Auth;
using Firebase.Database;
using Firebase.Unity.Editor;
using Firebase.RemoteConfig;
using UnityEngine;

public class FirebaseStarter : MonoSingleton<FirebaseStarter>
{
    public override FirebaseStarter Myself => this;

    public static Action<bool> OnSignStateChange;

    public static Firebase.FirebaseApp app;

    public static Queue<Action> ExecuteOnInitialization = new Queue<Action>();

    private static bool firebaseInitilized;
    public static bool FirebaseInitilized
    {
        get { return firebaseInitilized; }
    }

    private Task fetchTask = null;
    public static  FirebaseAuth Auth;
    public static  FirebaseDatabase Database;
    public static  FirebaseUser User;

    public static bool SignedIn = false;
    protected override void Awake()
    {
        base.Awake();
        PlayServicesManager.OnAuthCode += PlayGamesAuthCallback;

        Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            Debug.Log($"<color=orange> FIREBASE </color> CheckAndFixDependenciesAsync");
            DependencyStatus dependencyStatus = task.Result;
            if (dependencyStatus == Firebase.DependencyStatus.Available)
            {
                Initialization();
            }
            else
            {
                UnityEngine.Debug.LogError(System.String.Format(
                    "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                // Firebase Unity SDK is not safe to use here.
            }
        });
    }


    private void Initialization()
    {
        Debug.Log($"<color=orange> FIREBASE </color> Initialization");
        app = Firebase.FirebaseApp.DefaultInstance;
        firebaseInitilized = true;

        InitializeDatabase();
        InitializeAuth();
        InitializeRemoteConfig();

        foreach (Action action in ExecuteOnInitialization)
        {
            action?.Invoke();
        }
    }
 
    private void InitializeAuth()
    {
        Auth = FirebaseAuth.DefaultInstance;
        Auth.StateChanged += AuthStateChanged;
        AuthStateChanged(this, null);
    }

    void AuthStateChanged(object sender, System.EventArgs eventArgs)
    {
        Debug.Log("AuthStateChanged: " + eventArgs);
        if (Auth.CurrentUser != User)
        {
            SignedIn = User != Auth.CurrentUser && Auth.CurrentUser != null;
            if (!SignedIn && User != null)
            {
                Debug.Log("Signed out " + User.UserId);
                OnSignStateChange?.Invoke(false);
            }
            User = Auth.CurrentUser;
            if (SignedIn)
            {
                Debug.Log("Signed in " + User.UserId);
                OnSignStateChange?.Invoke(true);
            }
        }
                            
    }
    
    private void InitializeDatabase()
    {
        app.SetEditorDatabaseUrl("https://bownz-64640818.firebaseio.com/");
        Database = FirebaseDatabase.DefaultInstance;
    }

    private void InitializeRemoteConfig()
    {
        if (Application.isEditor)
        {
            ConfigSettings nSettings = FirebaseRemoteConfig.Settings;
            nSettings.IsDeveloperMode = true;
            FirebaseRemoteConfig.Settings = nSettings;
        }

        fetchTask = FirebaseRemoteConfig.FetchAsync((Application.isEditor)
            ? new TimeSpan(0)
            : FirebaseRemoteConfig.DefaultCacheExpiration);
    }


    private void PlayGamesAuthCallback(string authCode)
    {
        if (!firebaseInitilized)
        {
            Debug.LogError("Firebase Auth Enqueue");
            ExecuteOnInitialization.Enqueue(()=> PlayGamesAuthCallback(authCode));
            return;
        }
              
        Debug.Log("Firebase Auth Started");

        Credential credential =
            PlayGamesAuthProvider.GetCredential(authCode);
        Auth.SignInWithCredentialAsync(credential).ContinueWith(task =>
        {
            if (task.IsCanceled)
            {
                Debug.LogError("SignInWithCredentialAsync was canceled.");
                return;
            }

            if (task.IsFaulted)
            {
                Debug.LogError("SignInWithCredentialAsync encountered an error: " + task.Exception);
                return;
            }

            User = task.Result;
            Debug.LogFormat("User signed in successfully: {0} ({1})",
                User.DisplayName, User.UserId);
        });
    }

    public void Update()
    {
        if (fetchTask != null && fetchTask.IsCompleted)
        {
            Debug.Log("<color=green> Fetch Completed </color>");
            FirebaseRemoteConfig.ActivateFetched();
            fetchTask = null;

            foreach (string key in FirebaseRemoteConfig.Keys)
            {
                Debug.Log($"Key: {key} = {FirebaseRemoteConfig.GetValue(key).StringValue }");
            }
            this.PostNotification(Notifications.RemoteConfigFetched);
        }
    }

    public static void SaveDataAsJson_WithTransaction(string dataName, string jsonData, DataMutator dataMutator = null, Action<bool> callback = null)
    {
        if (!firebaseInitilized || !SignedIn)
        {
            Debug.LogWarning("Intentando mandar datos a la base de datos SIN iniciar firebase");
            return;
        }

        DatabaseReference node = GetNodeReferenceByPath(new []{"users", Auth.CurrentUser.UserId, dataName});

        node.RunTransaction(mutableData =>  JsonDataTransaction(mutableData, jsonData, dataMutator)).ContinueWith(
            task =>
            {
                if (task.Exception != null)
                     Debug.Log(task.Exception.ToString());
                else if (task.IsCompleted)
                    Debug.Log("Transaction complete.");

                callback?.Invoke(task.Exception == null);
            });
    }

    //Funcion que recive los datos viejos y los datos nuevos los procesa y genera el dato final
    public delegate string DataMutator(string oldData, string newData);

    public static TransactionResult JsonDataTransaction(MutableData mutableData, string newData, DataMutator mutator = null)
    {
        if (mutableData.Value != null)
        {
            string oldData = mutableData.Value as string ?? string.Empty;
            string mutatedData = mutator != null ? mutator(oldData, newData) : newData;

            // You must set the Value to indicate data at that location has changed.
            mutableData.Value = mutatedData;
        }
        else
        {
            mutableData.Value = newData;
        }

        //return and log success
        return TransactionResult.Success(mutableData);
    }

    private static DatabaseReference GetNodeReferenceByPath(string[] pathNodes)
    {
        DatabaseReference databaseReference = Database.RootReference;
        for (int i = 0; i < pathNodes.Length; i++)
            databaseReference = databaseReference.Child(pathNodes[i]);
        return databaseReference;
    }

    public static void LoadDataFromDatabase(string dataName, Action<bool,string> callback)
    {
        if(!firebaseInitilized || !SignedIn) return;
        DatabaseReference node = GetNodeReferenceByPath(new []{"users", Auth.CurrentUser.UserId, dataName});
        node.GetValueAsync().ContinueWith(
            task =>
            {
                if (task.Exception == null)
                    callback.Invoke(true, task.Result.Value as string);
                else
                    callback.Invoke(false, task.Result.Value as string);
            });
    }
}
