﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(menuName = "Managers/Time")]
public class TimeManager : ScriptableSingleton<TimeManager>
{
    public override TimeManager Myself => this;

    public CodeAnimatorCurve curveIn;
    public CodeAnimatorCurve curveOut;
    public CodeAnimatorCurve resetCurve;

    IEnumerator rutine;

    private static bool inTransition = false;
    private static bool canBeOverrided = false;

    private static bool inPause = false;
    private static float beforePauseValue = 1;
    private static float BeforePauseValue
    {
        get => beforePauseValue;
        set
        {
            //Debug.Log($"Saving before pause timeScale: {beforePauseValue}");
            beforePauseValue = value;
        }
        
    }

    private Empty monoProxy;
    public Empty MonoProxy
    {
        get
        {
            if (monoProxy == null)
            {
                monoProxy = new GameObject().AddComponent<Empty>();
                monoProxy.gameObject.name = this + "_Proxy";
            }
            return monoProxy;
        }
        set { monoProxy = value; }
    }

    float defaulFixedDeltaTime;

    [ShowInInspector, ReadOnly] bool _InTransition => inTransition;
    [ShowInInspector, ReadOnly] float TimeScale => Time.timeScale;
    [ShowInInspector, ReadOnly] float _BaseTimeScale => BaseTimeScale;

 
    private float shiftBackSmooth = .20f;


    private static float baseTimeScale = 1;
    public static float BaseTimeScale
    {
        get { return baseTimeScale; }
        set
        {
            baseTimeScale = value;

            if (!inTransition)
                Instance.StartTimeShiftBack();
        }
    }

    protected void OnEnable()
    {
        defaulFixedDeltaTime = Time.fixedDeltaTime;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
    }

    #region Static

    public static void _ResetAll()
    {
        _SetTimeScale(1);
        BaseTimeScale = 1;
        inTransition = false;
    }

    public static void _Accelerate_BaseTime(float percentaje)
    {
        BaseTimeScale *= percentaje;
    }

    public static void _Deaccelerate_BaseTime(float percentaje)
    {
        BaseTimeScale -= BaseTimeScale * percentaje;
    }

    public static void _ModifyFlat_BaseTime(float ammount)
    {
        BaseTimeScale += ammount;
    }

    public static void _StartTimeShift(float target, float duration, bool canBeOverrided)
    {
        TimeManager.canBeOverrided = canBeOverrided;
        Instance.StartTimeShift(target, duration);
    }

    public static void _StartTimeShiftBack()
    {
        if (inTransition && !canBeOverrided) return;
        Instance.StartTimeShiftBack();
    }

    private static void _SetTimeScale(float nTimeScale)
    {
        Time.timeScale = nTimeScale;
        Time.fixedDeltaTime = Instance.defaulFixedDeltaTime * nTimeScale;
    }

    #endregion

    public static void Pause()
    {
        if(inPause) return;
        //Debug.LogWarning("Pause");
        inPause = true;
        BeforePauseValue = Time.timeScale;
        _SetTimeScale(0);
    }

    public static void Resume()
    {
        if(!inPause) return;
        //Debug.LogWarning("Resume");
        inPause = false;
        _SetTimeScale(BeforePauseValue);
        //Time.timeScale = BeforePauseValue;
    }


    public void StartTimeShiftBack()
    {
        //Debug.Log("StartTimeShiftBack");
        if (inTransition && !canBeOverrided)
            Debug.LogError("Ya hay una transicion activa");

        if (rutine != null) MonoProxy.StopCoroutine(rutine);
        rutine = TimeShiftBack(true);
        MonoProxy.StartCoroutine(rutine);
    }

    [Button]
    public void StartTimeShift(float target, float duration)
    {
        //Debug.Log("StartTimeShift");
        if (rutine != null) MonoProxy.StopCoroutine(rutine);
        rutine = StartTemporalShiftRutine(target, duration);
        MonoProxy.StartCoroutine(rutine);
    }

    IEnumerator StartTemporalShiftRutine(float targetTime, float duration)
    {
        yield return TimeShiftTo(targetTime, curveIn, false);
        yield return new WaitForSecondsRealtime(duration);
        yield return TimeShiftBack(false);
    }
    IEnumerator TimeShiftTo(float targetTime, CodeAnimatorCurve curve, bool resetModifyFlag)
    {
        inTransition = true;
        float startTime = Time.timeScale;
        float x = 0;
        yield return new WaitForFixedUpdate();
        do
        {
            if (inPause)
            {
                yield return null;
                continue;
            }

            x += Time.fixedDeltaTime / curve.Time;
            _SetTimeScale(Mathf.Lerp(startTime, targetTime, curve.Curve.Evaluate(x)));

            yield return new WaitForFixedUpdate();
        } while (x < 1);
        if(resetModifyFlag) inTransition = false;
    }

    IEnumerator TimeShiftBack(bool resetModifyFlag)
    {
        inTransition = true;
        float vel = 0;
        do
        {
            if (inPause)
            {
                yield return null;
                continue;
            }
            _SetTimeScale(Mathf.SmoothDamp(Time.timeScale, BaseTimeScale, ref vel, shiftBackSmooth));
            yield return new WaitForFixedUpdate();

        } while (Mathf.Abs(Time.timeScale - BaseTimeScale) > .005f);
        _SetTimeScale(BaseTimeScale);
       if(resetModifyFlag) inTransition = false;
    }

}

public struct TimeShift
{
    public float TimeScale;
    public float Duration;

    public TimeShift(float targetTime, float duration)
    {
        TimeScale = targetTime;
        Duration = duration;
    }
}