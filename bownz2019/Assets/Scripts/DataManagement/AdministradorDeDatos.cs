﻿using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public static class AdministradorDeDatos
{
    static BinaryFormatter bf = new BinaryFormatter();
    static FileStream file;

    public static bool CargarDatos<T>(ref T ar, string nombreFisico)
    {
        if (File.Exists(Application.persistentDataPath + "/" + nombreFisico))
        {
            Debug.Log("<color=green> Opening File: </color> " + Application.persistentDataPath + "/" + nombreFisico);
            file = File.Open(Application.persistentDataPath + "/" + nombreFisico, FileMode.Open);
            ar = (T)bf.Deserialize(file);
            file.Close();
            return true;
        }
        else
        {
            Debug.Log("Archivo no encontrado: " + nombreFisico);
            return false;
        }
    }


    public static bool CargarRecurso<T>(ref T ar, string nombreFisico)
    {
        if (File.Exists(Application.dataPath + "/Resources" + "/" + nombreFisico))
        {
            file = File.Open(Application.dataPath + "/Resources" + "/" + nombreFisico, FileMode.Open);
            ar = (T)bf.Deserialize(file);
            file.Close();
            return true;
        }
        else
        {
            Debug.Log("Archivo no encontrado: " + nombreFisico);
            return false;
        }
    }

    public static void GuardarDatos<T>(ref T ar, string nombreFisico)
    {
        file = File.Create(Application.persistentDataPath + "/" + nombreFisico);
        bf.Serialize(file, ar);
        file.Close();
    }

    public static void GuardarRecurso<T>(ref T ar, string nombreFisico)
    {
        file = File.Create(Application.dataPath + "/Resources" + "/" + nombreFisico);
        bf.Serialize(file, ar);
        file.Close();
    }

    public static bool Exist(string nombreFisico)
    {
        return File.Exists(Application.persistentDataPath + "/" + nombreFisico);
    }

    public static string[] GetFilesAt(string nombreFisico)
    {
        return Directory.GetFiles(Application.persistentDataPath + "/" + nombreFisico);
    }

    public static bool EraseFile(string nombreFisico)
    {
        Debug.Log("<color=yellow>EraseFile: </color>" + nombreFisico);
        if (Exist(nombreFisico))
        {
            File.Delete(Application.persistentDataPath + "/" + nombreFisico);
            return true;
        }
        else
        {
            Debug.Log("<color=yellow>Archivo no encontrado: </color>" + nombreFisico);
            return false;
        }
    }
}
