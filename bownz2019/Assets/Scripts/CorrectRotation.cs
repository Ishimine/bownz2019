﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class CorrectRotation : MonoBehaviour
{
    public Transform terrainTransform;

    [Button]
    public void Correct()
    {
        List<Transform> children = new List<Transform>();
        foreach (Transform item in terrainTransform)    children.Add(item);

        foreach (Transform item in children)
        {
            if (item.gameObject.tag.Equals("Terrain"))
            {
                item.rotation = Quaternion.identity;
            }
            else
            {
                Vector3 angles = item.localEulerAngles;
                item.rotation = Quaternion.Euler(angles.x, angles.y, Mathf.RoundToInt(item.localEulerAngles.z));
            }
        }
    }

}
