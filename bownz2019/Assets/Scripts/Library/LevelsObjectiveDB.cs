﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RotaryHeart.Lib.SerializableDictionary;
using Sirenix.OdinInspector;

[CreateAssetMenu]
public class LevelsObjectiveDB : ScriptableSingleton<LevelsObjectiveDB>
{
    public override LevelsObjectiveDB Myself => this;

    public LevelProgressDictionary objectives;

    public GameObjectDataBase[] levelsDatabases;

    [Button]
    public void ProcessObjectives()
    {
        List<KeyValuePair<string,LevelProgress>> allProgressData = new List<KeyValuePair<string,LevelProgress>>();
        for (int i = 0; i < levelsDatabases.Length; i++)
            allProgressData.AddRange(CollectAllLevelsProgressData(levelsDatabases[i]));

     Clear();
     for (int i = 0; i < allProgressData.Count; i++)
        {
            objectives.Add(allProgressData[i].Key, allProgressData[i].Value);
        }
    }

    [Button]
    public void Clear()
    {
        objectives.Clear();
    }

    public List<KeyValuePair<string,LevelProgress>> CollectAllLevelsProgressData( GameObjectDataBase db)
    {
        List<GameObject> objects = new List<GameObject>(db.Elements.Values);
        List<KeyValuePair<string,LevelProgress>> retValue = new List<KeyValuePair<string,LevelProgress>>();
        for (int i = 0; i < objects.Count; i++)
        {
            Level_Objectives levelObjectives = objects[i].GetComponent<Level_Objectives>();
            if (objectives == null) continue;

            string key = GetLevelId(db, i, levelObjectives, false);
            retValue.Add(new KeyValuePair<string, LevelProgress>( key, levelObjectives.Normal));
                               
            string keyChallenge = GetLevelId(db, i, levelObjectives, true);
            retValue.Add(new KeyValuePair<string, LevelProgress>( keyChallenge, levelObjectives.Challenge));
            levelObjectives.SetKeys(key, keyChallenge);
        }
        return retValue;
    }

    public static string GetLevelId(GameObjectDataBase dataBase, int levelIndex, Level_Objectives levelObjectives, bool isChallenge)
    {
        return isChallenge ? $"{dataBase.name}_{levelIndex}_Challenge" : $"{dataBase.name}_{levelIndex}";
    }

}

[System.Serializable]
public class LevelProgressDictionary : SerializableDictionaryBase<string, LevelProgress>
{

}
