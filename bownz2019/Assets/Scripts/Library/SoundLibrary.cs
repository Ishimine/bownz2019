﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEditor;
using RotaryHeart.Lib.SerializableDictionary;
using Sirenix.OdinInspector;

[CreateAssetMenu(menuName = "Library/SoundLibrary")]
public class SoundLibrary : BaseLibrary<SoundDictionary ,Sound>
{
    public static List<string> GetKeys()
    {
        return new List<string>(Instance.Library.Keys);
    }

    public static List<string> GetKeysFrom(string key)
    {
        return GetKeys().FindAll(x => x.Contains(key));
    }


    #region Editor

#if UNITY_EDITOR

    public AudioMixerGroup defaultMixer;


    [OnValueChanged("QuickAddMany")]
    public AudioClip[] addMany;

    protected void QuickAddMany(AudioClip[] audioClip)
    {
        foreach (var item in addMany)
        {
            QuickAdd(item);
        }
        addMany = null;
    }

    [OnValueChanged("QuickAdd")]
    public AudioClip add;

    protected void QuickAdd(AudioClip audioClip)
    {
        if (!Instance.ContainElementKey(audioClip.name))
        {
            Instance.AddElement(audioClip.name, new Sound(audioClip, .5f, defaultMixer));
        add = null;
        }
    }

    protected override void QuickAdd(Sound prefab)
    {
        base.QuickAdd(prefab);
    }

#endif

#endregion
}

[System.Serializable]
public class SoundDictionary : SerializableDictionaryBase<string,Sound>
{
}