﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RotaryHeart.Lib.SerializableDictionary;
using Sirenix.OdinInspector;

[CreateAssetMenu(menuName = "Library/GameObject")]
public class GameObjectLibrary : BaseLibrary<GameObjectDictionary, GameObject>
{
    public static GameObject Instantiate(string key, Transform parent = null)
    {
        var rValue = Object.Instantiate(Get(key), parent);
        rValue.name = key;
        return rValue;
    }
                               
    public static T Instantiate<T>(string key, Transform parent = null)
    {
        return Instantiate(key, parent).GetComponent<T>();
    }

    [Button]
    public void InstantiateElement(string key, Transform parent = null)
    {
        var rValue = Object.Instantiate(Get(key), parent);
        rValue.name = key;
    }
}

[System.Serializable]
public class GameObjectDictionary : SerializableDictionaryBase<string, GameObject>
{
}