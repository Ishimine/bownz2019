﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RotaryHeart.Lib.SerializableDictionary;
using Sirenix.OdinInspector;

[CreateAssetMenu(menuName = "Library/Sprite")]
public class SpriteLibrary : BaseLibrary<SpriteDictionary, Sprite>
{
    public override Sprite GetElement(string key)
    {
        if (ContainsKey(key))
            return base.GetElement(key);
        else
            return null;
    }
}

[System.Serializable]
public class SpriteDictionary : SerializableDictionaryBase<string, Sprite>
{
}
