﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using RotaryHeart.Lib;
using RotaryHeart.Lib.SerializableDictionary;

/// <summary>
/// Clase de libreria ScriptableObject base, Para que los hijos de esta clase puedan ser serializados por unity
/// tiene que declararse como clase aparte "SerializableDictionaryBase<string,T>". para que unity pueda levantarla
/// </summary>
/// <typeparam name="B"></typeparam>
/// <typeparam name="T"></typeparam>

public abstract class BaseLibrary<B,T> : ScriptableSingleton<BaseLibrary<B,T>> where B : SerializableDictionaryBase<string,T> , new()
{
    public override BaseLibrary<B,T> Myself => this;

    [SerializeField]
    private bool printInteractions = false;
    public bool PrintInteractions
    {
        get { return printInteractions; }
        set { printInteractions = value; }
    }

    [SerializeField]
    private B library = new B();
    public B Library
    {
        get { return library; }
        set { library = value; }
    }

    public void AddElement(string key, T nElement)
    {
        if(printInteractions) Debug.Log(string.Format("{0} {1}: {2}", this, "Add", key));
        library.Add(key, nElement);
    }

    public void RemoveElement(string key)
    {
        if(printInteractions) Debug.Log(string.Format("{0} {1}: {2}", this, "Remove", key));
        library.Remove(key);
    }

    public virtual T GetElement(string key)
    {
        if(printInteractions) Debug.Log(string.Format("{0} {1}: {2}", this, "Get", key));
        if (library.ContainsKey(key))
            return library[key];
        else
        {
            Debug.LogError(this + " elemento NO encontrado: " + key);
            return default;
        }
    }

    public bool ContainElementKey(string key)
    {
        return library.ContainsKey(key);
    }

    public bool ContainElementValue(T value)
    {
        return library.ContainsValue(value);
    }

    #region Statics
    public static void Add(string key, T nElement)
    {
        Instance.AddElement(key, nElement);
    }

    public static void Remove(string key)
    {
        Instance.RemoveElement(key);
    }

    public static T Get(string key)
    {
        return Instance.GetElement(key);
    }

    public static bool ContainsKey(string key)
    {
        return Instance.ContainElementKey(key);
    }

    public static bool ContainsValue(T value)
    {
        return Instance.ContainElementValue(value);
    }
    #endregion

    #region Editor


#if UNITY_EDITOR
    [Button]
    protected virtual void QuickAdd(T prefab)
    {
        if (!Instance.ContainElementKey(prefab.ToString())) Instance.AddElement(prefab.ToString(), prefab);
    }
#endif
    #endregion
}

