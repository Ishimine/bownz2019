﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RotaryHeart.Lib.SerializableDictionary;

[CreateAssetMenu(menuName = "Library/ScriptableDataBaseLibrary")]
public class ScriptableDataBaseLibrary : BaseLibrary<ScriptableDataBaseDictionary, ScriptableObject>
{
  /*  public static GameObject Instantiate(string key, Transform parent = null)
    {
        var rValue = Object.Instantiate(Get(key), parent);
        rValue.name = key;
        return rValue;
    }

    [Button]
    public void InstantiateElement(string key, Transform parent = null)
    {
        var rValue = Object.Instantiate(Get(key), parent);
        rValue.name = key;
    }*/
}

[System.Serializable]
public class ScriptableDataBaseDictionary : SerializableDictionaryBase<string, ScriptableObject>
{
}

