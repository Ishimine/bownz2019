﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using RotaryHeart.Lib.SerializableDictionary;

[CreateAssetMenu(menuName = "DataBase/GameObjectsDB")]
public class GameObjectDataBase : ScriptableDataBase<GameObjectDictionary, GameObject>
{
    [Button]
    public void Add(GameObject[] gos)
    {
        Elements.Clear();
        for (int i = 0; i < gos.Length; i++)
        {
            Elements.Add(i.ToString(), gos[i]);
        }
    }
}
