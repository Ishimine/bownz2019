﻿using RotaryHeart.Lib.SerializableDictionary;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[CreateAssetMenu(menuName ="DataBase/ScriptableObjects")]
public abstract class ScriptableDataBase<T,B> : ScriptableObject where T : SerializableDictionaryBase<string, B>
{
    [SerializeField] private T elements = null;
    public T Elements
    {
        get { return elements; }
    }
}

