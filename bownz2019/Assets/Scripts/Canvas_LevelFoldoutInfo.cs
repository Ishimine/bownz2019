﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

public class Canvas_LevelFoldoutInfo : MonoBehaviour
{
    [SerializeField] private AnimationCurve sizeIn;
    [SerializeField] private AnimationCurve sizeOut;

    [SerializeField] private UI_ObjectiveInfo platformData;
    [SerializeField] private UI_ObjectiveInfo timeData;
    [SerializeField] private UI_ObjectiveInfo deathData;

    [SerializeField] private Text levelLabel;


    [SerializeField] private float closeDuration;
    [SerializeField] private float openDuration;

    bool inAnimation = false;

    public Transform conteiner;

    private CodeAnimator codeAnimator = new CodeAnimator();

    bool isOpen = false;

    LevelFoldoutInfo_Request request;

    private void Awake()
    {
        Hide();
    }

    [Button]
    private void Hide()
    {
        inAnimation = false;
        isOpen = false;
        conteiner.transform.localScale = Vector3.zero;
    }

    public void Close()
    {
        Close(null);
    }

    public void Close(Action callback)
    {
        if (inAnimation || !isOpen) return;
        inAnimation = false;

        //request = null;
        //request = LevelFoldoutInfo_Request.NullRequest();
        codeAnimator.StartAnimacion(this,
            x =>
            {
                conteiner.transform.localScale = Vector3.one * sizeOut.Evaluate(x);
            }, DeltaTimeType.deltaTime, AnimationType.Simple, null, 
            ()=>
            {
                isOpen = false;
                inAnimation = false;
                callback?.Invoke();
                //request = LevelFoldoutInfo_Request.NullRequest();
            }, closeDuration);
    }

    private void OnEnable()
    {
        this.AddObserver(Show, Notifications.LevelFoldoutInfoRequest);
        this.AddObserver(Close, Notifications.LevelFoldoutInfoClose);
    }

    private void Close(object sender, object postAction)
    {
        Close();
    }

    private void OnDisable()
    {
        this.RemoveObserver(Show, Notifications.LevelFoldoutInfoRequest);
        this.RemoveObserver(Close, Notifications.LevelFoldoutInfoClose);
    }

    private void Show(object arg1, object arg2)
    {
        Show((LevelFoldoutInfo_Request)arg2);
    }

    public void Show(LevelFoldoutInfo_Request nRequest)
    {
        if(request.Equals(nRequest) || inAnimation)
            return;

        request = nRequest;
        if (isOpen)
        {
            Close(
                ()=>
                {
                    Open(null);
                });
        }
        else
        {
            Open(null);
        }
    }

    private void ApplyRequestData()
    {
        levelLabel.text = request.Label;
        platformData.Show(request.PlayerScore.Platforms, request.TargetScore.Platforms, "0.");
        timeData.Show(request.PlayerScore.Time, request.TargetScore.Time, ".0");
        deathData.Show(request.PlayerScore.Deaths, request.TargetScore.Deaths, "0.");
    }

    [Button]
    private void Open(Action callback)
    {
        if (inAnimation) return;

        ApplyRequestData();

        Vector2Int requestRoundPosition = new Vector2Int(Mathf.RoundToInt(request.Position.x),Mathf.RoundToInt(request.Position.y));
        Vector2Int canvasRoundPosition = new Vector2Int(Mathf.RoundToInt(transform.position.x),Mathf.RoundToInt(transform.position.y));

        Vector2 pivot = CalculatePivot(requestRoundPosition, canvasRoundPosition);

        Transform conteinerTransform = conteiner.transform;
        ((RectTransform) conteinerTransform).pivot = pivot;
        ((RectTransform) conteinerTransform).position = request.Position;

        inAnimation = true;
        codeAnimator.StartAnimacion(this,
            x =>
            {
                conteinerTransform.localScale = Vector3.one * sizeIn.Evaluate(x);
            }, DeltaTimeType.deltaTime, AnimationType.Simple, null,
           ()=>
           {
               inAnimation = false;
               isOpen = true;
               callback?.Invoke();
           }, closeDuration);
    }

    private static Vector2 CalculatePivot(Vector2Int requestRoundPosition, Vector2Int canvasRoundPosition)
    {
        Vector2 pivot;
        if (requestRoundPosition.x < canvasRoundPosition.x)
            pivot.x = 0;
        else if (requestRoundPosition.x > canvasRoundPosition.x)
            pivot.x = 1;
        else
            pivot.x = 0.5f;

        if (requestRoundPosition.y < canvasRoundPosition.y)
            pivot.y = 0;
        else if (requestRoundPosition.y > canvasRoundPosition.y)
            pivot.y = 1;
        else
            pivot.y = 0.5f;
        return pivot;
    }

    public void Play()
    {
        Close();
        this.PostNotification(
            Notifications.LevelLoadRequest, 
            new LevelRequest(request.ModeId, request.LevelIndex));
    }
}

public struct LevelFoldoutInfo_Request: IEquatable<LevelFoldoutInfo_Request>
{
    private string label;
    public string Label => label;
    private string modeId;
    public string ModeId => modeId;
    private int levelIndex;
    public int LevelIndex => levelIndex;
    private LevelProgress playerScore;
    public LevelProgress PlayerScore => playerScore;
    private LevelProgress targetScore;
    public LevelProgress TargetScore => targetScore;

    public Vector3 Position { get; set; }

    public LevelFoldoutInfo_Request(string label, int levelIndex, string modeId, LevelProgress playerScore, LevelProgress targetScore, Vector3 position)
    {
        this.label = label;
        this.modeId = modeId;
        this.levelIndex = levelIndex;
        this.playerScore = playerScore;
        this.targetScore = targetScore;
        Position = position;
    }


    public bool Equals(LevelFoldoutInfo_Request other)
    {
        return string.Equals(label, other.label) && string.Equals(modeId, other.modeId) && levelIndex == other.levelIndex && playerScore.Equals(other.playerScore) && targetScore.Equals(other.targetScore) && Position.Equals(other.Position);
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        return obj is LevelFoldoutInfo_Request other && Equals(other);
    }

    public override int GetHashCode()
    {
        unchecked
        {
            int hashCode = (label != null ? label.GetHashCode() : 0);
            hashCode = (hashCode * 397) ^ (modeId != null ? modeId.GetHashCode() : 0);
            hashCode = (hashCode * 397) ^ levelIndex;
            hashCode = (hashCode * 397) ^ playerScore.GetHashCode();
            hashCode = (hashCode * 397) ^ targetScore.GetHashCode();
            hashCode = (hashCode * 397) ^ Position.GetHashCode();
            return hashCode;
        }
    }

    public static LevelFoldoutInfo_Request NullRequest()
    {
        return new LevelFoldoutInfo_Request("",-1,"", new LevelProgress(), new LevelProgress(), Vector3.zero);
    }
}