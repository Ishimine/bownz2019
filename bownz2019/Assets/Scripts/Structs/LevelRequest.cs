﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct LevelRequest 
{
    public string Id;
    public int Index;

    public string FullId => Id + Index.ToString();

    public LevelRequest(string id, int index)
    {
        Id = id;
        Index = index;
    }
}
