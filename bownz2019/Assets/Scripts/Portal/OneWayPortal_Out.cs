﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class OneWayPortal_Out : MonoBehaviour, ITrigger<BallStateMachine>
{
    public float outForce = 10;

    private Action<BallStateMachine> onBallTeleport_Receive;
    public Action<BallStateMachine> OnBallTeleport_Receive { get { return onBallTeleport_Receive; } set { onBallTeleport_Receive = value; } }

    public Action<BallStateMachine> OnTrigger { get => OnBallTeleport_Receive; set => OnBallTeleport_Receive = value; }

    public void Receive(BallStateMachine ballStateMachine)
    {
        Debug.Log("Receive: " + ballStateMachine);
        OnBallTeleport_Receive?.Invoke(ballStateMachine);

        ballStateMachine.transform.position = transform.position;

        var rb = ballStateMachine.GetComponent<Rigidbody2D>();
        var animation = ballStateMachine.GetComponent<Ball_Animation>();

        ballStateMachine.AppearToLiving(
            ()=>
            {
                animation.SetFace(animation.faces.Happy.GetRandom());
                rb.AddForce(transform.TransformDirection(Vector3.right) * outForce, ForceMode2D.Impulse);
            });
    }
}


public interface ITrigger
{
    Action OnTrigger { get; set; }
}

public interface ITrigger<T>
{
    Action<T> OnTrigger { get; set; }
}