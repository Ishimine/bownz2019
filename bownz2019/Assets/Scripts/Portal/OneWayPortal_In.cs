﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class OneWayPortal_In : MonoBehaviour, ITrigger<BallStateMachine>
{
    private Action<BallStateMachine> onBallTeleport_Send;
    public Action<BallStateMachine> OnBallTeleport_Send { get { return onBallTeleport_Send; } set { onBallTeleport_Send = value; } }

    private CodeAnimator codeAnimator = new CodeAnimator();

    private Action<BallStateMachine> onBallTeleport_Start;
    public Action<BallStateMachine> OnTrigger { get => onBallTeleport_Start; set => onBallTeleport_Start = value; }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        BallStateMachine ball = collision.gameObject.GetComponent<BallStateMachine>();
       
        if (ball != null)
        {
            OnTrigger?.Invoke(ball);
            ball.ChangeToPosing();
            ball.Disappear(() => Send(ball));
            Vector2 startPosition = ball.transform.position;
            codeAnimator.StartAnimacion(this,
                x =>
                {
                    ball.transform.position = Vector2.Lerp(startPosition, transform.position, x);
                }, DeltaTimeType.deltaTime, AnimationType.Simple, ball.disappearCurve.Curve, ball.disappearCurve.Time/2);

        }
    }

    public void Send(BallStateMachine ball)
    {
        OnBallTeleport_Send?.Invoke(ball);
    }
}
