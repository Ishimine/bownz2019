﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TwoWayPortal : MonoBehaviour
{

    private Dictionary<Portal, Portal> fromToPairs;

    public PortalPair[] portalPairs;

    private void Awake()
    {
        Initialize();
    }

    private void Initialize()
    {
        fromToPairs = new Dictionary<Portal, Portal>();
        foreach (var item in portalPairs)
        {
            fromToPairs.Add(item.From, item.To);
            item.From.OnSendBody -= OnSendBody_Manage;
            item.From.OnSendBody += OnSendBody_Manage;
        }
    }

    private void OnSendBody_Manage(TeleportData obj)
    {
        fromToPairs[obj.SourcePortal].ReceiveBody(obj);
    }

    

    private void OnDrawGizmosSelected()
    {
        foreach (var item in portalPairs)
            item.DrawDebug();
    }
}


[System.Serializable]
public struct PortalPair
{
    public Portal From;
    public Portal To;

    public PortalPair(Portal from, Portal to)
    {
        From = from;
        To = to;
    }

    public void DrawDebug()
    {
        DrawArrow.ForDebug(From.transform.position, To.transform.position  - From.transform.position,2,20);
    }
}

