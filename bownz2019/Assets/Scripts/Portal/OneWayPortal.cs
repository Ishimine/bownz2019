﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneWayPortal : MonoBehaviour
{
    public OneWayPortal_In portalIn;
    public OneWayPortal_Out portalOut;

    public CodeAnimatorCurve CodeAnimatorCurve;

    private CodeAnimator codeAnimator = new CodeAnimator();

    private void Awake()
    {
        LinkPortals();
    }

    private void OnDestroy()
    {
        UnlinkPortals();
    }

    public void LinkPortals()
    {
        portalIn.OnBallTeleport_Send -= portalOut.Receive;
        portalIn.OnBallTeleport_Send += portalOut.Receive;
    }

    public void UnlinkPortals()
    {
        portalIn.OnBallTeleport_Send -= portalOut.Receive;
    }

}
