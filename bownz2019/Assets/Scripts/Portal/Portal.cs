﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Sirenix.OdinInspector;

public class Portal : MonoBehaviour
{
    public float teleportDuration = .25f;

    public bool drawDebug = false;
    public bool CanSend = true;

    public bool ResetPositionToCenterOfPortal = false;

    public bool ModifyVelocityBasedOnDirection = true;
    public bool OutVelocityEqualsOutPortalDirection = false;

    private HashSet<Teleportable> received = new HashSet<Teleportable>();
    
    private Action<TeleportData> onSendBody;
    public Action<TeleportData> OnSendBody
    {
        get { return onSendBody; }
        set { onSendBody = value; }
    }

    private Action<TeleportData> onReceiveBody;
    public Action<TeleportData> OnReceiveBody
    {
        get { return onReceiveBody; }
        set { onReceiveBody = value; }
    }

    [SerializeField]  private TriggerFX[] fXs;
    public TriggerFX[] FXs
    {
        get { return fXs; }
        set { fXs = value; }
    }

    public virtual void ReceiveBody(TeleportData tpData)
    {
        Debug.Log(this + "  ReceiveBody " + tpData.ToString());

        foreach (var item in FXs)
            item.Receive(tpData);

        received.Add(tpData.Teleportable);
        tpData.Teleportable.transform.position = (ResetPositionToCenterOfPortal) ? transform.position : transform.TransformPoint(tpData.LocalPosition);

        if (ModifyVelocityBasedOnDirection) RotateVelocityOfRB(tpData);

        StartCoroutine(WaitAndExecute(()=>
        {
            tpData.Teleportable.TeleportationEnded();
            OnReceiveBody?.Invoke(tpData);
        }));
    }

    protected IEnumerator WaitAndExecute(Action postAction)
    {
        yield return new WaitForSeconds(teleportDuration);
        postAction?.Invoke();
    }

    private void RotateVelocityOfRB(TeleportData tpData)
    {
        var rb = tpData.Teleportable.Rb;
        if (OutVelocityEqualsOutPortalDirection)
            rb.velocity = transform.TransformDirection(Vector2.right).normalized * rb.velocity.magnitude;
        else
        {
            rb.velocity = Vector2.Reflect(rb.velocity, tpData.SourcePortal.transform.right);
            rb.velocity = Quaternion.Euler(0, 0, Mathf.DeltaAngle(tpData.SourcePortal.transform.eulerAngles.z, transform.eulerAngles.z)) * rb.velocity;
        }
    }

    public virtual void SendBody(TeleportData tpData)
    {
        if (!CanSend) return;

        Debug.Log(this + " "+ transform.position + "  Sending body " + tpData.ToString());

        foreach (var item in FXs)
            item.Send(tpData);

        tpData.Teleportable.TeleportationStarted();
        OnSendBody?.Invoke(tpData);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        Teleportable tp = collision.attachedRigidbody.GetComponent<Teleportable>();
        if(tp != null && received.Contains(tp))
        {
            received.Remove(tp);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.attachedRigidbody != null)
        {
            Teleportable tp = collision.attachedRigidbody.GetComponent<Teleportable>();
            if (tp != null && !received.Contains(tp))
                SendBody(new TeleportData(tp, this));
        }
        else
        {
            Debug.Log(collision.gameObject.name + " RB is null");
        }
  
    }


    private void OnDrawGizmos()
    {
        if (!drawDebug) return;
        Vector3 targetPos = transform.TransformPoint(Vector2.right*2);
        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(transform.position, 1);
        Gizmos.color = Color.green;
        Gizmos.DrawCube(targetPos, Vector3.one*.5f);
        Gizmos.color = Color.white;
        Gizmos.DrawLine(transform.position, targetPos);
    }
}

public struct TeleportData
{
    public Teleportable Teleportable;
    public Portal SourcePortal;
    private Vector2 localPosition;

    public Vector2 LocalPosition => localPosition;

    public TeleportData(Teleportable teleportable, Portal sourcePortal)
    {
        Teleportable = teleportable;
        SourcePortal = sourcePortal;
        localPosition = sourcePortal.transform.InverseTransformPoint(teleportable.transform.position);
    }

    public Vector3 Difference()
    {
        return SourcePortal.transform.position- Teleportable.transform.position;
    }

    public override string ToString()
    {
        return string.Format("Teleporting <color=green> {0} </color>  from <color=blue> {1} </color> ", Teleportable.name, SourcePortal.name);
    }
}