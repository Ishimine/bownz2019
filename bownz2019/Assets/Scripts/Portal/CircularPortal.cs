﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircularPortal : Portal
{
    [SerializeField] private CodeAnimatorCurve curveAnimation = new CodeAnimatorCurve();

    CodeAnimator codeAnimator = new CodeAnimator();

    public override void ReceiveBody(TeleportData tpData)
    {
        tpData.Teleportable.gameObject.PostNotification(Notifications.Activate);
        base.ReceiveBody(tpData);
    }

    public override void SendBody(TeleportData tpData)
    {
        tpData.Teleportable.gameObject.PostNotification(Notifications.Deactivate);
        Vector2 startPosition = tpData.Teleportable.transform.position;
        codeAnimator.StartAnimacion(this,
            x=>
            {
                tpData.Teleportable.transform.position = Vector3.Lerp(startPosition, transform.position, x);
            }, DeltaTimeType.deltaTime, AnimationType.Simple, curveAnimation.Curve,()=> base.SendBody(tpData), curveAnimation.Time);
    }
}
