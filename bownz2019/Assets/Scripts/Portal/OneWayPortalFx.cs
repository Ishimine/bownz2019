﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneWayPortalFx : MonoBehaviour
{
    public ITrigger<BallStateMachine> source;
    public ShineOnContact onContact;

    private void Awake()
    {
        source = GetComponent<ITrigger<BallStateMachine>>();
    }

    private void OnEnable()
    {
        source.OnTrigger -= ReceiveDone;
        source.OnTrigger += ReceiveDone;
    }

    private void OnDisable()
    {
        source.OnTrigger -= ReceiveDone;
    }

    private void ReceiveDone(BallStateMachine obj)
    {
        onContact.OnCollisionEnter2D(null);
    }
}
