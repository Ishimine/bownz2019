﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_NextLevelButton : MonoBehaviour
{
    public UI_EnableByFlag EnableByFlag;
    public Button button;

    private void OnEnable()
    {
        EnableByFlag.OnChange += Refresh;
        SetState(DoesNextLevelExist());
    }

    private void OnDisable()
    {
        EnableByFlag.OnChange -= Refresh;
    }

    private void Refresh(bool obj)
    {
        SetState(DoesNextLevelExist());
    }

    public bool DoesNextLevelExist()
    {
        BaseException queryBool = new BaseException(false);
        this.PostNotification(Notifications.Query_DoesNextLevelExist, queryBool);
        return queryBool.toggle;
    }

    public void SetState(bool value)
    {
        button.interactable = value;
        transform.localScale = (value)?Vector3.one:Vector3.zero;
    }
}

