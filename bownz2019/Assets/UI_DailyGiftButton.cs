﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_DailyGiftButton : MonoBehaviour
{
    public Button button;

    public void TryToClaim()
    {
        int lastGiftDay = PlayerPrefs.GetInt("LastGiftDay", -1);
        int lastGiftYear = PlayerPrefs.GetInt("LastGiftYear", -1);

        int currentDay = DateTime.Today.DayOfYear;
        int currentYear = DateTime.Today.Year;

        if (currentYear > lastGiftYear)
            lastGiftDay = 0;

        if (currentYear >= lastGiftYear && currentDay > lastGiftDay && Shop.Instance.AvailableProducts.Count > 0)
        {
            UI_Messenger.Show(
                new Interactive_Msg(
                    "Daily Gift", 
                    "Get a free Gift Box every day", 
                    new Button_Option[]
                    {
                        new Button_Option("CLAIM",()=> Claim(currentDay,currentYear), 0),
                    }));
        }
    }

    private void Claim(int currentDay, int currentYear)
    {
        Inventory.Add(Inventory.IDs.GiftBox, 1, true);
        PlayerPrefs.SetInt("LastGiftDay", currentDay);
        PlayerPrefs.SetInt("LastGiftYear", currentYear);
        RefreshState();

        UI_Messenger.Show(new Toast_Msg(" ( ＾◡＾)♡ Comeback tomorrow to claim a new FREE Gift ♡", MsgPosition.Top, 2.5f, null));
    }

    private void OnEnable()
    {
        RefreshState();
    }

    private void RefreshState()
    {
        button.gameObject.SetActive(CanClaim());
    }

    private bool CanClaim()
    {
        int lastGiftDay = PlayerPrefs.GetInt("LastGiftDay", -1);
        int lastGiftYear = PlayerPrefs.GetInt("LastGiftYear", -1);

        int currentDay = DateTime.Today.DayOfYear;
        int currentYear = DateTime.Today.Year;

        if (currentYear > lastGiftYear)
            lastGiftDay = 0;

        return currentYear >= lastGiftYear && currentDay > lastGiftDay;
    }
}
