﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class TravelTo : MonoBehaviour
{
    private BallStateMachine ball;
    private Vector2 targetPos;
    private Vector2 startPos;

    public AnimationCurve speedCurve;
    public AnimationCurve sizeCurve;
    public float maxSpeed = 10;

    public ParticleSystem chargeParticles;
    public ParticleSystem explotionParticles;

    private CodeAnimator codeAnimator = new CodeAnimator();

    public void Initialize(BallStateMachine nBall, Vector2 nTargetPos, Vector2 nStartPosition)
    {
        ball = nBall;
        targetPos = nTargetPos;
        startPos = nStartPosition;
    }

    public void Initialize(BallStateMachine nBall, Vector2 nTargetPos)
    {
        Initialize(nBall, targetPos, nBall.transform.position);
    }

    private void Position(Action callback)
    {
        Vector2 start = ball.transform.position;
        codeAnimator.StartAnimacion(this,
            x => { ball.transform.position = Vector3.Lerp(start, startPos, x); }
            , DeltaTimeType.deltaTime, AnimationType.Simple, AnimationCurve.EaseInOut(0,0,1,1),callback, 1);
    }

    [Button]
    public void Execute(Action startCallback = null, Action endCallback = null)
    {
        chargeParticles.transform.position = startPos;
        explotionParticles.transform.position = startPos;
        ball.Rb.simulated = false;
        if (ball.transform.position != (Vector3)startPos)
            Position(()=> Execute(startCallback, endCallback));
        else
        {
            chargeParticles.Play();
            codeAnimator.StartWaitAndExecute(this, 2, ()=> Fire(startCallback, endCallback), false);
        }
    }

    [Button]
    public void Fire(Action startCallback,  Action endCallback = null)
    {
        startCallback?.Invoke();
        chargeParticles.Stop();
        explotionParticles.Play();
        Vector2 dir = startPos.GetDirection(targetPos);
        Vector2 startsize = ball.transform.localScale;
        Vector2 maxSize = startsize*1.5f;

        float duration =  Vector2.Distance(startPos, targetPos) / maxSpeed;
            codeAnimator.StartAnimacion(this,
                x =>
                {
                    ball.transform.localScale = Vector3.Lerp(startsize, maxSize, sizeCurve.Evaluate(x));
                    ball.transform.position = Vector3.LerpUnclamped(startPos, targetPos, speedCurve.Evaluate(x));
                }, DeltaTimeType.fixedDeltaTime, AnimationType.Simple, null,
                ()=>
                {
                    endCallback?.Invoke();
                    Done();
                }, duration);
    }

    private void Done()
    {
        ball.Rb.simulated = true;
        ball.Rb.velocity = Vector2.up;
        Destroy(this.gameObject);
    }
}
