﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_UseVibration : MonoBehaviour
{
    public void SwapUseVibration()
    {
        Settings.UseVibration = !Settings.UseVibration;
    }
}
