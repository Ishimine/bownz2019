﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_BackgroundToggle : MonoBehaviour
{
   public Text txt;
   public void Pressed()
   {
      Settings.Background_Enable = !Settings.Background_Enable;
      txt.text = $"Background: {(Settings.Background_Enable?"On":"Off")}";
   }
}
