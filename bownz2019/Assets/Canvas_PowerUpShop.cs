﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Canvas_PowerUpShop : MonoBehaviour
{
    
    
    public UI_ConteinerOpenClose OpenClose;
    public Transform powerUpConteiner;

    public List<UI_ShopPowerUpItem> items;

    private void OnEnable()
    {
        this.AddObserver(OnPowerUpShop, Notifications.ShopOpenClose);
        this.AddObserver(OnGameEnd, Notifications.GameMode_End);
        this.AddObserver(OnGamePlay_Start, Notifications.GamePlay_Start);
                                                      
        powerUpConteiner.GetComponentsInChildren(true, items);
        foreach (UI_ShopPowerUpItem item in items)
        {
            item.OnRewardItemCallback -=  CloseShop;
            item.OnRewardItemCallback +=  CloseShop;
        }

        AdsManager.VideoLoadedEvent += OnVideoLoadedEvent;
        AdsManager.VideoClosedEvent += VideoClosedEvent;

        if (AdsManager.IsVideoReady() && CanBuy())
        {        
            this.PostNotification(Notifications.PowerUpCanBuy,CanBuy());
        }
    }

    private void CloseShop()
    {
        OpenOrClose(false, ()=> this.PostNotification(Notifications.ShopClosed));
    }

    private void OnDisable()
    {
        AdsManager.VideoLoadedEvent -= OnVideoLoadedEvent;
        AdsManager.VideoClosedEvent -= VideoClosedEvent;

        this.RemoveObserver(OnPowerUpShop, Notifications.ShopOpenClose);
        this.RemoveObserver(OnGameEnd, Notifications.GameMode_End);
        this.RemoveObserver(OnGamePlay_Start, Notifications.GamePlay_Start);
    }

    private void OnGamePlay_Start(object arg1, object arg2)
    {
       this.PostNotification(Notifications.PowerUpCanBuy,CanBuy());
    }

    private void VideoClosedEvent()
    {
        OpenClose.Close(
            () =>
            {
                this.PostNotification(Notifications.PowerUpCanBuy,CanBuy());
            });
    }


    private void OnVideoLoadedEvent()
    {
        this.PostNotification(Notifications.PowerUpCanBuy, CanBuy());
    }


    private void OnGameEnd(object arg1, object arg2)
    {
        OpenClose.Hide();
    }

    private void OnPowerUpShop(object arg1, object arg2)
    {
        OpenOrClose((bool)arg2);
    }

    private void OpenOrClose(bool value, Action callback = null)
    {
        this.PostNotification(Notifications.PauseRequest, value);
        this.PostNotification(Notifications.PowerUpCanBuy,CanBuy());
        OpenClose.OpenOrClose(value, callback);
    }

    private bool CanBuy()
    {
        if (!AdsManager.IsVideoReady()) return false;
        return true;
    }
}
