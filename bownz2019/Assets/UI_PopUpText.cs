﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;
public class UI_PopUpText : MonoBehaviour
{
    [SerializeField] private AnimationCurve curve;
    [SerializeField] private AnimationCurve alphaCurve;
    [SerializeField] private float duration = .5f;
    [SerializeField] private float height = 10;
    [SerializeField] private Text text;

    private Vector2 posOffset;

    public string CurrentText => text.text;

    private void Start()
    {
        gameObject.SetActive(false);
    }

    [Button]
    public void Play(string text)
    {
        Play(Vector2.zero,text);
    }

    public void Play(Vector2 offset,string text)
    {
        this.text.text = text;
        posOffset = offset;
        gameObject.SetActive(true);
        StartCoroutine(Animation());
    }

    IEnumerator Animation()
    {
        float x = 0;
        Color baseColor = text.color;
        do
        {
            x += Time.deltaTime / duration;
            transform.localPosition = Vector3.up * curve.Evaluate(x) * height + (Vector3)posOffset;
            baseColor.a = alphaCurve.Evaluate(x);
            text.color = baseColor;
            yield return null;
        } while (x < 1);
        gameObject.SetActive(false);
    }
}
