using System;
using UnityEngine;

public abstract class ShopReward : MonoBehaviour
{ 
    public bool infinityUses = false;
    public int used = 0;
    public int maxUses = 1;

    public abstract string RewardId { get; }

    private void Awake()
    {
        this.AddObserver(OnGameGoResponse, Notifications.GamePlay_Start);
    }

    protected void OnDestroy()
    {
        this.RemoveObserver(OnGameGoResponse, Notifications.GamePlay_Start);
    }

    private void OnGameGoResponse(object arg1, object arg2)
    {
        used = 0;
    }

    public bool CanBuy()
    {
        return (infinityUses || used < maxUses) && _CanBuy(RewardId);
    }

    public void Reward()
    {
        used++;
        _Reward();
    }

    protected abstract void _Reward();
    
    protected bool _CanBuy(string rewardId)
    {
        Debug.Log("_CanBuy " + rewardId);
        Argument<bool, string> validator = new Argument<bool, string>(true,rewardId);
        this.PostNotification(Notifications.ValidateBuy+rewardId, validator);
        Debug.Log("Is " + validator.Item0);
        return validator.Item0;
    }
}

