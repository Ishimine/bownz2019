﻿using System.Collections;
using System.Collections.Generic;
using TriangleNet.Geometry;
using UnityEngine;

public class TerrainCameraConfiner : MonoBehaviour
{
    public PolygonCollider2D polyCollider;

    public float verticalOffset = 12.5f;
    public float lateralDisplacement = 9;
    public void SetHeight(float height)
    {
        Vector2[] points = polyCollider.points;
        points[0] = Vector2.left * lateralDisplacement + Vector2.down * verticalOffset; 
        points[1] = Vector2.right * lateralDisplacement   + Vector2.down * verticalOffset; 
        points[2] = Vector2.up * (height+2) + Vector2.right * lateralDisplacement + Vector2.down * verticalOffset; 
        points[3] = Vector2.up * (height+2) + Vector2.left * lateralDisplacement + Vector2.down * verticalOffset;
        polyCollider.points = points;
    }
}
