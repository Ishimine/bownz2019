﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GiftCounter : MonoBehaviour
{
    public TextMesh text;

    private void OnEnable()
    {
//        Inventory.OnElementChange -= OnElementChange;
//        Inventory.OnElementChange += OnElementChange;
        Inventory.OnInventoyChange -= OnInventoryChange;
        Inventory.OnInventoyChange += OnInventoryChange;

        UpdateValue();
    }

    private void OnDisable()
    {
        Inventory.OnInventoyChange -= OnInventoryChange;
//        Inventory.OnElementChange -= OnElementChange;
    }

    private void OnInventoryChange()
    {
        UpdateValue();
    }

    public void UpdateValue()
    {
        text.text = Inventory.Get(Inventory.IDs.GiftBox).ToString();
    }

    private void OnElementChange(string arg1, int arg2, int arg3)
    {
        UpdateValue();
    }
}
