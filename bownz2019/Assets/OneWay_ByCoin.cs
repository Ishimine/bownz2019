﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneWay_ByCoin : MonoBehaviour
{

    public Action<float> OnUnlockProgress { get; set; }
    public Action<int> OnInitialize { get; set; }

    public PlatformEffector2D platformEffector;
    public SpriteRenderer render;

    public List<StarCoin> stars = new List<StarCoin>();

    private int totalSteps;

    public void Initialize(List<StarCoin> stars)
    {
        foreach (StarCoin star in stars)
        {
            StartAsNormalCoin startAsNormalCoin = star.GetComponent<StartAsNormalCoin>();
            if(startAsNormalCoin != null)
                Destroy(startAsNormalCoin);

            star.Show(star,new KeyCoin(star), star.transform.position);
        }

        this.stars = stars;
        totalSteps = stars.Count;
        RefreshActiveState();
        OnInitialize?.Invoke(totalSteps);
        OnUnlockProgress?.Invoke(0);
    }

    private void RefreshActiveState()
    {
        bool value = stars.Count == 0;
        platformEffector.useOneWay = value;
        render.color = (value) ? Color.white : Color.gray;
    }

    private void OnEnable()
    {
        this.AddObserver(StarCoinKeyPickedUp, Notifications.StarCoinKeyPickedUp);
        RefreshActiveState();
    }

    private void OnDisable()
    {
        this.RemoveObserver(StarCoinKeyPickedUp, Notifications.StarCoinKeyPickedUp);
    }

    private void StarCoinKeyPickedUp(object sender, object tipeId)
    {
        CoinType coin = (CoinType)sender;
        if (!stars.Contains(coin.Owner)) return;

        stars.Remove(coin.Owner);

        Action callback = null;

        callback =
            ()=>
            {
                if(totalSteps != 0)
                    OnUnlockProgress?.Invoke((float)(totalSteps - stars.Count) / totalSteps);
                RefreshActiveState();
            };

        platformEffector.useOneWay = stars.Count == 0;

        Vector2 startPos = ((CoinType)sender).Position;
        Vector2 targetPos = transform.position;


        GameObjectLibrary.Instantiate("StarUnlockFx").GetComponent<StarUnlockFx>().Initialize(startPos, targetPos, callback);
    }
  
}


public class UnlockModule : IUnlock
{
    public Action OnUnlock { get; set; }
    public Action OnLock { get; set; }
    public Action<float> OnUpdateProgress { get; set; }

    public void Unlocked()
    {
        OnUnlock?.Invoke();
    }

    public void Locked()
    {       
        OnLock?.Invoke();
    }

    public void UpdateProgress(float ammount)
    {        
        OnUpdateProgress?.Invoke(ammount);
    }
}

public interface IUnlock
{
    void Unlocked();
    void Locked();
    void UpdateProgress(float ammount);
}