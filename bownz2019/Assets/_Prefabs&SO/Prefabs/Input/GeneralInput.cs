﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneralInput : MonoBehaviour
{
    public const string KeyHold_Notification = "KeyHold";
    public const string KeyDown_Notification = "KeyDown";
    public const string KeyUp_Notification = "KeyUp";
    private int length;

    private void Awake()
    {
        length = Enum.GetNames(typeof(KeyCode)).Length;
    }

    void Update()
    {
        for (int i = 0; i < length; i++)
        {
            KeyCode currentKey = (KeyCode) i;
            if(Input.GetKey(currentKey)) this.PostNotification(KeyHold_Notification, currentKey);
            if(Input.GetKeyDown(currentKey)) this.PostNotification(KeyDown_Notification, currentKey);
            if(Input.GetKeyUp(currentKey)) this.PostNotification(KeyUp_Notification, currentKey);
        }
    }
}
