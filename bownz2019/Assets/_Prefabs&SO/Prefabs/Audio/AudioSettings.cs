﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using UnityEngine.Audio;

[CreateAssetMenu(menuName = "Manager/AudioSettings")]
public class AudioSettings : ScriptableSingleton<AudioSettings>
{
    public override AudioSettings Myself => this;

    public static Action<bool> OnMusicChange { get; set; }
    public static Action<bool> OnSoundFxChange { get; set; }

    public float musicOnVolume = 0;
    public float musicOffVolume = -40;

    public float soundFxOnVolume = 0;
    public float soundFxOffVolume = -40;

    public AudioMixer mixer;

    [SerializeField] private bool music;
    public bool _Music
    {
        get { return Instance.music; }
        set
        {
            PlayerPrefs.SetInt("Music", (value)?1:0);
            Instance.music = value;
            Instance.mixer.SetFloat("Music_Volume", (value)? Instance.musicOnVolume : Instance.musicOffVolume);
            Debug.Log("=> Music: " + value);
            OnMusicChange?.Invoke(value);
        }
    }
    public static bool Music { get => Instance._Music; set => Instance._Music = value; } 


    [SerializeField] private bool soundFx;
    public bool _SoundFx
    {
        get { return Instance.soundFx; }
        set
        {
            PlayerPrefs.SetInt("SoundFx", (value)?1:0);
            Instance.soundFx = value;
            Instance.mixer.SetFloat("Fx_Volume", (value)? Instance.soundFxOnVolume : Instance.soundFxOffVolume);
            Debug.Log("SET=> SoundFx: " + value);
            OnSoundFxChange?.Invoke(value);
        }
    }
    public static bool SoundFx { get => Instance._SoundFx; set => Instance._SoundFx = value; }

    public override void Start()
    {
        Debug.Log("INITIALIZE Sound " + PlayerPrefs.GetInt("SoundFx", 1));
        Debug.Log("INITIALIZE Music " + PlayerPrefs.GetInt("Music", 1));
        SoundFx = PlayerPrefs.GetInt("SoundFx", 1) == 1;
        Music = PlayerPrefs.GetInt("Music", 1) == 1;
    }

    private void OnEnable()
    {
        music = true;
        soundFx = true;
    }
}
