using Sirenix.OdinInspector;
using UnityEngine;

public class RootCsm : CompositeStateMachine
{
      
  public RootCsm(CompositeStateMachine superState) : base(superState)
    {
        Enter();
        AddListeners();
    }

    public override Transform Root => null;

    protected override void Enter()
    {
        Debug.Log("<color=yellow> Root </color>");
    }

    protected override void Exit()
    {
    }

    protected override void AddListeners()
    {
        base.AddListeners();
        this.AddObserver(GoToGiftBox, Notifications.GoToGiftBox);
    }

    protected override void RemoveListeners()
    {
        base.RemoveListeners();
        this.RemoveObserver(GoToGiftBox, Notifications.GoToGiftBox);
    }

    private void GoToGiftBox(object arg1, object arg2)
    {
        Debug.Log("=> GoToGiftBox");
        UI_ScreenTransitioner.OutIn(()=>SwitchSubState(new GiftBoxMode(this)));
    }

    public void PreloadDone()
    {
        GoToDaily();
    }

    [Button]
    public void Shop()
    {
        UI_ScreenTransitioner.OutIn(()=>SwitchSubState(new ShopState(this)));
    }

    public void BackToMain()
    {
        GoToDaily();
    }

    public void GoToDaily(bool useTransition = true)
    {
        if (useTransition)
            UI_ScreenTransitioner.OutIn(() => SwitchSubState(new DailyMode(this)));
        else
            SwitchSubState(new DailyMode(this));
    }

    public void GoToTutorial(bool useTransition = true)
    {
        if (useTransition)
            UI_ScreenTransitioner.Out(() => SwitchSubState(new TutorialMode(this)));
        else
            SwitchSubState(new TutorialMode(this));
    }

    public void Initialize()
    {
        if (PlayerPrefs.GetInt("TutorialDone", 0) == 1)
                 GoToDaily(true);
             else
                 GoToTutorial(true);
    }
}