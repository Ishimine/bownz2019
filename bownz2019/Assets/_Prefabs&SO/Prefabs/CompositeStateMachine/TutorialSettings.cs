using Procedural;
using UnityEngine;

[CreateAssetMenu]
public class TutorialSettings : ScriptableSingleton<TutorialSettings>
{
    public override TutorialSettings Myself => this;

    public Blueprint bp;
}