﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

public abstract class CompositeStateMachine
{
        public abstract Transform Root { get; }
        protected CompositeStateMachine SuperState { get; set; }
        public CompositeStateMachine SubState { get; set; }

        public Action OnEnter { get; set; }
        public Action OnExit { get; set; }

        protected CompositeStateMachine(CompositeStateMachine superState)
        {
            SuperState = superState;
        }

        private CompositeStateMachine SwitchState(CompositeStateMachine nState)
        {
            SuperState.SwitchSubState(nState);
            return nState;
        }

        protected CompositeStateMachine SwitchSubState(CompositeStateMachine nState)
        {
            SubState?._Exit();
            SetSubState(nState);
            SubState?._Enter();
            return SubState;
        }

        private void SetSubState(CompositeStateMachine nState)
        {
            SubState = nState;
        }

        private void _Enter()
        {
            AddListeners();
            Enter();
            OnEnter?.Invoke();
        }

        private  void _Exit()
        {
            SubState?._Exit();
            RemoveListeners();
            Exit();
            OnExit?.Invoke();
        }

        protected abstract void Enter();
        protected abstract void Exit();

        protected virtual void OnDestroy()
        {
            RemoveListeners();
        }

        protected virtual void AddListeners()
        {
        }

        protected virtual void RemoveListeners()
        {
        }
    }

public class CSM<T> : CompositeStateMachine where T : CompositeStateMachine
    {
        private readonly Transform root;

        public T Owner => SuperState as T;
        public override Transform Root => root;

        public bool DestroyObjectsOnExit = true;

        public CSM(CompositeStateMachine superState) : base(superState)
        {
            root = new GameObject().transform;
            if(SuperState != null)
                root.SetParent(SuperState.Root);
            root.name = "> " + GetType().Name;
        }

        protected override void Enter()
        {
            Debug.Log($"<color=green> Enter </color> => <color=blue>  {GetType().Name} </color>" );
            AnalyticsManager.SendCurrentScreen(GetType().ToString(), typeof(T).ToString());
        }

        protected override void Exit()
        {
            if(DestroyObjectsOnExit)
                Object.Destroy(root.gameObject);
            else
                root.gameObject.SetActive(false);

            Debug.Log($"<color=orange> Exit </color> => <color=blue> {GetType().Name} </color>" );
        }

        protected B Instantiate<B>(string prefabId) where B: Component => GameObjectLibrary.Instantiate<B>(prefabId, root);
        protected GameObject Instantiate(string prefabId)  => GameObjectLibrary.Instantiate(prefabId, root);
    }
