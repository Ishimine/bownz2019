using UnityEngine;

public class GiftBoxMode : CSM<RootCsm>
{
    private readonly GiftBox giftBox;
    private readonly Canvas_Gift canvas_GiftBox;
    private readonly GameObject giftCounter;
    public GiftBoxMode(CompositeStateMachine superState) : base(superState)
    {
        giftBox = Instantiate<GiftBox>("GiftBox");
        canvas_GiftBox = Instantiate<Canvas_Gift>("Canvas_GiftBox");
        giftCounter = Instantiate("GiftCounter");
        giftCounter.transform.position = Vector3.up * 6;
        giftBox.gameObject.SetActive(false);
    }

    protected override void Enter()
    {
        base.Enter();
        MainCameraManager.Instance.SwitchToStaticCamera();
        TryShowGift();
    }

    protected override void AddListeners()
    {
        base.AddListeners();
        this.AddObserver(OnGiftAnimationEnd, Notifications.OnGiftBoxAnimationEnd);
        this.AddObserver(OnBackToMain,Notifications.BackToMain);
    }

    protected override void RemoveListeners()
    {
        base.RemoveListeners();
        this.RemoveObserver(OnGiftAnimationEnd, Notifications.OnGiftBoxAnimationEnd);
        this.RemoveObserver(OnBackToMain,Notifications.BackToMain);
    }

    private void OnBackToMain(object arg1, object arg2)
    {
        Owner.BackToMain();
    }

    private void OnGiftAnimationEnd(object arg1, object arg2)
    {
        TryShowGift();
    }

    private void TryShowGift()
    {
        if (Inventory.Get(Inventory.IDs.GiftBox) > 0 && Shop.Instance.AvailableProducts.Count > 0)
            ShowGift();
    }

    private void ShowGift()
    {
        giftBox.StartUnlockRutine(Vector3.zero);
    }
}