﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonoCSM : MonoBehaviour
{
    public RootCsm root = new RootCsm(null);
    Reporter reporter;

    private void Start()
    {
     //   reporter = GameObjectLibrary.Instantiate<Reporter>("Reporter");
        Application.targetFrameRate = 60;
        StartCoroutine(WaitAndExecute());
    }

    IEnumerator WaitAndExecute()
    {
        yield return null;
        root.Initialize();
    }
}