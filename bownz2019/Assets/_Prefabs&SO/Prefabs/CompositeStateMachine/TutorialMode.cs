using System.Collections.Generic;
using BallAnimation;
using UnityEngine;

public class TutorialMode : CSM<RootCsm>
{
    private ProceduralLevel level;
    private BallStateMachine ball;
    
    public TutorialMode(CompositeStateMachine superState) : base(superState)
    {
        level = Instantiate<ProceduralLevel>("ProceduralLevel");
        ball = Instantiate<BallStateMachine>("Ball");

    }




    protected override void Enter()
    {
        base.Enter();
        AnalyticsManager.SendEvent_TutorialBegin();
        SwitchSubState(new TutorialLoad(this, level, ball));
    }
    
    protected override void Exit()
    {
        base.Exit();
    }

    public void LevelLoadDone()
    {
        SwitchSubState(new TutorialGamePlay(this, level, ball));
    }

    public void GoToDaily()
    {
        Owner.GoToDaily();
    }
}