using System;

public class TutorialPlaying : CSM<TutorialGamePlay>
{
    private bool swipeDone = false;
    private int starsPickedUp = 0;
    
    public TutorialPlaying(CompositeStateMachine superState) : base(superState)
    {
    }

    protected override void Enter()
    {
        base.Enter();
        this.PostNotification(Notifications.SwipeSpawnerSetEnable,true);
    }

    protected override void AddListeners()
    {
        base.AddListeners();
        this.AddObserver(OnSwipeTouched,Notifications.SwipePlatform_Touched);
        this.AddObserver(OnStarPickedUp,Notifications.OnCoinPickedUp);
        this.AddObserver(OnWinStarPickedUp,Notifications.WinCoinPickedUp);
    }

    protected override void RemoveListeners()
    {
        base.RemoveListeners();
        this.RemoveObserver(OnSwipeTouched,Notifications.SwipePlatform_Touched);
        this.RemoveObserver(OnStarPickedUp,Notifications.OnCoinPickedUp);
        this.RemoveObserver(OnWinStarPickedUp,Notifications.WinCoinPickedUp);
    }

    private void OnWinStarPickedUp(object arg1, object arg2)
    {
        Owner.Win();
    }

    private void OnStarPickedUp(object arg1, object arg2)
    {
        if ((int) arg2 == 0)
        {
            starsPickedUp++;
            AnalyticsManager.SendEvent_TutorialStarPickedUp(starsPickedUp);
        }
    }

    private void OnSwipeTouched(object arg1, object arg2)
    {
        if(!swipeDone)
            AnalyticsManager.SendEvent_TutorialSwipeTouched();
    }
    
}