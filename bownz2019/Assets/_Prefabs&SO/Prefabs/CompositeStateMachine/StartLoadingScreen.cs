using UnityEngine;

public class StartLoadingScreen :  CSM<RootCsm>
{
    private readonly Canvas_Splash canvasSplash;

    public StartLoadingScreen(CompositeStateMachine super) : base(super)
    {
        canvasSplash = Instantiate<Canvas_Splash>("Canvas_Splash");
        canvasSplash.gameObject.SetActive(false);
    }

    protected override void Enter()
    {    
        base.Enter();
        canvasSplash.gameObject.SetActive(true);
        canvasSplash.Execute(Owner.PreloadDone);
    }

    protected override void Exit()
    {
        base.Exit();
        Object.Destroy(canvasSplash.gameObject);
    }
}