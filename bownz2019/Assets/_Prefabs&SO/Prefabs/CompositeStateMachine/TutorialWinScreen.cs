using UnityEngine;

public class TutorialWinScreen : CSM<TutorialGamePlay>
{
    private Canvas_DailyWinScreen canvas;
    private float duration;
    public TutorialWinScreen(CompositeStateMachine superState, float duration) : base(superState)
    {
        this.duration = duration;
        canvas = Instantiate<Canvas_DailyWinScreen>("Canvas_DailyWinScreen");
    }

    protected override void Enter()
    {
        base.Enter();
        canvas.SetTime(duration);
        canvas.Show();
        
        AnalyticsManager.SendEvent_TutorialComplete();
        PlayerPrefs.SetInt("TutorialDone", 1);

        Settings.UiInteraction = true;

        this.PostNotification(Notifications.SwipeSpawnerSetEnable, false);
    }

    protected override void Exit()
    {
        base.Exit();
        canvas.gameObject.SetActive(false);
    }
    
    protected override void AddListeners()
    {
        base.AddListeners();
        this.AddObserver(OnRequestLevelSelection_Response, Notifications.GameModeRequest_BackToLevelSelection);
    }

    protected override void RemoveListeners()
    {
        base.RemoveListeners();
        this.RemoveObserver(OnRequestLevelSelection_Response, Notifications.GameModeRequest_BackToLevelSelection);
    }

    private void OnRequestLevelSelection_Response(object arg1, object arg2)
    {
        Debug.Log("OnRequestLevelSelection_Response");
        Owner.GoToDaily();
    }
}