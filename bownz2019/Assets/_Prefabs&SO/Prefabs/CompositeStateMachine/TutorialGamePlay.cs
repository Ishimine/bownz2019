
using UnityEngine;

public class TutorialGamePlay : CSM<TutorialMode>
{
    private ProceduralLevel level;
    private BallStateMachine ball;

    private float startTime;
    public int progress;
    public int totalProgress;

    private Vector3 respawnPosition;

    private Canvas_SettingsMenu Canvas_Settings;

    public TutorialGamePlay(CompositeStateMachine superState, ProceduralLevel level, BallStateMachine ball) : base(superState)
    {
        this.level = level;
        this.ball = ball;
        Canvas_Settings = Instantiate<Canvas_SettingsMenu>("Canvas_Settings");
    }

    protected override void Enter()
    {
        base.Enter();
        ball.AppearToLiving();

        SwitchSubState(new TutorialPlaying(this));
        startTime = Time.time;
        totalProgress = level.Checkpoints.Count;
        respawnPosition = Vector3.zero;
        Canvas_Settings.gameObject.SetActive(true);
    }

    protected override void Exit()
    {
        base.Exit();
        Canvas_Settings.gameObject.SetActive(false);
    }

    protected override void AddListeners()
    {
        base.AddListeners();
        this.AddObserver(OnPauseRequest, Notifications.PauseRequest);
        this.AddObserver(OnCheckpointActivated,Checkpoint.NotificationActivated);
        this.AddObserver(OnPlayerDead,Notifications.Ball_DeadEnd);
    }

    protected override void RemoveListeners()
    {
        base.RemoveListeners();
        this.RemoveObserver(OnPauseRequest, Notifications.PauseRequest);
        this.RemoveObserver(OnCheckpointActivated,Checkpoint.NotificationActivated);
        this.RemoveObserver(OnPlayerDead,Notifications.Ball_DeadEnd);
    }

    private void OnPlayerDead(object arg1, object arg2)
    {
        ball.gameObject.SetActive(false);
        ball.transform.position = respawnPosition;
        ball.AppearToLiving();
    }

    private void OnCheckpointActivated(object arg1, object arg2)
    {
        Debug.Log(arg2);
        progress++;
        respawnPosition = ((Checkpoint) arg1).GetRespawnPoint();
        AnalyticsManager.SendEvent("Tutorial", "Progress", ((float)progress/totalProgress).ToString("0.0"));
    }

    private void OnPauseRequest(object arg1, object arg2)
    {
        if ((bool) arg2)
            SwitchSubState(new TutorialPause(this));
        else
            SwitchSubState(new TutorialPlaying(this));
    }

    public void Win()
    {
        SwitchSubState(new TutorialWinScreen(this, Time.time - startTime));
    }

    public void GoToDaily()
    {
        Owner.GoToDaily();
    }
}