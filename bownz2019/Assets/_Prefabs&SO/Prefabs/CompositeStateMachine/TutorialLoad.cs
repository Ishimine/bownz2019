using UnityEngine;

public class TutorialLoad : CSM<TutorialMode>
{
    private ProceduralLevel level;
    private BallStateMachine player;
    
    public TutorialLoad(CompositeStateMachine superState, ProceduralLevel level, BallStateMachine player) : base(superState)
    {
        this.level = level;
        this.player = player;
    }
    
    protected override void AddListeners()
    {
        base.AddListeners();
        this.AddObserver(OnBallWaiting_Response, Notifications.BallWaiting);

      
    }

    protected override void RemoveListeners()
    {
        base.RemoveListeners();
        this.RemoveObserver(OnBallWaiting_Response, Notifications.BallWaiting);
        SwipePlatform.OnAnyAliveState -= OnAnyAliveState_Response;
    }

    private void OnAnyAliveState_Response(SwipePlatform obj)
    {
        StartPlaying();
    }

    protected override void Enter()
    {
        base.Enter();
        Debug.Log("Enter()");
        InitializeTerrainAndCamera();
    }

    private void InitializeTerrainAndCamera()
    {
        Debug.Log("InitializeTerrainAndCamera");
        MainCameraManager.Instance.SwitchToFollowCamera(player.transform, FollowCamera.FollowType.Portrait);
        level.gameObject.SetActive(true);
        level.CreateLevel(TutorialSettings.Instance.bp, TransitionIn);
    }

    private void TransitionIn()
    {
        player.gameObject.SetActive(true);
        UI_ScreenTransitioner.In(PostAction);
        
        SwipePlatform.OnAnyAliveState -= OnAnyAliveState_Response;
        SwipePlatform.OnAnyAliveState += OnAnyAliveState_Response;
    }
    
    private void PostAction()
    {
        this.PostNotification(Notifications.GameMode_Ready);
        this.PostNotification(Notifications.ShowStars);
    }

    private void StartPlaying()
    {
        Owner.LevelLoadDone();
    }


    private void OnBallWaiting_Response(object arg1, object arg2)
    {
        this.PostNotification(Notifications.SwipeSpawnerSetEnable, true);
    }
}