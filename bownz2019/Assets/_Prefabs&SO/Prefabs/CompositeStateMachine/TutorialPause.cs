public class TutorialPause : CSM<TutorialGamePlay>
{
    public TutorialPause(CompositeStateMachine superState) : base(superState)
    {
    }

    protected override void Enter()
    {
        base.Enter();
        TimeManager.Pause();
        this.PostNotification(Notifications.SwipeSpawnerSetEnable,false);
    }

    protected override void Exit()
    {
        base.Exit();
        TimeManager.Resume();
    }
}