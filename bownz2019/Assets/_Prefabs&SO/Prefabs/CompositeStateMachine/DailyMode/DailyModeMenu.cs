using System;
using UnityEngine;

public class DailyModeMenu : CSM<DailyMode>
{
    public Canvas_LevelConfiguration CanvasLevelConfiguration { get; }
    public Canvas_DailyProcedural CanvasDaily { get; }
    private Canvas_PlayGames Canvas_PlayGames;
    private Canvas_SettingsMenu Canvas_Settings;

    //private UI_DailyGiftButton canvasDailyGift;

    public DailyModeMenu(CompositeStateMachine superState) : base(superState)
    {
        CanvasLevelConfiguration = Instantiate<Canvas_LevelConfiguration>("Canvas_Procedural_LevelSelection");
        CanvasDaily = Instantiate<Canvas_DailyProcedural>("Canvas_DailyProcedural");
        Canvas_PlayGames = Instantiate<Canvas_PlayGames>("Canvas_SocialPlayButtons");
        Canvas_Settings = Instantiate<Canvas_SettingsMenu>("Canvas_Settings");
        CanvasDaily.gameObject.SetActive(false);
    }

    protected override void Enter()
    {
        base.Enter();
        
        Canvas_Settings.gameObject.SetActive(true);
        Canvas_PlayGames.gameObject.SetActive(true);
        MainCameraManager.Instance.SwitchToStaticCamera();

        Owner.Player.gameObject.SetActive(false);
        Owner.Player.transform.position = Vector3.zero;
        Owner.Player.TrailComponent.UsePlayerSelection = true;
        Owner.Player.SkinComponent.UsePlayerSelection = true;
        Owner.Level.gameObject.SetActive(false);
        CanvasDaily.gameObject.SetActive(true);
                                         
        //canvasDailyGift.gameObject.SetActive(true);

        this.PostNotification(Notifications.SettingsMenu_Enable,true);
        this.PostNotification(Notifications.ShowPlayGamesBar,true);
        this.PostNotification(Notifications.SwipeSpawnerSetEnable,false);
        //this.PostNotification(Notifications.ShowGiftUnlockProgress, null); //Mostramos que se desbloquearon cosas
    }

    protected override void Exit()
    {
        base.Exit();
        Canvas_PlayGames.gameObject.SetActive(false);
        CanvasDaily.gameObject.SetActive(false);
        CanvasLevelConfiguration.gameObject.SetActive(false);
        this.PostNotification(Notifications.SettingsMenu_Enable,false);
        this.PostNotification(Notifications.ShowPlayGamesBar,false);
        Canvas_Settings.gameObject.SetActive(false);
    }

    protected override void AddListeners()
    {
        base.AddListeners();
        this.AddObserver(LoadLevel_Request, Notifications.ProceduralLevel_Request);
        this.AddObserver(NextDay, Notifications.NextDailyDate);
        this.AddObserver(PreviousDay, Notifications.PreviousDailyDate);
    }

    protected override void RemoveListeners()
    {
        base.RemoveListeners();
        this.RemoveObserver(LoadLevel_Request, Notifications.ProceduralLevel_Request);
        this.RemoveObserver(NextDay, Notifications.NextDailyDate);
        this.RemoveObserver(PreviousDay, Notifications.PreviousDailyDate);
    }

    private void LoadLevel_Request(object arg1, object arg2)
    {
        ProceduralLevelRequest request = (ProceduralLevelRequest)arg2;
        Owner.LoadLevel(request.Blueprint, request.LevelId);
    }

    private void PreviousDay(object arg1, object arg2)
    {
        Debug.Log("PreviousDay");
        Owner.SetDate(DailyMode.CurrentDate.Subtract(TimeSpan.FromDays(1))); 
    }

    private void NextDay(object arg1, object arg2)
    {
        Debug.Log("NextDate");
        Owner.SetDate(DailyMode.CurrentDate.AddDays(1));
    }
    
}