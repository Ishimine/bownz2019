using System.Collections.Generic;
using UnityEngine;

internal class DailyModeGamePlayPause : CSM<DailyModeGamePlay>
{
    public DailyModeGamePlayPause(CompositeStateMachine superState) : base(superState)
    {
    }

    protected override void AddListeners()
    {
        base.AddListeners();
        this.AddObserver(OnPowerUp_Shield, Notifications.RewardPowerUpShield);
        this.AddObserver(OnPowerUp_Magnet, Notifications.RewardPowerUpMagnet);
        this.AddObserver(OnSkipModule_Reward, Notifications.RewardSkipModule);
    }

    protected override void RemoveListeners()
    {
        base.RemoveListeners();
        this.RemoveObserver(OnPowerUp_Shield, Notifications.RewardPowerUpShield);
        this.RemoveObserver(OnPowerUp_Magnet, Notifications.RewardPowerUpMagnet);
        this.RemoveObserver(OnSkipModule_Reward, Notifications.RewardSkipModule);
    }

    private void OnPowerUp_Magnet(object arg1, object arg2)
    {
        Debug.Log("OnPowerUp_Magnet");
        GameObjectLibrary.Instantiate("Magnet").GetComponent<Magnet>().EquipTo(Owner.Player.gameObject);
    }

    private void OnSkipModule_Reward(object arg1, object arg2)
    {
        TravelTo travelTo = GameObjectLibrary.Instantiate("TravelTo").GetComponent<TravelTo>();
        Checkpoint nextCp = Owner.Level.GetNextCheckpoint();

        Rect rect = new Rect();

        rect.min = Owner.Level.CurrentCp.GetRespawnPoint() + Vector3.left * PlayArea.Area.width/2;
        rect.max = nextCp.GetRespawnPoint() + Vector3.right * PlayArea.Area.width/2;

        void PostAction()
        {
            List<StarCoin> starCoins = Owner.Level.GetStarsInsideRect(rect);
            foreach (StarCoin starCoin in starCoins)
            {
                starCoin.gameObject.AddComponent<GoToObject>().Initialize(Owner.Player.transform, true);
            }
            nextCp.Activate();
        }    

        travelTo.Initialize(Owner.Player, nextCp.GetRespawnPoint(), Owner.Level.CurrentCp.GetRespawnPoint());
        travelTo.Execute(null, PostAction);
    }

    private void OnPowerUp_Shield(object arg1, object arg2)
    {
        Shield.EquipTo(Owner.Player);
    }

    protected override void Enter()
    {
        base.Enter();
        TimeManager.Pause();
        this.PostNotification(Notifications.SwipeSpawnerSetEnable,false);
    }

    protected override void Exit()
    {
        base.Exit();
        TimeManager.Resume();
        this.PostNotification(Notifications.SwipeSpawnerSetEnable,true);
    }
}