using System.Collections.Generic;
using UnityEngine;

public class DailyModeGamePlayPlaying : CSM<DailyModeGamePlay>
{
    public DailyModeGamePlayPlaying(CompositeStateMachine superState) : base(superState)
    {
    }

    protected override void Enter()
    {
        base.Enter();
    }

    protected override void AddListeners()
    {
        base.AddListeners();
                             
        this.AddObserver(OnBallDeadStartResponse, Notifications.Ball_DeadStart);
        this.AddObserver(OnBallDeadEndResponse, Notifications.Ball_DeadEnd);
        this.AddObserver(OnWinCoinPickedUp, Notifications.WinCoinPickedUp);
        this.AddObserver(OnSwipePlatform_Touched_Response, Notifications.SwipePlatform_Touched);
                                                                                                                       
        this.AddObserver(OnBackButtonPressed, Notifications.BackButtonPressed);


        this.AddObserver(OnKeyDown, GeneralInput.KeyDown_Notification);
        this.AddObserver(OnWinLevel, Notifications.Debug_WinLevel);

        Owner.Canvas_Settings.gameObject.SetActive(true);
    }

    protected override void RemoveListeners()
    {
        base.RemoveListeners();
                                           
        this.RemoveObserver(OnBallDeadStartResponse, Notifications.Ball_DeadStart);
        this.RemoveObserver(OnBallDeadEndResponse, Notifications.Ball_DeadEnd);
        this.RemoveObserver(OnWinCoinPickedUp, Notifications.WinCoinPickedUp);
        this.RemoveObserver(OnSwipePlatform_Touched_Response, Notifications.SwipePlatform_Touched);

        this.RemoveObserver(OnBackButtonPressed, Notifications.BackButtonPressed);

        this.RemoveObserver(OnKeyDown,GeneralInput.KeyDown_Notification);
        this.RemoveObserver(OnWinLevel, Notifications.Debug_WinLevel);



    }

  
    private void OnBackButtonPressed(object arg1, object arg2)
    {
        Owner.Quit();
    }

    private void OnWinLevel(object arg1, object arg2)
    {
        Owner.Player.transform.position = Owner.Level.WinCoin.transform.position;
    }

    private void OnKeyDown(object arg1, object arg2)
    {
        if ((KeyCode) arg2 == KeyCode.G)
        {
            OnWinLevel(null, null);
        }
    }


    private void OnBallDeadEndResponse(object arg1, object arg2)
    {
        if (Checkpoint.Checkpoints.Count == 0 || Checkpoint.Active.Count < 1)
        {
            Owner.Player.transform.position = Vector3.zero;
            Owner.DeathCount = 0;
        }
        else
            Owner.Player.transform.position = Checkpoint.Active[Checkpoint.Active.Count - 1].GetRespawnPoint();

        Owner.Player.GetComponent<BallStateMachine>().AppearToLiving();
        this.PostNotification(Notifications.GameMode_PlayerRespawn);
    }

    private void OnBallDeadStartResponse(object sender, object arg2)
    {
        Owner.DeathCount++;
    }

    private void OnWinCoinPickedUp(object sender, object arg2)
    {
        Debug.Log($"OnWinCoinPickedUp StartTime:{Owner.StartTime}  EndTime:{Owner.EndTime}");
        Owner.EndTime = Time.time;

        ///Notificaciones
        this.PostNotification(Notifications.SwipeSpawnerSetEnable, false);
        this.PostNotification(Notifications.LevelWon);
        this.PostNotification(Notifications.GameMode_End);

        ///Ball
        Owner.Player.Disappear(GameMode_Win);
    }

    private void OnSwipePlatform_Touched_Response(object sender, object arg2)
    {
        Owner.TouchedPlatforms.Add((SwipePlatform)sender);
    }

    private void GameMode_Win()
    {
        this.PostNotification(Notifications.GameMode_Reset);
        Owner.Win();
    }

}