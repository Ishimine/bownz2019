using System.Collections.Generic;
using UnityEngine;

public class DailyModeWinScreen : CSM<DailyMode>
{
    private readonly Canvas_DailyWinScreen winScreen;
    private float duration;
  
    public DailyModeWinScreen(CompositeStateMachine superState, float duration) : base(superState)
    {
        this.duration = duration;
        winScreen = Instantiate<Canvas_DailyWinScreen>("Canvas_DailyWinScreen");

        //this.winScreen.Initialize(Owner.Objectives, score, LevelsProgress.Get(DailyMode.GameModeID, 0, Settings.IsChallengeMode));
        //LevelsProgress.AddLevelProgress(Owner.LevelId, System.DateTime.Now.Year, false, score, true);

    }

    protected override void Enter()
    {
        base.Enter();

        /*
        if (!DailyProgress.Contains(DailyMode.CurrentDate, Owner.SeedOffset.ToString()))// Si no contiene el nivel Regalamos una caja
            Inventory.Add(Inventory.IDs.GiftBox, 1);
        */
        Debug.Log("Duration: " + duration);

        winScreen.SetTime(duration);
        DailyProgress.Add(DailyMode.CurrentDate,Owner.SeedOffset.ToString());

        winScreen.Open();
        this.PostNotification(Notifications.SwipeSpawnerClear);
        Owner.Player.AppearToLiving();
                                                          
    }

    protected override void Exit()
    {
        base.Enter();
        winScreen.gameObject.SetActive(false);
    }

    protected override void AddListeners()
    {
        base.AddListeners();
        this.AddObserver(OnRequestLevelSelection_Response, Notifications.GameModeRequest_BackToLevelSelection);
        this.AddObserver(OnRequestRetry_Response, Notifications.GameModeRequest_Retry);
        this.AddObserver(OnRequestNext_Response, Notifications.GameModeRequest_Next);
    }

    protected override void RemoveListeners()
    {
        base.RemoveListeners();
        this.RemoveObserver(OnRequestLevelSelection_Response, Notifications.GameModeRequest_BackToLevelSelection);
        this.RemoveObserver(OnRequestRetry_Response, Notifications.GameModeRequest_Retry);
        this.RemoveObserver(OnRequestNext_Response, Notifications.GameModeRequest_Next);
    }

    private void OnRequestNext_Response(object arg1, object arg2)
    {
        Owner.LoadNextLevel();
    }

    private void OnRequestRetry_Response(object arg1, object arg2)
    {
        Owner.RetryLevel();
    }

    private void OnRequestLevelSelection_Response(object arg1, object arg2)
    {
        Debug.Log("OnRequestLevelSelection_Response");
        Owner.GoToMenu();
    }

}