using Procedural;

public class DailyModeLoadLevel : CSM<DailyMode>
{

    private Blueprint Blueprint
    {
        get => Owner.Blueprint;
        set => Owner.Blueprint = value;
    }

    private string LevelId
    {
        get => Owner.LevelId;
        set => Owner.LevelId = value;
    }

    private ProceduralLevel Level
    {
        get => Owner.Level;
        set => Owner.Level = value;
    }

    private BallStateMachine Player
    {
        get => Owner.Player;
        set => Owner.Player = value;
    }
                     
    public DailyModeLoadLevel(Blueprint blueprint, string levelId,CompositeStateMachine super) : base(super)
    {
        Blueprint = blueprint;
        LevelId = levelId;
    }
               
    protected override void Enter()
    {
        base.Enter();
        Level.gameObject.SetActive(false);
        UI_ScreenTransitioner.Out(InitializeTerrainAndCamera);
    }

    private void InitializeTerrainAndCamera()
    {
        MainCameraManager.Instance.SwitchToFollowCamera(Player.transform, FollowCamera.FollowType.Portrait);
        Level.gameObject.SetActive(true);
        Level.CreateLevel(Blueprint, TransitionIn);

    }

    private void TransitionIn()
    {
        Player.gameObject.SetActive(true);
        UI_ScreenTransitioner.In(PostAction);
        MainCameraManager.Instance.SetColliderConfiner(Level.CameraConfiner.polyCollider);
    }

    protected override void Exit()
    {
        base.Exit();
    }

    private void PostAction()
    {
        this.PostNotification(Notifications.GameMode_Ready);
        this.PostNotification(Notifications.ShowStars);
    }

    private void StartPlaying()
    {
        Owner.LevelLoadDone();
    }

    protected override void AddListeners()
    {
        base.AddListeners();
        this.AddObserver(OnBallWaiting_Response, Notifications.BallWaiting);

        SwipePlatform.OnAnyAliveState -= OnAnyPlatformAliveState_Response;
        SwipePlatform.OnAnyAliveState += OnAnyPlatformAliveState_Response;
    }

    protected override void RemoveListeners()
    {
        base.RemoveListeners();
        this.RemoveObserver(OnBallWaiting_Response, Notifications.BallWaiting);
        SwipePlatform.OnAnyAliveState -= OnAnyPlatformAliveState_Response;
    }

    private void OnAnyPlatformAliveState_Response(SwipePlatform obj)
    {
        StartPlaying();
    }

    private void OnBallWaiting_Response(object arg1, object arg2)
    {
        this.PostNotification(Notifications.SwipeSpawnerSetEnable, true);
    }
}