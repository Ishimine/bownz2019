using System;
using System.Collections.Generic;
using UnityEngine;

public class DailyModeGamePlay : CSM<DailyMode>
{
    public List<SwipePlatform> TouchedPlatforms { get; set; }
    public int PickedStarsCount { get; set; }

    public float StartTime { get; set; }
    public float EndTime { get; set; }

    public int DeathCount;

    public readonly BallStateMachine Player;
    public readonly ProceduralLevel Level;
    public readonly Canvas_PowerUpShop canvasShop;
    public readonly UI_StarCounter Canvas_StarsCounter;
    public readonly Canvas_SettingsMenu Canvas_Settings;


    public DailyModeGamePlay(CompositeStateMachine superState, BallStateMachine player, ProceduralLevel level) :
        base(superState)
    {
        Player = player;
        Level = level;
        canvasShop = Instantiate<Canvas_PowerUpShop>("Canvas_PowerUpShop");
        canvasShop.OpenClose.Hide();
        Canvas_StarsCounter = Instantiate<UI_StarCounter>("Canvas_Daily_StarsCounter");
        Canvas_StarsCounter.Hide();

        Canvas_Settings = Instantiate<Canvas_SettingsMenu>("Canvas_Settings_GP");
        Canvas_Settings.gameObject.SetActive(false);
    }

    protected override void AddListeners()
    {
        base.AddListeners();
        this.AddObserver(OnPauseRequest, Notifications.PauseRequest);
        this.AddObserver(OnCoinPickedUp, Notifications.OnCoinPickedUp);
        this.AddObserver(OnTryToBuy, Notifications.TryToBuy);
        this.AddObserver(OnHasEnoughStars, Notifications.HasEnoughCoins);
        this.AddObserver(ShopOpenClose, Notifications.ShopOpenClose);
        this.AddObserver(OnQuitFromMenu, Notifications.GameMode_Quit);
    }

    protected override void RemoveListeners()
    {
        base.RemoveListeners();
        this.RemoveObserver(OnPauseRequest, Notifications.PauseRequest);
        this.RemoveObserver(OnCoinPickedUp, Notifications.OnCoinPickedUp);
        this.RemoveObserver(OnTryToBuy, Notifications.TryToBuy);
        this.RemoveObserver(OnHasEnoughStars, Notifications.HasEnoughCoins);
        this.RemoveObserver(ShopOpenClose, Notifications.ShopOpenClose);
        this.RemoveObserver(OnQuitFromMenu, Notifications.GameMode_Quit);
    }

    private void OnQuitFromMenu(object arg1, object arg2)
    {
        Quit();
    }

    private void ShopOpenClose(object arg1, object arg2)
    {
        this.PostNotification(Notifications.ScoreUpdate, PickedStarsCount);
        Canvas_Settings.gameObject.SetActive(!(bool)arg2);
    }

    private void OnHasEnoughStars(object arg1, object arg2)
    {
        Argument<bool, int> arg = (Argument<bool, int>) arg2;
        arg.Item0 = PickedStarsCount >= arg.Item1;
    }

    private void OnTryToBuy(object arg1, object arg2)
    {
        Argument<bool, int> arg = (Argument<bool, int>) arg2;
        arg.Item0 = PickedStarsCount >= arg.Item1;
        if (arg.Item0)
        {
            PickedStarsCount -= arg.Item1;
            this.PostNotification(Notifications.ScoreUpdate, PickedStarsCount);
        }
    }

    private void OnCoinPickedUp(object arg1, object arg2)
    {
        PickedStarsCount++;
        this.PostNotification(Notifications.ScoreUpdate, PickedStarsCount);
    }

    private void OnPauseRequest(object arg1, object arg2)
    {
        bool inPause = (bool) arg2;
        if (inPause)
        {
            SwitchSubState(new DailyModeGamePlayPause(this));
        }
        else
        {
            SwitchSubState(new DailyModeGamePlayPlaying(this));
        }
    }

    protected override void Enter()
    {
        base.Enter();

        AnalyticsManager.SendEvent_LevelStart(GetLevelId(Level));

        Owner.Level.gameObject.SetActive(true);
        Owner.Player.transform.position = PlayArea.Area.center;
        Owner.Player.gameObject.SetActive(true);
        //Owner.Player.AirJump();
        //Owner.Player.AppearToWaiting();

        TouchedPlatforms = new List<SwipePlatform>();
        PickedStarsCount = 0;

        this.PostNotification(Notifications.GameMode_Go);

        DeathCount = 0;
        Settings.UiInteraction = true;

        SlowMotionWhileSwipe_Modificator.isInfinite = true;
        SlowMotionWhileSwipe_Modificator.Deactivate();

        StartTime = Time.time;
        Debug.Log($"StartTime:{StartTime}  EndTime:{EndTime}");

        this.PostNotification(Notifications.GamePlay_Start);

        SwitchSubState(new DailyModeGamePlayPlaying(this));
        Canvas_Settings.gameObject.SetActive(true);

    }

    private string GetLevelId(ProceduralLevel level)
    {
        return $"Daily_{DailyMode.CurrentDate}_{level.CurrentBlueprint.SeedOffset}";
    }

    protected override void Exit()
    {
        base.Exit();
        Canvas_Settings.gameObject.SetActive(false);
    }

    public void Win()
    {
        Debug.Log($"StartTime:{StartTime}  EndTime:{EndTime}");
        Owner.Win(EndTime - StartTime);
    }

    public void Quit()
    {
        Owner.GoToMenu();
    }

}

public class Argument<T0>
{
    public T0 Item0;

    public Argument(T0 item0)
    {
        Item0 = item0;
    }
}

public class Argument<T0,T1>:Argument<T0>
{
    public T1 Item1;

    public Argument(T0 item0, T1 item1) : base(item0)
    {
        Item1 = item1;
    }
}

public class Argument<T0,T1,T2>:Argument<T0,T1>
{
    public T2 Item2;

    public Argument(T0 item0, T1 item1, T2 item2) : base(item0, item1)
    {
        Item2 = item2;
    }
}
