using System;
using System.Runtime.CompilerServices;
using BallStates;
using Procedural;
using UnityEngine;

public class DailyMode : CSM<RootCsm>
{
    private static DateTime currentDate = DateTime.Today;

    public const string GameModeID = "Daily";
    public Procedural.Blueprint Blueprint { get;  set; }

    public string LevelId { get;  set; }

    public int SeedOffset { get; set; }
    public LevelProgress Objectives => new LevelProgress();
    public  ProceduralLevel Level { get; set; }
    public  BallStateMachine Player { get; set; }
    private Canvas_UnlockGift Canvas_UnlockGift;
    private UI_GiftAvailableButton Canvas_GiftBoxAvailable;
    public static DateTime CurrentDate
    {
        get => currentDate;
        private set
        {
            currentDate = value;
            Debug.Log("currentDate: " + currentDate);
            NotificationCenter.instance.PostNotification(Notifications.OnDailyDateChange, currentDate);
        }       
    }

    private const float GoldenRatio = 1.61803398875f;

    public static float CurrentDailySeed => Perlin.Noise((currentDate.DayOfYear * GoldenRatio / 366) * Mathf.PI);
    
    public void SetDate(DateTime nDate)
    {
        CurrentDate = nDate;
    }
    
    public DailyMode(CompositeStateMachine super) : base(super)
    {
        Level = Instantiate<ProceduralLevel>("ProceduralLevel");
        Player = Instantiate<BallStateMachine>("Ball");
        Canvas_UnlockGift = Instantiate<Canvas_UnlockGift>("Canvas_UnlockGift");
        Canvas_GiftBoxAvailable = Instantiate<UI_GiftAvailableButton>("Canvas_GiftBoxAvailable");


        Level.gameObject.SetActive(false);
        Player.gameObject.SetActive(false);
    }

    protected override void Enter()
    {
        base.Enter();
        SwitchSubState(new DailyModeMenu(this));
        Canvas_UnlockGift.gameObject.SetActive(true);
        Canvas_GiftBoxAvailable.gameObject.SetActive(true);
    }

    protected override void AddListeners()
    {
        base.AddListeners();
        this.AddObserver(GoToShop, Notifications.GoToShop);
        this.AddObserver(GoToMain, Notifications.GoToMain);
    }

    protected override void RemoveListeners()
    {
        base.RemoveListeners();
        this.RemoveObserver(GoToShop, Notifications.GoToShop);
        this.RemoveObserver(GoToMain, Notifications.GoToMain);
    }

    private void GoToMain(object arg1, object arg2)
    {
    }

    public void LoadLevel(Blueprint requestBlueprint, string requestLevelId)
    {
        SeedOffset = requestBlueprint.SeedOffset;
        SwitchSubState(new DailyModeLoadLevel(requestBlueprint, requestLevelId,this));
    }

    public void LevelLoadDone()
    {
        SwitchSubState(new DailyModeGamePlay(this, Player, Level));
    }

    public void GoToMenu()
    {
        Debug.Log("GoToMenu");
        TimeManager.Resume();
        UI_ScreenTransitioner.OutIn(()=> SwitchSubState(new DailyModeMenu(this)), ()=> Settings.UiInteraction = true);
    }

    public void RetryLevel()
    {
        LevelLoadDone();
    }

    public void LoadNextLevel()
    {
    }

    public void Win(float duration)
    {
        SwitchSubState(new DailyModeWinScreen(
            this,
            duration));
    }

    public void GoToShop(object o, object o1)
    {
        Owner.Shop();
    }

    protected override void Exit()
    {
        base.Exit();
        Canvas_UnlockGift.gameObject.SetActive(false);
        Canvas_GiftBoxAvailable.gameObject.SetActive(false);
    }
}