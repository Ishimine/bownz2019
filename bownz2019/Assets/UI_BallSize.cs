﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_BallSize : MonoBehaviour
{
    public Text pcText;

    public void OnEnable()
    {
        pcText.text = Settings.BallSize.ToString();
    }

    public void ApplyValue(string value)
    {
        Settings.BallSize = float.Parse(value);
    }
}
