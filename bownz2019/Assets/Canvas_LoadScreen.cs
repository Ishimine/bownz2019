﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Canvas_LoadScreen : MonoBehaviour
{
    public UI_ConteinerOpenClose OpenClose;
    public Image fill;

    public float targetValue;

    public float smoothTime = .2f;
    IEnumerator rutine;

    private void Start()
    {
        rutine = FillAnimation();
        this.AddObserver(OnShowLoadBar, Notifications.ShowLoadBar);
    }

    private void OnDestroy()
    {
        this.RemoveObserver(OnShowLoadBar, Notifications.ShowLoadBar);
    }

    private void OnShowLoadBar(object arg1, object arg2)
    {
        bool value = (bool) arg2;
        if (value)
        {
            StartCoroutine(rutine);
            OpenClose.Open();
        }
        else
        {
            StopCoroutine(rutine);
            OpenClose.Close();
        }
    }

    private void OnEnable()
    {
        this.AddObserver(OnLevelConstructionProgress_Response, Notifications.OnLevelConstructionProgress);
        this.AddObserver(OnLevelConstructionStart_Response, Notifications.OnLevelConstructionStart);
        this.AddObserver(OnLevelConstructionEnd_Response, Notifications.OnLevelConstructionEnd);
    }
                    
    private void OnDisable()
    {
        this.RemoveObserver(OnLevelConstructionProgress_Response, Notifications.OnLevelConstructionProgress);
        this.RemoveObserver(OnLevelConstructionStart_Response, Notifications.OnLevelConstructionStart);
        this.RemoveObserver(OnLevelConstructionEnd_Response, Notifications.OnLevelConstructionEnd);
    }

    private void OnLevelConstructionStart_Response(object arg1, object arg2)
    {
        fill.fillAmount = 0;
        targetValue = 0;
    }

    private void OnLevelConstructionEnd_Response(object arg1, object arg2)
    {
        targetValue = 1;
    }

    private void OnLevelConstructionProgress_Response(object sender, object fillAmmount)
    {
        targetValue = (float)fillAmmount;
    }

    IEnumerator FillAnimation()
    {
        float vel = 0;
        while (true)
        {
            fill.fillAmount = Mathf.SmoothDamp(fill.fillAmount, targetValue, ref vel, smoothTime); 
            yield return null;
            if (fill.fillAmount == 1)
                break;
        }
    }

}
