﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class UI_DebugUnlocker : MonoBehaviour
{
    public int[] password = {0,2,1,1,0,0};

    public Queue<int> inputs = new Queue<int>();

    private void Awake()
    {
        for (int i = 0; i < 6; i++)
            inputs.Enqueue(3);
    }

    public void Pressed(int value)
    {
        inputs.Dequeue();
        inputs.Enqueue(value);
        TryToUnlock();
    }

    private void TryToUnlock()
    {
        int[] values = inputs.ToArray();
        if (values.SequenceEqual(password))
            this.PostNotification(Notifications.Debug_OpenClose, true);
    }
}
