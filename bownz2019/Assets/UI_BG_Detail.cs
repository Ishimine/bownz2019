﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_BG_Detail : MonoBehaviour
{
    public Text txt;

    public void Aumentar()
    {
        Settings.Background_Detail++;
        RefreshText();
    }

    public void Disminuir()
    {
        Settings.Background_Detail--;
        RefreshText();
    }

    private void RefreshText()
    {
        txt.text = "BG_Detail: " + Settings.Background_Detail;
    }

    private void OnEnable()
    {
        this.AddObserver(OnBackgroundLevelChange, Notifications.Vfx_BackgroundSetLevel);
        RefreshText();
    }

    private void OnDisable()
    {
        this.RemoveObserver(OnBackgroundLevelChange, Notifications.Vfx_BackgroundSetLevel);
    }

    private void OnBackgroundLevelChange(object arg1, object arg2)
    {
        RefreshText();
    }
}
