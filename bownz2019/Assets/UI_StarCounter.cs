﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_StarCounter : MonoBehaviour
{
  public UI_ConteinerOpenClose OpenClose;

  public Text text;

  private CodeAnimator codeAnimator = new CodeAnimator();
  private void OnEnable()
  {
    this.AddObserver(OnStarPickedUp, Notifications.ScoreUpdate);
  }

  private void OnDisable()
  {
    this.RemoveObserver(OnStarPickedUp, Notifications.ScoreUpdate);
  }

  private void OnStarPickedUp(object arg1, object arg2)
  {
    OpenClose.Open();
    text.text = arg2.ToString();
    codeAnimator.StartWaitAndExecute(this, 2.5f, OpenClose.Close, false);
  }

  public void Hide() => OpenClose.Hide();
}
