﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using Random = UnityEngine.Random;

public class GodRay : MonoBehaviour
{
    public SpriteRenderer render;

    [MinMaxSlider(.25f,1)]
    public Vector2 randomAlpha = new Vector2(.3f,1);

    public float speed = .5f;
    
    public Vector2 speedRange = new Vector2(.2f,3);

    [MinMaxSlider(0,1)]
    public Vector2 alphaAnimationRange = new Vector2(0.3f,.6f);

    public bool animatedAlpha = true;
    private float alphaRange;
    private float startAlpha;
   [ShowInInspector] private Color renderColor;

    private void Awake()
    {
        renderColor = render.color;
        renderColor.a = Random.Range(randomAlpha.x,randomAlpha.y);
        render.color = renderColor;
        startAlpha = renderColor.a;
        speed = speedRange.GetRandomBetweenXY();

        alphaRange = alphaAnimationRange.GetRandomBetweenXY();
        animatedAlpha = Random.Range(0, 5) == 0;

        render.sortingLayerID = SortingLayer.layers.GetRandom().id;
    }

    public void Update()
    {
        if (!animatedAlpha) return;

        renderColor.a =   Mathf.Sin(Time.time * speed) * alphaRange + startAlpha;
        render.color = renderColor;
    }
}
