﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.Rendering;
using UnityEngine.Serialization;

public class GiftBox : MonoBehaviour
{
    public int hitsToOpen = 7;
    public float progressDecayDuration = 3;
    
    [Range(0,1),ReadOnly, ShowInInspector]private float progress;

    public GameObject fireworksPrefab;
    public Transform fireworksSpawnPoint;
    public Transform unlockProductRoot;
    public Transform visualRoot;
    public SpriteRenderer boxRender;
   
    [Header("Top")]
    public Rigidbody2D topRb;
    public float topJumpForce = 20;

    [TabGroup("Particles")] public ParticleSystem tapParticles;
    [TabGroup("Particles")] public ParticleSystem explotionParticles;
    [TabGroup("Particles")] public ParticleSystem fireworks;

    public Shop shop;

    [TabGroup("Animation"),Header("Tap")] public float tapDuration = .7f;
    [TabGroup("Animation")] public AnimationCurve tapSquashCurve;
    [TabGroup("Animation")] public float tapSquashAmmount = .2f;

    [TabGroup("Animation"),Header("Anticipation")] public float preJumpDuration = 1.2f;
    [TabGroup("Animation")] public AnimationCurve squash;
    [TabGroup("Animation")] public float squashAmmount = .25f;
   
    [TabGroup("Animation"),Header("Jump")]    public float jumpDuration = 5;
    [TabGroup("Animation")]  public AnimationCurve heightCurve;
    [TabGroup("Animation")]  public float jumpHeight = 20;

    [TabGroup("Animation"), Header("BoxIntro")] public float boxIntroDuration = .7f;
    [TabGroup("Animation")] public AnimationCurve boxIntroCurve;
    
    bool active;

    public float endWaitTime = 3.5f;

    private IEnumerator squashRutine;
    private IEnumerator WaitForTapsRutine;
    private IEnumerator OpenRutine;
    private IEnumerator AppearRutine;

    private void OnEnable()
    {
        topRb.simulated = false;
    }

    private Vector2 startPosition; 
    [Button]
    public void StartUnlockRutine(Vector3 position)
    {
        active = true;
        startPosition = position;
        gameObject.SetActive(true);
        gameObject.transform.localScale = Vector3.one;
        topRb.velocity = Vector2.zero;
        topRb.simulated = false;
        topRb.transform.rotation = Quaternion.Euler(0,0,-180);
        Settings.UiInteraction = false;
                             
        boxRender.color = Color.white;
        topRb.transform.position = Vector3.up * 2;
                               
        if(AppearRutine != null) StopCoroutine(AppearRutine);
        AppearRutine = BoxAppearRutine();
        StartCoroutine(AppearRutine);
    }

    IEnumerator BoxAppearRutine()
    {
        float x = 0;
        transform.localPosition = startPosition;
        do
        {
            transform.localPosition = Vector3.LerpUnclamped(Vector3.right * 20,  startPosition, boxIntroCurve.Evaluate(x));
            x += Time.deltaTime / boxIntroDuration;
            yield return null;
        } while (x < 1 );
        StartWaitForTapRutine();
    }

    private void StartWaitForTapRutine()
    {
      if(WaitForTapsRutine != null) StopCoroutine(WaitForTapsRutine);
        WaitForTapsRutine = WaitForTaps();
        StartCoroutine(WaitForTapsRutine);
    }

    IEnumerator WaitForTaps()
    {
        progress = 0;
        do
        {
            yield return null;
            progress -= Time.deltaTime / progressDecayDuration;
            if(progress < 0) progress = 0;
        } while (progress <= 1);
        OpenBox();
    }

    [Button]
    public void Tapped()
    {
        if(tapParticles!=null) tapParticles.Play();
        progress += (float) 1 / hitsToOpen;

        if (squashRutine != null)
            StopCoroutine(squashRutine);

         squashRutine = SquashAnimation();

        StartCoroutine(squashRutine);
    }


    IEnumerator SquashAnimation()
    {
        Vector3 startScale = visualRoot.localScale;
        Vector3 targetScale = new Vector3(.5f+startScale.x/2+1*tapSquashAmmount, .5f+startScale.y/2-1*tapSquashAmmount, 1);
        Vector3 dif = new Vector3(startScale.x-1, startScale.y-1, startScale.z-1);
        float x = 0;
        do
        {
            x += Time.deltaTime / tapDuration;
            visualRoot.localScale = Vector3.LerpUnclamped(startScale - (dif * x), targetScale,tapSquashCurve.Evaluate(x));
            yield return null;
        } while (x < 1);
    }
    
    [Button]
    private void OpenBox()
    {
        active = false;

        Debug.Log("<color=green> OpenBox </color>");
        Product unlockedProduct = shop.AvailableProducts.GetRandom();
        Inventory.Set(unlockedProduct.PrimaryId, 1, false);
        Inventory.Consume(Inventory.IDs.GiftBox, 1, true);
        ProductDisplay productDisplay = Instantiate(unlockedProduct.GetDisplayPrefab(),unlockProductRoot).GetComponent<ProductDisplay>();
                       
        productDisplay.transform.localPosition = (Vector3)startPosition + Vector3.up * 2;
        productDisplay.Display(unlockedProduct);
        productDisplay.priceTagConteiner.gameObject.SetActive(false);
        productDisplay.nameTagConteiner.gameObject.SetActive(false);
        productDisplay.gameObject.AddComponent<SortingGroup>().sortingOrder = -1;

        if(OpenRutine!= null) StopCoroutine(OpenRutine);
        OpenRutine = OpenBoxAnimation(productDisplay);
        StartCoroutine(OpenRutine);
    }

    private void FireTop()                                                                                                      
    {
        topRb.simulated = true;
        topRb.AddForce(new Vector2(Random.Range(-.4f,.4f), 1)*topJumpForce, ForceMode2D.Impulse);
        topRb.angularVelocity = Random.Range(-180, 180);
            
        if (explotionParticles != null) explotionParticles.Play();
        
        if (fireworks != null) Destroy(fireworks.gameObject);

        fireworks = Instantiate(fireworksPrefab, fireworksSpawnPoint).GetComponent<ParticleSystem>();
        
        if (fireworks != null) fireworks.Play();
    }

    private IEnumerator PreJumpAnimation()
    {
        Vector3 startScale = visualRoot.localScale;
        Vector3 targetScale = new Vector3(.5f+startScale.x/2+1*squashAmmount, .5f+startScale.y/2-1*squashAmmount, 1);
        Vector3 dif = new Vector3(startScale.x-1, startScale.y-1, startScale.z-1);
        float x=0;
        do
        {
            x += Time.deltaTime / preJumpDuration;
            visualRoot.transform.localScale = Vector3.LerpUnclamped(startScale - (dif * x) , targetScale, squash.Evaluate(x));
            yield return null;
        } while (x<1);
    }

    private IEnumerator JumpAnimation(ProductDisplay productDisplay)
    {
        float progress = 0;
        productDisplay.transform.localScale = Vector3.one * 2;
        Vector3 startPosition = Vector3.zero;
        Color color = boxRender.color;
        do
        {
            yield return null;
            progress += Time.deltaTime / jumpDuration;
            productDisplay.transform.localPosition = Vector3.Lerp(startPosition, Vector3.up * jumpHeight, heightCurve.Evaluate(progress));
            color = new Color(color.r, color.g, color.b, 1 - progress);
            boxRender.color = color;
        } while (progress < 1);
    }

    private IEnumerator OpenBoxAnimation(ProductDisplay productDisplay)
    {
        productDisplay.transform.localScale = Vector3.zero;
        yield return PreJumpAnimation();
        FireTop();
        yield return JumpAnimation(productDisplay);
        yield return new WaitForSeconds(endWaitTime);
        float x = 0;
        Vector2 startSize = productDisplay.gameObject.transform.localScale;
        AnimationCurve curve = AnimationCurve.EaseInOut(0,0,1,1);
        do
        {
            x += Time.deltaTime;
            productDisplay.gameObject.transform.localScale = Vector3.Lerp(startSize, Vector3.zero,curve.Evaluate(x));
            yield return null;
        } while (x < 1);
        Destroy(productDisplay.gameObject);
        OnAnimationDone();
    }

    private void OnAnimationDone()
    {
        this.PostNotification(Notifications.OnGiftBoxAnimationEnd);
        Settings.UiInteraction = true;
        Debug.Log("OnAnimationDone");
    }

    private void OnMouseDown()
    {
        if(!active) return;
        Debug.Log("Mouse Down");
        Tapped();
    }
}
