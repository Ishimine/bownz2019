﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
                using Sirenix.OdinInspector;
public class UI_GiftAvailableButton : MonoBehaviour
{

    [SerializeField]private AnimationCurve sizeCurve;
    [SerializeField]private float textAnimDuration =.5f;
    [SerializeField]   private UI_ConteinerOpenClose openClose;
    [SerializeField] public Image_ColorShine shine;
    [SerializeField] private Transform rootWhiteThings;
    [SerializeField] public Button button;
    public Text text;
    private CodeAnimator codeAnimator = new CodeAnimator();

    private int RemainingGifts
    {
        get
        {
            int v = Inventory.Get(Inventory.IDs.GiftBox);
            Debug.Log($"RemainingGifts: {v}");
            return v;
        }
    }


    private int currentValue;

    private void Awake()
    {
        openClose.gameObject.SetActive(false);
            button.interactable = false;
    }

    void OnEnable()
    {
        /*Inventory.OnElementChange -= OnInventoryUpdate;
        Inventory.OnElementChange += OnInventoryUpdate;*/

        Inventory.OnInventoyChange -= OnInventoryUpdate;
        Inventory.OnInventoyChange += OnInventoryUpdate;

        if(RemainingGifts > 0)
             StartUpdateValueAnimation(0,RemainingGifts);
        else
            openClose.Hide();
    }

    void OnDisable()
    {
        //Inventory.OnElementChange -= OnInventoryUpdate;
        Inventory.OnInventoyChange -= OnInventoryUpdate;
    }

    private void OnInventoryUpdate()
    {
        Debug.Log("OnInventoryUpdate");
        if(RemainingGifts > currentValue)
             StartUpdateValueAnimation(currentValue,RemainingGifts);
    }

    [Button]
    private void StartUpdateValueAnimation(int startValue, int endValue)
    {
        rootWhiteThings.transform.localScale = Vector3.zero;
        button.interactable = false;

        Debug.Log("StartUpdateValueAnimation");
        if (openClose.IsClosed)
        {
            codeAnimator.StartWaitAndExecute(this, 3.5f, 
                () =>
                {
                    openClose.Open(()=> StartUpdateValueAnimation(startValue,endValue));
                }, true);
            return;
        }

        button.interactable = true;
        shine.ShineTo(Color.clear);

        currentValue = endValue;
        text.text = endValue.ToString();
        codeAnimator.StartAnimacion(this, x =>
        {
            rootWhiteThings.transform.localScale = Vector3.one * x;
            text.transform.localScale = Vector3.one * sizeCurve.Evaluate(x);
        }, DeltaTimeType.deltaTime, AnimationType.Simple, sizeCurve,OnUpdateAnimationDone,textAnimDuration);
    }

    private void OnUpdateAnimationDone()
    {

        Debug.Log("OnUpdateAnimationDone");
        if (RemainingGifts == 0)
            openClose.Close();
    }

    public void Pressed()
    {
        this.PostNotification(Notifications.GoToGiftBox);
    }
}
