﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagnetAnimation : MonoBehaviour
{
    public Sprite sprite;

    public int repetitions = 3;

    public List<SpriteRenderer> renderers;

    private float speed = .55f;

    public Color showColor = new Color(1, 1, 1, .25f);
    public Color hideColor = new Color(1,1,1,0);

    float offset;

    private void Awake()
    {
        offset = 1f / repetitions;
        for (int i = 0; i < repetitions; i++)
        {
            SpriteRenderer renderer = new GameObject().AddComponent<SpriteRenderer>();
            renderer.sprite = sprite;
            renderer.transform.SetParent(transform);
            renderers.Add(renderer);
            renderer.color = hideColor;
        }
    }


    public void Update()
    {
        for (int i = 0; i < renderers.Count; i++)
        {
            float t = Mathf.Repeat(-Time.time * speed + offset* i, 1);
            renderers[i].transform.localScale = Vector3.one * t;
            renderers[i].color = Color.Lerp(hideColor, showColor,  Mathf.PingPong(t*2,1));
        }
    }

}
