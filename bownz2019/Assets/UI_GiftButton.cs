﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_GiftButton : MonoBehaviour
{
    public Button button;
    private CodeAnimator codeAnimator = new CodeAnimator();

    public ConstantRotation rays;

    private void OnEnable()
    {
        this.AddObserver(OnGiftButtonEnable, Notifications.GiftButtonSetEnable);
    }

    private void OnDisable()
    {
        this.RemoveObserver(OnGiftButtonEnable, Notifications.GiftButtonSetEnable);
    }

    public void OnGiftButtonEnable(object sender, object arg)
    {
        button.interactable = (bool) arg;
        if (button.interactable)
            StartAnimation();
        else
            codeAnimator.Stop(this);
    }

    private void StartAnimation()
    {
        codeAnimator.StartAnimacion(this,
            x => { rays.gameObject.transform.localScale = Vector3.one * x; }, DeltaTimeType.deltaTime,
            AnimationType.Simple, null, .5f);
    }
}
