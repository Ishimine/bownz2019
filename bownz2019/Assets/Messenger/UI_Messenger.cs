﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class UI_Messenger : MonoSingleton<UI_Messenger>
{
    public override UI_Messenger Myself => this;

    [SerializeField] GameObject defaultToastDisplay = null;
    [SerializeField] GameObject defaultInteractiveDisplay = null;
    [SerializeField] GameObject defaultValueChangeDisplay = null;


    private UI_ToastMsgDisplay toastDisplay;
    public UI_ToastMsgDisplay ToastDisplay
    {
        get
        {
            if (toastDisplay == null)
                toastDisplay = Instantiate(defaultToastDisplay, transform).GetComponent<UI_ToastMsgDisplay>();
            return toastDisplay;
        }
    }

    private UI_ValueChangeMsgDisplay valueChangeDisplay;
    public UI_ValueChangeMsgDisplay ValueChangeDisplay
    {
        get
        {
            if (valueChangeDisplay == null)
                valueChangeDisplay = Instantiate(defaultValueChangeDisplay, transform).GetComponent<UI_ValueChangeMsgDisplay>();
            return valueChangeDisplay;
        }
    }

    private UI_InteractiveMsgDisplay interactiveMsgDisplay;
    public UI_InteractiveMsgDisplay InteractiveMsgDisplay
    {
        get
        {
            if (interactiveMsgDisplay == null)
                interactiveMsgDisplay = Instantiate(defaultInteractiveDisplay, transform).GetComponent<UI_InteractiveMsgDisplay>();
            return interactiveMsgDisplay;
        }
        set { interactiveMsgDisplay = value; }
    }

    private Queue<Toast_Msg> msg_Toasts;

    public static void Show(Toast_Msg toast_Msg)
    {
        Instance.ToastDisplay.Show(toast_Msg);
    }

    public static void Show(Interactive_Msg options_Msg)
    {
        Instance.InteractiveMsgDisplay.Show(options_Msg);
    }

    public static void Show(ValueChangeMsg valueChangeMsg)
    {
        Instance.ValueChangeDisplay.Show(valueChangeMsg);
    }
}

public struct ValueChangeMsg
{
    readonly public MsgPosition Position;
    readonly public float Duration;

    readonly public float StartValue;
    readonly public float EndValue;
    readonly public string format;
    readonly public Sprite Icon;

    public ValueChangeMsg(MsgPosition position, float duration, float startValue, float endValue, string format, Sprite icon)
    {
        Position = position;
        Duration = duration;
        StartValue = startValue;
        EndValue = endValue;
        this.format = format;
        Icon = icon;
    }
}

public enum MsgPosition { Top, Bottom }

public struct Toast_Msg
{
    readonly public string Text;
    readonly public MsgPosition Position;
    readonly public float Duration;
    readonly public Sprite Icon;

    public Toast_Msg(string text, MsgPosition position, float duration, Sprite icon)
    {
        Text = text;
        Position = position;
        Duration = duration;
        Icon = icon;
    }
}

public struct Interactive_Msg
{
    readonly public string Title;
    readonly public string Description;
    readonly public Button_Option[] Options;

    public Interactive_Msg(string title, string description, Button_Option[] options)
    {
        Title = title;
        Description = description;
        Options = options;
    }
}

public struct Button_Option
{
    readonly public string Text;
    readonly public Action Action;
    readonly public int StyleId;
    public Button_Option(string text, Action action, int styleId)
    {
        Text = text;
        Action = action;
        StyleId = styleId;
    }
}