﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_ButtonOption : MonoBehaviour
{
    public UI_InteractiveMsgDisplay target;

    public Text txt;
    public Button button;

    private int optionIndex;
    public int OptionIndex
    {
        get { return optionIndex; }
        set { optionIndex = value; }
    }

    private void OnEnable()
    {
    }

    public void Pressed()
    {
        target.SelectOption(optionIndex);
    }

    public void SetText(string txt)
    {
        this.txt.text = txt;
    }
}
