﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

public class UI_DebugToastDisplay : UI_ToastMsgDisplay
{
    public float distFromBot;
    public float moveDistance;

    public RectTransform conteiner;
    public Text txt;
    public Image icon;

    private CodeAnimator codeAnimator = new CodeAnimator();
    private float currentDistance = 0;

    [SerializeField] private CodeAnimatorCurve animatorCurveIn = null;
    [SerializeField] private CodeAnimatorCurve animatorCurveOut = null;

    [Button]
    public override void Show(Toast_Msg toast_Msg)
    {
        if (toast_Msg.Icon == null)
            icon.color = Color.clear;
        else
        {
            icon.color = Color.white;
            icon.sprite = toast_Msg.Icon;
        }
        txt.text = toast_Msg.Text;
        SetPosition(toast_Msg.Position);
        Open(toast_Msg);
    }

    private void SetPosition(MsgPosition msgPosition)
    {
        if(msgPosition == MsgPosition.Bottom)
        {
            conteiner.anchorMin = new Vector2(.5f, 0);
            conteiner.anchorMax = new Vector2(.5f, 0);
            moveDistance = Mathf.Abs(distFromBot + conteiner.rect.height);
            conteiner.anchoredPosition = Vector3.down * conteiner.rect.height;
        }
        else
        {
            conteiner.anchorMin = new Vector2(.5f, 1);
            conteiner.anchorMax = new Vector2(.5f, 1);
            moveDistance = -Mathf.Abs(distFromBot + conteiner.rect.height);
            conteiner.anchoredPosition = Vector3.up * conteiner.rect.height;
        }
    }

    [Button]
    public void Open(Toast_Msg toast_Msg)
    {
        Vector2 startPos = conteiner.anchoredPosition;
        codeAnimator.StartAnimacion(this,
            x =>
            {
                conteiner.anchoredPosition = startPos + Vector2.up * moveDistance * x;
            }, DeltaTimeType.deltaTime, AnimationType.Simple, animatorCurveIn.Curve, ()=> Wait(toast_Msg.Duration), animatorCurveIn.Time);
    }

    private void Wait(float duration)
    {
        codeAnimator.StartWaitAndExecute(this, duration, Close, false);
    }

    [Button]
    private void Close()
    {
        Vector2 startPos = conteiner.anchoredPosition;
        codeAnimator.StartAnimacion(this,
           x =>
           {
               conteiner.anchoredPosition = startPos + Vector2.down * moveDistance * x;
           }, DeltaTimeType.deltaTime, AnimationType.Simple, animatorCurveOut.Curve, animatorCurveOut.Time);
    }
}
