﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class UI_InteractiveMsgDisplay : MonoBehaviour
{
    public abstract void Show(Interactive_Msg interactive_Msg);

    public abstract void SelectOption(int optionId);

}
