﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Sirenix.OdinInspector;

public class UI_DebugIntDisplay : UI_InteractiveMsgDisplay
{
    private bool active = false;

    public Text title;
    public Text text;
    public UI_ButtonOption[] buttons;

     [SerializeField]  private Transform conteiner;
    [SerializeField] CodeAnimatorCurve animatorCurve = null;

    private CodeAnimator codeAnimator = new CodeAnimator();

    Action[] optionsActions;

    private void OnEnable()
    {
     
        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i].OptionIndex = i;
            buttons[i].target = this;
        }
    }

    public override void SelectOption(int optionId)
    {
        active = false;
        Close(optionsActions[optionId]);
    }

    [Button]
    public override void Show(Interactive_Msg interactive_Msg)
    {
        title.text = interactive_Msg.Title;
        text.text = interactive_Msg.Description;
        active = true;
        optionsActions = new Action[interactive_Msg.Options.Length];
        for (int i = 0; i < optionsActions.Length; i++)
        {
            optionsActions[i] = interactive_Msg.Options[i].Action;
        }
        int count = Math.Min(buttons.Length, optionsActions.Length);
        for (int i = 0; i < buttons.Length; i++)
        {
            if(i < count)
            {
                buttons[i].gameObject.SetActive(true);
                buttons[i].SetText(interactive_Msg.Options[i].Text);
            }
            else
                buttons[i].gameObject.SetActive(false);
        }
        Open();
    }

    public void Close(Action postAction)
    {
        codeAnimator.StartAnimacion(this,
            x =>
            {
                conteiner.localScale = Vector3.one * (1 - x);
            }, DeltaTimeType.deltaTime, AnimationType.Simple, animatorCurve.Curve, postAction, animatorCurve.Time);
    }

    public void Open()
    {
        codeAnimator.StartAnimacion(this,
          x =>
          {
                conteiner.localScale = Vector3.one * x;
          }, DeltaTimeType.deltaTime, AnimationType.Simple, animatorCurve.Curve, animatorCurve.Time);
    }
}
