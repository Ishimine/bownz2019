﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class UI_ToastMsgDisplay : MonoBehaviour
{
    public abstract void Show(Toast_Msg toast_Msg);
}
