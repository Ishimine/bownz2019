﻿Shader "Custom/MultipleAnimatedShaderWTransparency" {

	Properties 
	{
		_Scale("Scale",float)=1

		[NoScaleOffset]_MainTex("MainTex", 2D) = "white" {}
		_Tint0("BaseColor", Color) = (1,1,1,1)

		_Texture1 ("Texture 1", 2D) = "white" {}
		_Tint1("Tint", Color) = (1,1,1,1)

		_Texture2 ("Texture 2", 2D) = "white" {}
		_Tint2("Tint", Color) = (1, 1, 1, 1)

		_Texture3("Texture 3", 2D) = "white" {}
		_Tint3("Tint", Color) = (1, 1, 1, 1)

		_Texture4("Texture 4", 2D) = "white" {}
		_Tint4("Tint", Color) = (1, 1, 1, 1)
	}

	SubShader {
			Pass {
			
			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM	
			
			#pragma vertex MyVertexProgram
			#pragma fragment MyFragmentProgram

			#include "UnityCG.cginc"

			sampler2D _MainTex,_Texture1, _Texture2, _Texture3, _Texture4;
			float4 _Tint0,_Tint1, _Tint2, _Tint3, _Tint4;
			float4 _MainTex_ST,_Texture1_ST, _Texture2_ST, _Texture3_ST, _Texture4_ST;
			float _Scale;
			float _Speed1, _Speed2, _Speed3, _Speed4;

			float2x2 rotationMatrix;

			struct VertexData 
			{
				float4 position : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct Interpolators 
			{
				float4 position : SV_POSITION;
				float2 uv0 : TEXCOORD0;
				float2 uv1 : TEXCOORD1;
				float2 uv2 : TEXCOORD2;
				float2 uv3 : TEXCOORD3;
				float2 uv4 : TEXCOORD4;
			};

			Interpolators MyVertexProgram (VertexData vertex) 
			{
				Interpolators i;
				i.position = UnityObjectToClipPos(vertex.position);
				
				rotationMatrix = float2x2(-0.7, 0.7, -0.7, -0.7);

				i.uv1 = TRANSFORM_TEX(vertex.uv, _Texture1) * _Scale + _Texture1_ST.wz * _Time;
				i.uv2 = TRANSFORM_TEX(vertex.uv, _Texture2)* _Scale + _Texture2_ST.wz * _Time;
				i.uv3 = TRANSFORM_TEX(vertex.uv, _Texture3)* _Scale + _Texture3_ST.wz * _Time;
				i.uv4 = TRANSFORM_TEX(vertex.uv, _Texture4)* _Scale + _Texture4_ST.wz * _Time;

				i.uv1.xy = mul(i.uv1.xy, rotationMatrix);
				i.uv2.xy = mul(i.uv2.xy, rotationMatrix);
				i.uv3.xy = mul(i.uv3.xy, rotationMatrix);
				i.uv4.xy = mul(i.uv4.xy, rotationMatrix);

				return i;
			}	

			float4 MyFragmentProgram(Interpolators i) : SV_TARGET
			{
				

				float4 tex1 = tex2D(_Texture1, i.uv1);
				float4 tex2 = tex2D(_Texture2, i.uv2);
				float4 tex3 = tex2D(_Texture3, i.uv3);
				float4 tex4 = tex2D(_Texture4, i.uv4);
				
				return _Tint0 +
					tex1 * tex1.a * _Tint1 +
					tex2 * tex2.a * _Tint2 +
					tex3 * tex3.a * _Tint3 +
					tex4 * tex4.a * _Tint4;
			}

			ENDCG
		}
	}
}