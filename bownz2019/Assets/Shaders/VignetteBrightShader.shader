﻿Shader "Custom/VignetteShader" {

	Properties
	{
		_MainTex("MainTex", 2D) = "white" {}
		_Tint0("Tint", Color) = (1,1,1,1)
	}

		SubShader{
				//Tags { "Queue" = "Transparent" "RenderType" = "Transparent" "IgnoreProjector" = "True" }

				Pass {

				//Blend SrcColor SrcAlpha
				//Blend One OneMinusSrcAlpha
				//Blend SrcColor OneMinusDstColor
				//Blend DstColor Zero
				//Blend DstColor SrcColor
			
				
				//Blend DstColor SrcColor
			
			 //Blend Overlay(SrcColor,SrcColor)

		//	Blend SrcColor SrcColor

			BlendOp Max
				Blend DstColor OneMinusSrcAlpha

				CGPROGRAM

				#pragma vertex MyVertexProgram
				#pragma fragment MyFragmentProgram

				#include "UnityCG.cginc"

				sampler2D _MainTex;
				float4 _Tint0;
				float4 _MainTex_ST;

				float4 Overlay(float4 a, float4 b)
				{
					fixed4 r = a < .5 ? 2.0 * a * b : 1.0 - 2.0 * (1.0 - a) * (1.0 - b);
					r.a = b.a;
					return r;
				}

				struct VertexData {
					float4 position : POSITION;
					float2 uv : TEXCOORD0;
					float4 color : COLOR;
				};

				struct Interpolators {
					float4 position : SV_POSITION;
					float2 uv1 : TEXCOORD0;
					float4 color : COLOR;
				};

				Interpolators MyVertexProgram(VertexData vertex)
				{
					Interpolators i;
					i.position = UnityObjectToClipPos(vertex.position);
					i.uv1 = TRANSFORM_TEX(vertex.uv, _MainTex) + _MainTex_ST.wz;
					i.color = vertex.color;

					return i;
				}

				float4 MyFragmentProgram(Interpolators i) : SV_TARGET
				{
					float4 tex1 = tex2D(_MainTex, i.uv1);
					/*float4 tex1 = tex2D(_MainTex, i.uv1);*/
				//	return _Tint0 * tex1 * tex1.a;
					return Overlay(tex1, i.color);
					//return _Tint0 * tex1 * tex1.a;
				}

				ENDCG
			}
		}
}