﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Custom/GodRayAndShadows" 
{

	Properties 
	{
		_PlayAreaHeight("_PlayAreaHeight", float) = 0
		_HeightOffset("HeightOffset", float) = 0
		_FloorOffset("FloorOffset", float) = 0
		_Tint0("BaseTint", Color) = (1,1,1,1)
	
		_Rotation("Rotation", vector) = (-.7,.7,-.7,.7)

		_Height("Height", float) = 1
		_OffsetAndScale("OffsetAndScacle", Vector) = (0,0,70,70)

		_Texture1 ("Texture 1", 2D) = "white" {}
		_Tint1("Tint", Color) = (1,1,1,1)
		_Height1("_Height", Range(0,1)) = .1

		_Texture2 ("Texture 2", 2D) = "white" {}
		_Tint2("Tint", Color) = (1, 1, 1, 1)
		_Height2("_Height", Range(0,1)) = .3

		_Texture3("Texture 3", 2D) = "white" {}
		_Tint3("Tint", Color) = (1, 1, 1, 1)
		_Height3("_Height", Range(0,1)) = .6
		_S("_S", float) = .6

		_Texture4("Shadow", 2D) = "white" {}
		_Tint4("Tint", Color) = (1, 1, 1, 1)
		_Height4("_Height", Range(0,1)) = .1

	}

	SubShader 
		{
			Pass 
			{
			
			//Blend SrcAlpha OneMinusSrcAlpha
			//Blend OneMinusDstColor One
			//Blend DstColor Zero 
			//Blend DstColor SrcColor 
			//Blend One One	
			//Blend SrcAlpha OneMinusDstColor

			//Blend SrcColor DstColor

			Blend One One


			CGPROGRAM
			
			#pragma vertex MyVertexProgram
			#pragma fragment MyFragmentProgram

			#include "UnityCG.cginc"

			sampler2D _Texture1, _Texture2, _Texture3, _Texture4;
			float4 _Tint0, _Tint1, _Tint2, _Tint3, _Tint4;
			float4 _Texture1_ST, _Texture2_ST, _Texture3_ST, _Texture4_ST;
			
			float _Height, _Height1,_Height2,_Height3,_Height4,_S, _HeightOffset, _FloorOffset, _PlayAreaHeight;
			vector _OffsetAndScale,_Rotation;

			float2x2 rotationMatrix;

			struct VertexData 
			{
				float4 position : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct Interpolators 
			{
				float4 position : SV_POSITION;
				float2 uv0 : TEXCOORD0;
				float2 uv1 : TEXCOORD1;
				float2 uv2 : TEXCOORD2;
				float2 uv3 : TEXCOORD3;
				float2 uv4 : TEXCOORD4;
			};
			
			Interpolators MyVertexProgram (VertexData vertex) 
			{
				Interpolators i;
				
				i.position = UnityObjectToClipPos(vertex.position);
				
				//rotationMatrix = float2x2(-_Rotation, _Rotation, -_Rotation, -_Rotation);
				rotationMatrix = _Rotation;

				i.uv1 = (TRANSFORM_TEX(vertex.uv, _Texture1) /_OffsetAndScale.wz + (_Texture1_ST.wz * _Time + _OffsetAndScale.xy));
				i.uv2 = (TRANSFORM_TEX(vertex.uv, _Texture2) /_OffsetAndScale.wz + (_Texture2_ST.wz * _Time + _OffsetAndScale.xy));
				i.uv3 = (TRANSFORM_TEX(vertex.uv, _Texture3) /_OffsetAndScale.wz + (sin(_Texture3_ST.wz * _Time * _S) + _OffsetAndScale.xy));
				i.uv4 = (TRANSFORM_TEX(vertex.uv, _Texture4) /_OffsetAndScale.wz + (_Texture4_ST.wz * _Time + _OffsetAndScale.xy));

                i.uv0 = mul(unity_ObjectToWorld, vertex.position).xy;

				i.uv1.xy = mul(i.uv1.xy, rotationMatrix);
				i.uv2.xy = mul(i.uv2.xy, rotationMatrix);
				i.uv3.xy = mul(i.uv3.xy, rotationMatrix);
				i.uv4.xy = mul(i.uv4.xy, rotationMatrix);
				return i;
			}	

			float GetHeight(inout float offset, inout float h)
			{
				return (h * offset + _HeightOffset + _FloorOffset);
			}
			
		    float GetAlpha(inout float offset, inout float currentHeight)
			{
				float personalHeight = GetHeight(offset, currentHeight);

				float totalHeight = (_Height + _HeightOffset);
				float aux = totalHeight - _PlayAreaHeight;

				//float alpha = lerp(0, 1, (currentHeight - aux) / _PlayAreaHeight);
				float alpha = lerp(0, .3, clamp( (currentHeight - aux),0,totalHeight)/ _PlayAreaHeight);

				return lerp(0, 1.2, personalHeight / totalHeight) + alpha;
			}
			
			float4 MyFragmentProgram(Interpolators i) : SV_TARGET
			{
				float4 tex1 = tex2D(_Texture1, i.uv1);
				float4 tex2 = tex2D(_Texture2, i.uv2);
				float4 tex3 = tex2D(_Texture3, i.uv3);
				float4 tex4 = tex2D(_Texture4, i.uv4);
				
				return 
					((tex1.a * _Tint1  *  GetAlpha(_Height1, i.uv0.y) +
					 tex2.a * _Tint2  *  GetAlpha(_Height2, i.uv0.y)  +
					 tex3.a * _Tint3  *  GetAlpha(_Height3, i.uv0.y)) * (-tex4.a + 1)) +
					 tex4.a * _Tint4 * GetAlpha(_Height4, i.uv0.y);
			}
			
			ENDCG
		}
	}
}