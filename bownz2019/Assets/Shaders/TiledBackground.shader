﻿Shader "Custom/TiledBackground" 
{
	Properties 
	{
		_Texture ("Texture", 2D) = "white" {}
		_Tint("BaseTint", Color) = (1,1,1,1)
		_Rotation("Rotation", vector) = (-.7,.7,-.7,.7)
		_OffsetAndScale("OffsetAndScacle", Vector) = (0,0,70,70)
	}
	SubShader 
		{
			Pass 
			{
			

			                          
			CGPROGRAM
			#pragma vertex MyVertexProgram
			#pragma fragment MyFragmentProgram

			#include "UnityCG.cginc"
			
			sampler2D _Texture;
			float4 _Tint;
			float4 _Texture_ST;
			
			vector _OffsetAndScale,_Rotation;

			float2x2 rotationMatrix;

			struct VertexData 
			{
				float4 position : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct Interpolators 
			{
				float4 position : SV_POSITION;
				float2 uv0 : TEXCOORD0;
			};
			
			Interpolators MyVertexProgram (VertexData vertex) 
			{
				Interpolators i;
				
				i.position = UnityObjectToClipPos(vertex.position);
				rotationMatrix = _Rotation;

				i.uv0 = (TRANSFORM_TEX(vertex.uv, _Texture) /_OffsetAndScale.wz + (_Texture_ST.wz * _Time + _OffsetAndScale.xy));
				i.uv0.xy = mul(i.uv0.xy, rotationMatrix);

                //i.uv0 = mul(unity_ObjectToWorld, vertex.position).xy;
				return i;
			}	

			float4 MyFragmentProgram(Interpolators i) : SV_TARGET
			{
				float4 tex = tex2D(_Texture, i.uv0);
				return tex * _Tint;
			}
			ENDCG
		}
	}
}