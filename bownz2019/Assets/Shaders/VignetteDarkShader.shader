﻿Shader "Custom/VignetteDarkShader" {

	Properties
	{
		_MainTex("MainTex", 2D) = "white" {}
		_Tint0("Tint", Color) = (1,1,1,1)
	}

		SubShader{
				Tags { "Queue" = "Transparent" "RenderType" = "Transparent" "IgnoreProjector" = "True" }

				Pass {

				//Blend SrcColor SrcAlpha
				//Blend One OneMinusSrcAlpha
				//Blend SrcColor OneMinusDstColor
				//Blend DstColor Zero
				//Blend DstColor SrcColor
			
				
				//Blend DstColor SrcColor
			
				Blend DstColor OneMinusSrcAlpha

				CGPROGRAM

				#pragma vertex MyVertexProgram
				#pragma fragment MyFragmentProgram

				#include "UnityCG.cginc"

				sampler2D _MainTex;
				float4 _Tint0;
				float4 _MainTex_ST;

				struct VertexData {
					float4 position : POSITION;
					float2 uv : TEXCOORD0;
				};

				struct Interpolators {
					float4 position : SV_POSITION;
					float2 uv1 : TEXCOORD0;
				};

				Interpolators MyVertexProgram(VertexData vertex)
				{
					Interpolators i;
					i.position = UnityObjectToClipPos(vertex.position);
					i.uv1 = TRANSFORM_TEX(vertex.uv, _MainTex) + _MainTex_ST.wz;
					return i;
				}

				float4 MyFragmentProgram(Interpolators i) : SV_TARGET
				{
					float4 tex1 = tex2D(_MainTex, i.uv1);
					return _Tint0 * tex1 * tex1.a;
				}

				ENDCG
			}
		}
}