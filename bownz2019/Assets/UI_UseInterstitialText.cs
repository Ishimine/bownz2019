﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_UseInterstitialText : MonoBehaviour
{
    public Text txt;

    private void OnEnable()
    {
        txt.text = $"UseInterstitial{(AdsManager.Instance.UseBanner ? "On" : "Off")}";
    }
}
