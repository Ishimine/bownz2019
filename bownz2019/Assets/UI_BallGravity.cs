﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_BallGravity : MonoBehaviour
{
    public Text pcText;

    public void OnEnable()
    {
        pcText.text = Settings.BallGravity.ToString();
    }

    public void ApplyValue(string value)
    {
        Settings.BallGravity = float.Parse(value);
    }
}
