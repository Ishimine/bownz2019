﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_ToggleVibration : UI_Button_Toggle
{
    private void OnEnable()
    {
        Initialize(Settings.UseVibration);
    }

    public override void SetState(bool nState)
    {
        base.SetState(nState);
        Settings.UseVibration = nState;
    }
}