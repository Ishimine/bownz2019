﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_UseVideoText : MonoBehaviour
{
   public Text txt;

    private void OnEnable()
    {
        txt.text = $"UseVideo{(AdsManager.Instance.UseBanner ? "On" : "Off")}";
    }
}