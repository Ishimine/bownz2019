﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableSwap : MonoBehaviour
{
    
    public void SwapState()
    {
        gameObject.SetActive(!gameObject.activeSelf);
    }
    
}
