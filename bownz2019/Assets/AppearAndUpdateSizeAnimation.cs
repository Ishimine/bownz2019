﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Rendering;

public class AppearAndUpdateSizeAnimation : MonoBehaviour
{
    public UnityEvent onAppearDone;

    public float activationDuration = .6f;

    public AnimationCurve appearCurve = AnimationCurve.Linear(0,0,1,1);

    public float frequency = .75f;
    public float size = 1;

    private IEnumerator currentRutine;

    private void OnEnable()
    {
        currentRutine = AppearRutine();
    }

    private void Update()
    {
        if (currentRutine == null) return;
        if (!currentRutine.MoveNext())
            StartUpdateAnimation();
    }

    private void StartUpdateAnimation()
    {
        currentRutine = UpdateRutine();
    }

    private IEnumerator AppearRutine()
    {
        Vector3 startSize = transform.localScale;
        Vector3 targetSize = Vector3.one * size;
        float x = 0;
        do
        {
            x += Time.deltaTime / activationDuration;
            transform.localScale = Vector3.Lerp(startSize, targetSize, appearCurve.Evaluate(x));
            yield return null;
        } while (transform.localScale != targetSize);
        onAppearDone?.Invoke();
    }

    private IEnumerator UpdateRutine()
    {
        do
        {
          //  transform.localScale = size * updateCurve.Evaluate(Mathf.PingPong(Time.time * frequency, 1)) * Vector3.one;
            yield return null;
        } while (true);
    }
}
