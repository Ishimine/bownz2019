﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_BloomSwap : MonoBehaviour
{
    public Text txt;
    public void OnPressed()
    {
        Settings.UseBloom = !Settings.UseBloom;
        txt.text = $"Bloon: {(Settings.UseBloom?"On":"Off")}";
    }
}
