﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingFx : MonoBehaviour
{
    public float rate = 1;

    public float magnitude = .5f;

    void Update()
    {
        transform.localPosition += (Vector3.up * Mathf.Sin(Time.time * rate * Mathf.PI) * magnitude) * Time.deltaTime;
    }

}
