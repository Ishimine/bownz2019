﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Preloader : MonoBehaviour
{

    AsyncOperation asyncOperation = null;

    private void Start()
    {
        UI_ScreenTransitioner.Out(LoadNextScene);

        this.PostNotification(Notifications.OnLevelConstructionStart);
    }

    private void LoadNextScene()
    {
        asyncOperation = SceneManager.LoadSceneAsync(1, LoadSceneMode.Single);
    }


    private void Update()
    {
        if (asyncOperation == null) return;

        this.PostNotification(Notifications.OnLevelConstructionProgress, asyncOperation.progress);

        if (asyncOperation.isDone)
        {
            this.PostNotification(Notifications.OnLevelConstructionEnd);
            StartCoroutine(WaitAndTransitionIn());
        }
    }

    IEnumerator WaitAndTransitionIn()
    {
        yield return new WaitForSeconds(.3f);
        UI_ScreenTransitioner.In();
        Destroy(this);
    }
}
