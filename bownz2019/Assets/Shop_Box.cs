﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
                              using Sirenix.OdinInspector;
using UnityEngine.Serialization;

public class Shop_Box : MonoBehaviour
{

   public SpriteRenderer boxRender;

   public Color openBoxColor;
   public Color openCloseColor;

   public Transform closedSignRoot;

   private bool isOpen = false;

   [FormerlySerializedAs("timeBeforeReopening")] public float timeBeforeReOpening = 1f;
   private float closedTime;

   private void Start()
   {
      if(transform.parent.localScale.x != 1)
         transform.localScale = new Vector3(-1,1,1);
   }

   private void OnEnable()
   {
      this.AddObserver(OnPowerUpCanBuy, Notifications.PowerUpCanBuy);
      this.AddObserver(OnShopClosed, Notifications.ShopClosed);
   }

   private void OnDisable()
   {
      this.RemoveObserver(OnPowerUpCanBuy, Notifications.PowerUpCanBuy);
      this.RemoveObserver(OnShopClosed, Notifications.ShopClosed);
   }

   private void OnShopClosed(object arg1, object arg2)
   {
      Debug.Log("Closed time updated: " + Time.time);
      closedTime = Time.time;
   }

   private void OnPowerUpCanBuy(object arg1, object arg2)
   {
      SetOpenClose((bool) arg2);
   }

   [Button]
   public void SetOpenClose(bool value)
   {
      if (value) SetOpen();
      else SetClose();
   }

   private void SetOpen()
   {
      isOpen = true;
      closedSignRoot.gameObject.SetActive(false);
      boxRender.color = openBoxColor;
   }

   private void SetClose()
   {
      isOpen = false;
      closedSignRoot.gameObject.SetActive(true);
      boxRender.color = openCloseColor;
   }

   private void OnCollisionEnter2D(Collision2D other)
   {
      if (!AdsManager.IsVideoReady() || (Time.time - closedTime) < timeBeforeReOpening) return;

      if (isOpen && other.gameObject.CompareTag("Player"))
      {
         OpenShop();
      }
   }

   public void OpenShop()
   {
      this.PostNotification(Notifications.ShopOpenClose,true);
   }

   public void CloseShop()
   {
      this.PostNotification(Notifications.ShopOpenClose,false);
   }

}
