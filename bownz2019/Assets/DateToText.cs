﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
        using Sirenix.OdinInspector;

public class DateToText : MonoBehaviour
{
    public Text txt;

    private void OnEnable()
    {
        ApplyDayToText();
        this.AddObserver(OnDailyDateChange, Notifications.OnDailyDateChange);
    }

    private void OnDisable()
    {
        this.RemoveObserver(OnDailyDateChange, Notifications.OnDailyDateChange);
    }

    private void OnDailyDateChange(object arg1, object arg2)
    {
        Debug.Log("OnDailyDateChange");
        ApplyDayToText();
    }

    [Button]
    private void ApplyDayToText()
    {
       txt.text = "Day " + DailyMode.CurrentDate.DayOfYear.ToString();
    }
}
