﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwapAppearOnSwipeUp : MonoBehaviour
{
    private Color baseColor;
    [SerializeField] Color dissableColor = new Color(1,1, 1,.2f);

    [SerializeField] private bool startActive = true;
    [SerializeField]   private SpriteRenderer render;
    [SerializeField]   private Collider2D col;
   
   private bool active = true;

  [SerializeField]     private AnimationCurve curveIn;
    public float inTime = .3f;

  [SerializeField]     private AnimationCurve curveOut;
    public float outTime = .3f;

    private CodeAnimator codeAnimator = new CodeAnimator();

    private void Awake()
    {
        baseColor = render.color;
        baseColor.a = 1;
        active = !startActive;
        OnFingerUp(null,null);
    }

    private void OnEnable()
    {
        this.AddObserver(OnFingerUp, Notifications.SwipePlatform_Touched);
    }

    private void OnDisable()
    {
        this.RemoveObserver(OnFingerUp, Notifications.SwipePlatform_Touched);
    }

    private void OnFingerUp(object arg1, object arg2)
    {
        active = !active;
        if(active)
            Appear();
        else
            Disappear();
    }

    public void Appear()
    {
        col.enabled = true;
        render.color = Color.white;
        codeAnimator.StartAnimacion(this,
            x =>
            {
                render.color = Color.Lerp(Color.white, baseColor, x); 
            }, DeltaTimeType.deltaTime, AnimationType.Simple, curveIn,inTime);
    }

    public void Disappear()
    {
        col.enabled = false;
      codeAnimator.StartAnimacion(this,
            x =>
            {
                render.color = Color.Lerp(Color.white, dissableColor, x); 
            }, DeltaTimeType.deltaTime, AnimationType.Simple, curveOut, outTime);
    }

    private void OnValidate()
    {
        if (Application.isPlaying) return;
        Color color = render.color;
        color = !startActive ? new Color(color.r,color.g, color.b, dissableColor.a) : new Color(color.r,color.g, color.b, 1);
        render.color = color;
    }
}
