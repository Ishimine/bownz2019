using UnityEngine;

public abstract class UI_VideoRewardButton : MonoBehaviour
{
    public int coinCost = 20;
                      
    public void TryToBuyReward()
    {
        Argument<bool, int> arg = new Argument<bool, int>(false, coinCost);
        this.PostNotification(Notifications.TryToBuy, arg);
        if (arg.Item0)
        {
            _OnReward();
        }
    }

    public void TryToVideoReward()
    {
        if (!AdsManager.IsVideoReady())
        {
            Debug.LogError("<color=red>VIDEO NO ESTA LISTO, EL BOTON NO DEBERIA SER INTERACTIVO</color>");
            return;
        }

        RegisterToRewardVideo();
        AdsManager.Instance.ShowVideo();
    }

    private void RegisterToRewardVideo()
    {
        UnregisterToRewardVideo();
        AdsManager.OnVideoRewardedEvent += _OnReward;
        AdsManager.VideoClosedEvent += _OnVideoClosed;
        AdsManager.OnVideoFailedToLoadEvent += _OnVideoFailed;
    }

    private void UnregisterToRewardVideo()
    {
        AdsManager.OnVideoRewardedEvent -= _OnReward;
        AdsManager.VideoClosedEvent -= _OnVideoClosed;
        AdsManager.OnVideoFailedToLoadEvent -= _OnVideoFailed;
    }

    private void _OnReward()
    {
        OnReward();
    }

    private void _OnVideoClosed()
    {
        UnregisterToRewardVideo();
    }

    private void _OnVideoFailed()
    {
        OnFailed();
    }
    protected abstract void OnFailed();

    protected abstract void OnReward();

    protected void OnDisable()
    {
        UnregisterToRewardVideo();
    }
}