﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_VibrationForce : MonoBehaviour
{

    public float GetVibrationForce()
    {
        return Settings.SwipePlatform_TouchVibrationForce;
    }

    public void ApplyVibrationForce(string value)
    {
        Settings.SwipePlatform_TouchVibrationForce = float.Parse(value);
    }
}
