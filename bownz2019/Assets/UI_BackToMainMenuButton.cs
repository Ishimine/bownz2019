﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_BackToMainMenuButton : MonoBehaviour, IPressed
{
    public void Pressed()
    {
        TimeManager.Resume();
        this.PostNotification(Notifications.GameCycle_Playing_BackToMenu);
    }
}
