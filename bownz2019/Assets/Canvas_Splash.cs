﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Canvas_Splash : MonoBehaviour
{

    public float imageTime = 2;
    public UI_ConteinerOpenClose OpenClose;

    public void Execute(Action callback)
    {
        OpenClose.Hide();
        OpenClose.Open(()=> StartWaitAndExecute(callback));
    }

    private void StartWaitAndExecute(Action callback)
    {
        StartCoroutine(WaitAndExecute(callback));
    }

    IEnumerator WaitAndExecute(Action callback)
    {
        yield return new WaitForSecondsRealtime(imageTime);
        OpenClose.Close();
        callback?.Invoke();
    }
}
