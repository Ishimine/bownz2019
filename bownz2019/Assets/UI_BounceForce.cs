﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_BounceForce : MonoBehaviour
{
    public Text pcText;

    public void OnEnable()
    {
        pcText.text = Settings.BounceForce.ToString();
    }

    public void ApplyValue(string value)
    {
        Settings.BounceForce = float.Parse(value);
    }
}
