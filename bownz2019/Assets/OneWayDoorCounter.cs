﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;

public class OneWayDoorCounter : MonoBehaviour
{
    public OneWay_ByCoin door;
    public SpriteRenderer doorRender;

    public GameObject lockPrefab;

    private DoorLock[] lockGo;

   public float closeDuration =.3f;
   public AnimationCurve closeSizeCurve;

   private IEnumerator rutine;

    private void Awake()
    {
        door.OnInitialize += Initialize;
        door.OnUnlockProgress += UpdateProgress;
    }

    private void Initialize(int lockSteps)
    {
        transform.localScale = Vector3.one;
        lockGo = new DoorLock[lockSteps];
        float displacement = doorRender.size.x / (lockSteps+1);
        Vector2 startPosition = ((lockSteps * displacement) / 2 - displacement / 2) * Vector2.left + Vector2.up*.5f;

        for (int i = 0; i < lockSteps; i++)
        {
            lockGo[i] = Instantiate(lockPrefab,transform).GetComponent<DoorLock>();
            lockGo[i].Initialize(false);
            lockGo[i].transform.localPosition = startPosition + Vector2.right * displacement * i;
        }
    }

    public void UpdateProgress(float value)
    {
        int maxUnlockedValue = Mathf.RoundToInt(Mathf.Lerp(0, lockGo.Length, value));
        for (int i = 0; i < maxUnlockedValue; i++)
        {
            lockGo[i].Unlock();
        }

        if (maxUnlockedValue == lockGo.Length)
            Close();
    }

   public void Close()
   {
       StopRutine();
       rutine = CloseRutine();
       StartCoroutine(rutine);
   }

   private void StopRutine()
   {
       if (rutine != null) StopCoroutine(rutine);
   }

   private IEnumerator CloseRutine()
   {
       yield return new WaitForSeconds(.4f);
       float x = 0;
       do
       {
           x += Time.deltaTime / closeDuration;
           transform.localScale = new Vector3(1,closeSizeCurve.Evaluate(x),1);
           yield return null;
       } while (x < 1);
   }

}


