﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

public class Canvas_UnlockGift : MonoBehaviour
{
    [Header("Dependencies")]
    public Button button;
    public Slider slider;
    public Image sliderFill;
    public Color sliderFillColor;
    public Color sliderFillShineColor;
    public AnimationCurve shineInCurve;
    public AnimationCurve shineOutCurve;

    public Text text;
    public UI_ConteinerOpenClose OpenClose;
    public UI_PopUpText popUpText;
    public Transform boxTransform;
    public GameObject giftPrefab;
            
    [Header("SucessAnimation"), SerializeField]
    private float progressAnimationDuration = .4f;
   
    [Header("SucessAnimation"), SerializeField]private float boxAnimDuration;
    [SerializeField]private AnimationCurve boxAnimSizeCurve;
    [SerializeField] private float shineDuration = .35f;
    public float postAnimationWaitTime = 2;
    private void Start()
    {            
        OpenClose.gameObject.SetActive(false);
    }
                      
    [Button]
    private void StartProgressAnimation(int startValue, int targetValue, int totalValue, Action callback)
    {
        Debug.Log($"StartProgressAnimation(int startValue:{startValue} targetValue:{targetValue}, int total{totalValue})");

        if (OpenClose.IsOpen)
        {
           StartCoroutine(ProgressAnimation(startValue,targetValue,totalValue, callback));
        }
        else
        {
            OpenClose.Hide();
            OpenClose.Open(()=> StartProgressAnimation(startValue,targetValue, totalValue,callback));
        }
    }

    IEnumerator ProgressAnimation(int startValue, int targetValue, int totalValue, Action callback)
    {
        float startProgress = (float) startValue / totalValue;
        float targetProgress = (float) targetValue / totalValue;

        int valueDifference = targetValue - startValue;
        float progressDifference = targetProgress - startProgress;

        float x = 0;
        do
        {
            x += Time.deltaTime / progressAnimationDuration;
            float currentProgress = startProgress + Mathf.Lerp(0, progressDifference, x);
            int currentValue = startValue + (int) Mathf.Lerp(0, valueDifference, x);
            SetText(Mathf.RoundToInt(currentValue), totalValue);
            slider.value = currentProgress;
            yield return null;
        } while (x <= 1);

        yield return new WaitForSeconds(postAnimationWaitTime);

        ProgressAnimationDone(null,true);
        callback?.Invoke();
    }
                 
    [Button]
    private void StartSucessAnimation(int exp)
    {
        if (OpenClose.IsOpen)
        {
            StartCoroutine(SucessAnimation(exp));
        }
        else
        {
            OpenClose.Hide();
            OpenClose.Open(()=> StartSucessAnimation(exp));
        }
    }

 IEnumerator SucessAnimation(int exp)
 {
     yield return FillAnimation(exp);
     yield return ShineBarAnimation();
     yield return new WaitForSeconds(postAnimationWaitTime);
     SuccessAnimationDone();
 }

 private IEnumerator FillAnimation(int exp)
 {
     SetText(exp, exp);
     float startSliderValue = slider.value;
     float x = 0;
     do //Llenado de barra
     {
         slider.value = Mathf.Lerp(startSliderValue, 1, x);
         x += Time.deltaTime;
         yield return null;
     } while (x <= 1);
 }

 private IEnumerator ShineBarAnimation()
 {
     float x = 0;
     do
     {
         x += Time.deltaTime / shineDuration;
         sliderFill.color = Color.Lerp(sliderFillColor,sliderFillShineColor, shineInCurve.Evaluate(x));
         yield return null; 
     } while (x < 1);
                  
     Vector3 scaleDif = Vector3.one - boxTransform.localScale;
     x = 0;
     popUpText.Play("+1");
     bool textPlayed = false;
     do
     {
         x += Time.deltaTime / boxAnimDuration;
         boxTransform.localScale = Vector3.one * boxAnimSizeCurve.Evaluate(x);
         sliderFill.color = Color.Lerp(sliderFillColor,sliderFillShineColor, shineOutCurve.Evaluate(x));
         yield return null;
     } while (x < 1);
 }

 private void SetText(int current, int total)
    {
        text.text = $"{current}/{total}";
    }

    public void ProgressAnimationDone(Action callback , bool close)
    {
        Debug.Log("ProgressAnimationDone");
        if(close)
            OpenClose.Close();
    }

    public void SuccessAnimationDone()
    {
        Debug.Log("SuccessAnimationDone");
        this.PostNotification(Notifications.GiftButtonSetEnable, true);
            OpenClose.Close();
    }

    [Button]
    public void GiftButtonPressed()
    {
        Debug.Log("GiftButtonPressed");
        if (Inventory.Contains(Inventory.IDs.GiftBox))
        {
            button.interactable = false;
            Inventory.Consume(Inventory.IDs.GiftBox,1, true);
            GiftBox gift = Instantiate(giftPrefab,null).GetComponent<GiftBox>();
            gift.StartUnlockRutine(MainCameraManager.Instance.currentCamera.transform.position - Vector3.forward * MainCameraManager.Instance.currentCamera.transform.position.z);
            this.PostNotification(Notifications.OnGiftButtonPressed);
        }
        else
            Debug.Log("NO tiene gift box");
        OpenClose.Close();
    }

    private void OnEnable()
    {
        this.AddObserver(OnShowGiftUnlockProgress, Notifications.ShowGiftUnlockProgress);
        this.AddObserver(OnShowGiftUnlockSuccess, Notifications.ShowGiftUnlockSuccess);
    }

    private void OnDisable()
    {
        this.RemoveObserver(OnShowGiftUnlockProgress, Notifications.ShowGiftUnlockProgress);
        this.RemoveObserver(OnShowGiftUnlockSuccess, Notifications.ShowGiftUnlockSuccess);
    }

    private void OnShowGiftUnlockProgress(object arg1, object arg2)
    {
        (int startValue, int targetValue, int totalValue) = (Tuple<int, int, int>) arg2;
        StartProgressAnimation(startValue, targetValue, totalValue, null);
    }
           
    private void OnShowGiftUnlockSuccess(object arg1, object arg2)
    {
        StartSucessAnimation((int)arg2);
    }

   
}

