﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor( typeof( IndirectLine ) )]
public class IndirectLineEditor : Editor
{
    public override void OnInspectorGUI()
    {
        IndirectLine cTarget = target as IndirectLine;
        base.OnInspectorGUI();
                         
        if (GUILayout.Button("AutoWaypoints"))
        {
            cTarget.CalculateWaypoints();
        }
    }

    private void OnSceneGUI()
    {
        IndirectLine cTarget = target as IndirectLine;
        for (int i = 0; i < cTarget.waypoints.Count; i++)
        {
            EditorGUI.BeginChangeCheck();
            Vector3 newTargetPosition = Handles.PositionHandle(cTarget.transform.TransformPoint(cTarget.waypoints[i]), Quaternion.identity);

            if (EditorGUI.EndChangeCheck())
            {
                newTargetPosition = newTargetPosition.SnapTo(cTarget.snapStep);
                cTarget.waypoints[i] = cTarget.transform.InverseTransformPoint( newTargetPosition);
                cTarget.ApplyToLine();
            }
        }

        
    }
}
