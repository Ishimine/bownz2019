﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class BallPreviewBox : MonoBehaviour
{
    public BallStateMachine ball;
    public Vector2 startForce = new Vector2(3,2);



    [Button]
    public void Pressed()
    {
        ball.transform.position = transform.position;
        ball.AppearToLiving(() => ball.Rb.AddForce(startForce, ForceMode2D.Impulse));
    }

}
