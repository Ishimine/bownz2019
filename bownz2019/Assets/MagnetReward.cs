﻿using System.Collections;
using System.Collections.Generic;

public class MagnetReward : ShopReward
{
    public const string Id = Notifications.RewardPowerUpMagnet;
    public override string RewardId => Id;

    protected override void _Reward()
    {
        this.PostNotification(Notifications.RewardPowerUpMagnet);
    }
}