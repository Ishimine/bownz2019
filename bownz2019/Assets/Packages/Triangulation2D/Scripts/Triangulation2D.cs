﻿/**
 * @author mattatz / http://mattatz.github.io
 *
 * Ruppert's Delaunay Refinement Algorithm
 * Jim Ruppert. A Delaunay Refinement Algorithm for Quality 2-Dimensional Mesh Generation / http://www.cis.upenn.edu/~cis610/ruppert.pdf
 * The Quake Group at Carnegie Mellon University / https://www.cs.cmu.edu/~quake/tripaper/triangle3.html
 * Wikipedia / https://en.wikipedia.org/wiki/Ruppert%27s_algorithm
 * ETH zurich CG13 Chapter 7 / http://www.ti.inf.ethz.ch/ew/Lehre/CG13/lecture/Chapter%207.pdf
 */

using UnityEngine;

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace mattatz.Triangulation2DSystem {

	public class Triangulation2D {

		const float kAngleMin = 10f, kAngleMax = 30f;

		public Triangle2D[] Triangles { get { return Tlist.ToArray(); } }

		List<Vertex2D> V = new List<Vertex2D>(); // vertices in PSLG
		List<Segment2D> S = new List<Segment2D>(); // segments in PSLG

		List<Vertex2D> P = new List<Vertex2D>(); // vertices in DT
		List<Segment2D> E = new List<Segment2D>(); // segments in DT

		List<Triangle2D> Tlist = new List<Triangle2D>();

        Triangle2D t;

        public Triangulation2D (Polygon2D polygon, float angle = 20f, float threshold = 0.1f) {
			angle = Mathf.Clamp(angle, kAngleMin, kAngleMax) * Mathf.Deg2Rad;

            Polygon2D PSLG = polygon;
			V = PSLG.Vertices.ToList();
			S = PSLG.Segments.ToList();
			Triangulate (polygon.Vertices.Select(v => v.Coordinate).ToArray(), angle, threshold);
		}

        public static Mesh BuildCombinedMesh(Triangulation2D[] triangulations, MeshFilter meshFilter)
        {
            Mesh combinedMesh = new Mesh();
            Mesh[] meshes = new Mesh[triangulations.Length];
            for (int i = 0; i < triangulations.Length; i++)
            {
                meshes[i] = triangulations[i].Build();
            }

            CombineInstance[] combine = new CombineInstance[triangulations.Length];
            for (int i = 0; i < triangulations.Length; i++)
            {
                combine[i].mesh = meshes[i];
                combine[i].transform = meshFilter.transform.localToWorldMatrix;
            }

            meshFilter.sharedMesh = combinedMesh;
            meshFilter.sharedMesh.CombineMeshes(combine);
            return combinedMesh;
        }

        Mesh mesh;
        Vector3[] vertices;
        int[] triangles;
        int i3;
        public Mesh Build ()
        {
            mesh = new Mesh();

			vertices = P.Select(p => { 
				Vector2 xy = p.Coordinate;
                Vector3 v = new Vector3(xy.x, xy.y, 0f);
				return v;
			}).ToArray();

			triangles = new int[Tlist.Count * 3];
            int a,b,c;

            for (int i = 0, n = Tlist.Count; i < n; i++)
            {
				 t = Tlist[i];
				 i3 = i * 3;

                a = P.IndexOf(t.a);
                b = P.IndexOf(t.b);
                c = P.IndexOf(t.c);
				if(Utils2D.LeftSide(t.a.Coordinate, t.b.Coordinate, t.c.Coordinate)) {
					triangles[i3] = a;
					triangles[i3 + 1] = c;
					triangles[i3 + 2] = b;
				} else {
					triangles[i3] = a;
					triangles[i3 + 1] = b;
					triangles[i3 + 2] = c;
				}
			}

			mesh.vertices = vertices;
			mesh.SetTriangles(triangles, 0);
			mesh.RecalculateNormals();
			return mesh;
		}

		int FindVertex (Vector2 p, List<Vertex2D> Vertices) {
			return Vertices.FindIndex (v => v.Coordinate == p);
		}

		int FindSegment (Vertex2D a, Vertex2D b, List<Segment2D> Segments) {
			return Segments.FindIndex (s => (s.a == a && s.b == b) || (s.a == b && s.b == a));
		}

        int idx;
        Vertex2D v;
		Vertex2D CheckAndAddVertex (Vector2 coord) {
			idx = FindVertex(coord, P);
			if(idx < 0) {
				v = new Vertex2D(coord);
				P.Add(v);
				return v;
			}
			return P[idx];
		}

		Segment2D CheckAndAddSegment (Vertex2D a, Vertex2D b) {
			 idx = FindSegment(a, b, E);
			Segment2D s;
			if(idx < 0) {
				s = new Segment2D(a, b);
				E.Add(s);
			} else {
				s = E[idx];
			}
			s.Increment();
			return s;
		}

        Segment2D s0, s1, s2;
		Triangle2D AddTriangle (Vertex2D a, Vertex2D b, Vertex2D c) {
			s0 = CheckAndAddSegment(a, b);
			s1 = CheckAndAddSegment(b, c);
			s2 = CheckAndAddSegment(c, a);
			t = new Triangle2D(s0, s1, s2);
			Tlist.Add(t);
			return t;
		}

		void RemoveTriangle (Triangle2D t) {
			Tlist.Remove(t);
			if(t.s0.Decrement() <= 0) RemoveSegment (t.s0);
			if(t.s1.Decrement() <= 0) RemoveSegment (t.s1);
			if(t.s2.Decrement() <= 0) RemoveSegment (t.s2);
		}

		void RemoveSegment (Segment2D s) {
			E.Remove(s);
			if(s.a.ReferenceCount <= 0) P.Remove(s.a);
			if(s.b.ReferenceCount <= 0) P.Remove(s.b);
		}

        Vector2 p;
		void Bound (Vector2[] points, out Vector2 min, out Vector2 max) {
			min = Vector2.one * float.MaxValue;
			max = Vector2.one * float.MinValue;
			for(int i = 0, n = points.Length; i < n; i++) {
				p = points[i];
				min.x = Mathf.Min (min.x, p.x);
				min.y = Mathf.Min (min.y, p.y);
				max.x = Mathf.Max (max.x, p.x);
				max.y = Mathf.Max (max.y, p.y);
			}
		}

        Vector2 center;
        float diagonal, dh, rdh;
		public Triangle2D AddExternalTriangle (Vector2 min, Vector2 max) {
			 center = (max + min) * 0.5f;
			 diagonal = (max - min).magnitude;
			 dh = diagonal * 0.5f;
			 rdh = Mathf.Sqrt(3f) * dh;
			return AddTriangle(
				CheckAndAddVertex(center + new Vector2(-rdh, -dh) * 3f),
				CheckAndAddVertex(center + new Vector2(rdh, -dh) * 3f),
				CheckAndAddVertex(center + new Vector2(0f, diagonal) * 3f)
			);
		}

        Vector2 min, max;
        Vector2 po;
        void Triangulate (Vector2[] points, float angle, float threshold)
        {
			Bound(points, out min, out max);
			AddExternalTriangle(min, max);
			for(int i = 0, n = points.Length; i < n; i++)
            {
                po = points[i];
				UpdateTriangulation (po);
			}
			Refine (angle, threshold);
			RemoveExternalPSLG ();
		}	

		void RemoveCommonTriangles (Triangle2D target)
        {
            for (int i = 0, n = Tlist.Count; i < n; i++) {
				t = Tlist[i];
				if(t.HasCommonPoint(target)) {
					RemoveTriangle (t);
					i--;
					n--;
				}
			}
		}

		void RemoveExternalPSLG ()
        {
            for (int i = 0, n = Tlist.Count; i < n; i++) {
				 t = Tlist[i];
				if(ExternalPSLG(t) || HasOuterSegments(t)) {
				// if(ExternalPSLG(t)) {
					RemoveTriangle (t);
					i--;
					n--;
				}
			}
		}

		bool ContainsSegments (Segment2D s, List<Segment2D> segments) {
			return segments.FindIndex (s2 => 
				(s2.a.Coordinate == s.a.Coordinate && s2.b.Coordinate == s.b.Coordinate) ||
				(s2.a.Coordinate == s.b.Coordinate && s2.b.Coordinate == s.a.Coordinate)
			) >= 0;
		}

		bool HasOuterSegments (Triangle2D t) {
			if(!ContainsSegments(t.s0, S)) {
				return ExternalPSLG(t.s0);
			}
			if(!ContainsSegments(t.s1, S)) {
				return ExternalPSLG(t.s1);
			}
			if(!ContainsSegments(t.s2, S)) {
				return ExternalPSLG(t.s2);
			}
			return false;
		}

        List<Triangle2D> tmpT = new List<Triangle2D>();
        List<Segment2D> tmpS = new List<Segment2D>();
        Segment2D s;
        List<Triangle2D> commonT;
        Triangle2D abc, abd;
        Vertex2D a, b, c, d;
        Segment2D[] segments0, segments1;
        Circle2D ec;
        void UpdateTriangulation (Vector2 p)
        {
            tmpT.Clear();
            tmpS.Clear();

			v = CheckAndAddVertex(p);
			tmpT = Tlist.FindAll(t => t.ContainsInExternalCircle(v));
			tmpT.ForEach(t => {
				tmpS.Add(t.s0);
				tmpS.Add(t.s1);
				tmpS.Add(t.s2);

				AddTriangle(t.a, t.b, v);
				AddTriangle(t.b, t.c, v);
				AddTriangle(t.c, t.a, v);
				RemoveTriangle (t);
			});

			while(tmpS.Count != 0) {
				s = tmpS.Last();
				tmpS.RemoveAt(tmpS.Count - 1);
				
				commonT = Tlist.FindAll(t => t.HasSegment(s));
				if(commonT.Count <= 1) continue;
				
				 abc = commonT[0];
				 abd = commonT[1];
				
				if(abc.Equals(abd)) {
					RemoveTriangle (abc);
					RemoveTriangle (abd);
					continue;
				}
				
				 a = s.a;
				 b = s.b;
				 c = abc.ExcludePoint(s);
				 d = abd.ExcludePoint(s);
				
				ec = Circle2D.GetCircumscribedCircle(abc);
				if(ec.Contains(d.Coordinate)) {
					RemoveTriangle (abc);
					RemoveTriangle (abd);
					
					AddTriangle(a, c, d); // add acd
					AddTriangle(b, c, d); // add bcd

					segments0 = abc.ExcludeSegment(s);
					tmpS.Add(segments0[0]);
					tmpS.Add(segments0[1]);
					
					 segments1 = abd.ExcludeSegment(s);
					tmpS.Add(segments1[0]);
					tmpS.Add(segments1[1]);
				}
			}

		}

		bool FindAndSplit (float threshold)
        {
			for(int i = 0, n = S.Count; i < n; i++) {
				 s = S[i];
				if(s.Length() < threshold) continue;

				for(int j = 0, m = P.Count; j < m; j++) {
					if(s.EncroachedUpon(P[j].Coordinate)) {
						SplitSegment(s);
						return true;
					}
				}
			}
			return false;
		}

		bool ExternalPSLG (Vector2 p) {
			return !Utils2D.Contains(p, V);
		}

		bool ExternalPSLG (Segment2D s) {
			return ExternalPSLG(s.Midpoint());
		}

		bool ExternalPSLG (Triangle2D t)
        {
			// return ExternalPSLG(t.s0) && ExternalPSLG(t.s1) && ExternalPSLG(t.s2);
			return 
				ExternalPSLG(t.a.Coordinate) ||
				ExternalPSLG(t.b.Coordinate) ||
				ExternalPSLG(t.c.Coordinate)
			;
		}

		void Refine (float angle, float threshold)  {
			while(Tlist.Any(t => !ExternalPSLG(t) && t.Skinny(angle, threshold))) {
				RefineSubRoutine(angle, threshold);
			}
		}

        Triangle2D skinny;
        List<Segment2D> segments;

        void RefineSubRoutine (float angle, float threshold)
        {
			while(true)
            {
				if(!FindAndSplit(threshold)) break; 
			}

			skinny = Tlist.Find (t => !ExternalPSLG(t) && t.Skinny(angle, threshold));
			p = skinny.Circumcenter();

            segments = S.FindAll(s => s.EncroachedUpon(p));
			if(segments.Count > 0) {
				segments.ForEach(s => SplitSegment(s));
			} else {
				SplitTriangle(skinny);
			}
		}

        Vector2 circ;
		void SplitTriangle (Triangle2D t)
        {
            circ = t.Circumcenter();
			UpdateTriangulation(circ);
		}

        int idxA, idxB, sidx;
        Vertex2D mv;
        void SplitSegment (Segment2D s) {
            a = s.a;
            b = s.b;
			 mv = new Vertex2D(s.Midpoint());

			// add mv to V 
			// the index is between a and b.
			 idxA = V.IndexOf(a);
			 idxB = V.IndexOf(b);
			if(Mathf.Abs(idxA - idxB) == 1) {
				idx = (idxA > idxB) ? idxA : idxB;
				V.Insert(idx, mv);
			} else {
				V.Add(mv);
			}

			UpdateTriangulation(mv.Coordinate);

			// Add two halves to S
			sidx = S.IndexOf(s);
			S.RemoveAt(sidx);

			S.Add(new Segment2D(s.a, mv));
			S.Add(new Segment2D(mv, s.b));
		}

        bool flag;
        Vertex2D v0, v1;
        Triangle2D t0, t1;

        bool CheckUnique () {
			flag = false;

			for(int i = 0, n = P.Count; i < n; i++) {
				 v0 = P[i];
				for(int j = i + 1; j < n; j++) {
					v1 = P[j];
					if(Utils2D.CheckEqual(v0, v1)) {
						Debug.LogWarning("vertex " + i + " equals " + j);
						flag = true;
					}
				}
			}

			for(int i = 0, n = E.Count; i < n; i++) {
				 s0 = E[i];
				for(int j = i + 1; j < n; j++) {
					 s1 = E[j];
					if(Utils2D.CheckEqual(s0, s1)) {
						Debug.LogWarning("segment " + i + " equals " + j);
						flag = true;
					}
				}
			}

			for(int i = 0, n = Tlist.Count; i < n; i++) {
				 t0 = Tlist[i];
				for(int j = i + 1; j < n; j++) {
					 t1 = Tlist[j];
					if(Utils2D.CheckEqual(t0, t1)) {
						Debug.LogWarning("triangle " + i + " equals " + j);
						flag = true;
					}
				}
			}

			for(int i = 0, n = Tlist.Count; i < n; i++) {
				 t = Tlist[i];
				if(Utils2D.CheckEqual(t.s0, t.s1) || Utils2D.CheckEqual(t.s0, t.s2) || Utils2D.CheckEqual(t.s1, t.s2)) {
					Debug.LogWarning("triangle " + i + " has duplicated segments");
					flag = true;
				}
			}

			return flag;
		}

		public void DrawGizmos () {
			Tlist.ForEach(t => t.DrawGizmos());
		}

	}

}
