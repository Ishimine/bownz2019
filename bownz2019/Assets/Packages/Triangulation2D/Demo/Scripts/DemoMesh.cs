﻿using UnityEngine;
using Random = UnityEngine.Random;

using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;

namespace mattatz.Triangulation2DSystem.Example
{
	public class DemoMesh : MonoBehaviour
    {
        public bool constantUvUpdate = false;

        float updateWaiting = 2;

        IEnumerator rutine;

        Vector2 lastPos;
        //[SerializeField] Material lineMat;
        Triangle2D[] triangles;

        private MeshFilter mFilter;

        public MeshFilter MFilter
        {
            get
            {
                if(mFilter == null)
                   mFilter = GetComponent<MeshFilter>();
                return mFilter;
            }
            set
            {
                mFilter = value;
            }
        }


        void Start ()
        {
			var body = GetComponent<Rigidbody>();
	        //		body.AddForce(Vector3.forward * Random.Range(150f, 160f));
	        //		body.AddTorque(Random.insideUnitSphere * Random.Range(10f, 20f));
		}

        private void OnDisable()
        {
            if (rutine != null)
                StartCoroutine(rutine);
        }

        private void OnEnable()
        {
            rutine = UpdateRutine();
            StartCoroutine(rutine);
        }

        IEnumerator UpdateRutine()
        {
            UpdateMyUv();
            yield return new WaitForSecondsRealtime(updateWaiting);
        }


        public void SetTriangulation (Triangulation2D triangulation)
        {
			var mesh = triangulation.Build();
			MFilter.sharedMesh = mesh;
			this.triangles = triangulation.Triangles;
		}

        public void SetMaterial(Material material)
        {
            GetComponent<Renderer>().material = material;
        }

        public void SetMultipletTriangulation(Triangulation2D[] triangulations)
        {
            SetUv(Triangulation2D.BuildCombinedMesh(triangulations, MFilter));
        }

        private void SetUv(Mesh mesh)
        {
            Vector3[] vertices = mesh.vertices;
            Vector2[] uvs = new Vector2[vertices.Length];
	        Transform transform1 = transform;
            for (int i = 0; i < uvs.Length; i++)
            {
	            uvs[i] = (new Vector3(vertices[i].x*transform1.parent.parent.localScale.x, vertices[i].y) + transform1.position)/10;
            }
            mesh.uv = uvs;
            mesh.RecalculateBounds();
        }

        [Button]
        public void UpdateMyUv()
        {
            SetUv(MFilter.mesh);
        }

        [Button]
        public void Clear()
        {
            if (MFilter.sharedMesh != null) MFilter.sharedMesh.Clear();
        }

       /* void OnRenderObject () {
			if(triangles == null) return;

			GL.PushMatrix();
			GL.MultMatrix (transform.localToWorldMatrix);

			lineMat.SetColor("_Color", Color.black);
			lineMat.SetPass(0);
			GL.Begin(GL.LINES);
			for(int i = 0, n = triangles.Length; i < n; i++) {
				var t = triangles[i];
				GL.Vertex(t.s0.a.Coordinate); GL.Vertex(t.s0.b.Coordinate);
				GL.Vertex(t.s1.a.Coordinate); GL.Vertex(t.s1.b.Coordinate);
				GL.Vertex(t.s2.a.Coordinate); GL.Vertex(t.s2.b.Coordinate);
			}
			GL.End();
			GL.PopMatrix();
		}*/

	}

}

