﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_RestartLevelButton : MonoBehaviour, IPressed
{
    public void Pressed()
    {
        this.PostNotification(Notifications.GameModeRequest_Retry);
    }
}
                                                                                                  