﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CopySpriteSize : MonoBehaviour
{
    public SpriteRenderer myRender;
    public SpriteRenderer targetRender;

    private void OnEnable()
    {
        myRender.size = targetRender.size;
    }

    private void OnValidate()
    {
        if(!Application.isPlaying)
            myRender.size = targetRender.size;
    }
}
