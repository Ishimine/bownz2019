﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkipModuleReward : ShopReward
{
    public const string Id = Notifications.RewardSkipModule;
    public override string RewardId => Id;

    protected override void _Reward()
    {
        this.PostNotification(Notifications.RewardSkipModule);
    }
    
}
