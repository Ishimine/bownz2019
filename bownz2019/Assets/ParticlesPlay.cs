﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticlesPlay : MonoBehaviour
{
   public ParticleSystem[] particles;

   public void Play()
   {
      foreach (var p in particles)
      {
         p.Play();
      }
   }

public void Stop()
   {
      foreach (var p in particles)
      {
         p.Stop();
      }
   }
}
