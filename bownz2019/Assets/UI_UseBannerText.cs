﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_UseBannerText : MonoBehaviour
{
    public Text txt;

    private void OnEnable()
    {
        txt.text = $"UseBanner{(AdsManager.Instance.UseBanner ? "On" : "Off")}";
    }
}
