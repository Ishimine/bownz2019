﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;
using RotaryHeart.Lib.SerializableDictionary;

[CreateAssetMenu(menuName ="Manager/Style")]
public class StyleManager : ScriptableSingleton<StyleManager>
{
    public override StyleManager Myself => this;

    public static Action<StyleProfile> OnStyleChanged { get; set; }

    public StyleProfile current;
    public static StyleProfile Current => Instance.current;

    [Header("Default")]
    public StyleProfile defaultProfile;

    public List<StyleProfile> styles;

    private int index;

    public bool randomChangeOnlevelLoad;


    public static Sprite GetSprite(string id)
    {
        return !Current.sprites.ContainsKey(id) ? null : Current.sprites[id];
    }

    public override void InitializeSingleton()
    {
        base.InitializeSingleton();
        ApplyDefaultValues();
        index = -1;

        this.RemoveObserver(LevelLoadRequest,Notifications.LevelLoadRequest);

        if(randomChangeOnlevelLoad)
            this.AddObserver(LevelLoadRequest,Notifications.LevelLoadRequest);
    }

    private void LevelLoadRequest(object arg1, object arg2)
    {
        RandomStyle();
    }

    private void RandomStyle()
    {
        Set(styles.GetRandom());
    }

    [Button]
    private void ApplyDefaultValues()
    {
        Set(defaultProfile);
    }

    private void Set(int index)
    {
        this.index = index;
        Set(styles[index]);
    }

    private void Set(StyleProfile styleProfile)
    {
        current = styleProfile;
        _PostChange();
    }


    [Button, ButtonGroup("A")]
    private void Previous()
    {
        Set((int)Mathf.Repeat(index - 1, styles.Count));
    }


    [Button, ButtonGroup("A")]
    private void Next()
    {
        Set((int)Mathf.Repeat(index + 1, styles.Count));
    }
 
    [Button]
    private void _PostChange()
    {
        OnStyleChanged?.Invoke(Instance.current);
    }

    public static void PostChange()
    {
        Instance._PostChange();
    }

}