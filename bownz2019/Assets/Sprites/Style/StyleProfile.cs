using UnityEngine;
using UnityEngine.U2D;

[System.Serializable][CreateAssetMenu]
public class StyleProfile: ScriptableObject
{
    public SpriteShape SpriteShape;
    public Material Background;
    public Material BackgroundGodrays;
    public Material ForegroundGodrays;

    public SpriteDictionary sprites;
}