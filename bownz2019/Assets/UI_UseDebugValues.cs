﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_UseDebugValues : MonoBehaviour
{
    public Text txt;


    public void SwapUseDebugValues()
    {
        Settings.useOnlyLocalValues = !Settings.useOnlyLocalValues;
        Refresh();
    }
    
    public void Refresh()
    {
        txt.text = $"Using Debug Values: {Settings.useOnlyLocalValues}";
    }

    public void OnEnable()
    {
        Refresh();
    }
}
